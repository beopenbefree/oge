/**
 * helpers.h
 *
 *  Created on: Mar 5, 2021
 *      Author: beopenbefree
 */

#ifndef INCLUDE_FRAMEWORK_HELPERS_H_
#define INCLUDE_FRAMEWORK_HELPERS_H_

#include <chrono>
#include <vector>
#include "lib/vulkanDestroyer.h"
#include "lib/helperRecipes.h"

namespace oge {
// Storage for mouse state parameters

class MouseStateParameters {
public:
	struct ButtonsState {
		bool IsPressed;
		bool WasClicked;
		bool WasRelease;
	} Buttons[2];
	struct Position {
		int X;
		int Y;
		struct Delta {
			int X;
			int Y;
		} Delta;
	} Position;
	struct WheelState {
		bool WasMoved;
		float Distance;
	} Wheel;

	MouseStateParameters();
	~MouseStateParameters();
};

// Class for simple time manipulations

class TimerStateParameters {
public:
	float GetTime() const;
	float GetDeltaTime() const;

	void Update();

	TimerStateParameters();
	~TimerStateParameters();

private:
	std::chrono::time_point<std::chrono::high_resolution_clock> Time;
	std::chrono::duration<float> DeltaTime;
};

// Simple containers for resources

struct QueueParameters {
	VkQueue Handle;
	uint32_t FamilyIndex;
};

struct SwapchainParameters {
	VkDestroyer(VkSwapchainKHR) Handle;
	VkFormat Format;
	VkExtent2D Size;
	std::vector<VkImage> Images;
	std::vector<VkDestroyer(VkImageView)> ImageViews;
	std::vector<VkImageView> ImageViewsRaw;
};

bool Load3DModelFromObjFile(char const *filename, bool load_normals,
		bool load_texcoords, bool generate_tangent_space_vectors, bool unify,
		Mesh &mesh, uint32_t *vertex_stride = nullptr);

} /* namespace oge */

#endif /* INCLUDE_FRAMEWORK_HELPERS_H_ */
