/**
 * application.h
 *
 *  Created on: Mar 5, 2021
 *      Author: beopenbefree
 */

#ifndef INCLUDE_FRAMEWORK_APPLICATION_H_
#define INCLUDE_FRAMEWORK_APPLICATION_H_

#include "framework/helpers.h"
#include "lib/imagePresentation.h"
#include "lib/instanceAndDevices.h"

namespace oge {

// Base class for implementing Vulkan Cookbook code samples

class ApplicationBase {
public:
	ApplicationBase();
	virtual ~ApplicationBase();

	virtual bool Initialize(GLFWwindow *window_parameters) = 0;
	virtual bool Draw() = 0;
	virtual bool Resize() = 0;
	virtual void Deinitialize() = 0;
	virtual void MouseClick(size_t button_index, bool state) final;
	virtual void MouseMove(int x, int y) final;
	virtual void MouseWheel(float distance) final;
	virtual void MouseReset() final;
	virtual void UpdateTime() final;
	virtual bool IsReady() final;

protected:
	virtual void OnMouseEvent();

	bool Ready;
	MouseStateParameters MouseState;
	TimerStateParameters TimerState;
};

// Base class for code samples with default instance, device and swapchain creation

class Application: public ApplicationBase {
public:
	VkDestroyer(VkInstance) Instance;
	VkPhysicalDevice PhysicalDevice;
	VkDestroyer(VkDevice) LogicalDevice;
	VkDestroyer(VkSurfaceKHR) PresentationSurface;
	QueueParameters GraphicsQueue;
	QueueParameters ComputeQueue;
	QueueParameters PresentQueue;
	SwapchainParameters Swapchain;
	VkDestroyer(VkCommandPool) CommandPool;
	std::vector<VkDestroyer(VkImage)> DepthImages;
	std::vector<VkDestroyer(VkDeviceMemory)> DepthImagesMemory;
	std::vector<FrameResources> FramesResources;
	static uint32_t const FramesCount = 3;
	static VkFormat const DepthFormat = VK_FORMAT_D16_UNORM;

	virtual bool InitializeVulkan(GLFWwindow *window_parameters,
			VkPhysicalDeviceFeatures *desired_device_features = nullptr,
			VkImageUsageFlags swapchain_image_usage =
					VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT, bool use_depth = true,
			VkImageUsageFlags depth_attachment_usage =
					VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT) final;
	virtual bool CreateSwapchain(VkImageUsageFlags swapchain_image_usage =
			VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT, bool use_depth = true,
			VkImageUsageFlags depth_attachment_usage =
					VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT) final;
	virtual void Deinitialize() final;

};

// Application starting point implementation

#define APPLICATION_FRAMEWORK( title, x, y, width, height, sample_type )   \
                                                                                    \
int main() {                                                                        \
  sample_type sample;                                                               \
  oge::WindowFramework window( "oge#" title, x, y, width, height, sample ); \
                                                                                    \
  window.Render();                                                                  \
                                                                                    \
  return 0;                                                                         \
}

} /* namespace oge */

#endif /* INCLUDE_FRAMEWORK_APPLICATION_H_ */
