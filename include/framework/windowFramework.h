/**
 * windowFramework.h
 *
 *  Created on: Mar 5, 2021
 *      Author: beopenbefree
 */

#ifndef INCLUDE_LIB_WINDOWFRAMEWORK_H_
#define INCLUDE_LIB_WINDOWFRAMEWORK_H_

#include <iostream>
#include "lib/vulkanFunctions.h"

namespace oge {

class ApplicationBase;

class WindowFramework {
public:
	WindowFramework(const char *window_title, int x, int y, int width,
			int height, ApplicationBase &sample);
	virtual ~WindowFramework();

	virtual void Render() final;

private:
	GLFWwindow *WindowParams;
	ApplicationBase &Sample;
	bool Initialized, Created;
	/// Setting an error callback
	static void error_callback(int error, const char *description);
	static void mouse_button_callback(GLFWwindow *window, int button,
			int action, int mods);
	static void cursor_position_callback(GLFWwindow *window, double xpos,
			double ypos);
	static void scroll_callback(GLFWwindow *window, double xoffset,
			double yoffset);
	void window_size_callback(GLFWwindow *window, int width, int height);

};

} /* namespace oge */

#endif /* INCLUDE_LIB_WINDOWFRAMEWORK_H_ */
