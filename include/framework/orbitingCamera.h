/**
 * orbitingCamera.h
 *
 *  Created on: Mar 5, 2021
 *      Author: beopenbefree
 */

#ifndef INCLUDE_LIB_ORBITINGCAMERA_H_
#define INCLUDE_LIB_ORBITINGCAMERA_H_

#include "camera.h"

namespace oge {

class OrbitingCamera: public Camera {
public:
	virtual Vector3 GetTarget() const final;
	virtual float GetDistance() const final;
	virtual float GetHorizontalAngle() const final;
	virtual float GetVerticalAngle() const final;

	void ChangeDistance(float distance_delta);
	void RotateHorizontally(float angle_delta);
	void RotateVertically(float angle_delta);

	OrbitingCamera();
	OrbitingCamera(Vector3 const &target, float distance,
			float horizontal_angle = 0.0f, float vertical_angle = 0.0f);
	OrbitingCamera(OrbitingCamera const &other);
	virtual ~OrbitingCamera();

	OrbitingCamera& operator=(OrbitingCamera const &i_OrbitingCamera);

private:
	Vector3 Target;
	float Distance;
	float HorizontalAngle;
	float VerticalAngle;
};

} /* namespace oge */

#endif /* INCLUDE_LIB_ORBITINGCAMERA_H_ */
