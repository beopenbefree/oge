/**
 * SceneObject.h
 *
 *  Created on: Feb 22, 2021
 *      Author: beopenbefree
 */

#ifndef INCLUDE_SCENEOBJECT_H_
#define INCLUDE_SCENEOBJECT_H_

#include <vector>
#include <glm/mat4x4.hpp>

namespace oge
{
/**
 * SceneObject.
 */
class SceneObject final
{
public:
	SceneObject();
	virtual ~SceneObject();

	void addChild(SceneObject *child);

	const glm::mat4& getTransform() const;
	void setTransform(const glm::mat4 &mTransform);

private:
	std::vector<SceneObject*> mChildren;
	glm::mat4 mTransform;

};

} // namespace oge

#endif /* INCLUDE_SCENEOBJECT_H_ */
