/**
 * SceneCreator.h
 *
 *  Created on: Feb 22, 2021
 *      Author: beopenbefree
 */

#ifndef INCLUDE_SCENECREATOR_H_
#define INCLUDE_SCENECREATOR_H_

#include <string>
#include <assimp/scene.h>
#include "SceneObject.h"

namespace oge
{

class SceneCreator final
{
public:
	SceneCreator(const std::string &pFile);
	virtual ~SceneCreator();

	const std::string& getFile() const;
	SceneObject* getObject() const;

private:
	SceneObject *mObject = nullptr;
	std::string mFile;
	bool doTheImportThing();
	void doWalkSceneInfos(aiNode *node, const std::string &indentStr, const aiScene*& scene);
	void doTheErrorLogging(const std::string &msg);
	void doCreateSceneHierarchy(const aiNode &node, SceneObject *parent,
			aiMatrix4x4 accTransform, const aiScene*& scene);
	void doCopyMeshes(const aiNode &node, SceneObject *object, const aiScene*& scene);

};

} // namespace oge

#endif /* INCLUDE_SCENECREATOR_H_ */
