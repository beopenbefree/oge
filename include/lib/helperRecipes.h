/**
 * helperRecipes.h
 *
 *  Created on: Mar 26, 2021
 *      Author: beopenbefree
 */

#ifndef INCLUDE_LIB_HELPERS_H_
#define INCLUDE_LIB_HELPERS_H_

#include <vector>
#include <cstdint>
#include <string>
#include <glm/glm.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/ext/matrix_clip_space.hpp>

namespace oge {

bool getBinaryFileContents(std::string const &filename,
		std::vector<unsigned char> &contents);

struct Mesh {
	std::vector<float> Data;

	struct Part {
		uint32_t VertexOffset;
		uint32_t VertexCount;
	};

	std::vector<Part> Parts;
};

using Vector3 = glm::vec3;
using Matrix4x4 = glm::mat4;

inline float Deg2Rad(float value) {
	return glm::radians(value);
}

inline float Dot(Vector3 const &left, Vector3 const &right) {
	return glm::dot(left, right);
}

inline Vector3 Cross(Vector3 const &left, Vector3 const &right) {
	return glm::cross(left, right);
}

inline Vector3 Normalize(Vector3 const &vector) {
	return glm::normalize(vector);
}

inline Vector3 operator+(Vector3 const &left, Vector3 const &right) {
	return left + right;
}

inline Vector3 operator-(Vector3 const &left, Vector3 const &right) {
	return left - right;
}

inline Vector3 operator+(float const &left, Vector3 const &right) {
	return Vector3 { left } + right;
}

inline Vector3 operator-(float const &left, Vector3 const &right) {
	return Vector3 { left } - right;
}

inline Vector3 operator+(Vector3 const &left, float const &right) {
	return left + Vector3 { right };
}

inline Vector3 operator-(Vector3 const &left, float const &right) {
	return left - Vector3 { right };
}

inline Vector3 operator*(float left, Vector3 const &right) {
	return right * left;
}

inline Vector3 operator*(Vector3 const &left, float right) {
	return left * right;
}

inline Vector3 operator*(Vector3 const &left, Matrix4x4 const &right) {
	return {
		left[0] * right[0][0] + left[1] * right[0][1] + left[2] * right[0][2],
		left[0] * right[1][0] + left[1] * right[1][1] + left[2] * right[1][2],
		left[0] * right[2][0] + left[1] * right[2][1] + left[2] * right[2][2]
	};

}

inline Vector3 operator-(Vector3 const &vector) {
	return -vector;
}

inline bool operator==(Vector3 const &left, Vector3 const &right) {
	return left == right;
}

//Matrix4x4 operator*(Matrix4x4 const &left, Matrix4x4 const &right) { FIXME: already defined in glm
//	return left * right;
//}

inline Matrix4x4 prepareTranslationMatrix(float x, float y, float z) {
	return glm::translate(Matrix4x4 { 1.0 }, Vector3 { x, y, z });
}

inline Matrix4x4 prepareRotationMatrix(float angle, Vector3 const &axis,
		float normalize_axis = true) {
	return glm::rotate(Matrix4x4 { 1.0 }, -angle,
			(normalize_axis ? Normalize(axis) : axis));
}

inline Matrix4x4 prepareScalingMatrix(float x, float y, float z) {
	return glm::scale(Matrix4x4 { 1.0 }, Vector3 { x, y, z });;
}

namespace {
// C transforms from OpenGL clip space to Vulkan clip space
constexpr glm::mat4 C { glm::vec4 { 1.0, 0.0, 0.0, 0.0 }, glm::vec4 { 0.0, -1.0,
		0.0, 0.0 }, glm::vec4 { 0.0, 0.0, 0.5, 0.0 }, glm::vec4 { 0.0, 0.0, 0.5,
		1.0 } };
}

inline Matrix4x4 preparePerspectiveProjectionMatrix(float aspect_ratio,
		float field_of_view, float near_plane, float far_plane) {

	return C
			* glm::perspective(glm::radians(field_of_view), aspect_ratio,
					near_plane, far_plane);
}

inline Matrix4x4 prepareOrthographicProjectionMatrix(float left_plane,
		float right_plane, float bottom_plane, float top_plane,
		float near_plane, float far_plane) {

	return C
			* glm::ortho(left_plane, right_plane, bottom_plane, top_plane,
					near_plane, far_plane);
}

}  // namespace oge

#endif /* INCLUDE_LIB_HELPERS_H_ */
