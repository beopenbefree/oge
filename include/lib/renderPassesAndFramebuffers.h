/**
 * renderPassesAndFramebuffers.h
 *
 *  Created on: Mar 24, 2021
 *      Author: beopenbefree
 */

#ifndef INCLUDE_LIB_RENDERPASSESANDFRAMEBUFFERS_H_
#define INCLUDE_LIB_RENDERPASSESANDFRAMEBUFFERS_H_

#include <vector>
#include "lib/vulkanFunctions.h"
#include "lib/common.h"

namespace oge {

void specifyAttachmentsDescriptions(
		std::vector<VkAttachmentDescription> const &attachments_descriptions);
void specifySubpassDescriptions(
		std::vector<SubpassParameters> const &subpass_parameters,
		std::vector<VkSubpassDescription> &subpass_descriptions);
void specifyDependenciesBetweenSubpasses(
		std::vector<VkSubpassDependency> const &subpasses_dependencies);
bool createRenderPass(VkDevice logical_device,
		std::vector<VkAttachmentDescription> const &attachments_descriptions,
		std::vector<SubpassParameters> const &subpass_parameters,
		std::vector<VkSubpassDependency> const &subpass_dependencies,
		VkRenderPass &render_pass);
bool createFramebuffer(VkDevice logical_device, VkRenderPass render_pass,
		std::vector<VkImageView> const &attachments, uint32_t width,
		uint32_t height, uint32_t layers, VkFramebuffer &framebuffer);
bool prepareRenderPassForGeometryRenderingAndPostprocessSubpasses(
		VkDevice logical_device, VkRenderPass render_pass);
bool prepareRenderPassAndFramebufferWithColorAndDepthAttachments(
		VkPhysicalDevice physical_device, VkDevice logical_device,
		uint32_t width, uint32_t height, VkImage &color_image,
		VkDeviceMemory &color_image_memory_object,
		VkImageView &color_image_view, VkImage &depth_image,
		VkDeviceMemory &depth_image_memory_object,
		VkImageView &depth_image_view, VkRenderPass &render_pass,
		VkFramebuffer &framebuffer);
void beginRenderPass(VkCommandBuffer command_buffer, VkRenderPass render_pass,
		VkFramebuffer framebuffer, VkRect2D render_area,
		std::vector<VkClearValue> const &clear_values,
		VkSubpassContents subpass_contents);
void progressToTheNextSubpass(VkCommandBuffer command_buffer,
		VkSubpassContents subpass_contents);
void endRenderPass(VkCommandBuffer command_buffer);
void destroyFramebuffer(VkDevice logical_device, VkFramebuffer &framebuffer);
void destroyRenderPass(VkDevice logical_device, VkRenderPass &render_pass);

}  // namespace oge

#endif /* INCLUDE_LIB_RENDERPASSESANDFRAMEBUFFERS_H_ */
