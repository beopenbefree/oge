/**
 * commandRecordingAndDrawing.h
 *
 *  Created on: Mar 26, 2021
 *      Author: beopenbefree
 */

#ifndef COMMANDRECORDINGANDDRAWING_H_
#define COMMANDRECORDINGANDDRAWING_H_

#include <vector>
#include <array>
#include "lib/vulkanFunctions.h"
#include "lib/common.h"
#include "lib/helperRecipes.h"

namespace oge {

void clearColorImage(VkCommandBuffer command_buffer, VkImage image,
		VkImageLayout image_layout,
		std::vector<VkImageSubresourceRange> const &image_subresource_ranges,
		VkClearColorValue &clear_color);
void clearDepthStencilImage(VkCommandBuffer command_buffer, VkImage image,
		VkImageLayout image_layout,
		std::vector<VkImageSubresourceRange> const &image_subresource_ranges,
		VkClearDepthStencilValue &clear_value);
void clearRenderPassAttachments(VkCommandBuffer command_buffer,
		std::vector<VkClearAttachment> const &attachments,
		std::vector<VkClearRect> const &rects);
void bindVertexBuffers(VkCommandBuffer command_buffer, uint32_t first_binding,
		std::vector<VertexBufferParameters> const &buffers_parameters);
void bindIndexBuffer(VkCommandBuffer command_buffer, VkBuffer buffer,
		VkDeviceSize memory_offset, VkIndexType index_type);
void provideDataToShadersThroughPushConstants(VkCommandBuffer command_buffer,
		VkPipelineLayout pipeline_layout, VkShaderStageFlags pipeline_stages,
		uint32_t offset, uint32_t size, void *data);
void setViewportStateDynamically(VkCommandBuffer command_buffer,
		uint32_t first_viewport, std::vector<VkViewport> const &viewports);
void setScissorStateDynamically(VkCommandBuffer command_buffer,
		uint32_t first_scissor, std::vector<VkRect2D> const &scissors);
void setLineWidthStateDynamically(VkCommandBuffer command_buffer,
		float line_width);
void setDepthBiasStateDynamically(VkCommandBuffer command_buffer,
		float constant_factor, float clamp_value, float slope_factor);
void setBlendConstantsStateDynamically(VkCommandBuffer command_buffer,
		std::array<float, 4> const &blend_constants);
void drawGeometry(VkCommandBuffer command_buffer, uint32_t vertex_count,
		uint32_t instance_count, uint32_t first_vertex,
		uint32_t first_instance);
void drawIndexedGeometry(VkCommandBuffer command_buffer, uint32_t index_count,
		uint32_t instance_count, uint32_t first_index, uint32_t vertex_offset,
		uint32_t first_instance);
void dispatchComputeWork(VkCommandBuffer command_buffer, uint32_t x_size,
		uint32_t y_size, uint32_t z_size);
void executeSecondaryCommandBufferInsidePrimaryCommandBuffer(
		VkCommandBuffer primary_command_buffer,
		std::vector<VkCommandBuffer> const &secondary_command_buffers);
bool recordCommandBufferThatDrawsGeometryWithDynamicViewportAndScissorStates(
		VkCommandBuffer command_buffer, VkImage swapchain_image,
		uint32_t present_queue_family_index,
		uint32_t graphics_queue_family_index, VkRenderPass render_pass,
		VkFramebuffer framebuffer, VkExtent2D framebuffer_size,
		std::vector<VkClearValue> const &clear_values,
		VkPipeline graphics_pipeline, uint32_t first_vertex_buffer_binding,
		std::vector<VertexBufferParameters> const &vertex_buffers_parameters,
		VkPipelineLayout pipeline_layout,
		std::vector<VkDescriptorSet> const &descriptor_sets,
		uint32_t index_for_first_descriptor_set, Mesh const &geometry,
		uint32_t instance_count, uint32_t first_instance);
bool recordCommandBuffersOnMultipleThreads(
		std::vector<CommandBufferRecordingThreadParameters> const &threads_parameters,
		VkQueue queue, std::vector<WaitSemaphoreInfo> wait_semaphore_infos,
		std::vector<VkSemaphore> signal_semaphores, VkFence fence);
bool prepareSingleFrameOfAnimation(VkDevice logical_device,
		VkQueue graphics_queue, VkQueue present_queue, VkSwapchainKHR swapchain,
		VkExtent2D swapchain_size,
		std::vector<VkImageView> const &swapchain_image_views,
		VkImageView depth_attachment,
		std::vector<WaitSemaphoreInfo> const &wait_infos,
		VkSemaphore image_acquired_semaphore,
		VkSemaphore ready_to_present_semaphore, VkFence finished_drawing_fence,
		std::function<bool(VkCommandBuffer, uint32_t, VkFramebuffer)> record_command_buffer,
		VkCommandBuffer command_buffer, VkRenderPass render_pass,
		VkDestroyer(VkFramebuffer) &framebuffer);
bool increasePerformanceThroughIncreasingTheNumberOfSeparatelyRenderedFrames(
		VkDevice logical_device, VkQueue graphics_queue, VkQueue present_queue,
		VkSwapchainKHR swapchain, VkExtent2D swapchain_size,
		std::vector<VkImageView> const &swapchain_image_views,
		VkRenderPass render_pass,
		std::vector<WaitSemaphoreInfo> const &wait_infos,
		std::function<bool(VkCommandBuffer, uint32_t, VkFramebuffer)> record_command_buffer,
		std::vector<FrameResources> &frame_resources);

}
// namespace oge

#endif /* COMMANDRECORDINGANDDRAWING_H_ */
