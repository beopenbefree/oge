/**
 * commandBuffersAndSynchronization.h
 *
 *  Created on: Mar 15, 2021
 *      Author: beopenbefree
 */

#ifndef INCLUDE_LIB_COMMANDBUFFERSANDSYNCHRONIZATION_H_
#define INCLUDE_LIB_COMMANDBUFFERSANDSYNCHRONIZATION_H_

#include <vector>
#include "lib/vulkanFunctions.h"
#include "lib/common.h"

namespace oge {

bool createCommandPool(VkDevice logical_device,
		VkCommandPoolCreateFlags command_pool_flags, uint32_t queue_family,
		VkCommandPool &command_pool);
bool allocateCommandBuffers(VkDevice logical_device, VkCommandPool command_pool,
		VkCommandBufferLevel level, uint32_t count,
		std::vector<VkCommandBuffer> &command_buffers);
bool beginCommandBufferRecordingOperation(VkCommandBuffer command_buffer,
		VkCommandBufferUsageFlags usage,
		VkCommandBufferInheritanceInfo *secondary_command_buffer_info);
bool endCommandBufferRecordingOperation(VkCommandBuffer command_buffer);
bool resetCommandBuffer(VkCommandBuffer command_buffer, bool release_resources);
bool resetCommandPool(VkDevice logical_device, VkCommandPool command_pool,
		bool release_resources);
bool createSemaphore(VkDevice logical_device, VkSemaphore &semaphore);
bool createFence(VkDevice logical_device, bool signaled, VkFence &fence);
bool waitForFences(VkDevice logical_device, std::vector<VkFence> const &fences,
		VkBool32 wait_for_all, uint64_t timeout);
bool resetFences(VkDevice logical_device, std::vector<VkFence> const &fences);
bool submitCommandBuffersToQueue(VkQueue queue,
		std::vector<WaitSemaphoreInfo> wait_semaphore_infos,
		std::vector<VkCommandBuffer> command_buffers,
		std::vector<VkSemaphore> signal_semaphores, VkFence fence);
bool submitCommandBufferBatchesToQueue(VkQueue queue,
		std::vector<std::vector<WaitSemaphoreInfo>> batches_wait_semaphore_infos,
		std::vector<std::vector<VkCommandBuffer>> batches_command_buffers,
		std::vector<std::vector<VkSemaphore>> batches_signal_semaphores,
		VkFence fence);
bool synchronizeTwoCommandBuffers(VkQueue first_queue,
		std::vector<WaitSemaphoreInfo> first_wait_semaphore_infos,
		std::vector<VkCommandBuffer> first_command_buffers,
		std::vector<WaitSemaphoreInfo> synchronizing_semaphores,
		VkQueue second_queue,
		std::vector<VkCommandBuffer> second_command_buffers,
		std::vector<VkSemaphore> second_signal_semaphores,
		VkFence second_fence);
bool checkIfProcessingOfSubmittedCommandBufferHasFinished(
		VkDevice logical_device, VkQueue queue,
		std::vector<WaitSemaphoreInfo> wait_semaphore_infos,
		std::vector<VkCommandBuffer> command_buffers,
		std::vector<VkSemaphore> signal_semaphores, VkFence fence,
		uint64_t timeout, VkResult &wait_status);
bool waitUntilAllCommandsSubmittedToQueueAreFinished(VkQueue queue);
bool waitForAllSubmittedCommandsToBeFinished(VkDevice logical_device);
void destroyFence(VkDevice logical_device, VkFence &fence);
void destroySemaphore(VkDevice logical_device, VkSemaphore &semaphore);
void freeCommandBuffers(VkDevice logical_device, VkCommandPool command_pool,
		std::vector<VkCommandBuffer> &command_buffers);
void destroyCommandPool(VkDevice logical_device, VkCommandPool &command_pool);

}  // namespace oge

#endif /* INCLUDE_LIB_COMMANDBUFFERSANDSYNCHRONIZATION_H_ */
