/**
 * resourcesAndMemory.h
 *
 *  Created on: Mar 17, 2021
 *      Author: beopenbefree
 */

#ifndef INCLUDE_LIB_RESOURCESANDMEMORY_H_
#define INCLUDE_LIB_RESOURCESANDMEMORY_H_

#include "lib/vulkanFunctions.h"
#include "lib/common.h"

namespace oge {

bool createBuffer(VkDevice logical_device, VkDeviceSize size,
		VkBufferUsageFlags usage, VkBuffer &buffer, VkSharingMode sharingMode =
				VK_SHARING_MODE_EXCLUSIVE, uint32_t queueFamilyIndexCount = 0,
		const uint32_t *pQueueFamilyIndices = nullptr);
bool allocateAndBindMemoryObjectToBuffer(VkPhysicalDevice physical_device,
		VkDevice logical_device, VkBuffer buffer,
		VkMemoryPropertyFlags memory_properties, VkDeviceMemory &memory_object);
void setBufferMemoryBarrier(VkCommandBuffer command_buffer,
		VkPipelineStageFlags generating_stages,
		VkPipelineStageFlags consuming_stages,
		std::vector<BufferTransition> buffer_transitions, VkDeviceSize offset =
				0, VkDeviceSize size = VK_WHOLE_SIZE);
bool createBufferView(VkDevice logical_device, VkBuffer buffer, VkFormat format,
		VkDeviceSize memory_offset, VkDeviceSize memory_range,
		VkBufferView &buffer_view);
bool createImage(VkDevice logical_device, VkImageType type, VkFormat format,
		VkExtent3D size, uint32_t num_mipmaps, uint32_t num_layers,
		VkSampleCountFlagBits samples, VkImageUsageFlags usage_scenarios,
		bool cubemap, VkImage &image, VkImageTiling tiling =
				VK_IMAGE_TILING_OPTIMAL, VkSharingMode sharingMode =
				VK_SHARING_MODE_EXCLUSIVE, VkImageLayout initialLayout =
				VK_IMAGE_LAYOUT_UNDEFINED);
bool allocateAndBindMemoryObjectToImage(VkPhysicalDevice physical_device,
		VkDevice logical_device, VkImage image,
		VkMemoryPropertyFlagBits memory_properties,
		VkDeviceMemory &memory_object);
void setImageMemoryBarrier(VkCommandBuffer command_buffer,
		VkPipelineStageFlags generating_stages,
		VkPipelineStageFlags consuming_stages,
		std::vector<ImageTransition> image_transitions);
bool createImageView(VkDevice logical_device, VkImage image,
		VkImageViewType view_type, VkFormat format, VkImageAspectFlags aspect,
		VkImageView &image_view, VkComponentMapping components = {
				VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY,
				VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY });
bool create2DImageAndView(VkPhysicalDevice physical_device,
		VkDevice logical_device, VkFormat format, VkExtent2D size,
		uint32_t num_mipmaps, uint32_t num_layers,
		VkSampleCountFlagBits samples, VkImageUsageFlags usage,
		VkImageAspectFlags aspect, VkImage &image,
		VkDeviceMemory &memory_object, VkImageView &image_view);
bool createLayered2DImageWithCubemapView(VkPhysicalDevice physical_device,
		VkDevice logical_device, uint32_t size, uint32_t num_mipmaps,
		VkImageUsageFlags usage, VkImageAspectFlags aspect, VkImage &image,
		VkDeviceMemory &memory_object, VkImageView &image_view);
bool mapUpdateAndUnmapHostVisibleMemory(VkDevice logical_device,
		VkDeviceMemory memory_object, VkDeviceSize offset,
		VkDeviceSize data_size, void *data, bool unmap, void **pointer);
bool mapHostVisibleMemory(VkDevice logical_device, VkDeviceMemory memory_object,
		VkDeviceSize offset, VkDeviceSize data_size, void **pointer);
bool updateHostVisibleMemory(VkDevice logical_device,
		VkDeviceMemory memory_object, VkDeviceSize offset,
		VkDeviceSize data_size, void *data, void *local_pointer);
bool unmapHostVisibleMemory(VkDevice logical_device,
		VkDeviceMemory memory_object);
void copyDataBetweenBuffers(VkCommandBuffer command_buffer,
		VkBuffer source_buffer, VkBuffer destination_buffer,
		std::vector<VkBufferCopy> regions);
void copyDataFromBufferToImage(VkCommandBuffer command_buffer,
		VkBuffer source_buffer, VkImage destination_image,
		VkImageLayout image_layout, std::vector<VkBufferImageCopy> regions);
void copyDataFromImageToBuffer(VkCommandBuffer command_buffer,
		VkImage source_image, VkImageLayout image_layout,
		VkBuffer destination_buffer, std::vector<VkBufferImageCopy> regions);
bool useStagingBufferToUpdateBufferWithDeviceLocalMemoryBound(
		VkPhysicalDevice physical_device, VkDevice logical_device,
		VkDeviceSize data_size, void *data, VkBuffer destination_buffer,
		VkDeviceSize destination_offset,
		VkAccessFlags destination_buffer_current_access,
		VkAccessFlags destination_buffer_new_access,
		VkPipelineStageFlags destination_buffer_generating_stages,
		VkPipelineStageFlags destination_buffer_consuming_stages, VkQueue queue,
		VkCommandBuffer command_buffer,
		std::vector<VkSemaphore> signal_semaphores);
bool useStagingBufferToUpdateImageWithDeviceLocalMemoryBound(
		VkPhysicalDevice physical_device, VkDevice logical_device,
		VkDeviceSize data_size, void *data, VkImage destination_image,
		VkImageSubresourceLayers destination_image_subresource,
		VkOffset3D destination_image_offset, VkExtent3D destination_image_size,
		VkImageLayout destination_image_current_layout,
		VkImageLayout destination_image_new_layout,
		VkAccessFlags destination_image_current_access,
		VkAccessFlags destination_image_new_access,
		VkImageAspectFlags destination_image_aspect,
		VkPipelineStageFlags destination_image_generating_stages,
		VkPipelineStageFlags destination_image_consuming_stages, VkQueue queue,
		VkCommandBuffer command_buffer,
		std::vector<VkSemaphore> signal_semaphores);
void destroyImageView(VkDevice logical_device, VkImageView &image_view);
void destroyImage(VkDevice logical_device, VkImage &image);
void destroyBufferView(VkDevice logical_device, VkBufferView &buffer_view);
void freeMemoryObject(VkDevice logical_device, VkDeviceMemory &memory_object);
void destroyBuffer(VkDevice logical_device, VkBuffer &buffer);

} // namespace oge

#endif /* INCLUDE_LIB_RESOURCESANDMEMORY_H_ */
