/**
 * graphicsAndComputePipelines.h
 *
 *  Created on: Mar 24, 2021
 *      Author: beopenbefree
 */

#ifndef INCLUDE_LIB_GRAPHICSANDCOMPUTEPIPELINES_H_
#define INCLUDE_LIB_GRAPHICSANDCOMPUTEPIPELINES_H_

#include <vector>
#include <array>
#include "lib/vulkanFunctions.h"
#include "lib/common.h"

namespace oge {

// REMARK: the "specify*" functions should not accept temporary object arguments for
// the const-reference parameters, since these temporaries could be referenced by
// the (output) object passed as the last non-const-reference argument, but they are
// destroyed at function return, so it could results in undefined behavior.

bool createShaderModule(VkDevice logical_device,
		std::vector<unsigned char> const &source_code,
		VkShaderModule &shader_module);
void specifyPipelineShaderStages(
		std::vector<ShaderStageParameters> const &shader_stage_params,
		std::vector<VkPipelineShaderStageCreateInfo> &shader_stage_create_infos);
void specifyPipelineVertexInputState(
		std::vector<VkVertexInputBindingDescription> const &binding_descriptions,
		std::vector<VkVertexInputAttributeDescription> const &attribute_descriptions,
		VkPipelineVertexInputStateCreateInfo &vertex_input_state_create_info);
void specifyPipelineInputAssemblyState(VkPrimitiveTopology topology,
		bool primitive_restart_enable,
		VkPipelineInputAssemblyStateCreateInfo &input_assembly_state_create_info);
void specifyPipelineTessellationState(uint32_t patch_control_points_count,
		VkPipelineTessellationStateCreateInfo &tessellation_state_create_info);
void specifyPipelineViewportAndScissorTestState(
		ViewportInfo const &viewport_infos,
		VkPipelineViewportStateCreateInfo &viewport_state_create_info);
void specifyPipelineRasterizationState(bool depth_clamp_enable,
		bool rasterizer_discard_enable, VkPolygonMode polygon_mode,
		VkCullModeFlags culling_mode, VkFrontFace front_face,
		bool depth_bias_enable, float depth_bias_constant_factor,
		float depth_bias_clamp, float depth_bias_slope_factor, float line_width,
		VkPipelineRasterizationStateCreateInfo &rasterization_state_create_info);
void specifyPipelineMultisampleState(VkSampleCountFlagBits sample_count,
		bool per_sample_shading_enable, float min_sample_shading,
		VkSampleMask const *sample_masks, bool alpha_to_coverage_enable,
		bool alpha_to_one_enable,
		VkPipelineMultisampleStateCreateInfo &multisample_state_create_info);
void specifyPipelineDepthAndStencilState(bool depth_test_enable,
		bool depth_write_enable, VkCompareOp depth_compare_op,
		bool depth_bounds_test_enable, float min_depth_bounds,
		float max_depth_bounds, bool stencil_test_enable,
		VkStencilOpState front_stencil_test_parameters,
		VkStencilOpState back_stencil_test_parameters,
		VkPipelineDepthStencilStateCreateInfo &depth_and_stencil_state_create_info);
void specifyPipelineBlendState(bool logic_op_enable, VkLogicOp logic_op,
		std::vector<VkPipelineColorBlendAttachmentState> const &attachment_blend_states,
		std::array<float, 4> const &blend_constants,
		VkPipelineColorBlendStateCreateInfo &blend_state_create_info);
void specifyPipelineDynamicStates(
		std::vector<VkDynamicState> const &dynamic_states,
		VkPipelineDynamicStateCreateInfo &dynamic_state_creat_info);
bool createPipelineLayout(VkDevice logical_device,
		std::vector<VkDescriptorSetLayout> const &descriptor_set_layouts,
		std::vector<VkPushConstantRange> const &push_constant_ranges,
		VkPipelineLayout &pipeline_layout);
void specifyGraphicsPipelineCreationParameters(
		VkPipelineCreateFlags additional_options,
		std::vector<VkPipelineShaderStageCreateInfo> const &shader_stage_create_infos,
		VkPipelineVertexInputStateCreateInfo const &vertex_input_state_create_info,
		VkPipelineInputAssemblyStateCreateInfo const &input_assembly_state_create_info,
		VkPipelineTessellationStateCreateInfo const *tessellation_state_create_info,
		VkPipelineViewportStateCreateInfo const *viewport_state_create_info,
		VkPipelineRasterizationStateCreateInfo const &rasterization_state_create_info,
		VkPipelineMultisampleStateCreateInfo const *multisample_state_create_info,
		VkPipelineDepthStencilStateCreateInfo const *depth_and_stencil_state_create_info,
		VkPipelineColorBlendStateCreateInfo const *blend_state_create_info,
		VkPipelineDynamicStateCreateInfo const *dynamic_state_creat_info,
		VkPipelineLayout pipeline_layout, VkRenderPass render_pass,
		uint32_t subpass, VkPipeline base_pipeline_handle,
		int32_t base_pipeline_index,
		VkGraphicsPipelineCreateInfo &graphics_pipeline_create_info);
bool createPipelineCacheObject(VkDevice logical_device,
		std::vector<unsigned char> const &cache_data,
		VkPipelineCache &pipeline_cache);
bool retrieveDataFromPipelineCache(VkDevice logical_device,
		VkPipelineCache pipeline_cache,
		std::vector<unsigned char> &pipeline_cache_data);
bool mergeMultiplePipelineCacheObjects(VkDevice logical_device,
		VkPipelineCache target_pipeline_cache,
		std::vector<VkPipelineCache> const &source_pipeline_caches);
bool createGraphicsPipelines(VkDevice logical_device,
		std::vector<VkGraphicsPipelineCreateInfo> const &graphics_pipeline_create_infos,
		VkPipelineCache pipeline_cache,
		std::vector<VkPipeline> &graphics_pipelines);
bool createComputePipeline(VkDevice logical_device,
		VkPipelineCreateFlags additional_options,
		VkPipelineShaderStageCreateInfo const &compute_shader_stage,
		VkPipelineLayout pipeline_layout, VkPipeline base_pipeline_handle,
		VkPipelineCache pipeline_cache, VkPipeline &compute_pipeline);
void bindPipelineObject(VkCommandBuffer command_buffer,
		VkPipelineBindPoint pipeline_type, VkPipeline pipeline);
bool createPipelineLayoutWithCombinedImageSamplerBufferAndPushConstantRanges(
		VkDevice logical_device,
		std::vector<VkPushConstantRange> const &push_constant_ranges,
		VkDescriptorSetLayout &descriptor_set_layout,
		VkPipelineLayout &pipeline_layout);
bool createGraphicsPipelineWithVertexAndFragmentShadersDepthTestEnabledAndWithDynamicViewportAndScissorTests(
		VkDevice logical_device, VkPipelineCreateFlags additional_options,
		std::string const &vertex_shader_filename,
		std::string const &fragment_shader_filename,
		std::vector<VkVertexInputBindingDescription> const &vertex_input_binding_descriptions,
		std::vector<VkVertexInputAttributeDescription> const &vertex_attribute_descriptions,
		VkPrimitiveTopology primitive_topology, bool primitive_restart_enable,
		VkPolygonMode polygon_mode, VkCullModeFlags culling_mode,
		VkFrontFace front_face, bool logic_op_enable, VkLogicOp logic_op,
		std::vector<VkPipelineColorBlendAttachmentState> const &attachment_blend_states,
		std::array<float, 4> const &blend_constants,
		VkPipelineLayout pipeline_layout, VkRenderPass render_pass,
		uint32_t subpass, VkPipeline base_pipeline_handle,
		VkPipelineCache pipeline_cache,
		std::vector<VkPipeline> &graphics_pipeline);
bool createMultipleGraphicsPipelinesOnMultipleThreads(VkDevice logical_device,
		std::string const &pipeline_cache_filename,
		std::vector<std::vector<VkGraphicsPipelineCreateInfo>> const &graphics_pipelines_create_infos,
		std::vector<std::vector<VkPipeline>> &graphics_pipelines);
void destroyPipeline(VkDevice logical_device, VkPipeline &pipeline);
void destroyPipelineCache(VkDevice logical_device,
		VkPipelineCache &pipeline_cache);
void destroyPipelineLayout(VkDevice logical_device,
		VkPipelineLayout &pipeline_layout);
void destroyShaderModule(VkDevice logical_device,
		VkShaderModule &shader_module);

}  // namespace oge

#endif /* INCLUDE_LIB_GRAPHICSANDCOMPUTEPIPELINES_H_ */
