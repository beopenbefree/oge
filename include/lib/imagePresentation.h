/**
 * imagePresentation.h
 *
 *  Created on: Mar 5, 2021
 *      Author: beopenbefree
 */

#ifndef INCLUDE_LIB_IMAGEPRESENTATION_H_
#define INCLUDE_LIB_IMAGEPRESENTATION_H_

#include <vector>
#include "lib/vulkanFunctions.h"
#include "lib/common.h"

namespace oge {

bool createVulkanInstanceWithWsiExtensionsEnabled(
		std::vector<char const*> &desired_extensions,
		char const *const application_name, VkInstance &instance);
bool createPresentationSurface(VkInstance instance, GLFWwindow *window,
		const VkAllocationCallbacks *allocator,
		VkSurfaceKHR *presentation_surface);
bool selectQueueFamilyThatSupportsPresentationToGivenSurface(
		VkPhysicalDevice physical_device, VkSurfaceKHR presentation_surface,
		uint32_t &queue_family_index);
bool createLogicalDeviceWithWsiExtensionsEnabled(
		VkPhysicalDevice physical_device, std::vector<QueueInfo> queue_infos,
		std::vector<char const*> &desired_extensions,
		VkPhysicalDeviceFeatures *desired_features, VkDevice &logical_device);
bool selectDesiredPresentationMode(VkPhysicalDevice physical_device,
		VkSurfaceKHR presentation_surface,
		VkPresentModeKHR desired_present_mode, VkPresentModeKHR &present_mode);
bool getCapabilitiesOfPresentationSurface(VkPhysicalDevice physical_device,
		VkSurfaceKHR presentation_surface,
		VkSurfaceCapabilitiesKHR &surface_capabilities);
bool selectNumberOfSwapchainImages(
		VkSurfaceCapabilitiesKHR const &surface_capabilities,
		uint32_t &number_of_images);
bool chooseSizeOfSwapchainImages(
		VkSurfaceCapabilitiesKHR const &surface_capabilities,
		VkExtent2D &size_of_images, const VkExtent2D &default_size_of_images = {
				640, 480 });
bool selectDesiredUsageScenariosOfSwapchainImages(
		VkSurfaceCapabilitiesKHR const &surface_capabilities,
		VkImageUsageFlags desired_usages, VkImageUsageFlags &image_usage);
bool selectTransformationOfSwapchainImages(
		VkSurfaceCapabilitiesKHR const &surface_capabilities,
		VkSurfaceTransformFlagBitsKHR desired_transform,
		VkSurfaceTransformFlagBitsKHR &surface_transform);
bool selectFormatOfSwapchainImages(VkPhysicalDevice physical_device,
		VkSurfaceKHR presentation_surface,
		VkSurfaceFormatKHR desired_surface_format, VkFormat &image_format,
		VkColorSpaceKHR &image_color_space);
bool createSwapchain(VkDevice logical_device, VkSurfaceKHR presentation_surface,
		uint32_t image_count, VkSurfaceFormatKHR surface_format,
		VkExtent2D image_size, VkImageUsageFlags image_usage,
		VkSurfaceTransformFlagBitsKHR surface_transform,
		VkPresentModeKHR present_mode, VkSwapchainKHR &old_swapchain,
		VkSwapchainKHR &swapchain, VkSharingMode imageSharingMode =
				VK_SHARING_MODE_EXCLUSIVE,
		std::vector<uint32_t> queueFamilyIndices = { },
		VkCompositeAlphaFlagBitsKHR compositeAlpha =
				VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR, VkBool32 clipped = VK_TRUE);
bool getHandlesOfSwapchainImages(VkDevice logical_device,
		VkSwapchainKHR swapchain, std::vector<VkImage> &swapchain_images);
bool createSwapchainWithR8G8B8A8FormatAndMailboxPresentMode(
		VkPhysicalDevice physical_device, VkSurfaceKHR presentation_surface,
		VkDevice logical_device, VkImageUsageFlags swapchain_image_usage,
		VkExtent2D &image_size, VkFormat &image_format,
		VkSwapchainKHR &old_swapchain, VkSwapchainKHR &swapchain,
		std::vector<VkImage> &swapchain_images);
bool acquireSwapchainImage(VkDevice logical_device, VkSwapchainKHR swapchain,
		VkSemaphore semaphore, VkFence fence, uint32_t &image_index);
bool presentImage(VkQueue queue, std::vector<VkSemaphore> rendering_semaphores,
		std::vector<PresentInfo> images_to_present);
void destroySwapchain(VkDevice logical_device, VkSwapchainKHR &swapchain);
void destroyPresentationSurface(VkInstance instance,
		VkSurfaceKHR &presentation_surface);

} // namespace oge

#endif /* INCLUDE_LIB_IMAGEPRESENTATION_H_ */
