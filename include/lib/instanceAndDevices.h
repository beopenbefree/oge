/**
 * instanceAndDevices.h
 *
 *  Created on: Feb 28, 2021
 *      Author: beopenbefree
 */

#ifndef INCLUDE_LIB_INSTANCEANDDEVICES_H_
#define INCLUDE_LIB_INSTANCEANDDEVICES_H_

#include <vector>
#include "lib/vulkanFunctions.h"
#include "lib/common.h"

namespace oge {

// base
bool connectWithVulkanLoaderLibrary();
bool loadFunctionExportedFromVulkanLoaderLibrary();
bool loadGlobalLevelFunctions();
// instance
bool checkAvailableInstanceExtensions(
		std::vector<VkExtensionProperties> &available_extensions);
bool getRequiredInstanceExtensions(
		std::vector<char const*> &available_extensions);
bool isExtensionSupported(
		std::vector<VkExtensionProperties> const &available_extensions,
		char const *const extension);
bool createVulkanInstance(std::vector<char const*> const &desired_extensions,
		char const *const application_name, VkInstance &instance);
bool loadInstanceLevelFunctions(VkInstance instance,
		std::vector<char const*> const &enabled_extensions);
// device
bool enumerateAvailablePhysicalDevices(VkInstance instance,
		std::vector<VkPhysicalDevice> &available_devices);
bool checkAvailableDeviceExtensions(VkPhysicalDevice physical_device,
		std::vector<VkExtensionProperties> &available_extensions);
void getFeaturesAndPropertiesOfPhysicalDevice(VkPhysicalDevice physical_device,
		VkPhysicalDeviceFeatures &device_features,
		VkPhysicalDeviceProperties &device_properties);
void getMemoryPropertiesOfPhysicalDevice(VkPhysicalDevice physical_device,
		VkPhysicalDeviceMemoryProperties &memory_properties);
bool checkAvailableQueueFamiliesAndTheirProperties(
		VkPhysicalDevice physical_device,
		std::vector<VkQueueFamilyProperties> &queue_families);
bool selectIndexOfQueueFamilyWithDesiredCapabilities(
		VkPhysicalDevice physical_device, VkQueueFlags desired_capabilities,
		uint32_t &queue_family_index);
bool createLogicalDevice(VkPhysicalDevice physical_device,
		std::vector<QueueInfo> queue_infos,
		std::vector<char const*> const &desired_extensions,
		VkPhysicalDeviceFeatures *desired_features, VkDevice &logical_device);
bool loadDeviceLevelFunctions(VkDevice logical_device,
		std::vector<char const*> const &enabled_extensions);
void getDeviceQueue(VkDevice logical_device, uint32_t queue_family_index,
		uint32_t queue_index, VkQueue &queue);
bool createLogicalDeviceWithGeometryShadersAndGraphicsAndComputeQueues(
		VkInstance instance, VkDevice &logical_device, VkQueue &graphics_queue,
		VkQueue &compute_queue,
		std::vector<char const*> const &desired_extensions = { },
		bool distinctQueuesHint = false);
void destroyLogicalDevice(VkDevice &logical_device);
void destroyVulkanInstance(VkInstance &instance);

} // namespace oge

#endif /* INCLUDE_LIB_INSTANCEANDDEVICES_H_ */
