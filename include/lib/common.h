/**
 * common.h
 *
 *  Created on: Mar 10, 2021
 *      Author: beopenbefree
 */

#ifndef INCLUDE_LIB_QUEUEINFO_H_
#define INCLUDE_LIB_QUEUEINFO_H_

#include <vector>
#include <string>
#include <functional>
#include "vulkanFunctions.h"
#include "vulkanDestroyer.h"

namespace oge {
struct QueueInfo {
	uint32_t FamilyIndex;
	std::vector<float> Priorities;
};

struct PresentInfo {
	VkSwapchainKHR Swapchain;
	uint32_t ImageIndex;
};

struct WaitSemaphoreInfo {
	VkSemaphore Semaphore;
	VkPipelineStageFlags WaitingStage;
};

struct BufferTransition {
	VkBuffer Buffer;
	VkAccessFlags CurrentAccess;
	VkAccessFlags NewAccess;
	uint32_t CurrentQueueFamily;
	uint32_t NewQueueFamily;
};

struct ImageTransition {
	VkImage Image;
	VkAccessFlags CurrentAccess;
	VkAccessFlags NewAccess;
	VkImageLayout CurrentLayout;
	VkImageLayout NewLayout;
	uint32_t CurrentQueueFamily;
	uint32_t NewQueueFamily;
	VkImageAspectFlags Aspect;
};

struct ImageDescriptorInfo {
	VkDescriptorSet TargetDescriptorSet;
	uint32_t TargetDescriptorBinding;
	uint32_t TargetArrayElement;
	VkDescriptorType TargetDescriptorType;
	std::vector<VkDescriptorImageInfo> ImageInfos;
};

struct BufferDescriptorInfo {
	VkDescriptorSet TargetDescriptorSet;
	uint32_t TargetDescriptorBinding;
	uint32_t TargetArrayElement;
	VkDescriptorType TargetDescriptorType;
	std::vector<VkDescriptorBufferInfo> BufferInfos;
};

struct TexelBufferDescriptorInfo {
	VkDescriptorSet TargetDescriptorSet;
	uint32_t TargetDescriptorBinding;
	uint32_t TargetArrayElement;
	VkDescriptorType TargetDescriptorType;
	std::vector<VkBufferView> TexelBufferViews;
};

struct CopyDescriptorInfo {
	VkDescriptorSet TargetDescriptorSet;
	uint32_t TargetDescriptorBinding;
	uint32_t TargetArrayElement;
	VkDescriptorSet SourceDescriptorSet;
	uint32_t SourceDescriptorBinding;
	uint32_t SourceArrayElement;
	uint32_t DescriptorCount;
};

struct SubpassParameters {
	VkPipelineBindPoint PipelineType;
	std::vector<VkAttachmentReference> InputAttachments;
	std::vector<VkAttachmentReference> ColorAttachments;
	std::vector<VkAttachmentReference> ResolveAttachments;
	VkAttachmentReference const *DepthStencilAttachment;
	std::vector<uint32_t> PreserveAttachments;
};

struct ShaderStageParameters {
	VkShaderStageFlagBits ShaderStage;
	VkShaderModule ShaderModule;
	char const *EntryPointName;
	VkSpecializationInfo const *SpecializationInfo;
};

struct ViewportInfo {
	std::vector<VkViewport> Viewports;
	std::vector<VkRect2D> Scissors;
};

struct VertexBufferParameters {
	VkBuffer Buffer;
	VkDeviceSize MemoryOffset;
};

struct CommandBufferRecordingThreadParameters {
	VkCommandBuffer CommandBuffer;
	std::function<bool(VkCommandBuffer)> RecordingFunction;
};

/*
 *	FIXME: In a real-life application, a single frame will be probably
 *	composed of multiple command buffers recorded in multiple threads
 */
struct FrameResources {
	VkCommandBuffer CommandBuffer; //fixme
	VkDestroyer(VkSemaphore) ImageAcquiredSemaphore;
	VkDestroyer(VkSemaphore) ReadyToPresentSemaphore;
	VkDestroyer(VkFence) DrawingFinishedFence;
	VkDestroyer(VkImageView) DepthAttachment;
	VkDestroyer(VkFramebuffer) Framebuffer;

	FrameResources(VkCommandBuffer &command_buffer,
	VkDestroyer(VkSemaphore) &image_acquired_semaphore,
	VkDestroyer(VkSemaphore) &ready_to_present_semaphore,
	VkDestroyer(VkFence) &drawing_finished_fence,
	VkDestroyer(VkImageView) &depth_attachment,
	VkDestroyer(VkFramebuffer) &framebuffer) :
			CommandBuffer(command_buffer), ImageAcquiredSemaphore(
					std::move(image_acquired_semaphore)), ReadyToPresentSemaphore(
					std::move(ready_to_present_semaphore)), DrawingFinishedFence(
					std::move(drawing_finished_fence)), DepthAttachment(
					std::move(depth_attachment)), Framebuffer(
					std::move(framebuffer)) {
	}

	FrameResources(FrameResources &&other) {
		*this = std::move(other);
	}

	FrameResources& operator=(FrameResources &&other) {
		if (this != &other) {
			VkCommandBuffer command_buffer = CommandBuffer;

			CommandBuffer = other.CommandBuffer;
			other.CommandBuffer = command_buffer;
			ImageAcquiredSemaphore = std::move(other.ImageAcquiredSemaphore);
			ReadyToPresentSemaphore = std::move(other.ReadyToPresentSemaphore);
			DrawingFinishedFence = std::move(other.DrawingFinishedFence);
			DepthAttachment = std::move(other.DepthAttachment);
			Framebuffer = std::move(other.Framebuffer);
		}
		return *this;
	}

	FrameResources(FrameResources const&) = delete;
	FrameResources& operator=(FrameResources const&) = delete;
};

}  // namespace oge

#endif /* INCLUDE_LIB_QUEUEINFO_H_ */
