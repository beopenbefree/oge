/**
 * vulkanFunctions.h
 *
 *  Created on: Feb 28, 2021
 *      Author: beopenbefree
 */

#ifndef INCLUDE_LIB_VULKANFUNCTIONS_H_
#define INCLUDE_LIB_VULKANFUNCTIONS_H_

/// Including the Vulkan and GLFW header files
#define GLFW_INCLUDE_NONE
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

namespace oge {

#define EXPORTED_VULKAN_FUNCTION( name ) extern PFN_##name name;
#define GLOBAL_LEVEL_VULKAN_FUNCTION( name ) extern PFN_##name name;
#define INSTANCE_LEVEL_VULKAN_FUNCTION( name ) extern PFN_##name name;
#define INSTANCE_LEVEL_VULKAN_FUNCTION_FROM_EXTENSION( name, extension ) extern PFN_##name name;
#define DEVICE_LEVEL_VULKAN_FUNCTION( name ) extern PFN_##name name;
#define DEVICE_LEVEL_VULKAN_FUNCTION_FROM_EXTENSION( name, extension ) extern PFN_##name name;

#include "listOfVulkanFunctions.inl"

}  // namespace oge

#endif /* INCLUDE_LIB_VULKANFUNCTIONS_H_ */
