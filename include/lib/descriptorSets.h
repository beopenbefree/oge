/**
 * descriptorSets.h
 *
 *  Created on: Mar 20, 2021
 *      Author: beopenbefree
 */

#ifndef INCLUDE_LIB_DESCRIPTORSETS_H_
#define INCLUDE_LIB_DESCRIPTORSETS_H_

#include <vector>
#include "lib/vulkanFunctions.h"
#include "lib/common.h"

namespace oge {

bool createSampler(VkDevice logical_device, VkFilter mag_filter,
		VkFilter min_filter, VkSamplerMipmapMode mipmap_mode,
		VkSamplerAddressMode u_address_mode,
		VkSamplerAddressMode v_address_mode,
		VkSamplerAddressMode w_address_mode, float lod_bias,
		bool anisotropy_enable, float max_anisotropy, bool compare_enable,
		VkCompareOp compare_operator, float min_lod, float max_lod,
		VkBorderColor border_color, bool unnormalized_coords,
		VkSampler &sampler);
bool createSampledImage(VkPhysicalDevice physical_device,
		VkDevice logical_device, VkImageType type, VkFormat format,
		VkExtent3D size, uint32_t num_mipmaps, uint32_t num_layers,
		VkImageUsageFlags usage, bool cubemap, VkImageViewType view_type,
		VkImageAspectFlags aspect, bool linear_filtering,
		VkImage &sampled_image, VkDeviceMemory &memory_object,
		VkImageView &sampled_image_view);
bool createCombinedImageSampler(VkPhysicalDevice physical_device,
		VkDevice logical_device, VkImageType type, VkFormat format,
		VkExtent3D size, uint32_t num_mipmaps, uint32_t num_layers,
		VkImageUsageFlags usage, bool cubemap, VkImageViewType view_type,
		VkImageAspectFlags aspect, VkFilter mag_filter, VkFilter min_filter,
		VkSamplerMipmapMode mipmap_mode, VkSamplerAddressMode u_address_mode,
		VkSamplerAddressMode v_address_mode,
		VkSamplerAddressMode w_address_mode, float lod_bias,
		bool anisotropy_enable, float max_anisotropy, bool compare_enable,
		VkCompareOp compare_operator, float min_lod, float max_lod,
		VkBorderColor border_color, bool unnormalized_coords,
		VkSampler &sampler, VkImage &sampled_image,
		VkDeviceMemory &memory_object, VkImageView &sampled_image_view);
bool createStorageImage(VkPhysicalDevice physical_device,
		VkDevice logical_device, VkImageType type, VkFormat format,
		VkExtent3D size, uint32_t num_mipmaps, uint32_t num_layers,
		VkImageUsageFlags usage, VkImageViewType view_type,
		VkImageAspectFlags aspect, bool atomic_operations,
		VkImage &storage_image, VkDeviceMemory &memory_object,
		VkImageView &storage_image_view);
bool createUniformTexelBuffer(VkPhysicalDevice physical_device,
		VkDevice logical_device, VkFormat format, VkDeviceSize size,
		VkImageUsageFlags usage, VkBuffer &uniform_texel_buffer,
		VkDeviceMemory &memory_object, VkBufferView &uniform_texel_buffer_view);
bool createStorageTexelBuffer(VkPhysicalDevice physical_device,
		VkDevice logical_device, VkFormat format, VkDeviceSize size,
		VkBufferUsageFlags usage, bool atomic_operations,
		VkBuffer &storage_texel_buffer, VkDeviceMemory &memory_object,
		VkBufferView &storage_texel_buffer_view);
bool createUniformBuffer(VkPhysicalDevice physical_device,
		VkDevice logical_device, VkDeviceSize size, VkBufferUsageFlags usage,
		VkBuffer &uniform_buffer, VkDeviceMemory &memory_object,
		VkMemoryPropertyFlags memory_properties =
				VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
bool createStorageBuffer(VkPhysicalDevice physical_device,
		VkDevice logical_device, VkDeviceSize size, VkBufferUsageFlags usage,
		VkBuffer &storage_buffer, VkDeviceMemory &memory_object);
bool createInputAttachment(VkPhysicalDevice physical_device,
		VkDevice logical_device, VkImageType type, VkFormat format,
		VkExtent3D size, VkImageUsageFlags usage, VkImageViewType view_type,
		VkImageAspectFlags aspect, VkImage &input_attachment,
		VkDeviceMemory &memory_object,
		VkImageView &input_attachment_image_view);
bool createDescriptorSetLayout(VkDevice logical_device,
		std::vector<VkDescriptorSetLayoutBinding> const &bindings,
		VkDescriptorSetLayout &descriptor_set_layout,
		VkDescriptorSetLayoutCreateFlags flags = 0);
bool createDescriptorPool(VkDevice logical_device, bool free_individual_sets,
		uint32_t max_sets_count,
		std::vector<VkDescriptorPoolSize> const &descriptor_types,
		VkDescriptorPool &descriptor_pool);
bool allocateDescriptorSets(VkDevice logical_device,
		VkDescriptorPool descriptor_pool,
		std::vector<VkDescriptorSetLayout> const &descriptor_set_layouts,
		std::vector<VkDescriptorSet> &descriptor_sets);
void updateDescriptorSets(VkDevice logical_device,
		std::vector<ImageDescriptorInfo> const &image_descriptor_infos,
		std::vector<BufferDescriptorInfo> const &buffer_descriptor_infos,
		std::vector<TexelBufferDescriptorInfo> const &texel_buffer_descriptor_infos,
		std::vector<CopyDescriptorInfo> const &copy_descriptor_infos);
void bindDescriptorSets(VkCommandBuffer command_buffer,
		VkPipelineBindPoint pipeline_type, VkPipelineLayout pipeline_layout,
		uint32_t index_for_first_set,
		std::vector<VkDescriptorSet> const &descriptor_sets,
		std::vector<uint32_t> const &dynamic_offsets);
bool createDescriptorsWithTextureAndUniformBuffer(
		VkPhysicalDevice physical_device, VkDevice logical_device,
		VkExtent3D sampled_image_size, uint32_t uniform_buffer_size,
		VkSampler &sampler, VkImage &sampled_image,
		VkDeviceMemory &sampled_image_memory_object,
		VkImageView &sampled_image_view, VkBuffer &uniform_buffer,
		VkDeviceMemory &uniform_buffer_memory_object,
		VkDescriptorSetLayout &descriptor_set_layout,
		VkDescriptorPool &descriptor_pool,
		std::vector<VkDescriptorSet> &descriptor_sets);
bool freeDescriptorSets(VkDevice logical_device,
		VkDescriptorPool descriptor_pool,
		std::vector<VkDescriptorSet> &descriptor_sets);
bool resetDescriptorPool(VkDevice logical_device,
		VkDescriptorPool descriptor_pool);
void destroyDescriptorPool(VkDevice logical_device,
		VkDescriptorPool &descriptor_pool);
void destroyDescriptorSetLayout(VkDevice logical_device,
		VkDescriptorSetLayout &descriptor_set_layout);
void destroySampler(VkDevice logical_device, VkSampler &sampler);

}  // namespace oge

#endif /* INCLUDE_LIB_DESCRIPTORSETS_H_ */
