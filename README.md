# Open Graphic Engine

**Open Graphic Engine** (**OGE**) is a very minimal graphic engine based on open graphic libraries.

### Vulkan application template

A typical Vulkan application would have the following structure:

1. **Initialization**
    * Window Framework
        - Setting an error callback
        - Initializing GLFW
        - Creating the window
    * Vulkan
        + Connecting with Vulkan loader library
        + Load vkGetInstanceProcAddr
        + Load global level functions (vkCreateInstance, ...)
        + Check available instance extensions
        + Creating an instance
        + Load instance level functions
        + Creating a Vulkan window surface
        + Enumerating available physical devices
        + Creating logical device with graphics, compute and presentation queues
        + Creating the command pool
        + for each frame
            * Allocating command buffer(s)
            * Creating acquired semaphore
            * Creating to present semaphore
            * Creating fence
        + Creating the swapchain
    * Vertex data && Staging buffer && Uniform buffer
        + Prepare vertex data
        + Creating a vertex buffer
        + Allocating and binding a memory object for a vertex buffer
        + Using a staging buffer to update a vertex buffer with a device-local memory bound
        + TODO
1. **TODO**
