/**
 * 01-Rendering_a_geometry_with_vertex_diffuse_lighting.cpp
 *
 *  Created on: Apr 4, 2021
 *      Author: beopenbefree
 */

#include <glm/gtc/type_ptr.hpp>
#include "framework/windowFramework.h"
#include "framework/application.h"
#include "framework/helpers.h"
#include "lib/resourcesAndMemory.h"
#include "lib/descriptorSets.h"
#include "lib/renderPassesAndFramebuffers.h"
#include "lib/graphicsAndComputePipelines.h"
#include "lib/commandBuffersAndSynchronization.h"
#include "lib/commandRecordingAndDrawing.h"

using namespace oge;

const char *modelPath = "data/Models/knot.obj";
const char *vertexShaderPath =
		"data/Shaders/11 Lighting/01 Rendering a geometry with vertex diffuse lighting/shader.vert.spv";
const char *fragmentShaderPath =
		"data/Shaders/11 Lighting/01 Rendering a geometry with vertex diffuse lighting/shader.frag.spv";

class Sample: public Application {
	Mesh Model;
	VkDestroyer(VkBuffer) VertexBuffer;
	VkDestroyer(VkDeviceMemory) VertexBufferMemory;

	VkDestroyer(VkDescriptorSetLayout) DescriptorSetLayout;
	VkDestroyer(VkDescriptorPool) DescriptorPool;
	std::vector<VkDescriptorSet> DescriptorSets;

	VkDestroyer(VkRenderPass) RenderPass;
	VkDestroyer(VkPipelineLayout) PipelineLayout;
	VkDestroyer(VkPipeline) Pipeline;

	VkDestroyer(VkBuffer) StagingBuffer;
	VkDestroyer(VkDeviceMemory) StagingBufferMemory;
	bool UpdateUniformBuffer;
	VkDestroyer(VkBuffer) UniformBuffer;
	VkDestroyer(VkDeviceMemory) UniformBufferMemory;

	virtual bool Initialize(GLFWwindow *window_parameters) override
	{
		if (!InitializeVulkan(window_parameters)) {
			return false;
		}

		// Vertex data
		if (!Load3DModelFromObjFile(modelPath, true, false, false, true,
				Model)) {
			return false;
		}

		InitVkDestroyer(LogicalDevice, VertexBuffer);
		if (!createBuffer(*LogicalDevice,
				sizeof(Model.Data[0]) * Model.Data.size(),
				VK_BUFFER_USAGE_TRANSFER_DST_BIT
						| VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, *VertexBuffer)) {
			return false;
		}

		InitVkDestroyer(LogicalDevice, VertexBufferMemory);
		if (!allocateAndBindMemoryObjectToBuffer(PhysicalDevice, *LogicalDevice,
				*VertexBuffer, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
				*VertexBufferMemory)) {
			return false;
		}

		if (!useStagingBufferToUpdateBufferWithDeviceLocalMemoryBound(
				PhysicalDevice, *LogicalDevice,
				sizeof(Model.Data[0]) * Model.Data.size(), &Model.Data[0],
				*VertexBuffer, 0, 0, VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT,
				VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
				VK_PIPELINE_STAGE_VERTEX_INPUT_BIT, GraphicsQueue.Handle,
				FramesResources.front().CommandBuffer, { })) {
			return false;
		}

		// Staging buffer
		InitVkDestroyer(LogicalDevice, StagingBuffer);
		if (!createBuffer(*LogicalDevice, 2 * 16 * sizeof(float),
				VK_BUFFER_USAGE_TRANSFER_SRC_BIT, *StagingBuffer)) {
			return false;
		}
		InitVkDestroyer(LogicalDevice, StagingBufferMemory);
		if (!allocateAndBindMemoryObjectToBuffer(PhysicalDevice, *LogicalDevice,
				*StagingBuffer, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
				*StagingBufferMemory)) {
			return false;
		}

		// Uniform buffer
		InitVkDestroyer(LogicalDevice, UniformBuffer);
		InitVkDestroyer(LogicalDevice, UniformBufferMemory);
		if (!createUniformBuffer(PhysicalDevice, *LogicalDevice,
				2 * 16 * sizeof(float),
				VK_BUFFER_USAGE_TRANSFER_DST_BIT
						| VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, *UniformBuffer,
				*UniformBufferMemory)) {
			return false;
		}

		if (!UpdateStagingBuffer(true)) {
			return false;
		}

		// Descriptor set with uniform buffer
		VkDescriptorSetLayoutBinding descriptor_set_layout_binding = { 0, // uint32_t             binding
				VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, // VkDescriptorType     descriptorType
				1,                       // uint32_t             descriptorCount
				VK_SHADER_STAGE_VERTEX_BIT,   // VkShaderStageFlags   stageFlags
				nullptr              // const VkSampler    * pImmutableSamplers
				};
		InitVkDestroyer(LogicalDevice, DescriptorSetLayout);
		if (!createDescriptorSetLayout(*LogicalDevice, {
				descriptor_set_layout_binding }, *DescriptorSetLayout)) {
			return false;
		}

		VkDescriptorPoolSize descriptor_pool_size = {
				VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,  // VkDescriptorType     type
				1                       // uint32_t             descriptorCount
				};
		InitVkDestroyer(LogicalDevice, DescriptorPool);
		if (!createDescriptorPool(*LogicalDevice, false, 1, {
				descriptor_pool_size }, *DescriptorPool)) {
			return false;
		}

		if (!allocateDescriptorSets(*LogicalDevice, *DescriptorPool, {
				*DescriptorSetLayout }, DescriptorSets)) {
			return false;
		}

		BufferDescriptorInfo buffer_descriptor_update = { DescriptorSets[0], // VkDescriptorSet                      TargetDescriptorSet
				0, // uint32_t                             TargetDescriptorBinding
				0,    // uint32_t                             TargetArrayElement
				VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, // VkDescriptorType                     TargetDescriptorType
				{            // std::vector<VkDescriptorBufferInfo>  BufferInfos
				{ *UniformBuffer, // VkBuffer                             buffer
						0,        // VkDeviceSize                         offset
						VK_WHOLE_SIZE // VkDeviceSize                         range
						} } };

		updateDescriptorSets(*LogicalDevice, { }, { buffer_descriptor_update },
				{ }, { });

		// Render pass
		std::vector<VkAttachmentDescription> attachment_descriptions = { { 0, // VkAttachmentDescriptionFlags     flags
				Swapchain.Format,     // VkFormat                         format
				VK_SAMPLE_COUNT_1_BIT, // VkSampleCountFlagBits            samples
				VK_ATTACHMENT_LOAD_OP_CLEAR, // VkAttachmentLoadOp               loadOp
				VK_ATTACHMENT_STORE_OP_STORE, // VkAttachmentStoreOp              storeOp
				VK_ATTACHMENT_LOAD_OP_DONT_CARE, // VkAttachmentLoadOp               stencilLoadOp
				VK_ATTACHMENT_STORE_OP_DONT_CARE, // VkAttachmentStoreOp              stencilStoreOp
				VK_IMAGE_LAYOUT_UNDEFINED, // VkImageLayout                    initialLayout
				VK_IMAGE_LAYOUT_PRESENT_SRC_KHR // VkImageLayout                    finalLayout
				}, { 0,                // VkAttachmentDescriptionFlags     flags
						DepthFormat,  // VkFormat                         format
						VK_SAMPLE_COUNT_1_BIT, // VkSampleCountFlagBits            samples
						VK_ATTACHMENT_LOAD_OP_CLEAR, // VkAttachmentLoadOp               loadOp
						VK_ATTACHMENT_STORE_OP_DONT_CARE, // VkAttachmentStoreOp              storeOp
						VK_ATTACHMENT_LOAD_OP_DONT_CARE, // VkAttachmentLoadOp               stencilLoadOp
						VK_ATTACHMENT_STORE_OP_DONT_CARE, // VkAttachmentStoreOp              stencilStoreOp
						VK_IMAGE_LAYOUT_UNDEFINED, // VkImageLayout                    initialLayout
						VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL // VkImageLayout                    finalLayout
				} };

		VkAttachmentReference depth_attachment = { 1, // uint32_t                             attachment
				VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL // VkImageLayout                        layout
				};

		std::vector<SubpassParameters> subpass_parameters = { {
				VK_PIPELINE_BIND_POINT_GRAPHICS, // VkPipelineBindPoint                  PipelineType
				{ },    // std::vector<VkAttachmentReference>   InputAttachments
				{       // std::vector<VkAttachmentReference>   ColorAttachments
				{ 0,          // uint32_t                             attachment
						VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, // VkImageLayout                        layout
						} }, { }, // std::vector<VkAttachmentReference>   ResolveAttachments
				&depth_attachment, // VkAttachmentReference const        * DepthStencilAttachment
				{ }  // std::vector<uint32_t>                PreserveAttachments
		} };

		std::vector<VkSubpassDependency> subpass_dependencies = { {
		VK_SUBPASS_EXTERNAL,    // uint32_t                   srcSubpass
				0,                      // uint32_t                   dstSubpass
				VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, // VkPipelineStageFlags       srcStageMask
				VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, // VkPipelineStageFlags       dstStageMask
				VK_ACCESS_MEMORY_READ_BIT, // VkAccessFlags              srcAccessMask
				VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, // VkAccessFlags              dstAccessMask
				VK_DEPENDENCY_BY_REGION_BIT // VkDependencyFlags          dependencyFlags
				}, { 0,                 // uint32_t                   srcSubpass
						VK_SUBPASS_EXTERNAL, // uint32_t                   dstSubpass
						VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, // VkPipelineStageFlags       srcStageMask
						VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, // VkPipelineStageFlags       dstStageMask
						VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, // VkAccessFlags              srcAccessMask
						VK_ACCESS_MEMORY_READ_BIT, // VkAccessFlags              dstAccessMask
						VK_DEPENDENCY_BY_REGION_BIT // VkDependencyFlags          dependencyFlags
				} };

		InitVkDestroyer(LogicalDevice, RenderPass);
		if (!createRenderPass(*LogicalDevice, attachment_descriptions,
				subpass_parameters, subpass_dependencies, *RenderPass)) {
			return false;
		}

		// Graphics pipeline

		InitVkDestroyer(LogicalDevice, PipelineLayout);
		if (!createPipelineLayout(*LogicalDevice, { *DescriptorSetLayout }, { },
				*PipelineLayout)) {
			return false;
		}

		std::vector<unsigned char> vertex_shader_spirv;
		if (!getBinaryFileContents(vertexShaderPath, vertex_shader_spirv)) {
			return false;
		}

		VkDestroyer (VkShaderModule) vertex_shader_module;
		InitVkDestroyer(LogicalDevice, vertex_shader_module);
		if (!createShaderModule(*LogicalDevice, vertex_shader_spirv,
				*vertex_shader_module)) {
			return false;
		}

		std::vector<unsigned char> fragment_shader_spirv;
		if (!getBinaryFileContents(fragmentShaderPath, fragment_shader_spirv)) {
			return false;
		}
		VkDestroyer (VkShaderModule) fragment_shader_module;
		InitVkDestroyer(LogicalDevice, fragment_shader_module);
		if (!createShaderModule(*LogicalDevice, fragment_shader_spirv,
				*fragment_shader_module)) {
			return false;
		}

		std::vector<ShaderStageParameters> shader_stage_params = { {
				VK_SHADER_STAGE_VERTEX_BIT, // VkShaderStageFlagBits        ShaderStage
				*vertex_shader_module, // VkShaderModule               ShaderModule
				"main",           // char const                 * EntryPointName
				nullptr      // VkSpecializationInfo const * SpecializationInfo
				}, { VK_SHADER_STAGE_FRAGMENT_BIT, // VkShaderStageFlagBits        ShaderStage
						*fragment_shader_module, // VkShaderModule               ShaderModule
						"main",   // char const                 * EntryPointName
						nullptr // VkSpecializationInfo const * SpecializationInfo
				} };

		std::vector<VkPipelineShaderStageCreateInfo> shader_stage_create_infos;
		specifyPipelineShaderStages(shader_stage_params,
				shader_stage_create_infos);

		std::vector<VkVertexInputBindingDescription> vertex_input_binding_descriptions =
				{ { 0,                   // uint32_t                     binding
						6 * sizeof(float), // uint32_t                     stride
						VK_VERTEX_INPUT_RATE_VERTEX // VkVertexInputRate            inputRate
						} };

		std::vector<VkVertexInputAttributeDescription> vertex_attribute_descriptions =
				{ { 0,                                    // uint32_t   location
						0,                                 // uint32_t   binding
						VK_FORMAT_R32G32B32_SFLOAT,         // VkFormat   format
						0                                  // uint32_t   offset
						}, { 1,                           // uint32_t   location
								0,                         // uint32_t   binding
								VK_FORMAT_R32G32B32_SFLOAT, // VkFormat   format
								3 * sizeof(float)           // uint32_t   offset
						} };

		VkPipelineVertexInputStateCreateInfo vertex_input_state_create_info;
		specifyPipelineVertexInputState(vertex_input_binding_descriptions,
				vertex_attribute_descriptions, vertex_input_state_create_info);

		VkPipelineInputAssemblyStateCreateInfo input_assembly_state_create_info;
		specifyPipelineInputAssemblyState(VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
				false, input_assembly_state_create_info);

		ViewportInfo viewport_infos = { { // std::vector<VkViewport>   Viewports
				{ 0.0f,               // float          x
						0.0f,               // float          y
						500.0f,             // float          width
						500.0f,             // float          height
						0.0f,               // float          minDepth
						1.0f                // float          maxDepth
						} }, {             // std::vector<VkRect2D>     Scissors
				{ {                   // VkOffset2D     offset
						0,                  // int32_t        x
								0                   // int32_t        y
						}, {                   // VkExtent2D     extent
						500,                // uint32_t       width
								500                 // uint32_t       height
						} } } };
		VkPipelineViewportStateCreateInfo viewport_state_create_info;
		specifyPipelineViewportAndScissorTestState(viewport_infos,
				viewport_state_create_info);

		VkPipelineRasterizationStateCreateInfo rasterization_state_create_info;
		specifyPipelineRasterizationState(false, false, VK_POLYGON_MODE_FILL,
				VK_CULL_MODE_BACK_BIT, VK_FRONT_FACE_COUNTER_CLOCKWISE, false,
				0.0f, 0.0f, 0.0f, 1.0f, rasterization_state_create_info);

		VkPipelineMultisampleStateCreateInfo multisample_state_create_info;
		specifyPipelineMultisampleState(VK_SAMPLE_COUNT_1_BIT, false, 0.0f,
				nullptr, false, false, multisample_state_create_info);

		VkPipelineDepthStencilStateCreateInfo depth_stencil_state_create_info;
		specifyPipelineDepthAndStencilState(true, true,
				VK_COMPARE_OP_LESS_OR_EQUAL, false, 0.0f, 1.0f, false, { }, { },
				depth_stencil_state_create_info);

		std::vector<VkPipelineColorBlendAttachmentState> attachment_blend_states =
				{ { false,               // VkBool32                 blendEnable
						VK_BLEND_FACTOR_ONE, // VkBlendFactor            srcColorBlendFactor
						VK_BLEND_FACTOR_ONE, // VkBlendFactor            dstColorBlendFactor
						VK_BLEND_OP_ADD, // VkBlendOp                colorBlendOp
						VK_BLEND_FACTOR_ONE, // VkBlendFactor            srcAlphaBlendFactor
						VK_BLEND_FACTOR_ONE, // VkBlendFactor            dstAlphaBlendFactor
						VK_BLEND_OP_ADD, // VkBlendOp                alphaBlendOp
						VK_COLOR_COMPONENT_R_BIT
								|    // VkColorComponentFlags    colorWriteMask
								VK_COLOR_COMPONENT_G_BIT
								| VK_COLOR_COMPONENT_B_BIT
								| VK_COLOR_COMPONENT_A_BIT } };
		VkPipelineColorBlendStateCreateInfo blend_state_create_info;
		specifyPipelineBlendState(false, VK_LOGIC_OP_COPY,
				attachment_blend_states, { 1.0f, 1.0f, 1.0f, 1.0f },
				blend_state_create_info);

		std::vector<VkDynamicState> dynamic_states = {
				VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR };
		VkPipelineDynamicStateCreateInfo dynamic_state_create_info;
		specifyPipelineDynamicStates(dynamic_states, dynamic_state_create_info);

		VkGraphicsPipelineCreateInfo pipeline_create_info;
		specifyGraphicsPipelineCreationParameters(0, shader_stage_create_infos,
				vertex_input_state_create_info,
				input_assembly_state_create_info, nullptr,
				&viewport_state_create_info, rasterization_state_create_info,
				&multisample_state_create_info,
				&depth_stencil_state_create_info, &blend_state_create_info,
				&dynamic_state_create_info, *PipelineLayout, *RenderPass, 0,
				VK_NULL_HANDLE, -1, pipeline_create_info);

		std::vector<VkPipeline> pipeline;
		if (!createGraphicsPipelines(*LogicalDevice, { pipeline_create_info },
		VK_NULL_HANDLE, pipeline)) {
			return false;
		}
		InitVkDestroyer(LogicalDevice, Pipeline);
		*Pipeline = pipeline[0];

		return true;
	}

	virtual bool Draw() override {
		auto prepare_frame = [&](VkCommandBuffer command_buffer,
				uint32_t swapchain_image_index, VkFramebuffer framebuffer) {
			if (!beginCommandBufferRecordingOperation(command_buffer,
					VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT, nullptr)) {
				return false;
			}

			if (UpdateUniformBuffer) {
				UpdateUniformBuffer = false;

				BufferTransition pre_transfer_transition = { *UniformBuffer, // VkBuffer         Buffer
						VK_ACCESS_UNIFORM_READ_BIT, // VkAccessFlags    CurrentAccess
						VK_ACCESS_TRANSFER_WRITE_BIT, // VkAccessFlags    NewAccess
						VK_QUEUE_FAMILY_IGNORED, // uint32_t         CurrentQueueFamily
						VK_QUEUE_FAMILY_IGNORED // uint32_t         NewQueueFamily
						};
				setBufferMemoryBarrier(command_buffer,
						VK_PIPELINE_STAGE_VERTEX_SHADER_BIT,
						VK_PIPELINE_STAGE_TRANSFER_BIT, {
								pre_transfer_transition });

				std::vector<VkBufferCopy> regions = { { 0, // VkDeviceSize     srcOffset
						0,                        // VkDeviceSize     dstOffset
						2 * 16 * sizeof(float)  // VkDeviceSize     size
				} };
				copyDataBetweenBuffers(command_buffer, *StagingBuffer,
						*UniformBuffer, regions);

				BufferTransition post_transfer_transition = { *UniformBuffer, // VkBuffer         Buffer
						VK_ACCESS_TRANSFER_WRITE_BIT, // VkAccessFlags    CurrentAccess
						VK_ACCESS_UNIFORM_READ_BIT, // VkAccessFlags    NewAccess
						VK_QUEUE_FAMILY_IGNORED, // uint32_t         CurrentQueueFamily
						VK_QUEUE_FAMILY_IGNORED // uint32_t         NewQueueFamily
						};
				setBufferMemoryBarrier(command_buffer,
						VK_PIPELINE_STAGE_TRANSFER_BIT,
						VK_PIPELINE_STAGE_VERTEX_SHADER_BIT, {
								post_transfer_transition });
			}

			if (PresentQueue.FamilyIndex != GraphicsQueue.FamilyIndex) {
				ImageTransition image_transition_before_drawing = {
						Swapchain.Images[swapchain_image_index], // VkImage              Image
						VK_ACCESS_MEMORY_READ_BIT, // VkAccessFlags        CurrentAccess
						VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, // VkAccessFlags        NewAccess
						VK_IMAGE_LAYOUT_UNDEFINED, // VkImageLayout        CurrentLayout
						VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, // VkImageLayout        NewLayout
						PresentQueue.FamilyIndex, // uint32_t             CurrentQueueFamily
						GraphicsQueue.FamilyIndex, // uint32_t             NewQueueFamily
						VK_IMAGE_ASPECT_COLOR_BIT // VkImageAspectFlags   Aspect
						};
				setImageMemoryBarrier(command_buffer,
						VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
						VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, {
								image_transition_before_drawing });
			}

			// Drawing
			beginRenderPass(command_buffer, *RenderPass, framebuffer, {
					{ 0, 0 }, Swapchain.Size }, { { 0.1f, 0.2f, 0.3f, 1.0f }, {
					1.0f, 0 } }, VK_SUBPASS_CONTENTS_INLINE);

			VkViewport viewport = { 0.0f,                          // float    x
					0.0f,                                       // float    y
					static_cast<float>(Swapchain.Size.width),  // float    width
					static_cast<float>(Swapchain.Size.height), // float    height
					0.0f,                                   // float    minDepth
					1.0f,                                  // float    maxDepth
					};
			setViewportStateDynamically(command_buffer, 0, { viewport });

			VkRect2D scissor = { {                      // VkOffset2D     offset
					0,                                       // int32_t        x
							0                               // int32_t        y
					}, {                                // VkExtent2D     extent
					Swapchain.Size.width,                // uint32_t       width
							Swapchain.Size.height      // uint32_t       height
					} };
			setScissorStateDynamically(command_buffer, 0, { scissor });

			bindVertexBuffers(command_buffer, 0, { { *VertexBuffer, 0 } });

			bindDescriptorSets(command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS,
					*PipelineLayout, 0, DescriptorSets, { });

			bindPipelineObject(command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS,
					*Pipeline);

			for (size_t i = 0; i < Model.Parts.size(); ++i) {
				drawGeometry(command_buffer, Model.Parts[i].VertexCount, 1,
						Model.Parts[i].VertexOffset, 0);
			}

			endRenderPass(command_buffer);

			if (PresentQueue.FamilyIndex != GraphicsQueue.FamilyIndex) {
				ImageTransition image_transition_before_present = {
						Swapchain.Images[swapchain_image_index], // VkImage              Image
						VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, // VkAccessFlags        CurrentAccess
						VK_ACCESS_MEMORY_READ_BIT, // VkAccessFlags        NewAccess
						VK_IMAGE_LAYOUT_PRESENT_SRC_KHR, // VkImageLayout        CurrentLayout
						VK_IMAGE_LAYOUT_PRESENT_SRC_KHR, // VkImageLayout        NewLayout
						GraphicsQueue.FamilyIndex, // uint32_t             CurrentQueueFamily
						PresentQueue.FamilyIndex, // uint32_t             NewQueueFamily
						VK_IMAGE_ASPECT_COLOR_BIT // VkImageAspectFlags   Aspect
						};
				setImageMemoryBarrier(command_buffer,
						VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
						VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, {
								image_transition_before_present });
			}

			if (!endCommandBufferRecordingOperation(command_buffer)) {
				return false;
			}
			return true;
		};

		return increasePerformanceThroughIncreasingTheNumberOfSeparatelyRenderedFrames(
				*LogicalDevice, GraphicsQueue.Handle, PresentQueue.Handle,
				*Swapchain.Handle, Swapchain.Size, Swapchain.ImageViewsRaw,
				*RenderPass, { }, prepare_frame, FramesResources);
	}

	void OnMouseEvent() {
		UpdateStagingBuffer(false);
	}

	bool UpdateStagingBuffer(bool force) {
		UpdateUniformBuffer = true;
		static float horizontal_angle = 0.0f;
		static float vertical_angle = 0.0f;
		if (MouseState.Buttons[0].IsPressed || force) {
			horizontal_angle += 0.5f * MouseState.Position.Delta.X;
			vertical_angle -= 0.5f * MouseState.Position.Delta.Y;
			if (vertical_angle > 90.0f) {
				vertical_angle = 90.0f;
			}
			if (vertical_angle < -90.0f) {
				vertical_angle = -90.0f;
			}

			Matrix4x4 rotation_matrix = prepareRotationMatrix(vertical_angle, {
					1.0f, 0.0f, 0.0f })
					* prepareRotationMatrix(horizontal_angle, { 0.0f, -1.0f,
							0.0f });
			Matrix4x4 translation_matrix = prepareTranslationMatrix(0.0f, 0.0f,
					-4.0f);
			Matrix4x4 model_view_matrix = translation_matrix * rotation_matrix;

			if (!mapUpdateAndUnmapHostVisibleMemory(*LogicalDevice,
					*StagingBufferMemory, 0,
					sizeof(model_view_matrix[0][0]) * model_view_matrix.length()
							* model_view_matrix[0].length(),
					glm::value_ptr(model_view_matrix), true, nullptr)) {
				return false;
			}

			Matrix4x4 perspective_matrix = preparePerspectiveProjectionMatrix(
					static_cast<float>(Swapchain.Size.width)
							/ static_cast<float>(Swapchain.Size.height), 50.0f,
					0.5f, 10.0f);

			if (!mapUpdateAndUnmapHostVisibleMemory(*LogicalDevice,
					*StagingBufferMemory,
					sizeof(model_view_matrix[0][0]) * model_view_matrix.length()
							* model_view_matrix[0].length(),
					sizeof(perspective_matrix[0][0])
							* perspective_matrix.length()
							* perspective_matrix[0].length(),
					glm::value_ptr(perspective_matrix), true, nullptr)) {
				return false;
			}
		}
		return true;
	}

	virtual bool Resize() override {
		if (!CreateSwapchain()) {
			return false;
		}

		if (IsReady()) {
			if (!UpdateStagingBuffer(true)) {
				return false;
			}
		}
		return true;
	}

};

APPLICATION_FRAMEWORK(
		"11/01 - Rendering a geometry with vertex diffuse lighting", 50, 25,
		1280, 800, Sample)

