/**
 * assimp.cpp
 *
 *  Created on: Feb 20, 2021
 *      Author: beopenbefree
 */

#include <string>
#include "SceneCreator.h"

std::string filename("data/bugatti.obj");

int main(int argc, char **argv) {
	oge::SceneCreator creator(filename);
	oge::SceneObject *object = creator.getObject();
	return EXIT_SUCCESS;
}

