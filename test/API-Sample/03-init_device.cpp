/**
 * 03-init_device.cpp
 *
 *  Created on: Mar 29, 2021
 *      Author: beopenbefree
 */

#include "samples.h"

#define APP_SHORT_NAME "sample_03_init_device"

std::string init_device(Info &info) {
	/// Checking available queue families and their properties
	if (!oge::checkAvailableQueueFamiliesAndTheirProperties(info.gpus[0],
			info.queue_props)) {
		return APP_SHORT_NAME"::""Checking available queue families and their properties";
	}

	/// Selecting the index of a queue family with the desired capabilities
	if (!oge::selectIndexOfQueueFamilyWithDesiredCapabilities(info.gpus[0],
			VK_QUEUE_GRAPHICS_BIT, info.graphics_queue_family_index)) {
		return APP_SHORT_NAME"::""Selecting the index of a queue family with the desired capabilities";
	}

	/// Creating a logical device
	info.requested_queues = { { info.graphics_queue_family_index, { 1.0 } } };
	if (!oge::createLogicalDevice(info.gpus[0], info.requested_queues,
			info.device_extension_names, nullptr, info.device)) {
		return APP_SHORT_NAME"::""Creating a logical device";
	}

	/// Loading device-level functions
	if (!oge::loadDeviceLevelFunctions(info.device,
			info.device_extension_names)) {
		return APP_SHORT_NAME"::""Loading device-level functions";
	}
	return "";
}

std::string sample_03_init_device(int argc, char *argv[], GLFWwindow *window) {
	std::string result { "" };
	struct Info info { };
	info.appName = APP_SHORT_NAME;

	result = init_instance(info);
	if (!result.empty()) {
		return result;
	}
	result = enumerate_devices(info);
	if (!result.empty()) {
		return result;
	}

	/* VULKAN_KEY_START */

	result = init_device(info);
	if (!result.empty()) {
		return result;
	}

	/// Checking the window close flag
	while (!glfwWindowShouldClose(window)) {
		/// Processing events
		glfwPollEvents();
	}

	/// Destroying a logical device
	oge::destroyLogicalDevice(info.device);

	/* VULKAN_KEY_END */

	oge::destroyVulkanInstance(info.instance);

	return "";
}

