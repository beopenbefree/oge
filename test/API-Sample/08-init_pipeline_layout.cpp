/**
 * 08-init_pipeline_layout.cpp
 *
 *  Created on: Apr 5, 2021
 *      Author: beopenbefree
 */

#include "samples.h"

#define APP_SHORT_NAME "sample_08_init_pipeline_layout"

std::string init_pipeline_layout(Info &info) {

	std::string result = init_descriptor_and_pipeline_layouts(info, false, 0);
	if (!result.empty()) {
		return result;
	}

	return "";
}

std::string init_descriptor_and_pipeline_layouts(Info &info, bool use_texture,
		VkDescriptorSetLayoutCreateFlags descSetLayoutCreateFlags) {

	/// set descriptor set layout binding(s)
	std::vector<VkDescriptorSetLayoutBinding> bindings { { 0,
			VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1, VK_SHADER_STAGE_VERTEX_BIT,
			nullptr } };
	if (use_texture) {
		bindings.push_back( { 1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1,
				VK_SHADER_STAGE_FRAGMENT_BIT, nullptr });
	}

	/// Creating a descriptor set layout
	info.desc_layout.resize(NUM_DESCRIPTOR_SETS);
	if (!oge::createDescriptorSetLayout(info.device, bindings,
			*info.desc_layout.data(), descSetLayoutCreateFlags)) {
		return APP_SHORT_NAME"::""Creating a descriptor set layout";
	}

	/// Creating a pipeline layout
	if (!oge::createPipelineLayout(info.device, info.desc_layout, { },
			info.pipeline_layout)) {
		return APP_SHORT_NAME"::""Creating a descriptor set layout";
	}

	return "";
}

std::string init_pipeline_cache(Info &info) {

	/// Creating a pipeline layout
	if (!oge::createPipelineCacheObject(info.device, { }, info.pipelineCache)) {
		return APP_SHORT_NAME"::""Creating a descriptor set layout";
	}

	return "";
}

std::string sample_08_init_pipeline_layout(int argc, char *argv[],
		GLFWwindow *window) {
	std::string result { "" };
	struct Info info { };
	info.appName = APP_SHORT_NAME;

	result = init_instance_wsi(info);
	if (!result.empty()) {
		return result;
	}
	result = enumerate_devices(info);
	if (!result.empty()) {
		return result;
	}
	result = init_device_wsi(info, window);
	if (!result.empty()) {
		return result;
	}

	/* VULKAN_KEY_START */

	result = init_pipeline_layout(info);
	if (!result.empty()) {
		return result;
	}

	/// Checking the window close flag
	while (!glfwWindowShouldClose(window)) {
		/// Processing events
		glfwPollEvents();
	}

	/// Destroying a descriptor set layout
	oge::destroyDescriptorSetLayout(info.device, *info.desc_layout.data());

	/// Destroying a pipeline layout
	oge::destroyPipelineLayout(info.device, info.pipeline_layout);

	/* VULKAN_KEY_END */

	oge::destroyPresentationSurface(info.instance, info.surface);
	oge::destroyLogicalDevice(info.device);
	oge::destroyVulkanInstance(info.instance);

	return result;
}

