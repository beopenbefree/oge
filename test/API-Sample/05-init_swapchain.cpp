/**
 * 05-init_swapchain.cpp
 *
 *  Created on: Apr 1, 2021
 *      Author: beopenbefree
 */

#include "samples.h"

#define APP_SHORT_NAME "sample_05_init_swapchain"

#define PREFERRED_SURFACE_FORMAT VK_FORMAT_B8G8R8A8_UNORM
#define PREFERRED_COLOR_SPACE VK_COLOR_SPACE_SRGB_NONLINEAR_KHR

std::string init_swapchain(Info &info, GLFWwindow *window) {

	std::string result = init_device_wsi(info, window);
	if (!result.empty()) {
		return result;
	}

	/// Getting the capabilities of a presentation surface
	VkSurfaceCapabilitiesKHR surfCapabilities;
	if (!oge::getCapabilitiesOfPresentationSurface(info.gpus[0], info.surface,
			surfCapabilities)) {
		return APP_SHORT_NAME"::""Getting the capabilities of a presentation surface";
	}

	/// Selecting a desired presentation mode
	VkPresentModeKHR swapchainPresentMode_sc;
	if (!oge::selectDesiredPresentationMode(info.gpus[0], info.surface,
			VK_PRESENT_MODE_FIFO_KHR, swapchainPresentMode_sc)) {
		return APP_SHORT_NAME"::""Selecting a desired presentation mode";
	}

	/// Choosing a size of swapchain images
	VkExtent2D swapchainExtent_sc;
	if (!oge::chooseSizeOfSwapchainImages(surfCapabilities,
			swapchainExtent_sc)) {
		return APP_SHORT_NAME"::""Choosing a size of swapchain images";
	}
	if ((0 == swapchainExtent_sc.width) || (0 == swapchainExtent_sc.height)) {
		return "";
	}

	/// Selecting desired usage scenarios of swapchain images
	VkImageUsageFlags imageUsage_sc;
	if (!oge::selectDesiredUsageScenariosOfSwapchainImages(surfCapabilities,
			VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT, imageUsage_sc)) {
		return APP_SHORT_NAME"::""Selecting desired usage scenarios of swapchain images";
	}

	/// Selecting a number of swapchain images
	uint32_t desiredNumberOfSwapChainImages_sc;
	if (!oge::selectNumberOfSwapchainImages(surfCapabilities,
			desiredNumberOfSwapChainImages_sc)) {
		return APP_SHORT_NAME"::""Selecting a number of swapchain images";
	}

	/// Selecting a transformation of swapchain images
	VkSurfaceTransformFlagBitsKHR preTransform_sc;
	oge::selectTransformationOfSwapchainImages(surfCapabilities,
			VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR, preTransform_sc);

	/// Different Queue Families for Graphics and Present
	VkSharingMode imageSharingMode_sc;
	std::vector<uint32_t> queueFamilyIndices_sc;
	if (info.graphics_queue_family_index == info.present_queue_family_index) {
		// The graphics and present queues are the same
		imageSharingMode_sc = VK_SHARING_MODE_EXCLUSIVE;
		queueFamilyIndices_sc = { };
	} else {
		// If the graphics and present queues are from different queue families,
		// we either have to explicitly transfer ownership of images between
		// the queues, or we have to create the swapchain with imageSharingMode
		// as VK_SHARING_MODE_CONCURRENT
		imageSharingMode_sc = VK_SHARING_MODE_CONCURRENT;
		queueFamilyIndices_sc = { info.requested_queues[0].FamilyIndex,
				info.requested_queues[1].FamilyIndex };
	}

	/// Find a supported composite alpha mode - one of these is guaranteed to be set
	VkCompositeAlphaFlagBitsKHR compositeAlpha_sc =
			VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	VkCompositeAlphaFlagBitsKHR compositeAlphaFlags[4] = {
			VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
			VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR,
			VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR,
			VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR, };
	for (uint32_t i = 0;
			i < sizeof(compositeAlphaFlags) / sizeof(compositeAlphaFlags[0]);
			i++) {
		if (surfCapabilities.supportedCompositeAlpha & compositeAlphaFlags[i]) {
			compositeAlpha_sc = compositeAlphaFlags[i];
			break;
		}
	}

	/// Creating a swapchain
	VkSwapchainKHR oldSwapchain_sc = VK_NULL_HANDLE;
	if (!oge::createSwapchain(info.device, info.surface,
			desiredNumberOfSwapChainImages_sc,
			{ info.format, info.image_color_space }, swapchainExtent_sc,
			imageUsage_sc, preTransform_sc, swapchainPresentMode_sc,
			oldSwapchain_sc, info.swap_chain, imageSharingMode_sc,
			queueFamilyIndices_sc, compositeAlpha_sc, VK_TRUE)) {
		return APP_SHORT_NAME"::""Creating a swapchain";
	}

	/// Getting handles of swapchain images
	std::vector<VkImage> swapchain_images;
	if (!oge::getHandlesOfSwapchainImages(info.device, info.swap_chain,
			swapchain_images)) {
		return APP_SHORT_NAME"::""Getting handles of swapchain images";
	}

	for (const auto &image : swapchain_images) {
		/// Creating an image view
		VkImageView image_view;
		if (!oge::createImageView(info.device, image, VK_IMAGE_VIEW_TYPE_2D,
				info.format, VK_IMAGE_ASPECT_COLOR_BIT, image_view, {
						VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G,
						VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A })) {
			return APP_SHORT_NAME"::""Creating an image view";
		}
		info.buffers.push_back( { image, image_view });
	}

	info.current_buffer = 0;

	return "";
}

std::string init_device_wsi(Info &info, GLFWwindow *window) {

	std::string result = init_swapchain_extension(info, window);
	if (!result.empty()) {
		return result;
	}

	/// Creating a logical device with WSI extensions enabled
	if (!oge::createLogicalDeviceWithWsiExtensionsEnabled(info.gpus[0],
			info.requested_queues, info.device_extension_names, nullptr,
			info.device)) {
		return APP_SHORT_NAME"::""Creating a logical device with WSI extensions enabled";
	}

	/// Loading device-level functions
	if (!oge::loadDeviceLevelFunctions(info.device,
			info.device_extension_names)) {
		return APP_SHORT_NAME"::""Loading device-level functions";
	}

	return "";
}

std::string init_swapchain_extension(Info &info, GLFWwindow *window) {

	/// Creating a Vulkan window surface
	if (!oge::createPresentationSurface(info.instance, window, nullptr,
			&info.surface)) {
		return APP_SHORT_NAME"::""Creating a Vulkan window surface";
	}

	/// graphics queue must be supported
	if (!oge::selectIndexOfQueueFamilyWithDesiredCapabilities(info.gpus[0],
			VK_QUEUE_GRAPHICS_BIT, info.graphics_queue_family_index)) {
		return APP_SHORT_NAME"::""graphics queue must be supported";
	}

	/// presentation to surface queue must be supported
	if (!oge::selectQueueFamilyThatSupportsPresentationToGivenSurface(
			info.gpus[0], info.surface, info.present_queue_family_index)) {
		return APP_SHORT_NAME"::""presentation to surface queue must be supported";
	}

	/// create a list of queues (possibly overlapping)
	info.requested_queues = { { info.graphics_queue_family_index, { 1.0f } } };
	if (info.present_queue_family_index != info.graphics_queue_family_index) {
		info.requested_queues.push_back( { info.present_queue_family_index, {
				1.0f } });
	}

	/// Selecting a format of swapchain images
	if (!oge::selectFormatOfSwapchainImages(info.gpus[0], info.surface, {
	PREFERRED_SURFACE_FORMAT, PREFERRED_COLOR_SPACE }, info.format,
			info.image_color_space)) {
		return APP_SHORT_NAME"::""Selecting a format of swapchain images";
	}

	return "";
}

void init_device_queue(Info &info) {
	/// Getting a device queue
	oge::getDeviceQueue(info.device, info.graphics_queue_family_index, 0,
			info.graphics_queue);

	if (info.graphics_queue_family_index == info.present_queue_family_index) {
		info.present_queue = info.graphics_queue;
	} else {
		/// Getting a device queue
		oge::getDeviceQueue(info.device, info.present_queue_family_index, 0,
				info.present_queue);
	}
}

std::string sample_05_init_swapchain(int argc, char *argv[],
		GLFWwindow *window) {
	std::string result { "" };
	struct Info info { };
	info.appName = APP_SHORT_NAME;

	result = init_instance_wsi(info);
	if (!result.empty()) {
		return result;
	}
	result = enumerate_devices(info);
	if (!result.empty()) {
		return result;
	}

	/* VULKAN_KEY_START */

	result = init_swapchain(info, window);
	if (!result.empty()) {
		return result;
	}

	/// Checking the window close flag
	while (!glfwWindowShouldClose(window)) {
		/// Processing events
		glfwPollEvents();
	}

	for (auto &buffer : info.buffers) {
		/// Destroying an image view
		oge::destroyImageView(info.device, buffer.view);
	}

	/// Destroying a swapchain
	oge::destroySwapchain(info.device, info.swap_chain);

	/// Destroying a presentation surface
	oge::destroyPresentationSurface(info.instance, info.surface);

	/* VULKAN_KEY_END */

	oge::destroyLogicalDevice(info.device);
	oge::destroyVulkanInstance(info.instance);

	return "";
}

