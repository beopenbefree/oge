/**
 * draw_cube_oge.h
 *
 *  Created on: Jun 8, 2021
 *      Author: beopenbefree
 */

#ifndef TEST_API_SAMPLE_DRAW_CUBE_OGE_H_
#define TEST_API_SAMPLE_DRAW_CUBE_OGE_H_

/// Including the Vulkan and GLFW header files
#define GLFW_INCLUDE_NONE
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <iostream>
#include "lib/instanceAndDevices.h"
#include "lib/imagePresentation.h"
#include "lib/commandBuffersAndSynchronization.h"
#include "lib/resourcesAndMemory.h"
#include "framework/helpers.h"

/// DATA
class APP_DATA {
public:
	/// Application common
	GLFWwindow *WindowParams = nullptr;
	bool Ready = false;
	bool Initialized = false;
	bool Created = false;
	oge::VkDestroyer(oge::VkInstance) Instance;
	VkPhysicalDevice PhysicalDevice;
	oge::VkDestroyer(oge::VkDevice) LogicalDevice;
	oge::VkDestroyer(oge::VkSurfaceKHR) PresentationSurface;
	oge::QueueParameters GraphicsQueue;
	oge::QueueParameters ComputeQueue;
	oge::QueueParameters PresentQueue;
	oge::SwapchainParameters Swapchain;
	oge::VkDestroyer(oge::VkCommandPool) CommandPool;
	std::vector<oge::VkDestroyer(oge::VkImage)> DepthImages;
	std::vector<oge::VkDestroyer(oge::VkDeviceMemory)> DepthImagesMemory;
	std::vector<oge::FrameResources> FramesResources;
	static uint32_t const FramesCount = 3;
	static VkFormat const DepthFormat = VK_FORMAT_D16_UNORM;
	/// Application specific
	oge::Mesh Model; //fixme
	oge::VkDestroyer(oge::VkBuffer) VertexBuffer;
	oge::VkDestroyer(oge::VkDeviceMemory) VertexBufferMemory;

};

/// FUNCTIONS
void initializeWindow(const char *window_title, int x, int y, int width,
		int height);
bool initializeVulkan(GLFWwindow *window_parameters,
		VkPhysicalDeviceFeatures *desired_device_features = nullptr,
		VkImageUsageFlags swapchain_image_usage =
				VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT, bool use_depth = true,
		VkImageUsageFlags depth_attachment_usage =
				VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT);
bool createSwapchain(VkImageUsageFlags swapchain_image_usage =
		VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT, bool use_depth = true,
		VkImageUsageFlags depth_attachment_usage =
				VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT);

#endif /* TEST_API_SAMPLE_DRAW_CUBE_OGE_H_ */
