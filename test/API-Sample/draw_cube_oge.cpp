/**
 * draw_cube_oge.cpp
 *
 *  Created on: Jun 7, 2021
 *      Author: beopenbefree
 */

#include "draw_cube_oge.h"

APP_DATA gData;

int main() {

	initializeWindow("draw cube oge", 50, 25, 1280, 800);

	//	window.Render();xxx

	/// Initialize
	{
		// Application common
		if (!initializeVulkan(gData.WindowParams)) {
			return false;
		}

		// Application specific

		// Vertex data todo
//	if (!Load3DModelFromObjFile(modelPath, true, false, false, true, Model)) { fixme
//		return false;
//	}

		InitVkDestroyer(gData.LogicalDevice, gData.VertexBuffer);
		if (!oge::createBuffer(*gData.LogicalDevice,
				sizeof(gData.Model.Data[0]) * gData.Model.Data.size(),
				VK_BUFFER_USAGE_TRANSFER_DST_BIT
						| VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
				*gData.VertexBuffer)) {
			return false;
		}

		// todo
//	InitVkDestroyer(LogicalDevice, VertexBufferMemory);
//	if (!allocateAndBindMemoryObjectToBuffer(PhysicalDevice, *LogicalDevice,
//			*VertexBuffer, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
//			*VertexBufferMemory)) {
//		return false;
//	}
//
//	if (!useStagingBufferToUpdateBufferWithDeviceLocalMemoryBound(
//			PhysicalDevice, *LogicalDevice,
//			sizeof(Model.Data[0]) * Model.Data.size(), &Model.Data[0],
//			*VertexBuffer, 0, 0, VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT,
//			VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
//			VK_PIPELINE_STAGE_VERTEX_INPUT_BIT, GraphicsQueue.Handle,
//			FramesResources.front().CommandBuffer, { })) {
//		return false;
//	}
//
//	// Staging buffer
//	InitVkDestroyer(LogicalDevice, StagingBuffer);
//	if (!createBuffer(*LogicalDevice, 2 * 16 * sizeof(float),
//			VK_BUFFER_USAGE_TRANSFER_SRC_BIT, *StagingBuffer)) {
//		return false;
//	}
//	InitVkDestroyer(LogicalDevice, StagingBufferMemory);
//	if (!allocateAndBindMemoryObjectToBuffer(PhysicalDevice, *LogicalDevice,
//			*StagingBuffer, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
//			*StagingBufferMemory)) {
//		return false;
//	}
//
//	// Uniform buffer
//	InitVkDestroyer(LogicalDevice, UniformBuffer);
//	InitVkDestroyer(LogicalDevice, UniformBufferMemory);
//	if (!createUniformBuffer(PhysicalDevice, *LogicalDevice,
//			2 * 16 * sizeof(float),
//			VK_BUFFER_USAGE_TRANSFER_DST_BIT
//					| VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, *UniformBuffer,
//			*UniformBufferMemory)) {
//		return false;
//	}
//
//	if (!UpdateStagingBuffer(true)) {
//		return false;
//	}
//
//	// Descriptor set with uniform buffer
//	VkDescriptorSetLayoutBinding descriptor_set_layout_binding = { 0, // uint32_t             binding
//			VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, // VkDescriptorType     descriptorType
//			1,                       // uint32_t             descriptorCount
//			VK_SHADER_STAGE_VERTEX_BIT,   // VkShaderStageFlags   stageFlags
//			nullptr              // const VkSampler    * pImmutableSamplers
//			};
//	InitVkDestroyer(LogicalDevice, DescriptorSetLayout);
//	if (!createDescriptorSetLayout(*LogicalDevice, {
//			descriptor_set_layout_binding }, *DescriptorSetLayout)) {
//		return false;
//	}
//
//	VkDescriptorPoolSize descriptor_pool_size = {
//			VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,  // VkDescriptorType     type
//			1                       // uint32_t             descriptorCount
//			};
//	InitVkDestroyer(LogicalDevice, DescriptorPool);
//	if (!createDescriptorPool(*LogicalDevice, false, 1, {
//			descriptor_pool_size }, *DescriptorPool)) {
//		return false;
//	}
//
//	if (!allocateDescriptorSets(*LogicalDevice, *DescriptorPool, {
//			*DescriptorSetLayout }, DescriptorSets)) {
//		return false;
//	}
//
//	BufferDescriptorInfo buffer_descriptor_update = { DescriptorSets[0], // VkDescriptorSet                      TargetDescriptorSet
//			0, // uint32_t                             TargetDescriptorBinding
//			0,    // uint32_t                             TargetArrayElement
//			VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, // VkDescriptorType                     TargetDescriptorType
//			{            // std::vector<VkDescriptorBufferInfo>  BufferInfos
//			{ *UniformBuffer, // VkBuffer                             buffer
//					0,        // VkDeviceSize                         offset
//					VK_WHOLE_SIZE // VkDeviceSize                         range
//					} } };
//
//	updateDescriptorSets(*LogicalDevice, { }, { buffer_descriptor_update },
//			{ }, { });
//
//	// Render pass
//	std::vector<VkAttachmentDescription> attachment_descriptions = { { 0, // VkAttachmentDescriptionFlags     flags
//			Swapchain.Format,     // VkFormat                         format
//			VK_SAMPLE_COUNT_1_BIT, // VkSampleCountFlagBits            samples
//			VK_ATTACHMENT_LOAD_OP_CLEAR, // VkAttachmentLoadOp               loadOp
//			VK_ATTACHMENT_STORE_OP_STORE, // VkAttachmentStoreOp              storeOp
//			VK_ATTACHMENT_LOAD_OP_DONT_CARE, // VkAttachmentLoadOp               stencilLoadOp
//			VK_ATTACHMENT_STORE_OP_DONT_CARE, // VkAttachmentStoreOp              stencilStoreOp
//			VK_IMAGE_LAYOUT_UNDEFINED, // VkImageLayout                    initialLayout
//			VK_IMAGE_LAYOUT_PRESENT_SRC_KHR // VkImageLayout                    finalLayout
//			}, { 0,                // VkAttachmentDescriptionFlags     flags
//					DepthFormat,  // VkFormat                         format
//					VK_SAMPLE_COUNT_1_BIT, // VkSampleCountFlagBits            samples
//					VK_ATTACHMENT_LOAD_OP_CLEAR, // VkAttachmentLoadOp               loadOp
//					VK_ATTACHMENT_STORE_OP_DONT_CARE, // VkAttachmentStoreOp              storeOp
//					VK_ATTACHMENT_LOAD_OP_DONT_CARE, // VkAttachmentLoadOp               stencilLoadOp
//					VK_ATTACHMENT_STORE_OP_DONT_CARE, // VkAttachmentStoreOp              stencilStoreOp
//					VK_IMAGE_LAYOUT_UNDEFINED, // VkImageLayout                    initialLayout
//					VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL // VkImageLayout                    finalLayout
//			} };
//
//	VkAttachmentReference depth_attachment = { 1, // uint32_t                             attachment
//			VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL // VkImageLayout                        layout
//			};
//
//	std::vector<SubpassParameters> subpass_parameters = { {
//			VK_PIPELINE_BIND_POINT_GRAPHICS, // VkPipelineBindPoint                  PipelineType
//			{ },    // std::vector<VkAttachmentReference>   InputAttachments
//			{       // std::vector<VkAttachmentReference>   ColorAttachments
//			{ 0,          // uint32_t                             attachment
//					VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, // VkImageLayout                        layout
//					} }, { }, // std::vector<VkAttachmentReference>   ResolveAttachments
//			&depth_attachment, // VkAttachmentReference const        * DepthStencilAttachment
//			{ }  // std::vector<uint32_t>                PreserveAttachments
//	} };
//
//	std::vector<VkSubpassDependency> subpass_dependencies = { {
//	VK_SUBPASS_EXTERNAL,    // uint32_t                   srcSubpass
//			0,                      // uint32_t                   dstSubpass
//			VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, // VkPipelineStageFlags       srcStageMask
//			VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, // VkPipelineStageFlags       dstStageMask
//			VK_ACCESS_MEMORY_READ_BIT, // VkAccessFlags              srcAccessMask
//			VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, // VkAccessFlags              dstAccessMask
//			VK_DEPENDENCY_BY_REGION_BIT // VkDependencyFlags          dependencyFlags
//			}, { 0,                 // uint32_t                   srcSubpass
//					VK_SUBPASS_EXTERNAL, // uint32_t                   dstSubpass
//					VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, // VkPipelineStageFlags       srcStageMask
//					VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, // VkPipelineStageFlags       dstStageMask
//					VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, // VkAccessFlags              srcAccessMask
//					VK_ACCESS_MEMORY_READ_BIT, // VkAccessFlags              dstAccessMask
//					VK_DEPENDENCY_BY_REGION_BIT // VkDependencyFlags          dependencyFlags
//			} };
//
//	InitVkDestroyer(LogicalDevice, RenderPass);
//	if (!createRenderPass(*LogicalDevice, attachment_descriptions,
//			subpass_parameters, subpass_dependencies, *RenderPass)) {
//		return false;
//	}
//
//	// Graphics pipeline
//
//	InitVkDestroyer(LogicalDevice, PipelineLayout);
//	if (!createPipelineLayout(*LogicalDevice, { *DescriptorSetLayout }, { },
//			*PipelineLayout)) {
//		return false;
//	}
//
//	std::vector<unsigned char> vertex_shader_spirv;
//	if (!getBinaryFileContents(vertexShaderPath, vertex_shader_spirv)) {
//		return false;
//	}
//
//	VkDestroyer (VkShaderModule) vertex_shader_module;
//	InitVkDestroyer(LogicalDevice, vertex_shader_module);
//	if (!createShaderModule(*LogicalDevice, vertex_shader_spirv,
//			*vertex_shader_module)) {
//		return false;
//	}
//
//	std::vector<unsigned char> fragment_shader_spirv;
//	if (!getBinaryFileContents(fragmentShaderPath, fragment_shader_spirv)) {
//		return false;
//	}
//	VkDestroyer (VkShaderModule) fragment_shader_module;
//	InitVkDestroyer(LogicalDevice, fragment_shader_module);
//	if (!createShaderModule(*LogicalDevice, fragment_shader_spirv,
//			*fragment_shader_module)) {
//		return false;
//	}
//
//	std::vector<ShaderStageParameters> shader_stage_params = { {
//			VK_SHADER_STAGE_VERTEX_BIT, // VkShaderStageFlagBits        ShaderStage
//			*vertex_shader_module, // VkShaderModule               ShaderModule
//			"main",           // char const                 * EntryPointName
//			nullptr      // VkSpecializationInfo const * SpecializationInfo
//			}, { VK_SHADER_STAGE_FRAGMENT_BIT, // VkShaderStageFlagBits        ShaderStage
//					*fragment_shader_module, // VkShaderModule               ShaderModule
//					"main",   // char const                 * EntryPointName
//					nullptr // VkSpecializationInfo const * SpecializationInfo
//			} };
//
//	std::vector<VkPipelineShaderStageCreateInfo> shader_stage_create_infos;
//	specifyPipelineShaderStages(shader_stage_params,
//			shader_stage_create_infos);
//
//	std::vector<VkVertexInputBindingDescription> vertex_input_binding_descriptions =
//			{ { 0,                   // uint32_t                     binding
//					6 * sizeof(float), // uint32_t                     stride
//					VK_VERTEX_INPUT_RATE_VERTEX // VkVertexInputRate            inputRate
//					} };
//
//	std::vector<VkVertexInputAttributeDescription> vertex_attribute_descriptions =
//			{ { 0,                                    // uint32_t   location
//					0,                                 // uint32_t   binding
//					VK_FORMAT_R32G32B32_SFLOAT,         // VkFormat   format
//					0                                  // uint32_t   offset
//					}, { 1,                           // uint32_t   location
//							0,                         // uint32_t   binding
//							VK_FORMAT_R32G32B32_SFLOAT, // VkFormat   format
//							3 * sizeof(float)           // uint32_t   offset
//					} };
//
//	VkPipelineVertexInputStateCreateInfo vertex_input_state_create_info;
//	specifyPipelineVertexInputState(vertex_input_binding_descriptions,
//			vertex_attribute_descriptions, vertex_input_state_create_info);
//
//	VkPipelineInputAssemblyStateCreateInfo input_assembly_state_create_info;
//	specifyPipelineInputAssemblyState(VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
//			false, input_assembly_state_create_info);
//
//	ViewportInfo viewport_infos = { { // std::vector<VkViewport>   Viewports
//			{ 0.0f,               // float          x
//					0.0f,               // float          y
//					500.0f,             // float          width
//					500.0f,             // float          height
//					0.0f,               // float          minDepth
//					1.0f                // float          maxDepth
//					} }, {             // std::vector<VkRect2D>     Scissors
//			{ {                   // VkOffset2D     offset
//					0,                  // int32_t        x
//							0                   // int32_t        y
//					}, {                   // VkExtent2D     extent
//					500,                // uint32_t       width
//							500                 // uint32_t       height
//					} } } };
//	VkPipelineViewportStateCreateInfo viewport_state_create_info;
//	specifyPipelineViewportAndScissorTestState(viewport_infos,
//			viewport_state_create_info);
//
//	VkPipelineRasterizationStateCreateInfo rasterization_state_create_info;
//	specifyPipelineRasterizationState(false, false, VK_POLYGON_MODE_FILL,
//			VK_CULL_MODE_BACK_BIT, VK_FRONT_FACE_COUNTER_CLOCKWISE, false,
//			0.0f, 0.0f, 0.0f, 1.0f, rasterization_state_create_info);
//
//	VkPipelineMultisampleStateCreateInfo multisample_state_create_info;
//	specifyPipelineMultisampleState(VK_SAMPLE_COUNT_1_BIT, false, 0.0f,
//			nullptr, false, false, multisample_state_create_info);
//
//	VkPipelineDepthStencilStateCreateInfo depth_stencil_state_create_info;
//	specifyPipelineDepthAndStencilState(true, true,
//			VK_COMPARE_OP_LESS_OR_EQUAL, false, 0.0f, 1.0f, false, { }, { },
//			depth_stencil_state_create_info);
//
//	std::vector<VkPipelineColorBlendAttachmentState> attachment_blend_states =
//			{ { false,               // VkBool32                 blendEnable
//					VK_BLEND_FACTOR_ONE, // VkBlendFactor            srcColorBlendFactor
//					VK_BLEND_FACTOR_ONE, // VkBlendFactor            dstColorBlendFactor
//					VK_BLEND_OP_ADD, // VkBlendOp                colorBlendOp
//					VK_BLEND_FACTOR_ONE, // VkBlendFactor            srcAlphaBlendFactor
//					VK_BLEND_FACTOR_ONE, // VkBlendFactor            dstAlphaBlendFactor
//					VK_BLEND_OP_ADD, // VkBlendOp                alphaBlendOp
//					VK_COLOR_COMPONENT_R_BIT
//							|    // VkColorComponentFlags    colorWriteMask
//							VK_COLOR_COMPONENT_G_BIT
//							| VK_COLOR_COMPONENT_B_BIT
//							| VK_COLOR_COMPONENT_A_BIT } };
//	VkPipelineColorBlendStateCreateInfo blend_state_create_info;
//	specifyPipelineBlendState(false, VK_LOGIC_OP_COPY,
//			attachment_blend_states, { 1.0f, 1.0f, 1.0f, 1.0f },
//			blend_state_create_info);
//
//	std::vector<VkDynamicState> dynamic_states = {
//			VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR };
//	VkPipelineDynamicStateCreateInfo dynamic_state_create_info;
//	specifyPipelineDynamicStates(dynamic_states, dynamic_state_create_info);
//
//	VkGraphicsPipelineCreateInfo pipeline_create_info;
//	specifyGraphicsPipelineCreationParameters(0, shader_stage_create_infos,
//			vertex_input_state_create_info,
//			input_assembly_state_create_info, nullptr,
//			&viewport_state_create_info, rasterization_state_create_info,
//			&multisample_state_create_info,
//			&depth_stencil_state_create_info, &blend_state_create_info,
//			&dynamic_state_create_info, *PipelineLayout, *RenderPass, 0,
//			VK_NULL_HANDLE, -1, pipeline_create_info);
//
//	std::vector<VkPipeline> pipeline;
//	if (!createGraphicsPipelines(*LogicalDevice, { pipeline_create_info },
//	VK_NULL_HANDLE, pipeline)) {
//		return false;
//	}
//	InitVkDestroyer(LogicalDevice, Pipeline);
//	*Pipeline = pipeline[0];
	}

	/// Deinitialize
	{
		if (gData.LogicalDevice) {
			oge::waitForAllSubmittedCommandsToBeFinished(*gData.LogicalDevice);
		}
	}
	return 0;
}

void error_callback(int error, const char *description) {
	std::cerr << "Error: " << description << std::endl;
}

void initializeWindow(const char *window_title, int x, int y, int width,
		int height) {
	/// Setting an error callback
	glfwSetErrorCallback(error_callback);

	/// Initializing and terminating GLFW
	if (!glfwInit()) {
		return;
	}
	gData.Initialized = true;

	/// Creating the window
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API); // mandatory

	// windowed
	gData.WindowParams = glfwCreateWindow(width, height, window_title, nullptr,
			nullptr);

	if (!gData.WindowParams) {
		return;
	}
	gData.Created = true;

}

bool initializeVulkan(GLFWwindow *window_parameters,
		VkPhysicalDeviceFeatures *desired_device_features,
		VkImageUsageFlags swapchain_image_usage, bool use_depth,
		VkImageUsageFlags depth_attachment_usage) {

	if (!oge::connectWithVulkanLoaderLibrary()) {
		return false;
	}

	if (!oge::loadFunctionExportedFromVulkanLoaderLibrary()) {
		return false;
	}

	if (!oge::loadGlobalLevelFunctions()) {
		return false;
	}

	std::vector<char const*> instance_extensions;
	InitVkDestroyer(gData.Instance);
	if (!oge::createVulkanInstanceWithWsiExtensionsEnabled(instance_extensions,
			"Vulkan Cookbook", *gData.Instance)) {
		return false;
	}

	if (!oge::loadInstanceLevelFunctions(*gData.Instance,
			instance_extensions)) {
		return false;
	}

	InitVkDestroyer(gData.Instance, gData.PresentationSurface);
	if (!oge::createPresentationSurface(*gData.Instance, window_parameters,
			nullptr, &(*gData.PresentationSurface))) {
		return false;
	}

	std::vector<VkPhysicalDevice> physical_devices;
	oge::enumerateAvailablePhysicalDevices(*gData.Instance, physical_devices);

	for (auto &physical_device : physical_devices) {
		if (!oge::selectIndexOfQueueFamilyWithDesiredCapabilities(
				physical_device, VK_QUEUE_GRAPHICS_BIT,
				gData.GraphicsQueue.FamilyIndex)) {
			continue;
		}

		if (!oge::selectIndexOfQueueFamilyWithDesiredCapabilities(
				physical_device, VK_QUEUE_COMPUTE_BIT,
				gData.ComputeQueue.FamilyIndex)) {
			continue;
		}

		if (!oge::selectQueueFamilyThatSupportsPresentationToGivenSurface(
				physical_device, *gData.PresentationSurface,
				gData.PresentQueue.FamilyIndex)) {
			continue;
		}

		std::vector<oge::QueueInfo> requested_queues = { {
				gData.GraphicsQueue.FamilyIndex, { 1.0f } } };
		if (gData.GraphicsQueue.FamilyIndex != gData.ComputeQueue.FamilyIndex) {
			requested_queues.push_back( { gData.ComputeQueue.FamilyIndex,
					{ 1.0f } });
		}
		if ((gData.GraphicsQueue.FamilyIndex != gData.PresentQueue.FamilyIndex)
				&& (gData.ComputeQueue.FamilyIndex
						!= gData.PresentQueue.FamilyIndex)) {
			requested_queues.push_back( { gData.PresentQueue.FamilyIndex,
					{ 1.0f } });
		}
		std::vector<char const*> device_extensions;
		InitVkDestroyer(gData.LogicalDevice);
		if (!createLogicalDeviceWithWsiExtensionsEnabled(physical_device,
				requested_queues, device_extensions, desired_device_features,
				*gData.LogicalDevice)) {
			continue;
		} else {
			gData.PhysicalDevice = physical_device;
			oge::loadDeviceLevelFunctions(*gData.LogicalDevice,
					device_extensions);
			oge::getDeviceQueue(*gData.LogicalDevice,
					gData.GraphicsQueue.FamilyIndex, 0,
					gData.GraphicsQueue.Handle);
			oge::getDeviceQueue(*gData.LogicalDevice,
					gData.ComputeQueue.FamilyIndex, 0,
					gData.ComputeQueue.Handle);
			oge::getDeviceQueue(*gData.LogicalDevice,
					gData.PresentQueue.FamilyIndex, 0,
					gData.PresentQueue.Handle);
			break;
		}
	}

	if (!gData.LogicalDevice) {
		return false;
	}

	// Prepare frame resources

	oge::InitVkDestroyer(gData.LogicalDevice, gData.CommandPool);
	if (!oge::createCommandPool(*gData.LogicalDevice,
			VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
			gData.GraphicsQueue.FamilyIndex, *gData.CommandPool)) {
		return false;
	}

	for (uint32_t i = 0; i < gData.FramesCount; ++i) {
		std::vector<VkCommandBuffer> command_buffer;
		oge::VkDestroyer (oge::VkSemaphore) image_acquired_semaphore;
		InitVkDestroyer(gData.LogicalDevice, image_acquired_semaphore);
		oge::VkDestroyer (oge::VkSemaphore) ready_to_present_semaphore;
		InitVkDestroyer(gData.LogicalDevice, ready_to_present_semaphore);
		oge::VkDestroyer (oge::VkFence) drawing_finished_fence;
		InitVkDestroyer(gData.LogicalDevice, drawing_finished_fence);
		oge::VkDestroyer (oge::VkImageView) depth_attachment;
		InitVkDestroyer(gData.LogicalDevice, depth_attachment);

		if (!oge::allocateCommandBuffers(*gData.LogicalDevice,
				*gData.CommandPool, VK_COMMAND_BUFFER_LEVEL_PRIMARY, 1,
				command_buffer)) {
			return false;
		}
		if (!oge::createSemaphore(*gData.LogicalDevice,
				*image_acquired_semaphore)) {
			return false;
		}
		if (!oge::createSemaphore(*gData.LogicalDevice,
				*ready_to_present_semaphore)) {
			return false;
		}
		if (!oge::createFence(*gData.LogicalDevice, true,
				*drawing_finished_fence)) {
			return false;
		}

		oge::VkDestroyer (oge::VkFramebuffer) vkFramebuffer;

		gData.FramesResources.emplace_back(command_buffer[0],
				image_acquired_semaphore, ready_to_present_semaphore,
				drawing_finished_fence, depth_attachment, vkFramebuffer);
	}

	if (!createSwapchain(swapchain_image_usage, use_depth,
			depth_attachment_usage)) {
		return false;
	}

	return true;

}

bool createSwapchain(VkImageUsageFlags swapchain_image_usage, bool use_depth,
		VkImageUsageFlags depth_attachment_usage) {
	oge::waitForAllSubmittedCommandsToBeFinished(*gData.LogicalDevice);

	gData.Ready = false;

	gData.Swapchain.ImageViewsRaw.clear();
	gData.Swapchain.ImageViews.clear();
	gData.Swapchain.Images.clear();

	if (!gData.Swapchain.Handle) {
		InitVkDestroyer(gData.LogicalDevice, gData.Swapchain.Handle);
	}
	oge::VkDestroyer (oge::VkSwapchainKHR) old_swapchain = std::move(
			gData.Swapchain.Handle);
	InitVkDestroyer(gData.LogicalDevice, gData.Swapchain.Handle);
	if (!oge::createSwapchainWithR8G8B8A8FormatAndMailboxPresentMode(
			gData.PhysicalDevice, *gData.PresentationSurface,
			*gData.LogicalDevice, swapchain_image_usage, gData.Swapchain.Size,
			gData.Swapchain.Format, *old_swapchain, *gData.Swapchain.Handle,
			gData.Swapchain.Images)) {
		return false;
	}
	if (!gData.Swapchain.Handle) {
		return true;
	}

	for (size_t i = 0; i < gData.Swapchain.Images.size(); ++i) {
		gData.Swapchain.ImageViews.emplace_back(
				oge::VkDestroyer(oge::VkImageView)());
		InitVkDestroyer(gData.LogicalDevice, gData.Swapchain.ImageViews.back());
		if (!oge::createImageView(*gData.LogicalDevice,
				gData.Swapchain.Images[i], VK_IMAGE_VIEW_TYPE_2D,
				gData.Swapchain.Format, VK_IMAGE_ASPECT_COLOR_BIT,
				*gData.Swapchain.ImageViews.back())) {
			return false;
		}
		gData.Swapchain.ImageViewsRaw.push_back(
				*gData.Swapchain.ImageViews.back());
	}

	// When we want to use depth buffering, we need to use a depth attachment
	// It must have the same size as the swapchain, so we need to recreate it along with the swapchain
	gData.DepthImages.clear();
	gData.DepthImagesMemory.clear();

	if (use_depth) {
		for (uint32_t i = 0; i < gData.FramesCount; ++i) {
			gData.DepthImages.emplace_back(oge::VkDestroyer(oge::VkImage)());
			InitVkDestroyer(gData.LogicalDevice, gData.DepthImages.back());
			gData.DepthImagesMemory.emplace_back(
					oge::VkDestroyer(oge::VkDeviceMemory)());
			InitVkDestroyer(gData.LogicalDevice,
					gData.DepthImagesMemory.back());
			InitVkDestroyer(gData.LogicalDevice,
					gData.FramesResources[i].DepthAttachment);

			if (!oge::create2DImageAndView(gData.PhysicalDevice,
					*gData.LogicalDevice, gData.DepthFormat,
					gData.Swapchain.Size, 1, 1, VK_SAMPLE_COUNT_1_BIT,
					VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
					VK_IMAGE_ASPECT_DEPTH_BIT, *gData.DepthImages.back(),
					*gData.DepthImagesMemory.back(),
					*gData.FramesResources[i].DepthAttachment)) {
				return false;
			}
		}
	}

	gData.Ready = true;
	return true;
}

