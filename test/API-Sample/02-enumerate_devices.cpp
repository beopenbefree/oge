/**
 * 02-enumerate_devices.cpp
 *
 *  Created on: Mar 29, 2021
 *      Author: beopenbefree
 */

#include "samples.h"

#define APP_SHORT_NAME "sample_02_enumerate_devices"

std::string enumerate_devices(Info &info) {
	/// Enumerating available physical devices
	if (!oge::enumerateAvailablePhysicalDevices(info.instance, info.gpus)) {
		return APP_SHORT_NAME"::""Enumerating available physical devices";
	}

	return "";
}

std::string sample_02_enumerate_device(int argc, char *argv[],
		GLFWwindow *window) {
	std::string result { "" };
	struct Info info { };
	info.appName = APP_SHORT_NAME;

	result = init_instance(info);
	if (!result.empty()) {
		return result;
	}

	/* VULKAN_KEY_START */

	result = enumerate_devices(info);
	if (!result.empty()) {
		return result;
	}

	/// Checking the window close flag
	while (!glfwWindowShouldClose(window)) {
		/// Processing events
		glfwPollEvents();
	}

	/* VULKAN_KEY_END */

	oge::destroyVulkanInstance(info.instance);

	return result;
}
