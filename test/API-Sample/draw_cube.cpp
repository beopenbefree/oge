/**
 * draw_cube.cpp
 *
 *  Created on: May 22, 2021
 *      Author: beopenbefree
 */

#include "draw_cube.h"

#define APP_SHORT_NAME "draw_cube"

constexpr bool depthPresent { true };

// Commands to be executed inside "data" directory to get SPIRV shaders:
// for vertex spirv "./generate_spirv.py '15-draw_cube.vert' '15-draw_cube.vert'.spv glslangValidator false"
// for fragment spirv "./generate_spirv.py '15-draw_cube.frag' '15-draw_cube.frag'.spv glslangValidator false"

namespace {

#include "data/cube_data.h"

#include "data/15-draw_cube.vert.spv"
#include "data/15-draw_cube.frag.spv"

}  // namespace

int main(int argc, char *argv[]) {
	VkResult U_ASSERT_ONLY res;
	struct sample_info info { };
	char sample_title[] = "Draw Cube";
	info.appName = APP_SHORT_NAME;

	/// process_command_line_args REMARK
	{
		auto optionMatch = [&](const char *option, char *optionLine) {
			if (strncmp(option, optionLine, strlen(option)) == 0)
				return true;
			else
				return false;
		};

		auto process_command_line_args = [&](int argc, char *argv[]) {
			int i, n;

			for (i = 1, n = 1; i < argc; i++) {
				if (optionMatch("--save-images", argv[i]))
					info.save_images = true;
				else if (optionMatch("--help", argv[i])
						|| optionMatch("-h", argv[i])) {
					printf("\nOther options:\n");
					printf(
							"\t--save-images\n"
									"\t\tSave tests images as ppm files in current working "
		"directory.\n");
					exit(0);
				} else {
					printf("\nUnrecognized option: %s\n", argv[i]);
					printf("\nUse --help or -h for option list.\n");
					exit(0);
				}

				/*
				 * Since the above "consume" inputs, update argv
				 * so that it contains the trimmed list of args for glutInit
				 */

				argv[n] = argv[i];
				n++;
			}
		};
		process_command_line_args(argc, argv);
	}

	/// INITIALIZATION REMARK
	{
		/// Initializing GLFW
		if (!glfwInit()) {
			return EXIT_FAILURE;
		}

		/// Connecting with vulkan loader library
		if (!oge::connectWithVulkanLoaderLibrary()) {
			return EXIT_FAILURE;
		}

		/// Load vkGetInstanceProcAddr
		if (!oge::loadFunctionExportedFromVulkanLoaderLibrary()) {
			return EXIT_FAILURE;
		}

		/// Load global level functions (vkCreateInstance, ...)
		if (!oge::loadGlobalLevelFunctions()) {
			return EXIT_FAILURE;
		}
	}

	/// init_global_layer_properties REMARK
	{
		auto init_global_layer_properties = [&]() {
			uint32_t instance_layer_count;
			VkLayerProperties *vk_props = NULL;
			VkResult res;
#ifdef __ANDROID__
	// This place is the first place for samples to use Vulkan APIs.
	// Here, we are going to open Vulkan.so on the device and retrieve function pointers using
	// vulkan_wrapper helper.
	if (!InitVulkan()) {
		LOGE("Failied initializing Vulkan APIs!");
		return VK_ERROR_INITIALIZATION_FAILED;
	}
	LOGI("Loaded Vulkan APIs.");
#endif

			/*
			 * It's possible, though very rare, that the number of
			 * instance layers could change. For example, installing something
			 * could include new layers that the loader would pick up
			 * between the initial query for the count and the
			 * request for VkLayerProperties. The loader indicates that
			 * by returning a VK_INCOMPLETE status and will update the
			 * the count parameter.
			 * The count parameter will be updated with the number of
			 * entries loaded into the data pointer - in case the number
			 * of layers went down or is smaller than the size given.
			 */
			do {
				res = oge::vkEnumerateInstanceLayerProperties(
						&instance_layer_count, NULL);
				if (res)
					return res;

				if (instance_layer_count == 0) {
					return VK_SUCCESS;
				}

				vk_props = (VkLayerProperties*) realloc(vk_props,
						instance_layer_count * sizeof(VkLayerProperties));

				res = oge::vkEnumerateInstanceLayerProperties(
						&instance_layer_count, vk_props);
			} while (res == VK_INCOMPLETE);

			/// init_global_extension_properties REMARK
			auto init_global_extension_properties = [&](
					layer_properties &layer_props) {
				VkExtensionProperties *instance_extensions;
				uint32_t instance_extension_count;
				VkResult res;
				char *layer_name = NULL;

				layer_name = layer_props.properties.layerName;

				do {
					res = oge::vkEnumerateInstanceExtensionProperties(
							layer_name, &instance_extension_count, NULL);
					if (res)
						return res;

					if (instance_extension_count == 0) {
						return VK_SUCCESS;
					}

					layer_props.instance_extensions.resize(
							instance_extension_count);
					instance_extensions =
							layer_props.instance_extensions.data();
					res = oge::vkEnumerateInstanceExtensionProperties(
							layer_name, &instance_extension_count,
							instance_extensions);
				} while (res == VK_INCOMPLETE);

				return res;
			};

			/*
			 * Now gather the extension list for each instance layer.
			 */
			for (uint32_t i = 0; i < instance_layer_count; i++) {
				layer_properties layer_props;
				layer_props.properties = vk_props[i];
				res = init_global_extension_properties(layer_props);
				if (res)
					return res;
				info.instance_layer_properties.push_back(layer_props);
			}
			free(vk_props);

			return res;
		};
		init_global_layer_properties();
	}

	/// init_instance_extension_names REMARK
	{
		auto init_instance_extension_names =
				[&]() {
					info.instance_extension_names.push_back(
					VK_KHR_SURFACE_EXTENSION_NAME);
#ifdef __ANDROID__
	    info.instance_extension_names.push_back(VK_KHR_ANDROID_SURFACE_EXTENSION_NAME);
	#elif defined(_WIN32)
	    info.instance_extension_names.push_back(VK_KHR_WIN32_SURFACE_EXTENSION_NAME);
	#elif defined(VK_USE_PLATFORM_METAL_EXT)
	    info.instance_extension_names.push_back(VK_EXT_METAL_SURFACE_EXTENSION_NAME);
	#elif defined(VK_USE_PLATFORM_WAYLAND_KHR)
	    info.instance_extension_names.push_back(VK_KHR_WAYLAND_SURFACE_EXTENSION_NAME);
	#else
					info.instance_extension_names.push_back(
					VK_KHR_XCB_SURFACE_EXTENSION_NAME);
#endif
				};
		init_instance_extension_names();
	}

	/// init_device_extension_names REMARK
	{
		auto init_device_extension_names = [&]() {
			info.device_extension_names.push_back(
			VK_KHR_SWAPCHAIN_EXTENSION_NAME);
		};
		init_device_extension_names();
	}

	/// init_instance REMARK
	{
		auto init_instance = [&](char const *const app_short_name) {
			VkApplicationInfo app_info = { };
			app_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
			app_info.pNext = NULL;
			app_info.pApplicationName = app_short_name;
			app_info.applicationVersion = 1;
			app_info.pEngineName = app_short_name;
			app_info.engineVersion = 1;
			app_info.apiVersion = VK_API_VERSION_1_0;

			VkInstanceCreateInfo inst_info = { };
			inst_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
			inst_info.pNext = NULL;
			inst_info.flags = 0;
			inst_info.pApplicationInfo = &app_info;
			inst_info.enabledLayerCount = info.instance_layer_names.size();
			inst_info.ppEnabledLayerNames =
					info.instance_layer_names.size() ?
							info.instance_layer_names.data() : NULL;
			inst_info.enabledExtensionCount =
					info.instance_extension_names.size();
			inst_info.ppEnabledExtensionNames =
					info.instance_extension_names.data();

			VkResult res = oge::vkCreateInstance(&inst_info, NULL, &info.inst);
			assert(res == VK_SUCCESS);

			return res;
		};
		init_instance(sample_title);
		if (!oge::loadInstanceLevelFunctions(info.inst,
				info.instance_extension_names)) {
			return EXIT_FAILURE;
		}
	}

	/// init_enumerate_device REMARK
	{
		auto init_enumerate_device = [&](uint32_t gpu_count = 1) {
			uint32_t const U_ASSERT_ONLY req_count = gpu_count;
			VkResult res = oge::vkEnumeratePhysicalDevices(info.inst,
					&gpu_count, NULL);
			assert(gpu_count);
			info.gpus.resize(gpu_count);

			res = oge::vkEnumeratePhysicalDevices(info.inst, &gpu_count,
					info.gpus.data());
			assert(!res && gpu_count >= req_count);

			oge::vkGetPhysicalDeviceQueueFamilyProperties(info.gpus[0],
					&info.queue_family_count, NULL);
			assert(info.queue_family_count >= 1);

			info.queue_props.resize(info.queue_family_count);
			oge::vkGetPhysicalDeviceQueueFamilyProperties(info.gpus[0],
					&info.queue_family_count, info.queue_props.data());
			assert(info.queue_family_count >= 1);

			/* This is as good a place as any to do this */
			oge::vkGetPhysicalDeviceMemoryProperties(info.gpus[0],
					&info.memory_properties);
			oge::vkGetPhysicalDeviceProperties(info.gpus[0], &info.gpu_props);

			/// init_device_extension_properties REMARK
			auto init_device_extension_properties = [&](
					layer_properties &layer_props) {
				VkExtensionProperties *device_extensions;
				uint32_t device_extension_count;
				VkResult res;
				char *layer_name = NULL;

				layer_name = layer_props.properties.layerName;

				do {
					res = oge::vkEnumerateDeviceExtensionProperties(
							info.gpus[0], layer_name, &device_extension_count,
							NULL);
					if (res)
						return res;

					if (device_extension_count == 0) {
						return VK_SUCCESS;
					}

					layer_props.device_extensions.resize(
							device_extension_count);
					device_extensions = layer_props.device_extensions.data();
					res = oge::vkEnumerateDeviceExtensionProperties(
							info.gpus[0], layer_name, &device_extension_count,
							device_extensions);
				} while (res == VK_INCOMPLETE);

				return res;
			};

			/* query device extensions for enabled layers */
			for (auto &layer_props : info.instance_layer_properties) {
				init_device_extension_properties(layer_props);
			}

			return res;
		};
		init_enumerate_device();
	}

	/// init_window_size REMARK
	{
		auto init_window_size = [&](int32_t default_width,
				int32_t default_height) {
#ifdef __ANDROID__
		AndroidGetWindowSize(&info.width, &info.height);
#else
			info.width = default_width;
			info.height = default_height;
#endif
		};
		init_window_size(500, 500);
	}

	/// init_connection REMARK
	{
		auto init_connection =
				[&]() {
#if defined(VK_USE_PLATFORM_XCB_KHR)
					const xcb_setup_t *setup;
					xcb_screen_iterator_t iter;
					int scr;

					info.connection = xcb_connect(NULL, &scr);
					if (info.connection == NULL
							|| xcb_connection_has_error(info.connection)) {
						std::cout << "Unable to make an XCB connection\n";
						exit(-1);
					}

					setup = xcb_get_setup(info.connection);
					iter = xcb_setup_roots_iterator(setup);
					while (scr-- > 0)
						xcb_screen_next(&iter);

					info.screen = iter.data;
#elif defined(VK_USE_PLATFORM_WAYLAND_KHR)
		    info.display = wl_display_connect(nullptr);

		    if (info.display == nullptr) {
		        printf(
		            "Cannot find a compatible Vulkan installable client driver "
		            "(ICD).\nExiting ...\n");
		        fflush(stdout);
		        exit(1);
		    }

		    info.registry = wl_display_get_registry(info.display);
		    wl_registry_add_listener(info.registry, &registry_listener, &info);
		    wl_display_dispatch(info.display);
		#endif
				};
		init_connection();
	}

	/// init_window REMARK
	{
		auto init_window = [&]() {
			assert(info.width > 0);
			assert(info.height > 0);

			uint32_t value_mask, value_list[32];

			info.window = xcb_generate_id(info.connection);

			value_mask = XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK;
			value_list[0] = info.screen->black_pixel;
			value_list[1] = XCB_EVENT_MASK_KEY_RELEASE
					| XCB_EVENT_MASK_EXPOSURE;

			xcb_create_window(info.connection, XCB_COPY_FROM_PARENT,
					info.window, info.screen->root, 0, 0, info.width,
					info.height, 0, XCB_WINDOW_CLASS_INPUT_OUTPUT,
					info.screen->root_visual, value_mask, value_list);

			/* Magic code that will send notification when window is destroyed */
			xcb_intern_atom_cookie_t cookie = xcb_intern_atom(info.connection,
					1, 12, "WM_PROTOCOLS");
			xcb_intern_atom_reply_t *reply = xcb_intern_atom_reply(
					info.connection, cookie, 0);

			xcb_intern_atom_cookie_t cookie2 = xcb_intern_atom(info.connection,
					0, 16, "WM_DELETE_WINDOW");
			info.atom_wm_delete_window = xcb_intern_atom_reply(info.connection,
					cookie2, 0);

			xcb_change_property(info.connection, XCB_PROP_MODE_REPLACE,
					info.window, (*reply).atom, 4, 32, 1,
					&(*info.atom_wm_delete_window).atom);
			free(reply);

			xcb_map_window(info.connection, info.window);

			// Force the x/y coordinates to 100,100 results are identical in consecutive
			// runs
			const uint32_t coords[] = { 100, 100 };
			xcb_configure_window(info.connection, info.window,
					XCB_CONFIG_WINDOW_X | XCB_CONFIG_WINDOW_Y, coords);
			xcb_flush(info.connection);

			xcb_generic_event_t *e;
			while ((e = xcb_wait_for_event(info.connection))) {
				if ((e->response_type & ~0x80) == XCB_EXPOSE)
					break;
			}
		};
		init_window();
	}

	/// init_swapchain_extension REMARK
	{
		auto init_swapchain_extension =
				[&]() {
					/* DEPENDS on init_connection() and init_window() */

					VkResult U_ASSERT_ONLY res;

					// Construct the surface description:
#ifdef _WIN32
		    VkWin32SurfaceCreateInfoKHR createInfo = {};
		    createInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
		    createInfo.pNext = NULL;
		    createInfo.hinstance = info.connection;
		    createInfo.hwnd = info.window;
		    res = vkCreateWin32SurfaceKHR(info.inst, &createInfo, NULL, &info.surface);
		#elif defined(__ANDROID__)
		    GET_INSTANCE_PROC_ADDR(info.inst, CreateAndroidSurfaceKHR);

		    VkAndroidSurfaceCreateInfoKHR createInfo;
		    createInfo.sType = VK_STRUCTURE_TYPE_ANDROID_SURFACE_CREATE_INFO_KHR;
		    createInfo.pNext = nullptr;
		    createInfo.flags = 0;
		    createInfo.window = AndroidGetApplicationWindow();
		    res = info.fpCreateAndroidSurfaceKHR(info.inst, &createInfo, nullptr, &info.surface);
		#elif defined(VK_USE_PLATFORM_METAL_EXT)
		    VkMetalSurfaceCreateInfoEXT createInfo = {};
		    createInfo.sType = VK_STRUCTURE_TYPE_METAL_SURFACE_CREATE_INFO_EXT;
		    createInfo.pNext = NULL;
		    createInfo.flags = 0;
		    createInfo.pLayer = info.caMetalLayer;
		    res = vkCreateMetalSurfaceEXT(info.inst, &createInfo, NULL, &info.surface);
		#elif defined(VK_USE_PLATFORM_WAYLAND_KHR)
		    VkWaylandSurfaceCreateInfoKHR createInfo = {};
		    createInfo.sType = VK_STRUCTURE_TYPE_WAYLAND_SURFACE_CREATE_INFO_KHR;
		    createInfo.pNext = NULL;
		    createInfo.display = info.display;
		    createInfo.surface = info.window;
		    res = vkCreateWaylandSurfaceKHR(info.inst, &createInfo, NULL, &info.surface);
		#else
					VkXcbSurfaceCreateInfoKHR createInfo = { };
					createInfo.sType =
							VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR;
					createInfo.pNext = NULL;
					createInfo.connection = info.connection;
					createInfo.window = info.window;
					res = oge::vkCreateXcbSurfaceKHR(info.inst, &createInfo,
					NULL, &info.surface);
#endif  // __ANDROID__  && _WIN32
					assert(res == VK_SUCCESS);

					// Iterate over each queue to learn whether it supports presenting:
					VkBool32 *pSupportsPresent = (VkBool32*) malloc(
							info.queue_family_count * sizeof(VkBool32));
					for (uint32_t i = 0; i < info.queue_family_count; i++) {
						oge::vkGetPhysicalDeviceSurfaceSupportKHR(info.gpus[0],
								i, info.surface, &pSupportsPresent[i]);
					}

					// Search for a graphics and a present queue in the array of queue
					// families, try to find one that supports both
					info.graphics_queue_family_index = UINT32_MAX;
					info.present_queue_family_index = UINT32_MAX;
					for (uint32_t i = 0; i < info.queue_family_count; ++i) {
						if ((info.queue_props[i].queueFlags
								& VK_QUEUE_GRAPHICS_BIT) != 0) {
							if (info.graphics_queue_family_index == UINT32_MAX)
								info.graphics_queue_family_index = i;

							if (pSupportsPresent[i] == VK_TRUE) {
								info.graphics_queue_family_index = i;
								info.present_queue_family_index = i;
								break;
							}
						}
					}

					if (info.present_queue_family_index == UINT32_MAX) {
						// If didn't find a queue that supports both graphics and present, then
						// find a separate present queue.
						for (size_t i = 0; i < info.queue_family_count; ++i)
							if (pSupportsPresent[i] == VK_TRUE) {
								info.present_queue_family_index = i;
								break;
							}
					}
					free(pSupportsPresent);

					// Generate error if could not find queues that support graphics
					// and present
					if (info.graphics_queue_family_index == UINT32_MAX
							|| info.present_queue_family_index == UINT32_MAX) {
						std::cout
								<< "Could not find a queues for both graphics and present";
						exit(-1);
					}

					// Get the list of VkFormats that are supported:
					uint32_t formatCount;
					res = oge::vkGetPhysicalDeviceSurfaceFormatsKHR(
							info.gpus[0], info.surface, &formatCount, NULL);
					assert(res == VK_SUCCESS);
					VkSurfaceFormatKHR *surfFormats =
							(VkSurfaceFormatKHR*) malloc(
									formatCount * sizeof(VkSurfaceFormatKHR));
					res = oge::vkGetPhysicalDeviceSurfaceFormatsKHR(
							info.gpus[0], info.surface, &formatCount,
							surfFormats);
					assert(res == VK_SUCCESS);

					// If the device supports our preferred surface format, use it.
					// Otherwise, use whatever the device's first reported surface
					// format is.
					assert(formatCount >= 1);
					info.format = surfFormats[0].format;
					for (size_t i = 0; i < formatCount; ++i) {
						if (surfFormats[i].format == PREFERRED_SURFACE_FORMAT) {
							info.format = PREFERRED_SURFACE_FORMAT;
							break;
						}
					}
					free(surfFormats);
				};
		init_swapchain_extension();
	}

	/// init_device REMARK
	{
		auto init_device = [&]() {
			VkResult res;
			VkDeviceQueueCreateInfo queue_info = { };

			float queue_priorities[1] = { 0.0 };
			queue_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
			queue_info.pNext = NULL;
			queue_info.queueCount = 1;
			queue_info.pQueuePriorities = queue_priorities;
			queue_info.queueFamilyIndex = info.graphics_queue_family_index;

			VkDeviceCreateInfo device_info = { };
			device_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
			device_info.pNext = NULL;
			device_info.queueCreateInfoCount = 1;
			device_info.pQueueCreateInfos = &queue_info;
			device_info.enabledExtensionCount =
					info.device_extension_names.size();
			device_info.ppEnabledExtensionNames =
					device_info.enabledExtensionCount ?
							info.device_extension_names.data() : NULL;
			device_info.pEnabledFeatures = NULL;

			res = oge::vkCreateDevice(info.gpus[0], &device_info, NULL,
					&info.device);
			assert(res == VK_SUCCESS);

			return res;
		};
		init_device();
		if (!oge::loadDeviceLevelFunctions(info.device,
				info.device_extension_names)) {
			return EXIT_FAILURE;
		}
	}

	/// init_command_pool REMARK
	{
		auto init_command_pool = [&]() {
			/* DEPENDS on init_swapchain_extension() */
			VkResult U_ASSERT_ONLY res;

			VkCommandPoolCreateInfo cmd_pool_info = { };
			cmd_pool_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
			cmd_pool_info.pNext = NULL;
			cmd_pool_info.queueFamilyIndex = info.graphics_queue_family_index;
			cmd_pool_info.flags =
					VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

			res = oge::vkCreateCommandPool(info.device, &cmd_pool_info, NULL,
					&info.cmd_pool);
			assert(res == VK_SUCCESS);
		};
		init_command_pool();
	}

	/// init_command_buffer REMARK
	{
		auto init_command_buffer = [&]() {
			/* DEPENDS on init_swapchain_extension() and init_command_pool() */
			VkResult U_ASSERT_ONLY res;

			VkCommandBufferAllocateInfo cmd = { };
			cmd.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
			cmd.pNext = NULL;
			cmd.commandPool = info.cmd_pool;
			cmd.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
			cmd.commandBufferCount = 1;

			res = oge::vkAllocateCommandBuffers(info.device, &cmd, &info.cmd);
			assert(res == VK_SUCCESS);
		};
		init_command_buffer();
	}

	/// execute_begin_command_buffer REMARK
	{
		auto execute_begin_command_buffer = [&]() {
			/* DEPENDS on init_command_buffer() */
			VkResult U_ASSERT_ONLY res;

			VkCommandBufferBeginInfo cmd_buf_info = { };
			cmd_buf_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
			cmd_buf_info.pNext = NULL;
			cmd_buf_info.flags = 0;
			cmd_buf_info.pInheritanceInfo = NULL;

			res = oge::vkBeginCommandBuffer(info.cmd, &cmd_buf_info);
			assert(res == VK_SUCCESS);
		};
		execute_begin_command_buffer();
	}

	/// init_device_queue REMARK
	{
		auto init_device_queue = [&]() {
			/* DEPENDS on init_swapchain_extension() */

			oge::vkGetDeviceQueue(info.device, info.graphics_queue_family_index,
					0, &info.graphics_queue);
			if (info.graphics_queue_family_index
					== info.present_queue_family_index) {
				info.present_queue = info.graphics_queue;
			} else {
				oge::vkGetDeviceQueue(info.device,
						info.present_queue_family_index, 0,
						&info.present_queue);
			}
		};
		init_device_queue();
	}

	/// init_swap_chain REMARK
	{
		auto init_swap_chain = [&](
				VkImageUsageFlags usageFlags =
						VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT
								| VK_IMAGE_USAGE_TRANSFER_SRC_BIT) {
			/* DEPENDS on info.cmd and info.queue initialized */

			VkResult U_ASSERT_ONLY res;
			VkSurfaceCapabilitiesKHR surfCapabilities;

			res = oge::vkGetPhysicalDeviceSurfaceCapabilitiesKHR(info.gpus[0],
					info.surface, &surfCapabilities);
			assert(res == VK_SUCCESS);

			uint32_t presentModeCount;
			res = oge::vkGetPhysicalDeviceSurfacePresentModesKHR(info.gpus[0],
					info.surface, &presentModeCount, NULL);
			assert(res == VK_SUCCESS);
			VkPresentModeKHR *presentModes = (VkPresentModeKHR*) malloc(
					presentModeCount * sizeof(VkPresentModeKHR));
			assert(presentModes);
			res = oge::vkGetPhysicalDeviceSurfacePresentModesKHR(info.gpus[0],
					info.surface, &presentModeCount, presentModes);
			assert(res == VK_SUCCESS);

			VkExtent2D swapchainExtent;
			// width and height are either both 0xFFFFFFFF, or both not 0xFFFFFFFF.
			if (surfCapabilities.currentExtent.width == 0xFFFFFFFF) {
				// If the surface size is undefined, the size is set to
				// the size of the images requested.
				swapchainExtent.width = info.width;
				swapchainExtent.height = info.height;
				if (swapchainExtent.width
						< surfCapabilities.minImageExtent.width) {
					swapchainExtent.width =
							surfCapabilities.minImageExtent.width;
				} else if (swapchainExtent.width
						> surfCapabilities.maxImageExtent.width) {
					swapchainExtent.width =
							surfCapabilities.maxImageExtent.width;
				}

				if (swapchainExtent.height
						< surfCapabilities.minImageExtent.height) {
					swapchainExtent.height =
							surfCapabilities.minImageExtent.height;
				} else if (swapchainExtent.height
						> surfCapabilities.maxImageExtent.height) {
					swapchainExtent.height =
							surfCapabilities.maxImageExtent.height;
				}
			} else {
				// If the surface size is defined, the swap chain size must match
				swapchainExtent = surfCapabilities.currentExtent;
			}

			// The FIFO present mode is guaranteed by the spec to be supported
			// Also note that current Android driver only supports FIFO
			VkPresentModeKHR swapchainPresentMode = VK_PRESENT_MODE_FIFO_KHR;

			// Determine the number of VkImage's to use in the swap chain.
			// We need to acquire only 1 presentable image at at time.
			// Asking for minImageCount images ensures that we can acquire
			// 1 presentable image as long as we present it before attempting
			// to acquire another.
			uint32_t desiredNumberOfSwapChainImages =
					surfCapabilities.minImageCount;

			VkSurfaceTransformFlagBitsKHR preTransform;
			if (surfCapabilities.supportedTransforms
					& VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR) {
				preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
			} else {
				preTransform = surfCapabilities.currentTransform;
			}

			// Find a supported composite alpha mode - one of these is guaranteed to be set
			VkCompositeAlphaFlagBitsKHR compositeAlpha =
					VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
			VkCompositeAlphaFlagBitsKHR compositeAlphaFlags[4] = {
					VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
					VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR,
					VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR,
					VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR, };
			for (uint32_t i = 0;
					i
							< sizeof(compositeAlphaFlags)
									/ sizeof(compositeAlphaFlags[0]); i++) {
				if (surfCapabilities.supportedCompositeAlpha
						& compositeAlphaFlags[i]) {
					compositeAlpha = compositeAlphaFlags[i];
					break;
				}
			}

			VkSwapchainCreateInfoKHR swapchain_ci = { };
			swapchain_ci.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
			swapchain_ci.pNext = NULL;
			swapchain_ci.surface = info.surface;
			swapchain_ci.minImageCount = desiredNumberOfSwapChainImages;
			swapchain_ci.imageFormat = info.format;
			swapchain_ci.imageExtent.width = swapchainExtent.width;
			swapchain_ci.imageExtent.height = swapchainExtent.height;
			swapchain_ci.preTransform = preTransform;
			swapchain_ci.compositeAlpha = compositeAlpha;
			swapchain_ci.imageArrayLayers = 1;
			swapchain_ci.presentMode = swapchainPresentMode;
			swapchain_ci.oldSwapchain = VK_NULL_HANDLE;
#ifndef __ANDROID__
			swapchain_ci.clipped = true;
#else
		    swapchain_ci.clipped = false;
		#endif
			swapchain_ci.imageColorSpace = VK_COLORSPACE_SRGB_NONLINEAR_KHR;
			swapchain_ci.imageUsage = usageFlags;
			swapchain_ci.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
			swapchain_ci.queueFamilyIndexCount = 0;
			swapchain_ci.pQueueFamilyIndices = NULL;
			uint32_t queueFamilyIndices[2] = {
					(uint32_t) info.graphics_queue_family_index,
					(uint32_t) info.present_queue_family_index };
			if (info.graphics_queue_family_index
					!= info.present_queue_family_index) {
				// If the graphics and present queues are from different queue families,
				// we either have to explicitly transfer ownership of images between the
				// queues, or we have to create the swapchain with imageSharingMode
				// as VK_SHARING_MODE_CONCURRENT
				swapchain_ci.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
				swapchain_ci.queueFamilyIndexCount = 2;
				swapchain_ci.pQueueFamilyIndices = queueFamilyIndices;
			}

			res = oge::vkCreateSwapchainKHR(info.device, &swapchain_ci, NULL,
					&info.swap_chain);
			assert(res == VK_SUCCESS);

			res = oge::vkGetSwapchainImagesKHR(info.device, info.swap_chain,
					&info.swapchainImageCount, NULL);
			assert(res == VK_SUCCESS);

			VkImage *swapchainImages = (VkImage*) malloc(
					info.swapchainImageCount * sizeof(VkImage));
			assert(swapchainImages);
			res = oge::vkGetSwapchainImagesKHR(info.device, info.swap_chain,
					&info.swapchainImageCount, swapchainImages);
			assert(res == VK_SUCCESS);

			for (uint32_t i = 0; i < info.swapchainImageCount; i++) {
				swap_chain_buffer sc_buffer;

				VkImageViewCreateInfo color_image_view = { };
				color_image_view.sType =
						VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
				color_image_view.pNext = NULL;
				color_image_view.format = info.format;
				color_image_view.components.r = VK_COMPONENT_SWIZZLE_R;
				color_image_view.components.g = VK_COMPONENT_SWIZZLE_G;
				color_image_view.components.b = VK_COMPONENT_SWIZZLE_B;
				color_image_view.components.a = VK_COMPONENT_SWIZZLE_A;
				color_image_view.subresourceRange.aspectMask =
						VK_IMAGE_ASPECT_COLOR_BIT;
				color_image_view.subresourceRange.baseMipLevel = 0;
				color_image_view.subresourceRange.levelCount = 1;
				color_image_view.subresourceRange.baseArrayLayer = 0;
				color_image_view.subresourceRange.layerCount = 1;
				color_image_view.viewType = VK_IMAGE_VIEW_TYPE_2D;
				color_image_view.flags = 0;

				sc_buffer.image = swapchainImages[i];

				color_image_view.image = sc_buffer.image;

				res = oge::vkCreateImageView(info.device, &color_image_view,
				NULL, &sc_buffer.view);
				info.buffers.push_back(sc_buffer);
				assert(res == VK_SUCCESS);
			}
			free(swapchainImages);
			info.current_buffer = 0;

			if (NULL != presentModes) {
				free(presentModes);
			}
		};
		init_swap_chain();
	}

	/// init_depth_buffer REMARK
	{
		auto init_depth_buffer =
				[&]() {
					VkResult U_ASSERT_ONLY res;
					bool U_ASSERT_ONLY pass;
					VkImageCreateInfo image_info = { };
					VkFormatProperties props;

					/* allow custom depth formats */
#ifdef __ANDROID__
		    // Depth format needs to be VK_FORMAT_D24_UNORM_S8_UINT on Android (if available).
		    vkGetPhysicalDeviceFormatProperties(info.gpus[0], VK_FORMAT_D24_UNORM_S8_UINT, &props);
		    if ((props.linearTilingFeatures & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT) ||
		        (props.optimalTilingFeatures & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT))
		        info.depth.format = VK_FORMAT_D24_UNORM_S8_UINT;
		    else
		        info.depth.format = VK_FORMAT_D16_UNORM;
		#elif defined(VK_USE_PLATFORM_IOS_MVK)
		    if (info.depth.format == VK_FORMAT_UNDEFINED) info.depth.format = VK_FORMAT_D32_SFLOAT;
		#else
					if (info.depth.format == VK_FORMAT_UNDEFINED)
						info.depth.format = VK_FORMAT_D16_UNORM;
#endif

					const VkFormat depth_format = info.depth.format;
					oge::vkGetPhysicalDeviceFormatProperties(info.gpus[0],
							depth_format, &props);
					if (props.linearTilingFeatures
							& VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT) {
						image_info.tiling = VK_IMAGE_TILING_LINEAR;
					} else if (props.optimalTilingFeatures
							& VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT) {
						image_info.tiling = VK_IMAGE_TILING_OPTIMAL;
					} else {
						/* Try other depth formats? */
						std::cout << "depth_format " << depth_format
								<< " Unsupported.\n";
						exit(-1);
					}

					image_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
					image_info.pNext = NULL;
					image_info.imageType = VK_IMAGE_TYPE_2D;
					image_info.format = depth_format;
					image_info.extent.width = info.width;
					image_info.extent.height = info.height;
					image_info.extent.depth = 1;
					image_info.mipLevels = 1;
					image_info.arrayLayers = 1;
					image_info.samples = NUM_SAMPLES;
					image_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
					image_info.queueFamilyIndexCount = 0;
					image_info.pQueueFamilyIndices = NULL;
					image_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
					image_info.usage =
							VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
					image_info.flags = 0;

					VkMemoryAllocateInfo mem_alloc = { };
					mem_alloc.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
					mem_alloc.pNext = NULL;
					mem_alloc.allocationSize = 0;
					mem_alloc.memoryTypeIndex = 0;

					VkImageViewCreateInfo view_info = { };
					view_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
					view_info.pNext = NULL;
					view_info.image = VK_NULL_HANDLE;
					view_info.format = depth_format;
					view_info.components.r = VK_COMPONENT_SWIZZLE_R;
					view_info.components.g = VK_COMPONENT_SWIZZLE_G;
					view_info.components.b = VK_COMPONENT_SWIZZLE_B;
					view_info.components.a = VK_COMPONENT_SWIZZLE_A;
					view_info.subresourceRange.aspectMask =
							VK_IMAGE_ASPECT_DEPTH_BIT;
					view_info.subresourceRange.baseMipLevel = 0;
					view_info.subresourceRange.levelCount = 1;
					view_info.subresourceRange.baseArrayLayer = 0;
					view_info.subresourceRange.layerCount = 1;
					view_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
					view_info.flags = 0;

					if (depth_format == VK_FORMAT_D16_UNORM_S8_UINT
							|| depth_format == VK_FORMAT_D24_UNORM_S8_UINT
							|| depth_format == VK_FORMAT_D32_SFLOAT_S8_UINT) {
						view_info.subresourceRange.aspectMask |=
								VK_IMAGE_ASPECT_STENCIL_BIT;
					}

					VkMemoryRequirements mem_reqs;

					/* Create image */
					res = oge::vkCreateImage(info.device, &image_info, NULL,
							&info.depth.image);
					assert(res == VK_SUCCESS);

					oge::vkGetImageMemoryRequirements(info.device,
							info.depth.image, &mem_reqs);

					mem_alloc.allocationSize = mem_reqs.size;

					/// memory_type_from_properties REMARK
					auto memory_type_from_properties =
							[&](uint32_t typeBits, VkFlags requirements_mask,
									uint32_t *typeIndex) {
								// Search memtypes to find first index with those properties
								for (uint32_t i = 0;
										i
												< info.memory_properties.memoryTypeCount;
										i++) {
									if ((typeBits & 1) == 1) {
										// Type is available, does it match user properties?
										if ((info.memory_properties.memoryTypes[i].propertyFlags
												& requirements_mask)
												== requirements_mask) {
											*typeIndex = i;
											return true;
										}
									}
									typeBits >>= 1;
								}
								// No memory types matched, return failure
								return false;
							};

					/* Use the memory properties to determine the type of memory required */
					pass = memory_type_from_properties(mem_reqs.memoryTypeBits,
							VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
							&mem_alloc.memoryTypeIndex);
					assert(pass);

					/* Allocate memory */
					res = oge::vkAllocateMemory(info.device, &mem_alloc, NULL,
							&info.depth.mem);
					assert(res == VK_SUCCESS);

					/* Bind memory */
					res = oge::vkBindImageMemory(info.device, info.depth.image,
							info.depth.mem, 0);
					assert(res == VK_SUCCESS);

					/* Create image view */
					view_info.image = info.depth.image;
					res = oge::vkCreateImageView(info.device, &view_info, NULL,
							&info.depth.view);
					assert(res == VK_SUCCESS);
				};
		init_depth_buffer();
	}

	/// init_uniform_buffer REMARK
	{
		auto init_uniform_buffer = [&]() {
			VkResult U_ASSERT_ONLY res;
			bool U_ASSERT_ONLY pass;
			float fov = glm::radians(45.0f);
			if (info.width > info.height) {
				fov *= static_cast<float>(info.height)
						/ static_cast<float>(info.width);
			}
			info.Projection = glm::perspective(fov,
					static_cast<float>(info.width)
							/ static_cast<float>(info.height), 0.1f, 100.0f);
			info.View = glm::lookAt(glm::vec3(-5, 3, -10), // Camera is at (-5,3,-10), in World Space
			glm::vec3(0, 0, 0),     // and looks at the origin
			glm::vec3(0, -1, 0) // Head is up (set to 0,-1,0 to look upside-down)
					);
			info.Model = glm::mat4(1.0f);
			// Vulkan clip space has inverted Y and half Z.
			info.Clip = glm::mat4(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f,
					0.0f, 0.0f, 0.0f, 0.5f, 0.0f, 0.0f, 0.0f, 0.5f, 1.0f);

			info.MVP = info.Clip * info.Projection * info.View * info.Model;

			/* VULKAN_KEY_START */
			VkBufferCreateInfo buf_info = { };
			buf_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
			buf_info.pNext = NULL;
			buf_info.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
			buf_info.size = sizeof(info.MVP);
			buf_info.queueFamilyIndexCount = 0;
			buf_info.pQueueFamilyIndices = NULL;
			buf_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
			buf_info.flags = 0;
			res = oge::vkCreateBuffer(info.device, &buf_info, NULL,
					&info.uniform_data.buf);
			assert(res == VK_SUCCESS);

			VkMemoryRequirements mem_reqs;
			oge::vkGetBufferMemoryRequirements(info.device,
					info.uniform_data.buf, &mem_reqs);

			VkMemoryAllocateInfo alloc_info = { };
			alloc_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
			alloc_info.pNext = NULL;
			alloc_info.memoryTypeIndex = 0;

			alloc_info.allocationSize = mem_reqs.size;

			/// memory_type_from_properties REMARK
			auto memory_type_from_properties = [&](uint32_t typeBits,
					VkFlags requirements_mask, uint32_t *typeIndex) {
				// Search memtypes to find first index with those properties
				for (uint32_t i = 0; i < info.memory_properties.memoryTypeCount;
						i++) {
					if ((typeBits & 1) == 1) {
						// Type is available, does it match user properties?
						if ((info.memory_properties.memoryTypes[i].propertyFlags
								& requirements_mask) == requirements_mask) {
							*typeIndex = i;
							return true;
						}
					}
					typeBits >>= 1;
				}
				// No memory types matched, return failure
				return false;
			};

			pass = memory_type_from_properties(mem_reqs.memoryTypeBits,
					VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
							| VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
					&alloc_info.memoryTypeIndex);
			assert(pass && "No mappable, coherent memory");

			res = oge::vkAllocateMemory(info.device, &alloc_info, NULL,
					&(info.uniform_data.mem));
			assert(res == VK_SUCCESS);

			uint8_t *pData;
			res = oge::vkMapMemory(info.device, info.uniform_data.mem, 0,
					mem_reqs.size, 0, (void**) &pData);
			assert(res == VK_SUCCESS);

			memcpy(pData, &info.MVP, sizeof(info.MVP));

			oge::vkUnmapMemory(info.device, info.uniform_data.mem);

			res = oge::vkBindBufferMemory(info.device, info.uniform_data.buf,
					info.uniform_data.mem, 0);
			assert(res == VK_SUCCESS);

			info.uniform_data.buffer_info.buffer = info.uniform_data.buf;
			info.uniform_data.buffer_info.offset = 0;
			info.uniform_data.buffer_info.range = sizeof(info.MVP);
		};
		init_uniform_buffer();
	}

	/// init_descriptor_and_pipeline_layouts REMARK
	{
		auto init_descriptor_and_pipeline_layouts = [&](bool use_texture,
				VkDescriptorSetLayoutCreateFlags descSetLayoutCreateFlags = 0) {
			VkDescriptorSetLayoutBinding layout_bindings[2];
			layout_bindings[0].binding = 0;
			layout_bindings[0].descriptorType =
					VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
			layout_bindings[0].descriptorCount = 1;
			layout_bindings[0].stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
			layout_bindings[0].pImmutableSamplers = NULL;

			if (use_texture) {
				layout_bindings[1].binding = 1;
				layout_bindings[1].descriptorType =
						VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
				layout_bindings[1].descriptorCount = 1;
				layout_bindings[1].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
				layout_bindings[1].pImmutableSamplers = NULL;
			}

			/* Next take layout bindings and use them to create a descriptor set layout
			 */
			VkDescriptorSetLayoutCreateInfo descriptor_layout = { };
			descriptor_layout.sType =
					VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
			descriptor_layout.pNext = NULL;
			descriptor_layout.flags = descSetLayoutCreateFlags;
			descriptor_layout.bindingCount = use_texture ? 2 : 1;
			descriptor_layout.pBindings = layout_bindings;

			VkResult U_ASSERT_ONLY res;

			info.desc_layout.resize(NUM_DESCRIPTOR_SETS);
			res = oge::vkCreateDescriptorSetLayout(info.device,
					&descriptor_layout,
					NULL, info.desc_layout.data());
			assert(res == VK_SUCCESS);

			/* Now use the descriptor layout to create a pipeline layout */
			VkPipelineLayoutCreateInfo pPipelineLayoutCreateInfo = { };
			pPipelineLayoutCreateInfo.sType =
					VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
			pPipelineLayoutCreateInfo.pNext = NULL;
			pPipelineLayoutCreateInfo.pushConstantRangeCount = 0;
			pPipelineLayoutCreateInfo.pPushConstantRanges = NULL;
			pPipelineLayoutCreateInfo.setLayoutCount = NUM_DESCRIPTOR_SETS;
			pPipelineLayoutCreateInfo.pSetLayouts = info.desc_layout.data();

			res = oge::vkCreatePipelineLayout(info.device,
					&pPipelineLayoutCreateInfo, NULL, &info.pipeline_layout);
			assert(res == VK_SUCCESS);
		};
		init_descriptor_and_pipeline_layouts(false);
	}

	/// init_renderpass REMARK
	{
		auto init_renderpass = [&](bool include_depth, bool clear = true,
				VkImageLayout finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
				VkImageLayout initialLayout = VK_IMAGE_LAYOUT_UNDEFINED) {
			/* DEPENDS on init_swap_chain() and init_depth_buffer() */

			assert(clear || (initialLayout != VK_IMAGE_LAYOUT_UNDEFINED));

			VkResult U_ASSERT_ONLY res;
			/* Need attachments for render target and depth buffer */
			VkAttachmentDescription attachments[2];
			attachments[0].format = info.format;
			attachments[0].samples = NUM_SAMPLES;
			attachments[0].loadOp =
					clear ? VK_ATTACHMENT_LOAD_OP_CLEAR : VK_ATTACHMENT_LOAD_OP_LOAD;
			attachments[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
			attachments[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
			attachments[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
			attachments[0].initialLayout = initialLayout;
			attachments[0].finalLayout = finalLayout;
			attachments[0].flags = 0;

			if (include_depth) {
				attachments[1].format = info.depth.format;
				attachments[1].samples = NUM_SAMPLES;
				attachments[1].loadOp =
						clear ? VK_ATTACHMENT_LOAD_OP_CLEAR : VK_ATTACHMENT_LOAD_OP_DONT_CARE;
				attachments[1].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
				attachments[1].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
				attachments[1].stencilStoreOp = VK_ATTACHMENT_STORE_OP_STORE;
				attachments[1].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
				attachments[1].finalLayout =
						VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
				attachments[1].flags = 0;
			}

			VkAttachmentReference color_reference = { };
			color_reference.attachment = 0;
			color_reference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

			VkAttachmentReference depth_reference = { };
			depth_reference.attachment = 1;
			depth_reference.layout =
					VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

			VkSubpassDescription subpass = { };
			subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
			subpass.flags = 0;
			subpass.inputAttachmentCount = 0;
			subpass.pInputAttachments = NULL;
			subpass.colorAttachmentCount = 1;
			subpass.pColorAttachments = &color_reference;
			subpass.pResolveAttachments = NULL;
			subpass.pDepthStencilAttachment =
					include_depth ? &depth_reference : NULL;
			subpass.preserveAttachmentCount = 0;
			subpass.pPreserveAttachments = NULL;

			// Subpass dependency to wait for wsi image acquired semaphore before starting layout transition
			VkSubpassDependency subpass_dependency = { };
			subpass_dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
			subpass_dependency.dstSubpass = 0;
			subpass_dependency.srcStageMask =
					VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
			subpass_dependency.dstStageMask =
					VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
			subpass_dependency.srcAccessMask = 0;
			subpass_dependency.dstAccessMask =
					VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
			subpass_dependency.dependencyFlags = 0;

			VkRenderPassCreateInfo rp_info = { };
			rp_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
			rp_info.pNext = NULL;
			rp_info.attachmentCount = include_depth ? 2 : 1;
			rp_info.pAttachments = attachments;
			rp_info.subpassCount = 1;
			rp_info.pSubpasses = &subpass;
			rp_info.dependencyCount = 1;
			rp_info.pDependencies = &subpass_dependency;

			res = oge::vkCreateRenderPass(info.device, &rp_info, NULL,
					&info.render_pass);
			assert(res == VK_SUCCESS);
		};
		init_renderpass(depthPresent);
	}

	/// init_shaders REMARK
	{
		VkShaderModuleCreateInfo vert_info = { };
		VkShaderModuleCreateInfo frag_info = { };
		vert_info.sType = frag_info.sType =
				VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
		vert_info.codeSize = sizeof(__draw_cube_vert);
		vert_info.pCode = __draw_cube_vert;
		frag_info.codeSize = sizeof(__draw_cube_frag);
		frag_info.pCode = __draw_cube_frag;

		auto init_shaders = [&](const VkShaderModuleCreateInfo *vertShaderCI,
				const VkShaderModuleCreateInfo *fragShaderCI) {
			VkResult U_ASSERT_ONLY res;

			if (vertShaderCI) {
				info.shaderStages[0].sType =
						VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
				info.shaderStages[0].pNext = NULL;
				info.shaderStages[0].pSpecializationInfo = NULL;
				info.shaderStages[0].flags = 0;
				info.shaderStages[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
				info.shaderStages[0].pName = "main";
				res = oge::vkCreateShaderModule(info.device, vertShaderCI, NULL,
						&info.shaderStages[0].module);
				assert(res == VK_SUCCESS);
			}

			if (fragShaderCI) {
				std::vector<unsigned int> vtx_spv;
				info.shaderStages[1].sType =
						VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
				info.shaderStages[1].pNext = NULL;
				info.shaderStages[1].pSpecializationInfo = NULL;
				info.shaderStages[1].flags = 0;
				info.shaderStages[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
				info.shaderStages[1].pName = "main";
				res = oge::vkCreateShaderModule(info.device, fragShaderCI, NULL,
						&info.shaderStages[1].module);
				assert(res == VK_SUCCESS);
			}
		};
		init_shaders(&vert_info, &frag_info);
	}

	/// init_framebuffers REMARK
	{
		auto init_framebuffers = [&](bool include_depth) {
			/* DEPENDS on init_depth_buffer(), init_renderpass() and
			 * init_swapchain_extension() */

			VkResult U_ASSERT_ONLY res;
			VkImageView attachments[2];
			attachments[1] = info.depth.view;

			VkFramebufferCreateInfo fb_info = { };
			fb_info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
			fb_info.pNext = NULL;
			fb_info.renderPass = info.render_pass;
			fb_info.attachmentCount = include_depth ? 2 : 1;
			fb_info.pAttachments = attachments;
			fb_info.width = info.width;
			fb_info.height = info.height;
			fb_info.layers = 1;

			uint32_t i;

			info.framebuffers = (VkFramebuffer*) malloc(
					info.swapchainImageCount * sizeof(VkFramebuffer));

			for (i = 0; i < info.swapchainImageCount; i++) {
				attachments[0] = info.buffers[i].view;
				res = oge::vkCreateFramebuffer(info.device, &fb_info, NULL,
						&info.framebuffers[i]);
				assert(res == VK_SUCCESS);
			}
		};
		init_framebuffers(depthPresent);
	}

	/// init_vertex_buffer REMARK
	{
		auto init_vertex_buffer = [&](const void *vertexData, uint32_t dataSize,
				uint32_t dataStride, bool use_texture) {
			VkResult U_ASSERT_ONLY res;
			bool U_ASSERT_ONLY pass;

			VkBufferCreateInfo buf_info = { };
			buf_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
			buf_info.pNext = NULL;
			buf_info.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
			buf_info.size = dataSize;
			buf_info.queueFamilyIndexCount = 0;
			buf_info.pQueueFamilyIndices = NULL;
			buf_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
			buf_info.flags = 0;
			res = oge::vkCreateBuffer(info.device, &buf_info, NULL,
					&info.vertex_buffer.buf);
			assert(res == VK_SUCCESS);

			VkMemoryRequirements mem_reqs;
			oge::vkGetBufferMemoryRequirements(info.device,
					info.vertex_buffer.buf, &mem_reqs);

			VkMemoryAllocateInfo alloc_info = { };
			alloc_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
			alloc_info.pNext = NULL;
			alloc_info.memoryTypeIndex = 0;

			alloc_info.allocationSize = mem_reqs.size;

			/// memory_type_from_properties REMARK
			auto memory_type_from_properties = [&](uint32_t typeBits,
					VkFlags requirements_mask, uint32_t *typeIndex) {
				// Search memtypes to find first index with those properties
				for (uint32_t i = 0; i < info.memory_properties.memoryTypeCount;
						i++) {
					if ((typeBits & 1) == 1) {
						// Type is available, does it match user properties?
						if ((info.memory_properties.memoryTypes[i].propertyFlags
								& requirements_mask) == requirements_mask) {
							*typeIndex = i;
							return true;
						}
					}
					typeBits >>= 1;
				}
				// No memory types matched, return failure
				return false;
			};

			pass = memory_type_from_properties(mem_reqs.memoryTypeBits,
					VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
							| VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
					&alloc_info.memoryTypeIndex);
			assert(pass && "No mappable, coherent memory");

			res = oge::vkAllocateMemory(info.device, &alloc_info, NULL,
					&(info.vertex_buffer.mem));
			assert(res == VK_SUCCESS);
			info.vertex_buffer.buffer_info.range = mem_reqs.size;
			info.vertex_buffer.buffer_info.offset = 0;

			uint8_t *pData;
			res = oge::vkMapMemory(info.device, info.vertex_buffer.mem, 0,
					mem_reqs.size, 0, (void**) &pData);
			assert(res == VK_SUCCESS);

			memcpy(pData, vertexData, dataSize);

			oge::vkUnmapMemory(info.device, info.vertex_buffer.mem);

			res = oge::vkBindBufferMemory(info.device, info.vertex_buffer.buf,
					info.vertex_buffer.mem, 0);
			assert(res == VK_SUCCESS);

			info.vi_binding.binding = 0;
			info.vi_binding.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
			info.vi_binding.stride = dataStride;

			info.vi_attribs[0].binding = 0;
			info.vi_attribs[0].location = 0;
			info.vi_attribs[0].format = VK_FORMAT_R32G32B32A32_SFLOAT;
			info.vi_attribs[0].offset = 0;
			info.vi_attribs[1].binding = 0;
			info.vi_attribs[1].location = 1;
			info.vi_attribs[1].format =
					use_texture ?
							VK_FORMAT_R32G32_SFLOAT :
							VK_FORMAT_R32G32B32A32_SFLOAT;
			info.vi_attribs[1].offset = 16;
		};
		init_vertex_buffer(g_vb_solid_face_colors_Data,
				sizeof(g_vb_solid_face_colors_Data),
				sizeof(g_vb_solid_face_colors_Data[0]), false);
	}

	/// init_descriptor_pool REMARK
	{
		auto init_descriptor_pool = [&](bool use_texture) {
			/* DEPENDS on init_uniform_buffer() and
			 * init_descriptor_and_pipeline_layouts() */

			VkResult U_ASSERT_ONLY res;
			VkDescriptorPoolSize type_count[2];
			type_count[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
			type_count[0].descriptorCount = 1;
			if (use_texture) {
				type_count[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
				type_count[1].descriptorCount = 1;
			}

			VkDescriptorPoolCreateInfo descriptor_pool = { };
			descriptor_pool.sType =
					VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
			descriptor_pool.pNext = NULL;
			descriptor_pool.maxSets = 1;
			descriptor_pool.poolSizeCount = use_texture ? 2 : 1;
			descriptor_pool.pPoolSizes = type_count;

			res = oge::vkCreateDescriptorPool(info.device, &descriptor_pool,
			NULL, &info.desc_pool);
			assert(res == VK_SUCCESS);
		};
		init_descriptor_pool(false);
	}

	/// init_descriptor_set REMARK
	{
		auto init_descriptor_set = [&](bool use_texture) {
			/* DEPENDS on init_descriptor_pool() */

			VkResult U_ASSERT_ONLY res;

			VkDescriptorSetAllocateInfo alloc_info[1];
			alloc_info[0].sType =
					VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
			alloc_info[0].pNext = NULL;
			alloc_info[0].descriptorPool = info.desc_pool;
			alloc_info[0].descriptorSetCount = NUM_DESCRIPTOR_SETS;
			alloc_info[0].pSetLayouts = info.desc_layout.data();

			info.desc_set.resize(NUM_DESCRIPTOR_SETS);
			res = oge::vkAllocateDescriptorSets(info.device, alloc_info,
					info.desc_set.data());
			assert(res == VK_SUCCESS);

			VkWriteDescriptorSet writes[2];

			writes[0] = { };
			writes[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			writes[0].pNext = NULL;
			writes[0].dstSet = info.desc_set[0];
			writes[0].descriptorCount = 1;
			writes[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
			writes[0].pBufferInfo = &info.uniform_data.buffer_info;
			writes[0].dstArrayElement = 0;
			writes[0].dstBinding = 0;

			if (use_texture) {
				writes[1] = { };
				writes[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
				writes[1].dstSet = info.desc_set[0];
				writes[1].dstBinding = 1;
				writes[1].descriptorCount = 1;
				writes[1].descriptorType =
						VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
				writes[1].pImageInfo = &info.texture_data.image_info;
				writes[1].dstArrayElement = 0;
			}

			oge::vkUpdateDescriptorSets(info.device, use_texture ? 2 : 1,
					writes, 0,
					NULL);
		};
		init_descriptor_set(false);
	}

	/// init_pipeline_cache REMARK
	{
		auto init_pipeline_cache = [&]() {
			VkResult U_ASSERT_ONLY res;

			VkPipelineCacheCreateInfo pipelineCache;
			pipelineCache.sType = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO;
			pipelineCache.pNext = NULL;
			pipelineCache.initialDataSize = 0;
			pipelineCache.pInitialData = NULL;
			pipelineCache.flags = 0;
			res = oge::vkCreatePipelineCache(info.device, &pipelineCache, NULL,
					&info.pipelineCache);
			assert(res == VK_SUCCESS);
		};
		init_pipeline_cache();
	}

	/// init_pipeline REMARK
	{
		auto init_pipeline = [&](VkBool32 include_depth, VkBool32 include_vi =
				true) {
			VkResult U_ASSERT_ONLY res;

			VkDynamicState dynamicStateEnables[2];  // Viewport + Scissor
			VkPipelineDynamicStateCreateInfo dynamicState = { };
			memset(dynamicStateEnables, 0, sizeof dynamicStateEnables);
			dynamicState.sType =
					VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
			dynamicState.pNext = NULL;
			dynamicState.pDynamicStates = dynamicStateEnables;
			dynamicState.dynamicStateCount = 0;

			VkPipelineVertexInputStateCreateInfo vi;
			memset(&vi, 0, sizeof(vi));
			vi.sType =
					VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
			if (include_vi) {
				vi.pNext = NULL;
				vi.flags = 0;
				vi.vertexBindingDescriptionCount = 1;
				vi.pVertexBindingDescriptions = &info.vi_binding;
				vi.vertexAttributeDescriptionCount = 2;
				vi.pVertexAttributeDescriptions = info.vi_attribs;
			}
			VkPipelineInputAssemblyStateCreateInfo ia;
			ia.sType =
					VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
			ia.pNext = NULL;
			ia.flags = 0;
			ia.primitiveRestartEnable = VK_FALSE;
			ia.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;

			VkPipelineRasterizationStateCreateInfo rs;
			rs.sType =
					VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
			rs.pNext = NULL;
			rs.flags = 0;
			rs.polygonMode = VK_POLYGON_MODE_FILL;
			rs.cullMode = VK_CULL_MODE_BACK_BIT;
			rs.frontFace = VK_FRONT_FACE_CLOCKWISE;
			rs.depthClampEnable = VK_FALSE;
			rs.rasterizerDiscardEnable = VK_FALSE;
			rs.depthBiasEnable = VK_FALSE;
			rs.depthBiasConstantFactor = 0;
			rs.depthBiasClamp = 0;
			rs.depthBiasSlopeFactor = 0;
			rs.lineWidth = 1.0f;

			VkPipelineColorBlendStateCreateInfo cb;
			cb.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
			cb.flags = 0;
			cb.pNext = NULL;
			VkPipelineColorBlendAttachmentState att_state[1];
			att_state[0].colorWriteMask = 0xf;
			att_state[0].blendEnable = VK_FALSE;
			att_state[0].alphaBlendOp = VK_BLEND_OP_ADD;
			att_state[0].colorBlendOp = VK_BLEND_OP_ADD;
			att_state[0].srcColorBlendFactor = VK_BLEND_FACTOR_ZERO;
			att_state[0].dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
			att_state[0].srcAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
			att_state[0].dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
			cb.attachmentCount = 1;
			cb.pAttachments = att_state;
			cb.logicOpEnable = VK_FALSE;
			cb.logicOp = VK_LOGIC_OP_NO_OP;
			cb.blendConstants[0] = 1.0f;
			cb.blendConstants[1] = 1.0f;
			cb.blendConstants[2] = 1.0f;
			cb.blendConstants[3] = 1.0f;

			VkPipelineViewportStateCreateInfo vp = { };
			vp.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
			vp.pNext = NULL;
			vp.flags = 0;
#ifndef __ANDROID__
			vp.viewportCount = NUM_VIEWPORTS;
			dynamicStateEnables[dynamicState.dynamicStateCount++] =
					VK_DYNAMIC_STATE_VIEWPORT;
			vp.scissorCount = NUM_SCISSORS;
			dynamicStateEnables[dynamicState.dynamicStateCount++] =
					VK_DYNAMIC_STATE_SCISSOR;
			vp.pScissors = NULL;
			vp.pViewports = NULL;
#else
		    // Temporary disabling dynamic viewport on Android because some of drivers doesn't
		    // support the feature.
		    VkViewport viewports;
		    viewports.minDepth = 0.0f;
		    viewports.maxDepth = 1.0f;
		    viewports.x = 0;
		    viewports.y = 0;
		    viewports.width = info.width;
		    viewports.height = info.height;
		    VkRect2D scissor;
		    scissor.extent.width = info.width;
		    scissor.extent.height = info.height;
		    scissor.offset.x = 0;
		    scissor.offset.y = 0;
		    vp.viewportCount = NUM_VIEWPORTS;
		    vp.scissorCount = NUM_SCISSORS;
		    vp.pScissors = &scissor;
		    vp.pViewports = &viewports;
		#endif
			VkPipelineDepthStencilStateCreateInfo ds;
			ds.sType =
					VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
			ds.pNext = NULL;
			ds.flags = 0;
			ds.depthTestEnable = include_depth;
			ds.depthWriteEnable = include_depth;
			ds.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
			ds.depthBoundsTestEnable = VK_FALSE;
			ds.stencilTestEnable = VK_FALSE;
			ds.back.failOp = VK_STENCIL_OP_KEEP;
			ds.back.passOp = VK_STENCIL_OP_KEEP;
			ds.back.compareOp = VK_COMPARE_OP_ALWAYS;
			ds.back.compareMask = 0;
			ds.back.reference = 0;
			ds.back.depthFailOp = VK_STENCIL_OP_KEEP;
			ds.back.writeMask = 0;
			ds.minDepthBounds = 0;
			ds.maxDepthBounds = 0;
			ds.stencilTestEnable = VK_FALSE;
			ds.front = ds.back;

			VkPipelineMultisampleStateCreateInfo ms;
			ms.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
			ms.pNext = NULL;
			ms.flags = 0;
			ms.pSampleMask = NULL;
			ms.rasterizationSamples = NUM_SAMPLES;
			ms.sampleShadingEnable = VK_FALSE;
			ms.alphaToCoverageEnable = VK_FALSE;
			ms.alphaToOneEnable = VK_FALSE;
			ms.minSampleShading = 0.0;

			VkGraphicsPipelineCreateInfo pipeline;
			pipeline.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
			pipeline.pNext = NULL;
			pipeline.layout = info.pipeline_layout;
			pipeline.basePipelineHandle = VK_NULL_HANDLE;
			pipeline.basePipelineIndex = 0;
			pipeline.flags = 0;
			pipeline.pVertexInputState = &vi;
			pipeline.pInputAssemblyState = &ia;
			pipeline.pRasterizationState = &rs;
			pipeline.pColorBlendState = &cb;
			pipeline.pTessellationState = NULL;
			pipeline.pMultisampleState = &ms;
			pipeline.pDynamicState = &dynamicState;
			pipeline.pViewportState = &vp;
			pipeline.pDepthStencilState = &ds;
			pipeline.pStages = info.shaderStages;
			pipeline.stageCount = 2;
			pipeline.renderPass = info.render_pass;
			pipeline.subpass = 0;

			res = oge::vkCreateGraphicsPipelines(info.device,
					info.pipelineCache, 1, &pipeline, NULL, &info.pipeline);
			assert(res == VK_SUCCESS);
		};
		init_pipeline(depthPresent);
	}

	/// Draw Cube REMARK
	VkFence drawFence;
	VkSemaphore imageAcquiredSemaphore;
	{
		VkClearValue clear_values[2];
		clear_values[0].color.float32[0] = 0.2f;
		clear_values[0].color.float32[1] = 0.2f;
		clear_values[0].color.float32[2] = 0.2f;
		clear_values[0].color.float32[3] = 0.2f;
		clear_values[1].depthStencil.depth = 1.0f;
		clear_values[1].depthStencil.stencil = 0;

		VkSemaphoreCreateInfo imageAcquiredSemaphoreCreateInfo;
		imageAcquiredSemaphoreCreateInfo.sType =
				VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
		imageAcquiredSemaphoreCreateInfo.pNext = NULL;
		imageAcquiredSemaphoreCreateInfo.flags = 0;

		res = oge::vkCreateSemaphore(info.device,
				&imageAcquiredSemaphoreCreateInfo,
				NULL, &imageAcquiredSemaphore);
		assert(res == VK_SUCCESS);

		// Get the index of the next available swapchain image:
		res = oge::vkAcquireNextImageKHR(info.device, info.swap_chain,
		UINT64_MAX, imageAcquiredSemaphore, VK_NULL_HANDLE,
				&info.current_buffer);
		// TODO: Deal with the VK_SUBOPTIMAL_KHR and VK_ERROR_OUT_OF_DATE_KHR
		// return codes
		assert(res == VK_SUCCESS);

		VkRenderPassBeginInfo rp_begin;
		rp_begin.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		rp_begin.pNext = NULL;
		rp_begin.renderPass = info.render_pass;
		rp_begin.framebuffer = info.framebuffers[info.current_buffer];
		rp_begin.renderArea.offset.x = 0;
		rp_begin.renderArea.offset.y = 0;
		rp_begin.renderArea.extent.width = info.width;
		rp_begin.renderArea.extent.height = info.height;
		rp_begin.clearValueCount = 2;
		rp_begin.pClearValues = clear_values;

		oge::vkCmdBeginRenderPass(info.cmd, &rp_begin,
				VK_SUBPASS_CONTENTS_INLINE);

		oge::vkCmdBindPipeline(info.cmd, VK_PIPELINE_BIND_POINT_GRAPHICS,
				info.pipeline);
		oge::vkCmdBindDescriptorSets(info.cmd, VK_PIPELINE_BIND_POINT_GRAPHICS,
				info.pipeline_layout, 0, NUM_DESCRIPTOR_SETS,
				info.desc_set.data(), 0, NULL);

		const VkDeviceSize offsets[1] = { 0 };
		oge::vkCmdBindVertexBuffers(info.cmd, 0, 1, &info.vertex_buffer.buf,
				offsets);

		/// init_viewports REMARK
		{
			auto init_viewports = [&]() {
#ifdef __ANDROID__
		// Disable dynamic viewport on Android. Some drive has an issue with the dynamic viewport
		// feature.
		#else
				info.viewport.height = (float) info.height;
				info.viewport.width = (float) info.width;
				info.viewport.minDepth = (float) 0.0f;
				info.viewport.maxDepth = (float) 1.0f;
				info.viewport.x = 0;
				info.viewport.y = 0;
				oge::vkCmdSetViewport(info.cmd, 0, NUM_VIEWPORTS,
						&info.viewport);
#endif
			};
			init_viewports();
		}

		/// init_scissors REMARK
		{
			auto init_scissors = [&]() {
#ifdef __ANDROID__
// Disable dynamic viewport on Android. Some drive has an issue with the dynamic scissors
// feature.
#else
				info.scissor.extent.width = info.width;
				info.scissor.extent.height = info.height;
				info.scissor.offset.x = 0;
				info.scissor.offset.y = 0;
				oge::vkCmdSetScissor(info.cmd, 0, NUM_SCISSORS, &info.scissor);
#endif
			};
			init_scissors();
		}

		oge::vkCmdDraw(info.cmd, 12 * 3, 1, 0, 0);
		oge::vkCmdEndRenderPass(info.cmd);
		res = oge::vkEndCommandBuffer(info.cmd);
		const VkCommandBuffer cmd_bufs[] = { info.cmd };
		VkFenceCreateInfo fenceInfo;
		fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
		fenceInfo.pNext = NULL;
		fenceInfo.flags = 0;
		oge::vkCreateFence(info.device, &fenceInfo, NULL, &drawFence);

		VkPipelineStageFlags pipe_stage_flags =
				VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		VkSubmitInfo submit_info[1] = { };
		submit_info[0].pNext = NULL;
		submit_info[0].sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		submit_info[0].waitSemaphoreCount = 1;
		submit_info[0].pWaitSemaphores = &imageAcquiredSemaphore;
		submit_info[0].pWaitDstStageMask = &pipe_stage_flags;
		submit_info[0].commandBufferCount = 1;
		submit_info[0].pCommandBuffers = cmd_bufs;
		submit_info[0].signalSemaphoreCount = 0;
		submit_info[0].pSignalSemaphores = NULL;

		/* Queue the command buffer for execution */
		res = oge::vkQueueSubmit(info.graphics_queue, 1, submit_info,
				drawFence);
		assert(res == VK_SUCCESS);

		/* Now present the image in the window */

		VkPresentInfoKHR present;
		present.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
		present.pNext = NULL;
		present.swapchainCount = 1;
		present.pSwapchains = &info.swap_chain;
		present.pImageIndices = &info.current_buffer;
		present.pWaitSemaphores = NULL;
		present.waitSemaphoreCount = 0;
		present.pResults = NULL;

		/* Make sure command buffer is finished before presenting */
		do {
			res = oge::vkWaitForFences(info.device, 1, &drawFence, VK_TRUE,
			FENCE_TIMEOUT);
		} while (res == VK_TIMEOUT);

		assert(res == VK_SUCCESS);
		res = oge::vkQueuePresentKHR(info.present_queue, &present);
		assert(res == VK_SUCCESS);

		/// wait_seconds REMARK
		{
			auto wait_seconds = [&](int seconds) {
#ifdef WIN32
			    Sleep(seconds * 1000);
			#elif defined(__ANDROID__)
			    sleep(seconds);
			#else
				sleep(seconds);
#endif
			};
			wait_seconds(1);
		}
	}

	/// CLEANUP REMARK
	{
		/// write_ppm REMARK
		{
			/// set_image_layout REMARK
			auto set_image_layout = [&](VkImage image,
					VkImageAspectFlags aspectMask,
					VkImageLayout old_image_layout,
					VkImageLayout new_image_layout,
					VkPipelineStageFlags src_stages,
					VkPipelineStageFlags dest_stages) {
				/* DEPENDS on info.cmd and info.queue initialized */

				assert(info.cmd != VK_NULL_HANDLE);
				assert(info.graphics_queue != VK_NULL_HANDLE);

				VkImageMemoryBarrier image_memory_barrier = { };
				image_memory_barrier.sType =
						VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
				image_memory_barrier.pNext = NULL;
				image_memory_barrier.srcAccessMask = 0;
				image_memory_barrier.dstAccessMask = 0;
				image_memory_barrier.oldLayout = old_image_layout;
				image_memory_barrier.newLayout = new_image_layout;
				image_memory_barrier.srcQueueFamilyIndex =
				VK_QUEUE_FAMILY_IGNORED;
				image_memory_barrier.dstQueueFamilyIndex =
				VK_QUEUE_FAMILY_IGNORED;
				image_memory_barrier.image = image;
				image_memory_barrier.subresourceRange.aspectMask = aspectMask;
				image_memory_barrier.subresourceRange.baseMipLevel = 0;
				image_memory_barrier.subresourceRange.levelCount = 1;
				image_memory_barrier.subresourceRange.baseArrayLayer = 0;
				image_memory_barrier.subresourceRange.layerCount = 1;

				switch (old_image_layout) {
				case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL:
					image_memory_barrier.srcAccessMask =
							VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
					break;

				case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
					image_memory_barrier.srcAccessMask =
							VK_ACCESS_TRANSFER_WRITE_BIT;
					break;

				case VK_IMAGE_LAYOUT_PREINITIALIZED:
					image_memory_barrier.srcAccessMask =
							VK_ACCESS_HOST_WRITE_BIT;
					break;

				default:
					break;
				}

				switch (new_image_layout) {
				case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
					image_memory_barrier.dstAccessMask =
							VK_ACCESS_TRANSFER_WRITE_BIT;
					break;

				case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:
					image_memory_barrier.dstAccessMask =
							VK_ACCESS_TRANSFER_READ_BIT;
					break;

				case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:
					image_memory_barrier.dstAccessMask =
							VK_ACCESS_SHADER_READ_BIT;
					break;

				case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL:
					image_memory_barrier.dstAccessMask =
							VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
					break;

				case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL:
					image_memory_barrier.dstAccessMask =
							VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
					break;

				default:
					break;
				}

				oge::vkCmdPipelineBarrier(info.cmd, src_stages, dest_stages, 0,
						0,
						NULL, 0, NULL, 1, &image_memory_barrier);
			};

			/// memory_type_from_properties REMARK
			auto memory_type_from_properties = [&](uint32_t typeBits,
					VkFlags requirements_mask, uint32_t *typeIndex) {
				// Search memtypes to find first index with those properties
				for (uint32_t i = 0; i < info.memory_properties.memoryTypeCount;
						i++) {
					if ((typeBits & 1) == 1) {
						// Type is available, does it match user properties?
						if ((info.memory_properties.memoryTypes[i].propertyFlags
								& requirements_mask) == requirements_mask) {
							*typeIndex = i;
							return true;
						}
					}
					typeBits >>= 1;
				}
				// No memory types matched, return failure
				return false;
			};

			auto write_ppm = [&](const char *basename) {
				std::string filename;
				int x, y;
				VkResult res;

				VkImageCreateInfo image_create_info = { };
				image_create_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
				image_create_info.pNext = NULL;
				image_create_info.imageType = VK_IMAGE_TYPE_2D;
				image_create_info.format = info.format;
				image_create_info.extent.width = info.width;
				image_create_info.extent.height = info.height;
				image_create_info.extent.depth = 1;
				image_create_info.mipLevels = 1;
				image_create_info.arrayLayers = 1;
				image_create_info.samples = VK_SAMPLE_COUNT_1_BIT;
				image_create_info.tiling = VK_IMAGE_TILING_LINEAR;
				image_create_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
				image_create_info.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT;
				image_create_info.queueFamilyIndexCount = 0;
				image_create_info.pQueueFamilyIndices = NULL;
				image_create_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
				image_create_info.flags = 0;

				VkMemoryAllocateInfo mem_alloc = { };
				mem_alloc.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
				mem_alloc.pNext = NULL;
				mem_alloc.allocationSize = 0;
				mem_alloc.memoryTypeIndex = 0;

				VkImage mappableImage;
				VkDeviceMemory mappableMemory;

				/* Create a mappable image */
				res = oge::vkCreateImage(info.device, &image_create_info, NULL,
						&mappableImage);
				assert(res == VK_SUCCESS);

				VkMemoryRequirements mem_reqs;
				oge::vkGetImageMemoryRequirements(info.device, mappableImage,
						&mem_reqs);

				mem_alloc.allocationSize = mem_reqs.size;

				/* Find the memory type that is host mappable */
				bool U_ASSERT_ONLY pass = memory_type_from_properties(
						mem_reqs.memoryTypeBits,
						VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
								| VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
						&mem_alloc.memoryTypeIndex);
				assert(pass && "No mappable, coherent memory");

				/* allocate memory */
				res = oge::vkAllocateMemory(info.device, &mem_alloc, NULL,
						&(mappableMemory));
				assert(res == VK_SUCCESS);

				/* bind memory */
				res = oge::vkBindImageMemory(info.device, mappableImage,
						mappableMemory, 0);
				assert(res == VK_SUCCESS);

				VkCommandBufferBeginInfo cmd_buf_info = { };
				cmd_buf_info.sType =
						VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
				cmd_buf_info.pNext = NULL;
				cmd_buf_info.flags = 0;
				cmd_buf_info.pInheritanceInfo = NULL;

				res = oge::vkBeginCommandBuffer(info.cmd, &cmd_buf_info);
				set_image_layout(mappableImage, VK_IMAGE_ASPECT_COLOR_BIT,
						VK_IMAGE_LAYOUT_UNDEFINED,
						VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
						VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
						VK_PIPELINE_STAGE_TRANSFER_BIT);

				set_image_layout(info.buffers[info.current_buffer].image,
						VK_IMAGE_ASPECT_COLOR_BIT,
						VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
						VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
						VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
						VK_PIPELINE_STAGE_TRANSFER_BIT);

				VkImageCopy copy_region;
				copy_region.srcSubresource.aspectMask =
						VK_IMAGE_ASPECT_COLOR_BIT;
				copy_region.srcSubresource.mipLevel = 0;
				copy_region.srcSubresource.baseArrayLayer = 0;
				copy_region.srcSubresource.layerCount = 1;
				copy_region.srcOffset.x = 0;
				copy_region.srcOffset.y = 0;
				copy_region.srcOffset.z = 0;
				copy_region.dstSubresource.aspectMask =
						VK_IMAGE_ASPECT_COLOR_BIT;
				copy_region.dstSubresource.mipLevel = 0;
				copy_region.dstSubresource.baseArrayLayer = 0;
				copy_region.dstSubresource.layerCount = 1;
				copy_region.dstOffset.x = 0;
				copy_region.dstOffset.y = 0;
				copy_region.dstOffset.z = 0;
				copy_region.extent.width = info.width;
				copy_region.extent.height = info.height;
				copy_region.extent.depth = 1;

				/* Put the copy command into the command buffer */
				oge::vkCmdCopyImage(info.cmd,
						info.buffers[info.current_buffer].image,
						VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, mappableImage,
						VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &copy_region);

				set_image_layout(mappableImage, VK_IMAGE_ASPECT_COLOR_BIT,
						VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
						VK_IMAGE_LAYOUT_GENERAL, VK_PIPELINE_STAGE_TRANSFER_BIT,
						VK_PIPELINE_STAGE_HOST_BIT);

				res = oge::vkEndCommandBuffer(info.cmd);
				assert(res == VK_SUCCESS);
				const VkCommandBuffer cmd_bufs[] = { info.cmd };
				VkFenceCreateInfo fenceInfo;
				VkFence cmdFence;
				fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
				fenceInfo.pNext = NULL;
				fenceInfo.flags = 0;
				oge::vkCreateFence(info.device, &fenceInfo, NULL, &cmdFence);

				VkSubmitInfo submit_info[1] = { };
				submit_info[0].pNext = NULL;
				submit_info[0].sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
				submit_info[0].waitSemaphoreCount = 0;
				submit_info[0].pWaitSemaphores = NULL;
				submit_info[0].pWaitDstStageMask = NULL;
				submit_info[0].commandBufferCount = 1;
				submit_info[0].pCommandBuffers = cmd_bufs;
				submit_info[0].signalSemaphoreCount = 0;
				submit_info[0].pSignalSemaphores = NULL;

				/* Queue the command buffer for execution */
				res = oge::vkQueueSubmit(info.graphics_queue, 1, submit_info,
						cmdFence);
				assert(res == VK_SUCCESS);

				/* Make sure command buffer is finished before mapping */
				do {
					res = oge::vkWaitForFences(info.device, 1, &cmdFence,
					VK_TRUE,
					FENCE_TIMEOUT);
				} while (res == VK_TIMEOUT);
				assert(res == VK_SUCCESS);

				oge::vkDestroyFence(info.device, cmdFence, NULL);

				filename.append(basename);
				filename.append(".ppm");

				VkImageSubresource subres = { };
				subres.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
				subres.mipLevel = 0;
				subres.arrayLayer = 0;
				VkSubresourceLayout sr_layout;
				oge::vkGetImageSubresourceLayout(info.device, mappableImage,
						&subres, &sr_layout);

				char *ptr;
				res = oge::vkMapMemory(info.device, mappableMemory, 0,
						mem_reqs.size, 0, (void**) &ptr);
				assert(res == VK_SUCCESS);

				ptr += sr_layout.offset;
				std::ofstream file(filename.c_str(), std::ios::binary);

				file << "P6\n";
				file << info.width << " ";
				file << info.height << "\n";
				file << 255 << "\n";

				for (y = 0; y < info.height; y++) {
					const int *row = (const int*) ptr;
					int swapped;

					if (info.format == VK_FORMAT_B8G8R8A8_UNORM
							|| info.format == VK_FORMAT_B8G8R8A8_SRGB) {
						for (x = 0; x < info.width; x++) {
							swapped = (*row & 0xff00ff00)
									| (*row & 0x000000ff) << 16
									| (*row & 0x00ff0000) >> 16;
							file.write((char*) &swapped, 3);
							row++;
						}
					} else if (info.format == VK_FORMAT_R8G8B8A8_UNORM) {
						for (x = 0; x < info.width; x++) {
							file.write((char*) row, 3);
							row++;
						}
					} else {
						printf(
								"Unrecognized image format - will not write image files");
			break;
		}

					ptr += sr_layout.rowPitch;
				}

				file.close();
				oge::vkUnmapMemory(info.device, mappableMemory);
				oge::vkDestroyImage(info.device, mappableImage, NULL);
				oge::vkFreeMemory(info.device, mappableMemory, NULL);
			};
			if (info.save_images) {
				write_ppm("15-draw_cube");
			}
		}

		oge::vkDestroySemaphore(info.device, imageAcquiredSemaphore, NULL);
		oge::vkDestroyFence(info.device, drawFence, NULL);
		/// destroy_pipeline
		{
			oge::vkDestroyPipeline(info.device, info.pipeline, NULL);
		}
		/// destroy_pipeline_cache
		{
			oge::vkDestroyPipelineCache(info.device, info.pipelineCache, NULL);
		}
		/// destroy_descriptor_pool
		{
			oge::vkDestroyDescriptorPool(info.device, info.desc_pool, NULL);
		}
		/// destroy_vertex_buffer
		{
			oge::vkDestroyBuffer(info.device, info.vertex_buffer.buf, NULL);
			oge::vkFreeMemory(info.device, info.vertex_buffer.mem, NULL);
		}
		/// destroy_framebuffers
		{
			for (uint32_t i = 0; i < info.swapchainImageCount; i++) {
				oge::vkDestroyFramebuffer(info.device, info.framebuffers[i],
				NULL);
			}
			free(info.framebuffers);
		}
		/// destroy_shaders
		{
			oge::vkDestroyShaderModule(info.device, info.shaderStages[0].module,
			NULL);
			oge::vkDestroyShaderModule(info.device, info.shaderStages[1].module,
			NULL);
		}
		/// destroy_renderpass
		{
			oge::vkDestroyRenderPass(info.device, info.render_pass, NULL);
		}
		/// destroy_descriptor_and_pipeline_layouts
		{
			for (int i = 0; i < NUM_DESCRIPTOR_SETS; i++) {
				oge::vkDestroyDescriptorSetLayout(info.device,
						info.desc_layout[i], NULL);
			}
			oge::vkDestroyPipelineLayout(info.device, info.pipeline_layout,
			NULL);
		}
		/// destroy_uniform_buffer(info);
		{
			oge::vkDestroyBuffer(info.device, info.uniform_data.buf, NULL);
			oge::vkFreeMemory(info.device, info.uniform_data.mem, NULL);
		}
		/// destroy_depth_buffer(info);
		{
			oge::vkDestroyImageView(info.device, info.depth.view, NULL);
			oge::vkDestroyImage(info.device, info.depth.image, NULL);
			oge::vkFreeMemory(info.device, info.depth.mem, NULL);
		}
		/// destroy_swap_chain(info);
		{
			for (uint32_t i = 0; i < info.swapchainImageCount; i++) {
				oge::vkDestroyImageView(info.device, info.buffers[i].view,
				NULL);
			}
			oge::vkDestroySwapchainKHR(info.device, info.swap_chain, NULL);
		}
		/// destroy_command_buffer(info);
		{
			VkCommandBuffer cmd_bufs[1] = { info.cmd };
			oge::vkFreeCommandBuffers(info.device, info.cmd_pool, 1, cmd_bufs);
		}
		/// destroy_command_pool(info);
		{
			oge::vkDestroyCommandPool(info.device, info.cmd_pool, NULL);
		}
		/// destroy_device(info);
		{
			oge::vkDeviceWaitIdle(info.device);
			oge::vkDestroyDevice(info.device, NULL);
		}
		/// destroy_window(info);
		{
			oge::vkDestroySurfaceKHR(info.inst, info.surface, NULL);
			xcb_destroy_window(info.connection, info.window);
			xcb_disconnect(info.connection);
		}
		/// destroy_instance(info);
		{
			oge::vkDestroyInstance(info.inst, NULL);
		}
	}

	return EXIT_SUCCESS;
}

