/**
 * 09-init_descriptor_set.cpp
 *
 *  Created on: Apr 7, 2021
 *      Author: beopenbefree
 */

#include "samples.h"

#define APP_SHORT_NAME "sample_09_init_descriptor_set"

std::string init_descriptor_set(Info &info, bool use_texture) {

	/// Creating a descriptor pool
	std::vector<VkDescriptorPoolSize> type_count { {
			VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1 } };
	if (use_texture) {
		type_count.push_back( { VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1 });
	}
	if (!oge::createDescriptorPool(info.device, 0, 1, type_count,
			info.desc_pool)) {
		return APP_SHORT_NAME"::""Creating a descriptor pool";
	}

	/// Allocating descriptor sets
	if (!oge::allocateDescriptorSets(info.device, info.desc_pool,
			info.desc_layout, info.desc_set)) {
		return APP_SHORT_NAME"::""Allocating descriptor sets";
	}

	/// Updating descriptor sets
	std::vector<oge::ImageDescriptorInfo> image_descriptor_infos { };
	if (use_texture) {
		image_descriptor_infos.push_back(
				{ info.desc_set[0], 1, 0,
						VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, {
								info.texture_data.image_info } });
	}
	oge::updateDescriptorSets(info.device, image_descriptor_infos,
			{ { info.desc_set[0], 0, 0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, {
					info.uniform_data.buffer_info } } }, { }, { });

	return "";
}

std::string sample_09_init_descriptor_set(int argc, char *argv[],
		GLFWwindow *window) {
	std::string result { "" };
	struct Info info { };
	info.appName = APP_SHORT_NAME;

	result = init_instance_wsi(info);
	if (!result.empty()) {
		return result;
	}
	result = enumerate_devices(info);
	if (!result.empty()) {
		return result;
	}
	result = init_device_wsi(info, window);
	if (!result.empty()) {
		return result;
	}
	result = init_uniform_buffer(info);
	if (!result.empty()) {
		return result;
	}
	result = init_descriptor_and_pipeline_layouts(info, false, 0);
	if (!result.empty()) {
		return result;
	}

	/* VULKAN_KEY_START */

	result = init_descriptor_set(info);
	if (!result.empty()) {
		return result;
	}

	/// Checking the window close flag
	while (!glfwWindowShouldClose(window)) {
		/// Processing events
		glfwPollEvents();
	}

	/// Destroying a descriptor pool
	oge::destroyDescriptorPool(info.device, info.desc_pool);

	/* VULKAN_KEY_END */

	oge::destroyDescriptorSetLayout(info.device, *info.desc_layout.data());
	oge::destroyPipelineLayout(info.device, info.pipeline_layout);
	oge::destroyBuffer(info.device, info.uniform_data.buf);
	oge::freeMemoryObject(info.device, info.uniform_data.mem);
	oge::destroyPresentationSurface(info.instance, info.surface);
	oge::destroyLogicalDevice(info.device);
	oge::destroyVulkanInstance(info.instance);

	return result;
}

