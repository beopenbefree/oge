/**
 * 12-init_frame_buffers.cpp
 *
 *  Created on: Apr 10, 2021
 *      Author: beopenbefree
 */

#include "samples.h"
#include <cassert>

#define APP_SHORT_NAME "sample_12_init_frame_buffers"

constexpr bool depthPresent { true };

std::string init_frame_buffers(Info &info) {

	/// Getting handles of swapchain images
	std::vector<VkImage> swapchain_images;
	if (!oge::getHandlesOfSwapchainImages(info.device, info.swap_chain,
			swapchain_images)) {
		return APP_SHORT_NAME"::""Getting handles of swapchain images";
	}
	assert(swapchain_images.size() == info.buffers.size());

	for (const auto &buf : info.buffers) {
		info.framebuffers.push_back(VK_NULL_HANDLE);
		/// Creating a framebuffer
		if (!oge::createFramebuffer(info.device, info.render_pass, { buf.view,
				info.depth.view }, sample_main[0].WIN_WIDTH,
				sample_main[0].WIN_HEIGHT, 1, info.framebuffers.back())) {
			return APP_SHORT_NAME"::""Creating a framebuffer";
		}
	}

	return "";
}

std::string sample_12_init_frame_buffers(int argc, char *argv[],
		GLFWwindow *window) {
	std::string result { "" };
	struct Info info { };
	info.appName = APP_SHORT_NAME;

	result = init_instance_wsi(info);
	if (!result.empty()) {
		return result;
	}
	result = enumerate_devices(info);
	if (!result.empty()) {
		return result;
	}
	result = init_swapchain(info, window);
	if (!result.empty()) {
		return result;
	}

	init_device_queue(info);

	result = init_command_buffer(info);
	if (!result.empty()) {
		return result;
	}
	result = execute_begin_command_buffer(info);
	if (!result.empty()) {
		return result;
	}
	result = init_depth_buffer(info);
	if (!result.empty()) {
		return result;
	}
	result = init_render_pass(info, depthPresent);
	if (!result.empty()) {
		return result;
	}

	/* VULKAN_KEY_START */

	result = init_frame_buffers(info);
	if (!result.empty()) {
		return result;
	}

	/// execute end command buffer
	result = execute_end_command_buffer(info);
	if (!result.empty()) {
		return result;
	}

	/// execute queue command buffer
	result = execute_queue_command_buffer(info);
	if (!result.empty()) {
		return result;
	}

	/// Checking the window close flag
	while (!glfwWindowShouldClose(window)) {
		/// Processing events
		glfwPollEvents();
	}

	for (auto &fbuf : info.framebuffers) {
		/// Destroying a framebuffer
		oge::destroyFramebuffer(info.device, fbuf);
	}

	/* VULKAN_KEY_END */

	oge::destroyRenderPass(info.device, info.render_pass);
	oge::destroyImageView(info.device, info.depth.view);
	oge::destroyImage(info.device, info.depth.image);
	oge::freeMemoryObject(info.device, info.depth.mem);
	oge::freeCommandBuffers(info.device, info.cmd_pool, info.cmd);
	oge::destroyCommandPool(info.device, info.cmd_pool);
	for (auto &buffer : info.buffers) {
		oge::destroyImageView(info.device, buffer.view);
	}
	oge::destroySwapchain(info.device, info.swap_chain);
	oge::destroyPresentationSurface(info.instance, info.surface);
	oge::destroyLogicalDevice(info.device);
	oge::destroyVulkanInstance(info.instance);

	return result;
}

