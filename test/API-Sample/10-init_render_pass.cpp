/**
 * 10-init_render_pass.cpp
 *
 *  Created on: Apr 8, 2021
 *      Author: beopenbefree
 */

#include "samples.h"

#define APP_SHORT_NAME "sample_10_init_render_pass"

std::string init_render_pass(Info &info) {

	/// Creating a semaphore
	if (!oge::createSemaphore(info.device, info.imageAcquiredSemaphore)) {
		return APP_SHORT_NAME"::""Creating a semaphore";
	}

	/// Acquiring a swapchain image
	if (!oge::acquireSwapchainImage(info.device, info.swap_chain,
			info.imageAcquiredSemaphore, VK_NULL_HANDLE, info.current_buffer)) {
		return APP_SHORT_NAME"::""Acquiring a swapchain image";
	}

	std::string result = init_render_pass(info, true, true,
			VK_IMAGE_LAYOUT_PRESENT_SRC_KHR, VK_IMAGE_LAYOUT_UNDEFINED);
	if (!result.empty()) {
		return result;
	}

	return "";
}

std::string init_render_pass(Info &info, bool include_depth, bool clear,
		VkImageLayout finalLayout, VkImageLayout initialLayout) {

	/// need attachments for render target and depth buffer
	std::vector<VkAttachmentDescription> attachments { { 0, info.format,
	NUM_SAMPLES, (
			clear ? VK_ATTACHMENT_LOAD_OP_CLEAR : VK_ATTACHMENT_LOAD_OP_LOAD),
			VK_ATTACHMENT_STORE_OP_STORE, VK_ATTACHMENT_LOAD_OP_DONT_CARE,
			VK_ATTACHMENT_STORE_OP_DONT_CARE, initialLayout, finalLayout } };
	if (include_depth) {
		attachments.push_back(
				{ 0, info.depth.format,
				NUM_SAMPLES,
						(clear ?
								VK_ATTACHMENT_LOAD_OP_CLEAR :
								VK_ATTACHMENT_LOAD_OP_DONT_CARE),
						VK_ATTACHMENT_STORE_OP_STORE,
						VK_ATTACHMENT_LOAD_OP_DONT_CARE,
						VK_ATTACHMENT_STORE_OP_STORE, VK_IMAGE_LAYOUT_UNDEFINED,
						VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL });
	}

	VkAttachmentReference depth_reference = { 1,
			VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL };
	std::vector<oge::SubpassParameters> subpasses { {
			VK_PIPELINE_BIND_POINT_GRAPHICS, { }, { { 0,
					VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL } }, { }, (
					include_depth ? &depth_reference : nullptr), { } } };

	/// subpass dependency to wait for wsi image acquired semaphore before starting layout transition
	std::vector<VkSubpassDependency> subpass_dependency { {
	VK_SUBPASS_EXTERNAL, 0, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
			VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, 0,
			VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, 0 } };

	/// Creating a render pass
	if (!oge::createRenderPass(info.device, attachments, subpasses,
			subpass_dependency, info.render_pass)) {
		return APP_SHORT_NAME"::""Creating a render pass";
	}

	return "";
}

std::string sample_10_init_render_pass(int argc, char *argv[],
		GLFWwindow *window) {
	std::string result { "" };
	struct Info info { };
	info.appName = APP_SHORT_NAME;

	result = init_instance_wsi(info);
	if (!result.empty()) {
		return result;
	}
	result = enumerate_devices(info);
	if (!result.empty()) {
		return result;
	}
	result = init_swapchain(info, window);
	if (!result.empty()) {
		return result;
	}
	init_device_queue(info);
	result = init_depth_buffer(info);
	if (!result.empty()) {
		return result;
	}

	/* VULKAN_KEY_START */

	result = init_render_pass(info);
	if (!result.empty()) {
		return result;
	}

	/// Checking the window close flag
	while (!glfwWindowShouldClose(window)) {
		/// Processing events
		glfwPollEvents();
	}

	/// Destroying a render pass
	oge::destroyRenderPass(info.device, info.render_pass);

	/// Destroying a semaphore
	oge::destroySemaphore(info.device, info.imageAcquiredSemaphore);

	/* VULKAN_KEY_END */

	oge::destroyImageView(info.device, info.depth.view);
	oge::destroyImage(info.device, info.depth.image);
	oge::freeMemoryObject(info.device, info.depth.mem);
	for (auto &buffer : info.buffers) {
		/// Destroying an image view
		oge::destroyImageView(info.device, buffer.view);
	}
	oge::destroySwapchain(info.device, info.swap_chain);
	oge::destroyPresentationSurface(info.instance, info.surface);
	oge::destroyLogicalDevice(info.device);
	oge::destroyVulkanInstance(info.instance);

	return result;
}

