/**
 * 13-init_vertex_buffer.cpp
 *
 *  Created on: Apr 11, 2021
 *      Author: beopenbefree
 */

#include "samples.h"

#define APP_SHORT_NAME "sample_13_init_vertex_buffer"

constexpr bool depthPresent { true };

namespace {

#include "data/cube_data.h"

}  // namespace

std::string init_vertex_buffer(Info &info, const void *vertexData,
		uint32_t dataSize, uint32_t dataStride, bool use_texture) {

	/// Creating a buffer
	if (!oge::createBuffer(info.device, dataSize,
			VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, info.vertex_buffer.buf)) {
		return APP_SHORT_NAME"::""Creating a buffer";
	}

	/// Allocating and binding a memory object for a buffer
	if (!oge::allocateAndBindMemoryObjectToBuffer(info.gpus[0], info.device,
			info.vertex_buffer.buf,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
					| VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
			info.vertex_buffer.mem)) {
		return APP_SHORT_NAME"::""Allocating and binding a memory object for a buffer";
	}

	/// Mapping, updating and unmapping host-visible memory
	if (!oge::mapUpdateAndUnmapHostVisibleMemory(info.device,
			info.vertex_buffer.mem, 0, dataSize, (void*) vertexData, true,
			nullptr)) {
		return APP_SHORT_NAME"::""Creating a uniform buffer";
	}

	/// we will need this info when creating the pipeline
	info.vi_binding = { 0, dataStride, VK_VERTEX_INPUT_RATE_VERTEX };

	info.vi_attribs =
			{ { 0, 0, VK_FORMAT_R32G32B32A32_SFLOAT, 0 }, { 1, 0,
					use_texture ?
							VK_FORMAT_R32G32_SFLOAT :
							VK_FORMAT_R32G32B32A32_SFLOAT, 16 } };

	return "";
}

std::string sample_13_init_vertex_buffer(int argc, char *argv[],
		GLFWwindow *window) {
	std::string result { "" };
	struct Info info { };
	info.appName = APP_SHORT_NAME;

	result = init_instance_wsi(info);
	if (!result.empty()) {
		return result;
	}
	result = enumerate_devices(info);
	if (!result.empty()) {
		return result;
	}
	result = init_swapchain(info, window);
	if (!result.empty()) {
		return result;
	}

	init_device_queue(info);

	result = init_command_buffer(info);
	if (!result.empty()) {
		return result;
	}
	result = execute_begin_command_buffer(info);
	if (!result.empty()) {
		return result;
	}
	result = init_depth_buffer(info);
	if (!result.empty()) {
		return result;
	}
	result = init_render_pass(info, depthPresent);
	if (!result.empty()) {
		return result;
	}
	result = init_frame_buffers(info);
	if (!result.empty()) {
		return result;
	}

	/* VULKAN_KEY_START */

	result = init_vertex_buffer(info, g_vb_solid_face_colors_Data,
			sizeof(g_vb_solid_face_colors_Data),
			sizeof(g_vb_solid_face_colors_Data[0]), false);
	if (!result.empty()) {
		return result;
	}

	/// Creating a semaphore
	VkSemaphore imageAcquiredSemaphore;
	if (!oge::createSemaphore(info.device, imageAcquiredSemaphore)) {
		return APP_SHORT_NAME"::""Creating a semaphore";
	}

	/// Acquiring a swapchain image
	if (!oge::acquireSwapchainImage(info.device, info.swap_chain,
			imageAcquiredSemaphore, VK_NULL_HANDLE, info.current_buffer)) {
		return APP_SHORT_NAME"::""Acquiring a swapchain image";
	}

	/// Beginning a render pass
	oge::beginRenderPass(info.cmd[0], info.render_pass,
			info.framebuffers[info.current_buffer], { { 0, 0 }, {
					static_cast<uint32_t>(sample_main[0].WIN_WIDTH),
					static_cast<uint32_t>(sample_main[0].WIN_HEIGHT) } }, { {
					0.2f, 0.2f, 0.2f, 0.2f }, { 1.0f, 0.0f } },
			VK_SUBPASS_CONTENTS_INLINE);

	/// Binding vertex buffers
	oge::bindVertexBuffers(info.cmd[0], 0, { { info.vertex_buffer.buf, 0 } });

	/// Ending a render pass
	oge::endRenderPass(info.cmd[0]);

	/// execute end command buffer
	result = execute_end_command_buffer(info);
	if (!result.empty()) {
		return result;
	}

	/// execute queue command buffer
	result = execute_queue_command_buffer(info);
	if (!result.empty()) {
		return result;
	}

	/// Checking the window close flag
	while (!glfwWindowShouldClose(window)) {
		/// Processing events
		glfwPollEvents();
	}

	/// Destroying a semaphore
	oge::destroySemaphore(info.device, imageAcquiredSemaphore);

	/// Destroying a buffer
	oge::destroyBuffer(info.device, info.vertex_buffer.buf);

	/// Freeing a memory object
	oge::freeMemoryObject(info.device, info.vertex_buffer.mem);

	/* VULKAN_KEY_END */

	for (auto &fbuf : info.framebuffers) {
		oge::destroyFramebuffer(info.device, fbuf);
	}
	oge::destroyRenderPass(info.device, info.render_pass);
	oge::destroyImageView(info.device, info.depth.view);
	oge::destroyImage(info.device, info.depth.image);
	oge::freeMemoryObject(info.device, info.depth.mem);
	oge::freeCommandBuffers(info.device, info.cmd_pool, info.cmd);
	oge::destroyCommandPool(info.device, info.cmd_pool);
	for (auto &buffer : info.buffers) {
		oge::destroyImageView(info.device, buffer.view);
	}
	oge::destroySwapchain(info.device, info.swap_chain);
	oge::destroyPresentationSurface(info.instance, info.surface);
	oge::destroyLogicalDevice(info.device);
	oge::destroyVulkanInstance(info.instance);

	return result;
}

