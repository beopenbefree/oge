/**
 * 01-init_instance.cpp
 *
 *  Created on: Mar 29, 2021
 *      Author: beopenbefree
 */

#include "samples.h"

#define APP_SHORT_NAME "sample_01_init_instance"

std::string init_instance(Info &info) {
	/// Check available instance extensions
	if (!oge::checkAvailableInstanceExtensions(
			info.instance_extension_properties)) {
		return APP_SHORT_NAME"::""Check available instance extensions";
	}

	/// Creating an instance
	if (!oge::createVulkanInstance(info.instance_extension_names, info.appName,
			info.instance)) {
		return APP_SHORT_NAME"::""Creating an instance";
	}

	/// Load instance level functions
	if (!oge::loadInstanceLevelFunctions(info.instance,
			info.instance_extension_names)) {
		return APP_SHORT_NAME"::""Load instance level functions";
	}
	return "";
}

std::string init_instance_wsi(Info &info) {
	/// Check available instance extensions
	if (!oge::checkAvailableInstanceExtensions(
			info.instance_extension_properties)) {
		return APP_SHORT_NAME"::""Check available instance extensions";
	}

	/// Creating an instance
	if (!oge::createVulkanInstanceWithWsiExtensionsEnabled(
			info.instance_extension_names, info.appName, info.instance)) {
		return APP_SHORT_NAME"::""Creating an instance";
	}

	/// Load instance level functions
	if (!oge::loadInstanceLevelFunctions(info.instance,
			info.instance_extension_names)) {
		return APP_SHORT_NAME"::""Load instance level functions";
	}
	return "";
}

std::string sample_01_init_instance(int argc, char *argv[],
		GLFWwindow *window) {
	std::string result { "" };
	struct Info info { };
	info.appName = APP_SHORT_NAME;

	/* VULKAN_KEY_START */

	result = init_instance(info);
	if (!result.empty()) {
		return result;
	}

	/// Checking the window close flag
	while (!glfwWindowShouldClose(window)) {
		/// Processing events
		glfwPollEvents();
	}

	/// Destroying a Vulkan Instance
	oge::destroyVulkanInstance(info.instance);

	/* VULKAN_KEY_END */

	return result;
}

