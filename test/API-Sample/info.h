/**
 * info.h
 *
 *  Created on: Mar 29, 2021
 *      Author: beopenbefree
 */

#ifndef TEST_INFO_H_
#define TEST_INFO_H_

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <limits>

#include "lib/instanceAndDevices.h"
#include "lib/commandBuffersAndSynchronization.h"
#include "lib/imagePresentation.h"
#include "lib/resourcesAndMemory.h"
#include "lib/descriptorSets.h"
#include "lib/graphicsAndComputePipelines.h"
#include "lib/renderPassesAndFramebuffers.h"
#include "lib/commandRecordingAndDrawing.h"

#define NUM_DESCRIPTOR_SETS 1
#define NUM_SAMPLES VK_SAMPLE_COUNT_1_BIT

struct swap_chain_buffer_data {
	VkImage image { VK_NULL_HANDLE };
	VkImageView view { VK_NULL_HANDLE };
};

struct depth_buffer_data {
	VkFormat format;
	VkImage image { VK_NULL_HANDLE };
	VkDeviceMemory mem { VK_NULL_HANDLE };
	VkImageView view { VK_NULL_HANDLE };
};

struct uniform_buffer_data {
	VkBuffer buf { VK_NULL_HANDLE };
	VkDeviceMemory mem { VK_NULL_HANDLE };
	VkDescriptorBufferInfo buffer_info;
};

struct vertex_buffer_data {
	VkBuffer buf { VK_NULL_HANDLE };
	VkDeviceMemory mem { VK_NULL_HANDLE };
	VkDescriptorBufferInfo buffer_info;
};

struct texture_data_struct {
	VkDescriptorImageInfo image_info;
};

struct Info {
	const char *appName { nullptr };
	std::vector<VkExtensionProperties> instance_extension_properties;
	VkInstance instance { VK_NULL_HANDLE };
	std::vector<const char*> instance_extension_names;
	std::vector<VkPhysicalDevice> gpus;
	std::vector<VkQueueFamilyProperties> queue_props;
	uint32_t graphics_queue_family_index { std::numeric_limits<uint32_t>::max() };
	VkQueue graphics_queue { VK_NULL_HANDLE };
	uint32_t present_queue_family_index { std::numeric_limits<uint32_t>::max() };
	VkQueue present_queue { VK_NULL_HANDLE };
	std::vector<oge::QueueInfo> requested_queues;
	std::vector<const char*> device_extension_names;
	VkDevice device { VK_NULL_HANDLE };
	VkCommandPool cmd_pool { VK_NULL_HANDLE };
	std::vector<VkCommandBuffer> cmd;
	VkSurfaceKHR surface { VK_NULL_HANDLE };
	VkFormat format;
	VkColorSpaceKHR image_color_space;
	VkSwapchainKHR swap_chain { VK_NULL_HANDLE };
	uint32_t current_buffer { std::numeric_limits<uint32_t>::max() };
	std::vector<swap_chain_buffer_data> buffers;
	depth_buffer_data depth;
	glm::mat4 Projection;
	glm::mat4 View;
	glm::mat4 Model;
	glm::mat4 Clip;
	glm::mat4 MVP;
	uniform_buffer_data uniform_data;
	std::vector<VkDescriptorSetLayout> desc_layout;
	VkPipelineLayout pipeline_layout { VK_NULL_HANDLE };
	VkDescriptorPool desc_pool { VK_NULL_HANDLE };
	std::vector<VkDescriptorSet> desc_set;
	VkSemaphore imageAcquiredSemaphore { VK_NULL_HANDLE };
	VkRenderPass render_pass { VK_NULL_HANDLE };
	std::vector<VkPipelineShaderStageCreateInfo> shaderStages { 2 };
	std::vector<VkFramebuffer> framebuffers;
	vertex_buffer_data vertex_buffer;
	VkVertexInputBindingDescription vi_binding;
	std::vector<VkVertexInputAttributeDescription> vi_attribs { 2 };
	texture_data_struct texture_data;
	VkPipelineCache pipelineCache { VK_NULL_HANDLE };
	std::vector<VkPipeline> pipelines;
	bool save_images = false;
};

#endif /* TEST_INFO_H_ */
