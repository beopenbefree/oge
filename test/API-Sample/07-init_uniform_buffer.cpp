/**
 * 07-init_uniform_buffer.cpp
 *
 *  Created on: Apr 4, 2021
 *      Author: beopenbefree
 */

#include "samples.h"

#define APP_SHORT_NAME "sample_07_init_uniform_buffer"

std::string init_uniform_buffer(Info &info) {

	/// set tranform matrices
	float fov = glm::radians(45.0f);
	float width = static_cast<float>(sample_main[0].WIN_HEIGHT);
	float height = static_cast<float>(sample_main[0].WIN_HEIGHT);
	if (width > height) {
		fov *= height / width;
	}
	info.Projection = glm::perspective(fov, width / height, 0.1f, 100.0f);
	info.View = glm::lookAt(glm::vec3(-5, 3, -10), // Camera is at (-5,3,-10), in World Space
	glm::vec3(0, 0, 0),     // and looks at the origin
	glm::vec3(0, -1, 0)     // Head is up (set to 0,-1,0 to look upside-down)
			);
	info.Model = glm::mat4(1.0f);
	// Vulkan clip space has inverted Y and half Z.
	info.Clip = glm::mat4(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.5f, 0.0f, 0.0f, 0.0f, 0.5f, 1.0f);

	info.MVP = info.Clip * info.Projection * info.View * info.Model;

	/// Creating a uniform buffer
	if (!oge::createUniformBuffer(info.gpus[0], info.device, sizeof(info.MVP),
			0, info.uniform_data.buf, info.uniform_data.mem,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
					| VK_MEMORY_PROPERTY_HOST_COHERENT_BIT)) {
		return APP_SHORT_NAME"::""Creating a uniform buffer";
	}

	/// Mapping, updating and unmapping host-visible memory
	if (!oge::mapUpdateAndUnmapHostVisibleMemory(info.device,
			info.uniform_data.mem, 0, sizeof(info.MVP),
			glm::value_ptr(info.MVP), true, nullptr)) {
		return APP_SHORT_NAME"::""Creating a uniform buffer";
	}

	/// set some data
	info.uniform_data.buffer_info.buffer = info.uniform_data.buf;
	info.uniform_data.buffer_info.offset = 0;
	info.uniform_data.buffer_info.range = sizeof(info.MVP);

	return "";
}

std::string sample_07_init_uniform_buffer(int argc, char *argv[],
		GLFWwindow *window) {
	std::string result { "" };
	struct Info info { };
	info.appName = APP_SHORT_NAME;

	result = init_instance_wsi(info);
	if (!result.empty()) {
		return result;
	}
	result = enumerate_devices(info);
	if (!result.empty()) {
		return result;
	}
	result = init_device_wsi(info, window);
	if (!result.empty()) {
		return result;
	}

	/* VULKAN_KEY_START */

	result = init_uniform_buffer(info);
	if (!result.empty()) {
		return result;
	}

	/// Checking the window close flag
	while (!glfwWindowShouldClose(window)) {
		/// Processing events
		glfwPollEvents();
	}

	/// Destroying a buffer
	oge::destroyBuffer(info.device, info.uniform_data.buf);

	/// Freeing a memory object
	oge::freeMemoryObject(info.device, info.uniform_data.mem);

	/* VULKAN_KEY_END */

	oge::destroyPresentationSurface(info.instance, info.surface);
	oge::destroyLogicalDevice(info.device);
	oge::destroyVulkanInstance(info.instance);

	return result;
}

