/**
 * samples.h
 *
 *  Created on: Mar 29, 2021
 *      Author: beopenbefree
 */

#ifndef TEST_API_SAMPLE_SAMPLES_H_
#define TEST_API_SAMPLE_SAMPLES_H_

#include "info.h"

using sample_main_ptr = std::string (*)(int, char *[], GLFWwindow*);

struct SAMPLE {
	sample_main_ptr sample;
	const int WIN_WIDTH;
	const int WIN_HEIGHT;
};
extern SAMPLE sample_main[];

// init instance
std::string sample_01_init_instance(int argc, char *argv[], GLFWwindow *window);
std::string init_instance(Info &info);
std::string init_instance_wsi(Info &info);
// enumerate device
std::string sample_02_enumerate_device(int argc, char *argv[],	GLFWwindow *window);
std::string enumerate_devices(Info &info);
// init device
std::string sample_03_init_device(int argc, char *argv[], GLFWwindow *window);
std::string init_device(Info &info);
// init command buffer
std::string sample_04_init_command_buffer(int argc, char *argv[], GLFWwindow *window);
std::string init_command_buffer(Info &info);
std::string execute_begin_command_buffer(Info &info);
std::string execute_end_command_buffer(Info &info);
std::string execute_queue_command_buffer(Info &info);
// init swapchain
std::string sample_05_init_swapchain(int argc, char *argv[], GLFWwindow *window);
std::string init_swapchain_extension(Info &info, GLFWwindow *window);
std::string init_device_wsi(Info &info, GLFWwindow *window);
std::string init_swapchain(Info &info, GLFWwindow *window);
void init_device_queue(Info &info);
// init depth buffer
std::string sample_06_init_depth_buffer(int argc, char *argv[], GLFWwindow *window);
std::string init_depth_buffer(Info &info);
// init uniform buffer
std::string sample_07_init_uniform_buffer(int argc, char *argv[], GLFWwindow *window);
std::string init_uniform_buffer(Info &info);
// init pipeline layout
std::string sample_08_init_pipeline_layout(int argc, char *argv[], GLFWwindow *window);
std::string init_pipeline_layout(Info &info);
std::string init_descriptor_and_pipeline_layouts(Info &info, bool use_texture, VkDescriptorSetLayoutCreateFlags descSetLayoutCreateFlags = 0);
std::string init_pipeline_cache(Info &info);
// init descriptor set
std::string sample_09_init_descriptor_set(int argc, char *argv[], GLFWwindow *window);
std::string init_descriptor_set(Info &info, bool use_texture = false);
// init render pass
std::string sample_10_init_render_pass(int argc, char *argv[], GLFWwindow *window);
std::string init_render_pass(Info &info);
std::string init_render_pass(Info &info, bool include_depth, bool clear = true, VkImageLayout finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR, VkImageLayout initialLayout = VK_IMAGE_LAYOUT_UNDEFINED);
// init shaders
std::string sample_11_init_shaders(int argc, char *argv[], GLFWwindow *window);
std::string init_shaders(Info &info, const uint32_t* shaders_vert, std::size_t len_vert, const uint32_t* shaders_frag, std::size_t len_frag);
// init frame buffers
std::string sample_12_init_frame_buffers(int argc, char *argv[], GLFWwindow *window);
std::string init_frame_buffers(Info &info);
// init vertex buffer
std::string sample_13_init_vertex_buffer(int argc, char *argv[], GLFWwindow *window);
std::string init_vertex_buffer(Info &info, const void *vertexData, uint32_t dataSize, uint32_t dataStride, bool use_texture);
// init pipeline
std::string sample_14_init_pipeline(int argc, char *argv[], GLFWwindow *window);
std::string init_pipeline(Info &info, VkBool32 include_depth, VkBool32 include_vi = true);
// draw cube
std::string sample_15_draw_cube(int argc, char *argv[], GLFWwindow *window);

#endif /* TEST_API_SAMPLE_SAMPLES_H_ */
