/**
 * samples.cpp
 *
 *  Created on: Mar 29, 2021
 *      Author: beopenbefree
 */

#include "samples.h"

SAMPLE sample_main[] = {
//		{ sample_01_init_instance, 800, 600},
//		{ sample_02_enumerate_device, 800, 600},
//		{ sample_03_init_device, 800, 600 },
//		{ sample_04_init_command_buffer, 800, 600 },
//		{ sample_05_init_swapchain, 800, 600},
//		{ sample_06_init_depth_buffer, 500, 500 },
//		{ sample_07_init_uniform_buffer, 800, 600 },
//		{ sample_08_init_pipeline_layout, 800, 600 },
//		{ sample_09_init_descriptor_set, 800, 600 },
//		{ sample_10_init_render_pass, 800, 600 },
//		{ sample_11_init_shaders, 800, 600 },
//		{ sample_12_init_frame_buffers, 800, 600 },
//		{ sample_13_init_vertex_buffer, 800, 600 },
//		{ sample_14_init_pipeline, 800, 600 },
		{ sample_15_draw_cube, 800, 600 },
};
