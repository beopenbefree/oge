/**
 * 15-draw_cube.cpp
 *
 *  Created on: Apr 14, 2021
 *      Author: beopenbefree
 */

#include "samples.h"

#define APP_SHORT_NAME "sample_15_draw_cube"

constexpr bool depthPresent { true };
constexpr uint64_t FENCE_TIMEOUT { 100000000 };

// Commands to be executed inside "data" directory to get SPIRV shaders:
// for vertex spirv "./generate_spirv.py '15-draw_cube.vert' '15-draw_cube.vert'.spv glslangValidator false"
// for fragment spirv "./generate_spirv.py '15-draw_cube.frag' '15-draw_cube.frag'.spv glslangValidator false"

namespace {

#include "data/cube_data.h"

#include "data/15-draw_cube.vert.spv"
#include "data/15-draw_cube.frag.spv"

}  // namespace

std::string sample_15_draw_cube(int argc, char *argv[], GLFWwindow *window) {
	std::string result { "" };
	struct Info info { };
	info.appName = APP_SHORT_NAME;

	result = init_instance_wsi(info);
	if (!result.empty()) {
		return result;
	}
	result = enumerate_devices(info);
	if (!result.empty()) {
		return result;
	}
	result = init_swapchain(info, window);
	if (!result.empty()) {
		return result;
	}

	init_device_queue(info);

	result = init_command_buffer(info);
	if (!result.empty()) {
		return result;
	}
	result = execute_begin_command_buffer(info);
	if (!result.empty()) {
		return result;
	}
	result = init_depth_buffer(info);
	if (!result.empty()) {
		return result;
	}
	result = init_uniform_buffer(info);
	if (!result.empty()) {
		return result;
	}
	result = init_descriptor_and_pipeline_layouts(info, false);
	if (!result.empty()) {
		return result;
	}
	result = init_render_pass(info, depthPresent);
	if (!result.empty()) {
		return result;
	}
	result = init_shaders(info, __draw_cube_vert,
			sizeof(__draw_cube_vert) / sizeof(uint32_t), __draw_cube_frag,
			sizeof(__draw_cube_frag) / sizeof(uint32_t));
	if (!result.empty()) {
		return result;
	}
	result = init_frame_buffers(info);
	if (!result.empty()) {
		return result;
	}
	result = init_vertex_buffer(info, g_vb_solid_face_colors_Data,
			sizeof(g_vb_solid_face_colors_Data),
			sizeof(g_vb_solid_face_colors_Data[0]), false);
	if (!result.empty()) {
		return result;
	}
	result = init_descriptor_set(info, false);
	if (!result.empty()) {
		return result;
	}
	result = init_pipeline_cache(info);
	if (!result.empty()) {
		return result;
	}
	result = init_pipeline(info, true);
	if (!result.empty()) {
		return result;
	}

	/* VULKAN_KEY_START */

	/// Creating a semaphore
	if (!oge::createSemaphore(info.device, info.imageAcquiredSemaphore)) {
		return APP_SHORT_NAME"::""Creating a semaphore";
	}

	// Get the index of the next available swapchain image:
	/// Acquiring a swapchain image
	if (!oge::acquireSwapchainImage(info.device, info.swap_chain,
			info.imageAcquiredSemaphore, VK_NULL_HANDLE, info.current_buffer)) {
		return APP_SHORT_NAME"::""Acquiring a swapchain image";
	}

	/// Beginning a render pass
	oge::beginRenderPass(info.cmd[0], info.render_pass,
			info.framebuffers[info.current_buffer], { { 0, 0 }, {
					static_cast<uint32_t>(sample_main[0].WIN_WIDTH),
					static_cast<uint32_t>(sample_main[0].WIN_HEIGHT) } }, { {
					0.2f, 0.2f, 0.2f, 0.2f }, { 1.0f, 0.0f } },
			VK_SUBPASS_CONTENTS_INLINE);

	/// Binding a pipeline object
	oge::bindPipelineObject(info.cmd[0], VK_PIPELINE_BIND_POINT_GRAPHICS,
			info.pipelines[0]);

	/// Binding descriptor sets
	oge::bindDescriptorSets(info.cmd[0], VK_PIPELINE_BIND_POINT_GRAPHICS,
			info.pipeline_layout, 0, info.desc_set, { });

	/// Binding vertex buffers
	oge::bindVertexBuffers(info.cmd[0], 0, { { info.vertex_buffer.buf, 0 } });

	/// Setting viewport states dynamically
	oge::setViewportStateDynamically(info.cmd[0], 0, { { 0, 0,
			static_cast<float>(sample_main[0].WIN_WIDTH),
			static_cast<float>(sample_main[0].WIN_HEIGHT), 0.0, 1.0, } });

	/// Setting scissor states dynamically
	oge::setScissorStateDynamically(info.cmd[0], 0, { { { 0, 0 }, {
			static_cast<uint32_t>(sample_main[0].WIN_WIDTH),
			static_cast<uint32_t>(sample_main[0].WIN_HEIGHT) } } });

	/// Drawing a geometry
	oge::drawGeometry(info.cmd[0], 12 * 3, 1, 0, 0);

	/// Ending a render pass
	oge::endRenderPass(info.cmd[0]);

	/// execute end command buffer
	result = execute_end_command_buffer(info);
	if (!result.empty()) {
		return result;
	}

	/// Creating a fence
	VkFence drawFence;
	if (!oge::createFence(info.device, false, drawFence)) {
		return APP_SHORT_NAME"::""Creating a fence";
	}

	/* Queue the command buffer for execution */
	/// Submitting command buffers to a queue
	if (!oge::submitCommandBuffersToQueue(info.graphics_queue,
			{ { info.imageAcquiredSemaphore,
					VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT } }, info.cmd,
			{ }, drawFence)) {
		return APP_SHORT_NAME"::""Submitting command buffers to a queue";
	}

	/* Make sure command buffer is finished before presenting */
	/// Waiting for fences
	if (!oge::waitForFences(info.device, { drawFence },
	VK_TRUE, FENCE_TIMEOUT)) {
		return APP_SHORT_NAME"::""Waiting for fences";
	};

	/* Now present the image in the window */
	/// Presenting an image
	if (!oge::presentImage(info.present_queue, { },
			{ { info.swap_chain, info.current_buffer } })) {
		return APP_SHORT_NAME"::""Presenting an image";
	};

	/// Checking the window close flag
	while (!glfwWindowShouldClose(window)) {
		/// Processing events
		glfwPollEvents();
	}

	/// Destroying a fence
	oge::destroyFence(info.device, drawFence);

	/// Destroying a semaphore
	oge::destroySemaphore(info.device, info.imageAcquiredSemaphore);

	/* VULKAN_KEY_END */

	for (auto &pipeline : info.pipelines) {
		oge::destroyPipeline(info.device, pipeline);
	}
	oge::destroyPipelineCache(info.device, info.pipelineCache);
	oge::destroyDescriptorPool(info.device, info.desc_pool);
	oge::destroyBuffer(info.device, info.vertex_buffer.buf);
	oge::freeMemoryObject(info.device, info.vertex_buffer.mem);
	for (auto &fbuf : info.framebuffers) {
		oge::destroyFramebuffer(info.device, fbuf);
	}
	oge::destroyShaderModule(info.device, info.shaderStages[0].module);
	oge::destroyShaderModule(info.device, info.shaderStages[1].module);
	oge::destroyRenderPass(info.device, info.render_pass);
	oge::destroyDescriptorSetLayout(info.device, *info.desc_layout.data());
	oge::destroyPipelineLayout(info.device, info.pipeline_layout);
	oge::destroyBuffer(info.device, info.uniform_data.buf);
	oge::freeMemoryObject(info.device, info.uniform_data.mem);
	oge::destroyImageView(info.device, info.depth.view);
	oge::destroyImage(info.device, info.depth.image);
	oge::freeMemoryObject(info.device, info.depth.mem);
	oge::freeCommandBuffers(info.device, info.cmd_pool, info.cmd);
	oge::destroyCommandPool(info.device, info.cmd_pool);
	for (auto &buffer : info.buffers) {
		oge::destroyImageView(info.device, buffer.view);
	}
	oge::destroySwapchain(info.device, info.swap_chain);
	oge::destroyPresentationSurface(info.instance, info.surface);
	oge::destroyLogicalDevice(info.device);
	oge::destroyVulkanInstance(info.instance);

	return result;
}

