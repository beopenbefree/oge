/**
 * 01-init_instance.cpp
 *
 *  Created on: Mar 29, 2021
 *      Author: beopenbefree
 */

#include <iostream>
#include "samples.h"

namespace {
/// Setting an error callback
void error_callback(int error, const char *description) {
	std::cerr << "Error: " << description << std::endl;
}

/// Receiving input events

void terminate(std::string msg = "ERROR:") {
	std::cerr << "ERROR: " << msg << std::endl;
	glfwTerminate();
	exit(EXIT_FAILURE);
}

}

int main(int argc, char **argv) {
	/// Setting an error callback
	glfwSetErrorCallback(error_callback);

	/// Initializing GLFW
	if (!glfwInit()) {
		exit(EXIT_FAILURE);
	}

	/// Connecting with vulkan loader library
	if (!oge::connectWithVulkanLoaderLibrary()) {
		terminate("Querying for Vulkan support");
	}

	/// Load vkGetInstanceProcAddr
	if (!oge::loadFunctionExportedFromVulkanLoaderLibrary()) {
		terminate("load vkGetInstanceProcAddr");
	}

	/// Load global level functions (vkCreateInstance, ...)
	if (!oge::loadGlobalLevelFunctions()) {
		terminate("load global level functions (vkCreateInstance, ...)");
	}

	/// Creating the window
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	GLFWwindow *window = glfwCreateWindow(sample_main[0].WIN_WIDTH,
			sample_main[0].WIN_HEIGHT, "test", nullptr, nullptr);

	if (!window) {
		terminate("Creating the window");
	}

	/// Call the Sample REMARK
	{
		std::string result = sample_main[0].sample(argc, argv, window); //fIXME
		if (!result.empty()) {
			terminate("ERROR: " + result);
		}
	}

	/// Destroying the window
	glfwDestroyWindow(window);

	/// Terminating GLFW
	glfwTerminate();
	return EXIT_SUCCESS;
}

