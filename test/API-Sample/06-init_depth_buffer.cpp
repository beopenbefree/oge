/**
 * 06-init_depth_buffer.cpp
 *
 *  Created on: Apr 3, 2021
 *      Author: beopenbefree
 */

#include "samples.h"

#define APP_SHORT_NAME "sample_06_init_depth_buffer"

#define NUM_SAMPLES VK_SAMPLE_COUNT_1_BIT

std::string init_depth_buffer(Info &info) {

	/// check VK_FORMAT_D16_UNORM support
	VkImageTiling tiling;
	info.depth.format = VK_FORMAT_D16_UNORM;
	VkFormatProperties props;
	oge::vkGetPhysicalDeviceFormatProperties(info.gpus[0], info.depth.format,
			&props);
	if (props.linearTilingFeatures
			& VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT) {
		tiling = VK_IMAGE_TILING_LINEAR;
	} else if (props.optimalTilingFeatures
			& VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT) {
		tiling = VK_IMAGE_TILING_OPTIMAL;
	} else {
		/* Try other depth formats? */
		return APP_SHORT_NAME"::""check VK_FORMAT_D16_UNORM support";
	}

	/// Creating an image
	uint32_t WIN_WIDTH = static_cast<uint32_t>(sample_main[0].WIN_WIDTH);
	uint32_t WIN_HEIGHT = static_cast<uint32_t>(sample_main[0].WIN_HEIGHT);
	if (!oge::createImage(info.device, VK_IMAGE_TYPE_2D, info.depth.format, {
			WIN_WIDTH, WIN_HEIGHT, 1 }, 1, 1,
	NUM_SAMPLES, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, false,
			info.depth.image, tiling)) {
		return APP_SHORT_NAME"::""Creating an image";
	}

	/// Allocating and binding a memory object to an image
	if (!oge::allocateAndBindMemoryObjectToImage(info.gpus[0], info.device,
			info.depth.image, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
			info.depth.mem)) {
		return APP_SHORT_NAME"::""Allocating and binding a memory object to an image";
	}

	/// Creating an image view
	if (!oge::createImageView(info.device, info.depth.image,
			VK_IMAGE_VIEW_TYPE_2D, info.depth.format, VK_IMAGE_ASPECT_DEPTH_BIT,
			info.depth.view, { VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G,
					VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A })) {
		return APP_SHORT_NAME"::""Creating an image view";
	}

	return "";
}

std::string sample_06_init_depth_buffer(int argc, char *argv[],
		GLFWwindow *window) {
	std::string result { "" };
	struct Info info { };
	info.appName = APP_SHORT_NAME;

	result = init_instance_wsi(info);
	if (!result.empty()) {
		return result;
	}
	result = enumerate_devices(info);
	if (!result.empty()) {
		return result;
	}
	result = init_device_wsi(info, window);
	if (!result.empty()) {
		return result;
	}

	/* VULKAN_KEY_START */

	result = init_depth_buffer(info);
	if (!result.empty()) {
		return result;
	}

	/// Checking the window close flag
	while (!glfwWindowShouldClose(window)) {
		/// Processing events
		glfwPollEvents();
	}

	/// Destroying an image view
	oge::destroyImageView(info.device, info.depth.view);

	/// Destroying an image
	oge::destroyImage(info.device, info.depth.image);

	/// Freeing a memory object
	oge::freeMemoryObject(info.device, info.depth.mem);

	/* VULKAN_KEY_END */

	oge::destroyPresentationSurface(info.instance, info.surface);
	oge::destroyLogicalDevice(info.device);
	oge::destroyVulkanInstance(info.instance);

	return result;
}

