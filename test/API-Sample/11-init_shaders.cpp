/**
 * 11-init_shaders.cpp
 *
 *  Created on: Apr 9, 2021
 *      Author: beopenbefree
 */

#include "samples.h"

#define APP_SHORT_NAME "sample_11_init_shaders"

// Commands to be executed inside "data" directory to get SPIRV shaders:
// for vertex spirv "./generate_spirv.py '11-init_shaders.vert' '11-init_shaders.vert'.spv glslangValidator false"
// for fragment spirv "./generate_spirv.py '11-init_shaders.frag' '11-init_shaders.frag'.spv glslangValidator false"

namespace {

union U32UChars {
	uint32_t w;
	unsigned char c[4];
};
std::vector<unsigned char> convertU32VectorToCharVector(
		const uint32_t *__init_shaders_vert, std::size_t len) {
	std::vector<unsigned char> res;
	for (std::size_t i = 0; i < len; i++) {
		U32UChars u32UChar;
		u32UChar.w = __init_shaders_vert[i];
		for (int j = 0; j < 4; j++) {
			res.push_back(u32UChar.c[j]);
		}
	}
	return res;
}

#include "data/11-init_shaders.vert.spv"
#include "data/11-init_shaders.frag.spv"

}

std::string init_shaders(Info &info, const uint32_t *shaders_vert,
		std::size_t len_vert, const uint32_t *shaders_frag,
		std::size_t len_frag) {

	info.shaderStages[0] = {
			VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO, nullptr, 0,
			VK_SHADER_STAGE_VERTEX_BIT,
			VK_NULL_HANDLE, "main", nullptr };
	auto source_code = convertU32VectorToCharVector(shaders_vert, len_vert);
	/// Creating a shader module
	if (!oge::createShaderModule(info.device, source_code,
			info.shaderStages[0].module)) {
		return APP_SHORT_NAME"::""Creating a shader module";
	}

	info.shaderStages[1] = {
			VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO, nullptr, 0,
			VK_SHADER_STAGE_FRAGMENT_BIT,
			VK_NULL_HANDLE, "main", nullptr };
	source_code = convertU32VectorToCharVector(shaders_frag, len_frag);
	/// Creating a shader module
	if (!oge::createShaderModule(info.device, source_code,
			info.shaderStages[1].module)) {
		return APP_SHORT_NAME"::""Creating a shader module";
	}

	return "";
}

std::string sample_11_init_shaders(int argc, char *argv[], GLFWwindow *window) {
	std::string result { "" };
	struct Info info { };
	info.appName = APP_SHORT_NAME;

	result = init_instance_wsi(info);
	if (!result.empty()) {
		return result;
	}
	result = enumerate_devices(info);
	if (!result.empty()) {
		return result;
	}
	result = init_device_wsi(info, window);
	if (!result.empty()) {
		return result;
	}

	/* VULKAN_KEY_START */

	result = init_shaders(info, __init_shaders_vert,
			sizeof(__init_shaders_vert) / sizeof(uint32_t), __init_shaders_frag,
			sizeof(__init_shaders_frag) / sizeof(uint32_t));
	if (!result.empty()) {
		return result;
	}

	/// Checking the window close flag
	while (!glfwWindowShouldClose(window)) {
		/// Processing events
		glfwPollEvents();
	}

	/// Destroying a shader module
	oge::destroyShaderModule(info.device, info.shaderStages[0].module);

	/// Destroying a shader module
	oge::destroyShaderModule(info.device, info.shaderStages[1].module);

	/* VULKAN_KEY_END */

	oge::destroyPresentationSurface(info.instance, info.surface);
	oge::destroyLogicalDevice(info.device);
	oge::destroyVulkanInstance(info.instance);

	return result;
}

