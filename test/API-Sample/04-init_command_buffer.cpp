/**
 * 04-init_command_buffer.cpp
 *
 *  Created on: Mar 31, 2021
 *      Author: beopenbefree
 */

#include "samples.h"

#define APP_SHORT_NAME "sample_04_init_command_buffer"

constexpr uint64_t FENCE_TIMEOUT { 100000000 };

std::string init_command_buffer(Info &info) {

	/// Creating a command pool
	if (!oge::createCommandPool(info.device, 0,
			info.graphics_queue_family_index, info.cmd_pool)) {
		return APP_SHORT_NAME"::""Creating a command pool";
	}

	/// Allocating command buffers
	if (!oge::allocateCommandBuffers(info.device, info.cmd_pool,
			VK_COMMAND_BUFFER_LEVEL_PRIMARY, 1, info.cmd)) {
		return APP_SHORT_NAME"::""Allocating command buffers";
	}

	return "";
}

std::string execute_begin_command_buffer(Info &info) {

	/// Beginning a command buffer recording operation
	if (!oge::beginCommandBufferRecordingOperation(info.cmd[0], 0, nullptr)) {
		return APP_SHORT_NAME"::""Beginning a command buffer recording operation";
	}

	return "";
}

std::string execute_end_command_buffer(Info &info) {

	/// Ending a command buffer recording operation
	if (!oge::endCommandBufferRecordingOperation(info.cmd[0])) {
		return APP_SHORT_NAME"::""Ending a command buffer recording operation";
	}

	return "";
}

std::string execute_queue_command_buffer(Info &info) {

	/// Creating a fence
	VkFence drawFence;
	if (!oge::createFence(info.device, false, drawFence)) {
		return APP_SHORT_NAME"::""Creating a fence";
	}

	/// Submitting command buffers to a queue
	if (!oge::submitCommandBuffersToQueue(info.graphics_queue, { }, // VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT
			info.cmd, { }, drawFence)) {
		return APP_SHORT_NAME"::""Submitting command buffers to a queue";
	}

	/// Waiting for fences
	if (!oge::waitForFences(info.device, { drawFence }, true, FENCE_TIMEOUT)) {
		return APP_SHORT_NAME"::""Waiting for fences";
	}

	/// Destroying a fence
	oge::destroyFence(info.device, drawFence);

	return "";
}

std::string sample_04_init_command_buffer(int argc, char *argv[],
		GLFWwindow *window) {
	std::string result { "" };
	struct Info info { };
	info.appName = APP_SHORT_NAME;

	result = init_instance(info);
	if (!result.empty()) {
		return result;
	}
	result = enumerate_devices(info);
	if (!result.empty()) {
		return result;
	}
	result = init_device(info);
	if (!result.empty()) {
		return result;
	}

	/* VULKAN_KEY_START */

	result = init_command_buffer(info);
	if (!result.empty()) {
		return result;
	}

	/// Checking the window close flag
	while (!glfwWindowShouldClose(window)) {
		/// Processing events
		glfwPollEvents();
	}

	/// Freeing command buffers
	oge::freeCommandBuffers(info.device, info.cmd_pool, info.cmd);

	/// Destroying a command pool
	oge::destroyCommandPool(info.device, info.cmd_pool);

	/* VULKAN_KEY_END */

	oge::destroyLogicalDevice(info.device);
	oge::destroyVulkanInstance(info.instance);

	return "";
}

