/**
 * 14-init_pipeline.cpp
 *
 *  Created on: Apr 12, 2021
 *      Author: beopenbefree
 */

#include "samples.h"

#define APP_SHORT_NAME "sample_14_init_pipeline"

constexpr bool depthPresent { true };

// Commands to be executed inside "data" directory to get SPIRV shaders:
// for vertex spirv "./generate_spirv.py '14-init_pipeline.vert' '14-init_pipeline.vert'.spv glslangValidator false"
// for fragment spirv "./generate_spirv.py '14-init_pipeline.frag' '14-init_pipeline.frag'.spv glslangValidator false"

namespace {

#include "data/cube_data.h"

#include "data/14-init_pipeline.vert.spv"
#include "data/14-init_pipeline.frag.spv"

}  // namespace

std::string init_pipeline(Info &info, VkBool32 include_depth,
		VkBool32 include_vi) {

	/// Specifying pipeline dynamic states
	std::vector<VkDynamicState> dynamicStateEnables { VK_DYNAMIC_STATE_VIEWPORT,
			VK_DYNAMIC_STATE_SCISSOR };
	VkPipelineDynamicStateCreateInfo dynamicState { };
	oge::specifyPipelineDynamicStates(dynamicStateEnables, dynamicState);

	/// Specifying a pipeline vertex input state
	VkPipelineVertexInputStateCreateInfo vi { };
	std::vector<VkVertexInputBindingDescription> binding_descriptions { };
	std::vector<VkVertexInputAttributeDescription> attribute_descriptions { };
	if (include_vi) {
		binding_descriptions = { info.vi_binding };
		attribute_descriptions = info.vi_attribs;
	}
	oge::specifyPipelineVertexInputState(binding_descriptions,
			attribute_descriptions, vi);

	/// Specifying a pipeline input assembly state
	VkPipelineInputAssemblyStateCreateInfo ia { };
	oge::specifyPipelineInputAssemblyState(VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
			false, ia);

	/// Specifying a pipeline rasterization state
	VkPipelineRasterizationStateCreateInfo rs { };
	oge::specifyPipelineRasterizationState(false, false, VK_POLYGON_MODE_FILL,
			VK_CULL_MODE_BACK_BIT, VK_FRONT_FACE_CLOCKWISE, false, 0, 0, 0,
			1.0f, rs);

	/// Specifying a pipeline blend state
	VkPipelineColorBlendStateCreateInfo cb { };
	std::vector<VkPipelineColorBlendAttachmentState> att_state { { false,
			VK_BLEND_FACTOR_ZERO, VK_BLEND_FACTOR_ZERO, VK_BLEND_OP_ADD,
			VK_BLEND_FACTOR_ZERO, VK_BLEND_FACTOR_ZERO, VK_BLEND_OP_ADD, 0 } };
	std::array<float, 4> blend_constants { 1.0, 1.0, 1.0, 1.0 };
	oge::specifyPipelineBlendState(false, VK_LOGIC_OP_NO_OP, att_state,
			blend_constants, cb);

	/// Specifying a pipeline viewport and scissor test state
	VkPipelineViewportStateCreateInfo vp { };
	oge::ViewportInfo viewport_infos { { { } }, { { } } };
	oge::specifyPipelineViewportAndScissorTestState(viewport_infos, vp);

	/// Specifying a pipeline depth and stencil state
	VkPipelineDepthStencilStateCreateInfo ds { };
	VkStencilOpState frontBackOpState { VK_STENCIL_OP_KEEP, VK_STENCIL_OP_KEEP,
			VK_STENCIL_OP_KEEP, VK_COMPARE_OP_ALWAYS, 0, 0, 0 };
	oge::specifyPipelineDepthAndStencilState(include_depth, include_depth,
			VK_COMPARE_OP_LESS_OR_EQUAL, false, 0, 0, false, frontBackOpState,
			frontBackOpState, ds);

	/// Specifying a pipeline multisample state
	VkPipelineMultisampleStateCreateInfo ms { };
	oge::specifyPipelineMultisampleState(NUM_SAMPLES, false, 0.0, nullptr,
			false, false, ms);

	/// Creating a graphics pipeline
	std::vector<VkGraphicsPipelineCreateInfo> graphics_pipeline_create_infos { {
			VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO, nullptr, 0, 2,
			info.shaderStages.data(), &vi, &ia, nullptr, &vp, &rs, &ms, &ds,
			&cb, &dynamicState, info.pipeline_layout, info.render_pass, 0,
			VK_NULL_HANDLE, 0 } };
	if (!oge::createGraphicsPipelines(info.device,
			graphics_pipeline_create_infos, info.pipelineCache,
			info.pipelines)) {
		return APP_SHORT_NAME"::""Creating a graphics pipeline";
	}

	return "";
}

std::string sample_14_init_pipeline(int argc, char *argv[],
		GLFWwindow *window) {
	std::string result { "" };
	struct Info info { };
	info.appName = APP_SHORT_NAME;

	result = init_instance_wsi(info);
	if (!result.empty()) {
		return result;
	}
	result = enumerate_devices(info);
	if (!result.empty()) {
		return result;
	}
	result = init_swapchain(info, window);
	if (!result.empty()) {
		return result;
	}

	init_device_queue(info);

	result = init_command_buffer(info);
	if (!result.empty()) {
		return result;
	}
	result = execute_begin_command_buffer(info);
	if (!result.empty()) {
		return result;
	}
	result = init_depth_buffer(info);
	if (!result.empty()) {
		return result;
	}
	result = init_uniform_buffer(info);
	if (!result.empty()) {
		return result;
	}
	result = init_render_pass(info, depthPresent);
	if (!result.empty()) {
		return result;
	}
	result = init_frame_buffers(info);
	if (!result.empty()) {
		return result;
	}
	result = init_vertex_buffer(info, g_vb_solid_face_colors_Data,
			sizeof(g_vb_solid_face_colors_Data),
			sizeof(g_vb_solid_face_colors_Data[0]), false);
	if (!result.empty()) {
		return result;
	}
	result = init_descriptor_and_pipeline_layouts(info, false);
	if (!result.empty()) {
		return result;
	}
	result = init_descriptor_set(info, false);
	if (!result.empty()) {
		return result;
	}
	result = init_shaders(info, __init_pipeline_vert,
			sizeof(__init_pipeline_vert) / sizeof(uint32_t),
			__init_pipeline_frag,
			sizeof(__init_pipeline_frag) / sizeof(uint32_t));
	if (!result.empty()) {
		return result;
	}

	/* VULKAN_KEY_START */

	result = init_pipeline(info, true);
	if (!result.empty()) {
		return result;
	}

	/// execute end command buffer
	result = execute_end_command_buffer(info);
	if (!result.empty()) {
		return result;
	}

	/// execute queue command buffer
	result = execute_queue_command_buffer(info);
	if (!result.empty()) {
		return result;
	}

	/// Checking the window close flag
	while (!glfwWindowShouldClose(window)) {
		/// Processing events
		glfwPollEvents();
	}

	/// Destroying a pipeline
	for (auto &pipeline : info.pipelines) {
		oge::destroyPipeline(info.device, pipeline);
	}

	/* VULKAN_KEY_END */

	oge::destroyShaderModule(info.device, info.shaderStages[0].module);
	oge::destroyShaderModule(info.device, info.shaderStages[1].module);
	oge::destroyDescriptorPool(info.device, info.desc_pool);
	oge::destroyDescriptorSetLayout(info.device, *info.desc_layout.data());
	oge::destroyPipelineLayout(info.device, info.pipeline_layout);
	oge::destroyBuffer(info.device, info.vertex_buffer.buf);
	oge::freeMemoryObject(info.device, info.vertex_buffer.mem);
	for (auto &fbuf : info.framebuffers) {
		oge::destroyFramebuffer(info.device, fbuf);
	}
	oge::destroyRenderPass(info.device, info.render_pass);
	oge::destroyBuffer(info.device, info.uniform_data.buf);
	oge::freeMemoryObject(info.device, info.uniform_data.mem);
	oge::destroyImageView(info.device, info.depth.view);
	oge::destroyImage(info.device, info.depth.image);
	oge::freeMemoryObject(info.device, info.depth.mem);
	oge::freeCommandBuffers(info.device, info.cmd_pool, info.cmd);
	oge::destroyCommandPool(info.device, info.cmd_pool);
	for (auto &buffer : info.buffers) {
		oge::destroyImageView(info.device, buffer.view);
	}
	oge::destroySwapchain(info.device, info.swap_chain);
	oge::destroyPresentationSurface(info.instance, info.surface);
	oge::destroyLogicalDevice(info.device);
	oge::destroyVulkanInstance(info.instance);

	return result;
}

