/**
 * glfw_opengl.cpp
 *
 *  Created on: Mar 5, 2021
 *      Author: beopenbefree
 */

/// Including the GLFW header REMARK
#include "GL/gl3w.h"
#include <GLFW/glfw3.h>

#include <glm/trigonometric.hpp>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/ext/matrix_clip_space.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <iostream>

namespace {
/// Setting an error callback REMARK
void error_callback(int error, const char *description) {
	std::cerr << "Error: (" << error << ", " << description << ")" << std::endl;
}

/// Setting a close callback REMARK
void window_close_callback(GLFWwindow *window) {
	std::cout << "Press again to close window..." << std::endl;
	glfwSetWindowCloseCallback(window, nullptr);
	/// Setting the window close flag manually
	glfwSetWindowShouldClose(window, GLFW_FALSE);
}

/// Receiving input events REMARK
bool fullScreen = false;
int width, height, xpos, ypos;
void key_callback(GLFWwindow *window, int key, int scancode, int action,
		int mods) {
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
		glfwSetWindowShouldClose(window, GLFW_TRUE);
	}
	if (key == GLFW_KEY_F && action == GLFW_PRESS) {
		GLFWmonitor *monitor = glfwGetPrimaryMonitor(); // fixme: get current monitor
		if (!fullScreen) {
			const GLFWvidmode *mode = glfwGetVideoMode(monitor);
			glfwGetWindowSize(window, &width, &height);
			glfwGetWindowPos(window, &xpos, &ypos);
			glfwSetWindowMonitor(window, monitor, 0, 0, width, height,
					mode->refreshRate);
		} else {
			glfwSetWindowMonitor(window, NULL, xpos, ypos, width, height, 0);
		}
		fullScreen = !fullScreen;
	}
	std::cout << (action == GLFW_PRESS ? "Pressed: " : "Released: ")
			<< (char) key << std::endl;
}

const struct {
	float x, y;
	float r, g, b;
} vertices[3] = { { -0.6f, -0.4f, 1.f, 0.f, 0.f },
		{ 0.6f, -0.4f, 0.f, 1.f, 0.f }, { 0.f, 0.6f, 0.f, 0.f, 1.f } };

const char *vertex_shader_text = "#version 130\n"
		"uniform mat4 MVP;\n"
		"attribute vec3 vCol;\n"
		"attribute vec2 vPos;\n"
		"varying vec3 color;\n"
		"void main()\n"
		"{\n"
		"    gl_Position = MVP * vec4(vPos, 0.0, 1.0);\n"
		"    color = vCol;\n"
		"}\n";

const char *fragment_shader_text = "#version 130\n"
		"varying vec3 color;\n"
		"void main()\n"
		"{\n"
		"    gl_FragColor = vec4(color, 1.0);\n"
		"}\n";

void printGLError(GLenum source, GLenum type, GLuint id, GLenum severity,
		GLsizei length, const GLchar *message, const void *userParam) {
	std::cerr << "GL CALLBACK: "
			<< (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : "")
			<< " type = 0x" << type << ", severity = 0x" << severity
			<< ", message = " << message << std::endl;
}

constexpr int WIDTH = 400, HEIGHT = 240;

}

int main(int argc, char **argv) {
	/// GLFW versions REMARK
	std::cout << "Compiled against GLFW " << GLFW_VERSION_MAJOR << "."
			<< GLFW_VERSION_MINOR << "." << GLFW_VERSION_REVISION << std::endl;
	int major, minor, revision;
	glfwGetVersion(&major, &minor, &revision);
	std::cout << "Running against GLFW " << major << "." << minor << "."
			<< revision << std::endl;
	std::cout << glfwGetVersionString() << std::endl;

	/// Setting an error callback REMARK
	glfwSetErrorCallback(error_callback);

	/// Initializing and terminating GLFW REMARK
	if (!glfwInit()) {
		exit(EXIT_FAILURE);
	}

	/// OpenGL: Creating a window and context REMARK
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
//	glfwWindowHint(GLFW_STEREO, GLFW_TRUE);
//	glfwWindowHint(GLFW_DOUBLEBUFFER, GLFW_FALSE);
//	glfwWindowHint(GLFW_CONTEXT_CREATION_API, GLFW_OSMESA_CONTEXT_API);
//	glfwWindowHint(GLFW_FOCUSED, GLFW_FALSE);
//	glfwWindowHint(GLFW_CENTER_CURSOR, GLFW_FALSE);
//	glfwWindowHint(GLFW_TRANSPARENT_FRAMEBUFFER, GLFW_TRUE);
//	glfwWindowHint(GLFW_SCALE_TO_MONITOR, GLFW_TRUE);
//	glfwWindowHint(GLFW_X11_CLASS_NAME, 10);
//	glfwWindowHint(GLFW_X11_INSTANCE_NAME, 11);

	GLFWwindow *window = glfwCreateWindow(WIDTH, HEIGHT, "My Title", NULL,
	NULL);
//	GLFWmonitor *monitor = glfwGetPrimaryMonitor();
//	const GLFWvidmode *mode = glfwGetVideoMode(monitor);
//	glfwWindowHint(GLFW_RED_BITS, mode->redBits);
//	glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
//	glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
//	glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);
//	GLFWwindow *window = glfwCreateWindow(mode->width, mode->height, "My Title",
//			monitor, NULL);

	if (!window) {
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	/// Receiving input events REMARK
	glfwSetKeyCallback(window, key_callback);

	/// OpenGL: Making the OpenGL context current REMARK
	glfwMakeContextCurrent(window);
	gl3wInit();

	/// Setting swap interval REMARK
	glfwSwapInterval(1);

	/// Setting a close callback REMARK
	glfwSetWindowCloseCallback(window, window_close_callback);

	/// OpenGL: Rendering with OpenGL REMARK
	GLuint vertex_buffer, vertex_shader, fragment_shader, program;
	GLint mvp_location, vpos_location, vcol_location;

	glEnable( GL_DEBUG_OUTPUT);
	glDebugMessageCallback(printGLError, 0);

	glGenBuffers(1, &vertex_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex_shader, 1, &vertex_shader_text, NULL);
	glCompileShader(vertex_shader);

	fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader, 1, &fragment_shader_text, NULL);
	glCompileShader(fragment_shader);

	program = glCreateProgram();
	glAttachShader(program, vertex_shader);
	glAttachShader(program, fragment_shader);
	glLinkProgram(program);

	mvp_location = glGetUniformLocation(program, "MVP");
	vpos_location = glGetAttribLocation(program, "vPos");
	vcol_location = glGetAttribLocation(program, "vCol");

	glEnableVertexAttribArray(vpos_location);
	glVertexAttribPointer(vpos_location, 2, GL_FLOAT, GL_FALSE,
			sizeof(vertices[0]), (void*) 0);
	glEnableVertexAttribArray(vcol_location);
	glVertexAttribPointer(vcol_location, 3, GL_FLOAT, GL_FALSE,
			sizeof(vertices[0]), (void*) (sizeof(float) * 2));
	///

	/// Checking the window close flag REMARK
	while (!glfwWindowShouldClose(window)) {
		/// OpenGL: Rendering with OpenGL REMARK
		int width, height;
		glfwGetFramebufferSize(window, &width, &height);
		float ratio = width / (float) height;

		glm::mat4x4 i = glm::mat4x4(1.0), m, v = i, p, mvp;
		glViewport(0, 0, width, height);
		glClear(GL_COLOR_BUFFER_BIT);
		// model
		/// Reading the timer REMARK
		m = glm::rotate(glm::mat4x4(1.0), (float) glfwGetTime(),
				glm::vec3(0.0, 1.0, 0.0));
		// view
		glm::mat4x4 tCam = glm::translate(i, glm::vec3(0.0, 5.0, 5.0));
		glm::mat4x4 rCam = glm::rotate(i, glm::radians(-45.f),
				glm::vec3(1.0, 0.0, 0.0));
		v = glm::inverse(tCam * rCam);
		// perspective
		p = glm::perspective(glm::radians(45.f), ratio, 0.1f, 10.f);
		//
		mvp = p * v * m;

		glUseProgram(program);
		glUniformMatrix4fv(mvp_location, 1, GL_FALSE,
				(const GLfloat*) glm::value_ptr(mvp));
		glDrawArrays(GL_TRIANGLES, 0, 3);
		///

		/// Swapping buffers REMARK
		glfwSwapBuffers(window);

		/// Processing events REMARK
		glfwPollEvents();
	}

	/// OpenGL: Destroying a window and context REMARK
	glfwDestroyWindow(window);

	/// Initializing and terminating GLFW REMARK
	glfwTerminate();
	return EXIT_SUCCESS;
}

