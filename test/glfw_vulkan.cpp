/**
 * glfw_vulkan.cpp
 *
 *  Created on: Mar 5, 2021
 *      Author: beopenbefree
 */

#include "lib/instanceAndDevices.h"
#include "lib/imagePresentation.h"

#include <iostream>

namespace {
/// Setting an error callback REMARK
void error_callback(int error, const char *description) {
	std::cerr << "Error: " << description << std::endl;
}

/// Setting a close callback REMARK
void window_close_callback(GLFWwindow *window) {
	std::cout << "Press again to close window..." << std::endl;
	glfwSetWindowCloseCallback(window, nullptr);
	/// Setting the window close flag manually
	glfwSetWindowShouldClose(window, GLFW_FALSE);
}

/// Receiving input events REMARK
bool fullScreen = false;
int width, height, xpos, ypos;
void key_callback(GLFWwindow *window, int key, int scancode, int action,
		int mods) {
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
		glfwSetWindowShouldClose(window, GLFW_TRUE);
	}
	if (key == GLFW_KEY_F && action == GLFW_PRESS) {
		GLFWmonitor *monitor = glfwGetPrimaryMonitor(); // fixme: get current monitor
		if (!fullScreen) {
			const GLFWvidmode *mode = glfwGetVideoMode(monitor);
			glfwGetWindowSize(window, &width, &height);
			glfwGetWindowPos(window, &xpos, &ypos);
			glfwSetWindowMonitor(window, monitor, 0, 0, width, height,
					mode->refreshRate);
		} else {
			glfwSetWindowMonitor(window, NULL, xpos, ypos, width, height, 0);
		}
		fullScreen = !fullScreen;
	}

	std::cout << (action == GLFW_PRESS ? "Pressed: " : "Released: ")
			<< (char) key << std::endl;
}

void character_callback(GLFWwindow *window, unsigned int codepoint) {
	std::wcout << wchar_t(codepoint) << std::endl;
}

//void cursor_position_callback(GLFWwindow *window, double xpos, double ypos)
//{
//	std::cout << "cursor position: " << xpos << "," << ypos << std::endl;
//}

void cursor_enter_callback(GLFWwindow *window, int entered) {
	if (entered) {
		std::cout << "cursor entering..." << std::endl;
	} else {
		std::cout << "cursor exiting..." << std::endl;
	}
}

void mouse_button_callback(GLFWwindow *window, int button, int action,
		int mods) {
	std::string btn;
	switch (button) {
	case GLFW_MOUSE_BUTTON_LEFT:
		btn = "left";
		break;
	case GLFW_MOUSE_BUTTON_RIGHT:
		btn = "right";
		break;
	case GLFW_MOUSE_BUTTON_MIDDLE:
		btn = "middle";
		break;
	default:
		break;
	}
	std::cout << btn + std::string(" button ")
			<< (action == GLFW_PRESS ? "pressed" : "released") << std::endl;
}

void terminate(std::string msg = "ERROR:") {
	std::cerr << "ERROR: " << msg << std::endl;
	glfwTerminate();
	exit(EXIT_FAILURE);
}

std::string getVersionStr(uint32_t version) {
	return std::to_string(VK_API_VERSION_VARIANT(version)) + "."
			+ std::to_string(VK_API_VERSION_MAJOR(version)) + "."
			+ std::to_string(VK_API_VERSION_MINOR(version)) + "."
			+ std::to_string(VK_API_VERSION_PATCH(version));
}

const char *PhysicalDeviceFeatures[] = { "robustBufferAccess",
		"fullDrawIndexUint32", "imageCubeArray", "independentBlend",
		"geometryShader", "tessellationShader", "sampleRateShading",
		"dualSrcBlend", "logicOp", "multiDrawIndirect",
		"drawIndirectFirstInstance", "depthClamp", "depthBiasClamp",
		"fillModeNonSolid", "depthBounds", "wideLines", "largePoints",
		"alphaToOne", "multiViewport", "samplerAnisotropy",
		"textureCompressionETC2", "textureCompressionASTC_LDR",
		"textureCompressionBC", "occlusionQueryPrecise",
		"pipelineStatisticsQuery", "vertexPipelineStoresAndAtomics",
		"fragmentStoresAndAtomics", "shaderTessellationAndGeometryPointSize",
		"shaderImageGatherExtended", "shaderStorageImageExtendedFormats",
		"shaderStorageImageMultisample", "shaderStorageImageReadWithoutFormat",
		"shaderStorageImageWriteWithoutFormat",
		"shaderUniformBufferArrayDynamicIndexing",
		"shaderSampledImageArrayDynamicIndexing",
		"shaderStorageBufferArrayDynamicIndexing",
		"shaderStorageImageArrayDynamicIndexing", "shaderClipDistance",
		"shaderCullDistance", "shaderFloat64", "shaderInt64", "shaderInt16",
		"shaderResourceResidency", "shaderResourceMinLod", "sparseBinding",
		"sparseResidencyBuffer", "sparseResidencyImage2D",
		"sparseResidencyImage3D", "sparseResidency2Samples",
		"sparseResidency4Samples", "sparseResidency8Samples",
		"sparseResidency16Samples", "sparseResidencyAliased",
		"variableMultisampleRate", "inheritedQueries" };

const char *PhysicalDeviceType[] = { "VK_PHYSICAL_DEVICE_TYPE_OTHER",
		"VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU",
		"VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU",
		"VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU", "VK_PHYSICAL_DEVICE_TYPE_CPU" };

std::string getQueueCapability(uint capability) {
	const char *QueueFlagBits[] = { "VK_QUEUE_GRAPHICS_BIT",
			"VK_QUEUE_COMPUTE_BIT2", "VK_QUEUE_TRANSFER_BIT",
			"VK_QUEUE_SPARSE_BINDING_BIT",
			// Provided by VK_VERSION_1_1
			"VK_QUEUE_PROTECTED_BIT", };

	std::string capStr;
	int capNum = (sizeof(QueueFlagBits) / sizeof(const char*));
	for (int i = 0; i < capNum; i++) {
		if (capability & (1 << i)) {
			if (capStr.empty()) {
				capStr += QueueFlagBits[i];
			} else {
				capStr += " | " + std::string(QueueFlagBits[i]);
			}
		}
	}
	return capStr;
}

constexpr int WIDTH = 800, HEIGHT = 600;

void window_content_scale_callback(GLFWwindow *window, float xscale,
		float yscale) {
	std::cout << "Window content scale: " << xscale << ", " << yscale
			<< std::endl;
}

}

int main(int argc, char **argv) {
	/// Setting an error callback REMARK
	glfwSetErrorCallback(error_callback);

	/// Initializing and terminating GLFW REMARK
	if (!glfwInit()) {
		exit(EXIT_FAILURE);
	}

	/// Retrieving monitors REMARK
	{
		int count;
		GLFWmonitor **monitors = glfwGetMonitors(&count);
		for (int i = 0; i < count; i++) {
			GLFWmonitor *monitor = monitors[i];
			std::cout << "monitor " << i << ": " << glfwGetMonitorName(monitor)
					<< std::endl;
			const GLFWvidmode *mode = glfwGetVideoMode(monitor);
			std::cout << "\tR,G,B: " << mode->redBits << "," << mode->greenBits
					<< "," << mode->blueBits << std::endl;
			std::cout << "\tw,h: " << mode->width << ", " << mode->height
					<< std::endl;
			std::cout << "\trefresh rate: " << mode->refreshRate << std::endl;
			float xscale, yscale;
			glfwGetMonitorContentScale(monitor, &xscale, &yscale);
			std::cout << "\txscale,yscale: " << xscale << ", " << yscale
					<< std::endl;
			int xpos, ypos;
			glfwGetMonitorPos(monitor, &xpos, &ypos);
			std::cout << "\txpos,ypos: " << xpos << ", " << ypos << std::endl;
			int width, height;
			glfwGetMonitorWorkarea(monitor, &xpos, &ypos, &width, &height);
			std::cout << "\t(work area) xpos,ypos,width,height: " << xpos
					<< ", " << ypos << ", " << width << ", " << height
					<< std::endl;

		}
	}

	/// Querying for Vulkan support REMARK
	if (!glfwVulkanSupported()) {
		terminate("Querying for Vulkan support");
	}
	std::cout << "Vulkan supported!" << std::endl;

	/// Load vkGetInstanceProcAddr REMARK
	if (!oge::loadFunctionExportedFromVulkanLoaderLibrary()) {
		terminate("load vkGetInstanceProcAddr");
	}

	/// Load global level functions (vkCreateInstance, ...) REMARK
	if (!oge::loadGlobalLevelFunctions()) {
		terminate("load global level functions (vkCreateInstance, ...)");
	}

	/// Check available instance extensions REMARK
	std::vector<VkExtensionProperties> available_instance_extensions;
	if (!oge::checkAvailableInstanceExtensions(available_instance_extensions)) {
		terminate("check available instance extensions");
	}
	std::cout << "available instance extensions:" << std::endl;
	for (const auto &ext : available_instance_extensions) {
		std::cout << "\t" << ext.extensionName << "(versiopn: "
				<< ext.specVersion << ")" << std::endl;
	}

	/// Creating an instance with wsi support REMARK
	uint32_t pApiVersion;
	VkResult res = oge::vkEnumerateInstanceVersion(&pApiVersion);
	if (res != VK_SUCCESS) {
		terminate("Enumerate instance version");
	}
	std::cout << "instance version: " << getVersionStr(pApiVersion)
			<< std::endl;
	VkInstance instance = instance;
	std::vector<const char*> required_instance_extensions;
	if (!oge::createVulkanInstanceWithWsiExtensionsEnabled(
			required_instance_extensions, "Vulkan test", instance)) {
		terminate("Creating an instance with wsi support");
	}
	std::cout << "required instance extensions:" << std::endl;
	for (const auto &ext : required_instance_extensions) {
		std::cout << "\t" << ext << std::endl;
	}

	/// Load instance level functions REMARK
	if (!oge::loadInstanceLevelFunctions(instance,
			required_instance_extensions)) {
		terminate("load instance level functions");
	}

	/// Enumerating available physical devices REMARK
	/// Checking available device extensions REMARK
	/// Getting features and properties of a physical device REMARK
	/// Checking available queue families and their properties REMARK
	std::vector<VkPhysicalDevice> available_physical_devices;
	if (!oge::enumerateAvailablePhysicalDevices(instance,
			available_physical_devices)) {
		terminate("Enumerating available physical devices");
	}
	std::cout << "available physical devices:" << std::endl;
	for (const auto &device : available_physical_devices) {
		std::cout << "\t" << device << std::endl;
		// extensions
		std::cout << "\t\tavailable extensions:" << std::endl;
		std::vector<VkExtensionProperties> available_device_extensions;
		if (!oge::checkAvailableDeviceExtensions(device,
				available_device_extensions)) {
			terminate("Checking available device extensions");
		}
		for (const auto &ext : available_device_extensions) {
			std::cout << "\t\t\t" << ext.extensionName << " (version: "
					<< ext.specVersion << ")" << std::endl;
		}
		// features & properties
		VkPhysicalDeviceFeatures device_features;
		VkPhysicalDeviceProperties device_properties;
		oge::getFeaturesAndPropertiesOfPhysicalDevice(device, device_features,
				device_properties);
		std::cout << "\t\tfeatures:" << std::endl;
		VkBool32 *pFeatures = reinterpret_cast<VkBool32*>(&device_features);
		uint8_t featuresNum = sizeof(PhysicalDeviceFeatures)
				/ sizeof(const char*);
		for (uint8_t i = 0; i < featuresNum; i++) {
			if (pFeatures[i]) {
				std::cout << "\t\t\t" << PhysicalDeviceFeatures[i] << std::endl;
			}
		}
		std::cout << "\t\tproperties:" << std::endl;
		{
			std::cout << "\t\t\tdeviceName: " << device_properties.deviceName
					<< std::endl;
			std::cout << "\t\t\tapiVersion: "
					<< getVersionStr(device_properties.apiVersion) << std::endl;
			std::cout << "\t\t\tdriverVersion: "
					<< getVersionStr(device_properties.driverVersion)
					<< std::endl;
			std::cout << "\t\t\tvendorID: " << device_properties.vendorID
					<< std::endl;
			std::cout << "\t\t\tdeviceID: " << device_properties.deviceID
					<< std::endl;
			std::cout << "\t\t\tdeviceType: "
					<< PhysicalDeviceType[device_properties.deviceType]
					<< std::endl;
			std::string pipelineCacheUUIDStr;
			for (int i = 0; i < VK_UUID_SIZE; i++) {
				pipelineCacheUUIDStr += std::to_string(
						device_properties.pipelineCacheUUID[i]);
				i < VK_UUID_SIZE - 1 ?
						pipelineCacheUUIDStr += ":" : pipelineCacheUUIDStr +=
								"";
			}
			std::cout << "\t\t\tpipelineCacheUUID: " << pipelineCacheUUIDStr
					<< std::endl;
			// VkPhysicalDeviceLimits limits; NOT IMPLEMENTED
			// VkPhysicalDeviceSparseProperties sparseProperties; NOT IMPLEMENTED
		}
		// queue families' properties
		std::cout << "\t\tqueue families:" << std::endl;
		std::vector<VkQueueFamilyProperties> queue_families;
		if (!oge::checkAvailableQueueFamiliesAndTheirProperties(device,
				queue_families)) {
			terminate("Checking available queue families and their properties");
		}
		for (uint i = 0; i < queue_families.size(); i++) {
			VkQueueFamilyProperties family = queue_families[i];
			std::cout << "\t\t\tfamily n." + std::to_string(i) + ": "
					<< std::endl;
			std::cout << "\t\t\t\tqueue capabilities: "
					<< getQueueCapability(family.queueFlags) << std::endl;
			std::cout << "\t\t\t\tqueue count: " << family.queueCount
					<< std::endl;
			std::cout << "\t\t\t\ttimestamp valid bits: "
					<< family.timestampValidBits << std::endl;
			std::cout << "\t\t\t\tmin image transfer granularity: " << "(width="
					<< family.minImageTransferGranularity.width << ", height="
					<< family.minImageTransferGranularity.height << ", depth="
					<< family.minImageTransferGranularity.depth << ")"
					<< std::endl;
		}
		// Physical Device Memory
		VkPhysicalDeviceMemoryProperties physical_device_memory_properties;
		oge::vkGetPhysicalDeviceMemoryProperties(device,
				&physical_device_memory_properties);
		std::cout << "\t\tmemory HEAPS (size, flags):" << std::endl;
		for (uint32_t h = 0;
				h < physical_device_memory_properties.memoryHeapCount; h++) {
			VkMemoryHeap heap = physical_device_memory_properties.memoryHeaps[h];
			std::vector<std::string> flags;
			heap.flags & VK_MEMORY_HEAP_DEVICE_LOCAL_BIT ?
					flags.emplace_back("VK_MEMORY_HEAP_DEVICE_LOCAL_BIT") :
					static_cast<void>(flags);
			heap.flags & VK_MEMORY_HEAP_MULTI_INSTANCE_BIT ?
					flags.emplace_back("VK_MEMORY_HEAP_MULTI_INSTANCE_BIT") :
					static_cast<void>(flags);
			std::string flagStr;
			for (const auto &flag : flags) {
				flagStr.empty() ? flagStr += flag : flagStr += ("|" + flag);
			}
			std::cout << "\t\t\theap " << h << " : (" << heap.size
					<< ", " << flagStr << ")" << std::endl;
		}
		std::cout << "\t\tmemory TYPES (flags, heap):" << std::endl;
		for (uint32_t m = 0;
				m < physical_device_memory_properties.memoryTypeCount; m++) {
			VkMemoryType memory =
					physical_device_memory_properties.memoryTypes[m];
			std::vector<std::string> flags;
			memory.propertyFlags & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT ?
					flags.emplace_back("VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT") :
					static_cast<void>(flags);
			memory.propertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT ?
					flags.emplace_back("VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT") :
					static_cast<void>(flags);
			memory.propertyFlags & VK_MEMORY_PROPERTY_HOST_COHERENT_BIT ?
					flags.emplace_back("VK_MEMORY_PROPERTY_HOST_COHERENT_BIT") :
					static_cast<void>(flags);
			memory.propertyFlags & VK_MEMORY_PROPERTY_HOST_CACHED_BIT ?
					flags.emplace_back("VK_MEMORY_PROPERTY_HOST_CACHED_BIT") :
					static_cast<void>(flags);
			memory.propertyFlags & VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT ?
					flags.emplace_back(
							"VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT") :
					static_cast<void>(flags);
			memory.propertyFlags & VK_MEMORY_PROPERTY_PROTECTED_BIT ?
					flags.emplace_back("VK_MEMORY_PROPERTY_PROTECTED_BIT") :
					static_cast<void>(flags);
			memory.propertyFlags & VK_MEMORY_PROPERTY_DEVICE_COHERENT_BIT_AMD ?
					flags.emplace_back(
							"VK_MEMORY_PROPERTY_DEVICE_COHERENT_BIT_AMD") :
					static_cast<void>(flags);
			memory.propertyFlags & VK_MEMORY_PROPERTY_DEVICE_UNCACHED_BIT_AMD ?
					flags.emplace_back(
							"VK_MEMORY_PROPERTY_DEVICE_UNCACHED_BIT_AMD") :
					static_cast<void>(flags);
			std::string flagStr;
			for (const auto &flag : flags) {
				flagStr.empty() ? flagStr += flag : flagStr += ("|" + flag);
			}
			std::cout << "\t\t\ttype " << m << " : (" << flagStr
					<< ", " << memory.heapIndex << ")" << std::endl;
		}
	}

	/// Selecting the index of a queue family with the desired capabilities REMARK
	VkQueueFlags desired_capabilities = VK_QUEUE_GRAPHICS_BIT
			| VK_QUEUE_COMPUTE_BIT | VK_QUEUE_TRANSFER_BIT;
	uint32_t queue_family_index;
	bool familyFound = false;
	for (const auto &device : available_physical_devices) {
		if (oge::selectIndexOfQueueFamilyWithDesiredCapabilities(device,
				desired_capabilities, queue_family_index)) {
			familyFound = true;
			break;
		}
	}
	if (!familyFound) {
		terminate(
				"Selecting the index of a queue family with the desired capabilities");
	}

	/// Creating the window REMARK
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
//	glfwWindowHint(GLFW_STEREO, GLFW_FALSE); // vulkan ignored
//	glfwWindowHint(GLFW_DOUBLEBUFFER, GLFW_TRUE); // vulkan ignored
//	glfwWindowHint(GLFW_CONTEXT_CREATION_API, GLFW_OSMESA_CONTEXT_API); // vulkan ignored
//	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
//	glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
//	glfwWindowHint(GLFW_DECORATED, GLFW_FALSE);
//	glfwWindowHint(GLFW_FOCUSED, GLFW_FALSE);
//	glfwWindowHint(GLFW_AUTO_ICONIFY, GLFW_FALSE);
//	glfwWindowHint(GLFW_FLOATING, GLFW_TRUE);
//	glfwWindowHint(GLFW_MAXIMIZED, GLFW_TRUE);
//	glfwWindowHint(GLFW_CENTER_CURSOR, GLFW_TRUE);
	glfwWindowHint(GLFW_TRANSPARENT_FRAMEBUFFER, GLFW_TRUE); // vulkan ignored
//	glfwWindowHint(GLFW_FOCUS_ON_SHOW, GLFW_TRUE);
//	glfwWindowHint(GLFW_SCALE_TO_MONITOR, GLFW_TRUE);
			// Framebuffer related hints: vulkan ignored
//	glfwWindowHint(GLFW_REFRESH_RATE, GLFW_TRUE); // vulkan ?
			// Context related hints: vulkan ignored
//	glfwWindowHint(GLFW_X11_CLASS_NAME, 10);
//	glfwWindowHint(GLFW_X11_INSTANCE_NAME, 11);

	// windowed
	GLFWwindow *window = glfwCreateWindow(WIDTH, HEIGHT, "Vulkan test", nullptr,
			nullptr);
	// full screen
//	GLFWmonitor *monitor = glfwGetPrimaryMonitor();
//	const GLFWvidmode *mode = glfwGetVideoMode(monitor);
//	glfwWindowHint(GLFW_RED_BITS, mode->redBits);
//	glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
//	glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
//	glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);
//	GLFWwindow *window = glfwCreateWindow(mode->width, mode->height, "My Title",
//			monitor, NULL);

	if (!window) {
		terminate("Creating the window");
	}

	/// Creating a Vulkan window surface REMARK
	VkSurfaceKHR presentation_surface;
	if (!oge::createPresentationSurface(instance, window, nullptr,
			&presentation_surface)) {
		terminate("Creating a Vulkan window surface");
	}

	/// Some query about window REMARK
	{
		std::cout << "Some query about window:" << std::endl;
		int width, height;
		glfwGetWindowSize(window, &width, &height);
		std::cout << "\tWindow size: " << width << ", " << height << std::endl;
		glfwGetFramebufferSize(window, &width, &height);
		std::cout << "\tFramebuffer size: " << width << ", " << height
				<< std::endl;
		int left, top, right, bottom;
		glfwGetWindowFrameSize(window, &left, &top, &right, &bottom);
		std::cout << "\tWindow Frame size: " << left << ", " << top << ", "
				<< right << ", " << bottom << std::endl;
		float xscale, yscale;
		glfwGetWindowContentScale(window, &xscale, &yscale);
		std::cout << "\tWindow content scale: " << xscale << ", " << yscale
				<< std::endl;
		glfwSetWindowContentScaleCallback(window,
				window_content_scale_callback);
		glfwSetWindowSizeLimits(window, WIDTH, HEIGHT, WIDTH * 1.5,
				HEIGHT * 1.5);
		glfwGetWindowSize(window, &width, &height);
		glfwSetWindowAspectRatio(window, width, height);
		glfwSetWindowPos(window, 160, 90);
//		glfwSetWindowOpacity(window, 0.5f);
		std::cout << "\tWindow opacity: " << glfwGetWindowOpacity(window)
				<< " - Transparent framebuffer: "
				<< (glfwGetWindowAttrib(window, GLFW_TRANSPARENT_FRAMEBUFFER)
						== 1 ? "yes" : "no") << std::endl;
	}

//	/// Creating a logical device REMARK
//	/// Loading device-level functions REMARK
//	/// Getting a device queue REMARK
//	/// Creating a logical device with geometry shaders, graphics, and compute queues REMARK
//	VkDevice logical_device;
//	VkQueue graphics_queue, compute_queue;
//	if (!oge::createLogicalDeviceWithGeometryShadersAndGraphicsAndComputeQueues(
//			instance, logical_device, graphics_queue, compute_queue, true))
//	{
//		terminate(
//				"Creating a logical device with geometry shaders, graphics, and compute queues");
//	}

//	/// Selecting a queue family that supports presentation to a given surface REMARK
//	for (const auto &device : available_physical_devices)
//	{
//		if (oge::selectQueueFamilyThatSupportsPresentationToGivenSurface(device,
//				presentation_surface, queue_family_index))
//		{
//			std::cout << "queue family '" << queue_family_index
//					<< "' supports presentation to a given surface for physical device "
//					<< device << std::endl;
//		}
//	}

	/// Creating a logical device with geometry shaders and with graphics, compute and presentation to surface (possibly overlapping) queues REMARK
	VkDevice logical_device;
	VkQueue graphics_queue, compute_queue, presentation_queue;
	VkPhysicalDevice physical_device_found = VK_NULL_HANDLE;
	// enumerate physical devices
	available_physical_devices.clear();
	oge::enumerateAvailablePhysicalDevices(instance,
			available_physical_devices);
	// find a suitable device
	for (auto &physical_device : available_physical_devices) {
		// get device's features and properties
		VkPhysicalDeviceFeatures device_features;
		VkPhysicalDeviceProperties device_properties;
		oge::getFeaturesAndPropertiesOfPhysicalDevice(physical_device,
				device_features, device_properties);
		// geometry shader feature must be supported
		if (!device_features.geometryShader) {
			continue;
		} else {
			device_features = { };
			device_features.geometryShader = VK_TRUE;
		}
		// graphics queue must be supported
		uint32_t graphics_queue_family_index;
		if (!oge::selectIndexOfQueueFamilyWithDesiredCapabilities(
				physical_device, VK_QUEUE_GRAPHICS_BIT,
				graphics_queue_family_index)) {
			continue;
		}
		// compute queue must be supported
		uint32_t compute_queue_family_index;
		if (!oge::selectIndexOfQueueFamilyWithDesiredCapabilities(
				physical_device, VK_QUEUE_COMPUTE_BIT,
				compute_queue_family_index)) {
			continue;
		}
		// presentation to surface queue must be supported
		uint32_t presentation_queue_family_index;
		if (!oge::selectQueueFamilyThatSupportsPresentationToGivenSurface(
				physical_device, presentation_surface,
				presentation_queue_family_index)) {
			continue;
		}
		// create a list of queues (possibly overlapping)
		std::vector<oge::QueueInfo> requested_queues = { {
				graphics_queue_family_index, { 1.0f } } };
		if (compute_queue_family_index != graphics_queue_family_index) {
			requested_queues.push_back(
					{ compute_queue_family_index, { 1.0f } });
		}
		if ((presentation_queue_family_index != graphics_queue_family_index)
				&& (presentation_queue_family_index
						!= compute_queue_family_index)) {
			requested_queues.push_back( { presentation_queue_family_index, {
					1.0f } });
		}
		// create the logical device
		std::vector<const char*> desired_extensions;
		if (!oge::createLogicalDeviceWithWsiExtensionsEnabled(physical_device,
				requested_queues, desired_extensions, &device_features,
				logical_device)) {
			continue;
		} else {
			// load device level functions (specify the desired extensions!)
			if (!oge::loadDeviceLevelFunctions(logical_device,
					desired_extensions)) {
				continue;
			}
			// get the queues
			oge::getDeviceQueue(logical_device, graphics_queue_family_index, 0,
					graphics_queue);
			oge::getDeviceQueue(logical_device, compute_queue_family_index, 0,
					compute_queue);
			oge::getDeviceQueue(logical_device, presentation_queue_family_index,
					0, presentation_queue);
			physical_device_found = physical_device;
			break;
		}
	}
	if (physical_device_found == VK_NULL_HANDLE) {
		terminate(
				"Creating a logical device with geometry shaders and with graphics, compute and presentation to surface (possibly overlapping) queues");
	}

	/// Selecting a desired presentation mode REMARK
	/// Getting the capabilities of a presentation surface REMARK
	/// Selecting a number of swapchain images REMARK
	/// Choosing a size of swapchain images REMARK
	/// Selecting desired usage scenarios of swapchain images REMARK
	/// Selecting a transformation of swapchain images REMARK
	/// Selecting a format of swapchain images REMARK
	/// Creating a swapchain REMARK
	/// Getting handles of swapchain images REMARK
	/// Creating a swapchain with R8G8B8A8 format and a mailbox present mode REMARK
	VkImageUsageFlags swapchain_image_usage =
			VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	VkExtent2D size_of_images;
	VkFormat image_format;
	VkSwapchainKHR old_swapchain = VK_NULL_HANDLE, swapchain;
	std::vector<VkImage> swapchain_images;
	glfwGetFramebufferSize(window,
			reinterpret_cast<int*>(&(size_of_images.width)),
			reinterpret_cast<int*>(&(size_of_images.height)));
	if (!oge::createSwapchainWithR8G8B8A8FormatAndMailboxPresentMode(
			physical_device_found, presentation_surface, logical_device,
			swapchain_image_usage, size_of_images, image_format, old_swapchain,
			swapchain, swapchain_images)) {
		terminate(
				"Creating a swapchain with R8G8B8A8 format and a mailbox present mode");
	}

	/// Receiving input events REMARK
	glfwSetKeyCallback(window, key_callback);
	glfwSetCharCallback(window, character_callback);
//	glfwSetCursorPosCallback(window, cursor_position_callback);
	if (glfwRawMouseMotionSupported()) {
		glfwSetInputMode(window, GLFW_RAW_MOUSE_MOTION, GLFW_TRUE);
		std::cout << "GLFW_RAW_MOUSE_MOTION" << std::endl;
	}
	GLFWcursor *cursor = glfwCreateStandardCursor(GLFW_HAND_CURSOR);
	glfwSetCursor(window, cursor);
	glfwSetCursorEnterCallback(window, cursor_enter_callback);
	glfwSetMouseButtonCallback(window, mouse_button_callback);

//	/// Making the OpenGL context current xxx
//	glfwMakeContextCurrent(window);
//	gl3wInit();

//	/// Setting swap interval xxx
//	glfwSwapInterval(1);

	/// Setting a close callback REMARK
	glfwSetWindowCloseCallback(window, window_close_callback);

//	/// Rendering with OpenGL xxx
//	GLuint vertex_buffer, vertex_shader, fragment_shader, program;
//	GLint mvp_location, vpos_location, vcol_location;
//
//	glGenBuffers(1, &vertex_buffer);
//	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
//	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
//
//	vertex_shader = glCreateShader(GL_VERTEX_SHADER);
//	glShaderSource(vertex_shader, 1, &vertex_shader_text, NULL);
//	glCompileShader(vertex_shader);
//
//	fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
//	glShaderSource(fragment_shader, 1, &fragment_shader_text, NULL);
//	glCompileShader(fragment_shader);
//
//	program = glCreateProgram();
//	glAttachShader(program, vertex_shader);
//	glAttachShader(program, fragment_shader);
//	glLinkProgram(program);
//
//	mvp_location = glGetUniformLocation(program, "MVP");
//	vpos_location = glGetAttribLocation(program, "vPos");
//	vcol_location = glGetAttribLocation(program, "vCol");
//
//	glEnableVertexAttribArray(vpos_location);
//	glVertexAttribPointer(vpos_location, 2, GL_FLOAT, GL_FALSE,
//			sizeof(vertices[0]), (void*) 0);
//	glEnableVertexAttribArray(vcol_location);
//	glVertexAttribPointer(vcol_location, 3, GL_FLOAT, GL_FALSE,
//			sizeof(vertices[0]), (void*) (sizeof(float) * 2));
//	///

	/// Checking the window close flag REMARK
	while (!glfwWindowShouldClose(window)) {
//		/// Rendering with OpenGL xxx
//		int width, height;
//		glfwGetFramebufferSize(window, &width, &height);
//		float ratio = width / (float) height;
//
//		glm::mat4x4 i = glm::mat4x4(1.0), m, v = i, p, mvp;
//		glViewport(0, 0, width, height);
//		glClear(GL_COLOR_BUFFER_BIT);
//		// model
//		/// Reading the timer REMARK
//		m = glm::rotate(glm::mat4x4(1.0), (float) glfwGetTime(),
//				glm::vec3(0.0, 1.0, 0.0));
//		// view
//		glm::mat4x4 tCam = glm::translate(i, glm::vec3(0.0, 5.0, 5.0));
//		glm::mat4x4 rCam = glm::rotate(i, glm::radians(-45.f),
//				glm::vec3(1.0, 0.0, 0.0));
//		v = glm::inverse(tCam * rCam);
//		// perspective
//		p = glm::perspective(glm::radians(45.f), ratio, 0.1f, 10.f);
//		//
//		mvp = p * v * m;
//
//		glUseProgram(program);
//		glUniformMatrix4fv(mvp_location, 1, GL_FALSE,
//				(const GLfloat*) glm::value_ptr(mvp));
//		glDrawArrays(GL_TRIANGLES, 0, 3);
//		///
//
//		/// Swapping buffers REMARK
//		glfwSwapBuffers(window);
//
		/// Processing events REMARK
		glfwPollEvents();
	}

	/// Destroying a swapchain REMARK
	oge::destroySwapchain(logical_device, swapchain);

	/// Destroying a presentation surface REMARK
	oge::destroyPresentationSurface(instance, presentation_surface);

	/// Destroying the window REMARK
	glfwDestroyWindow(window);

	/// Destroying a logical device REMARK
	oge::destroyLogicalDevice(logical_device);

	/// Destroying a Vulkan Instance REMARK
	oge::destroyVulkanInstance(instance);

	/// Initializing and terminating GLFW REMARK
	glfwTerminate();
	return EXIT_SUCCESS;
}

