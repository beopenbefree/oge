/**
 * vulkan_app.cpp
 *
 *  Created on: Apr 26, 2021
 *      Author: beopenbefree
 */

#include "lib/instanceAndDevices.h"
#include "lib/commandBuffersAndSynchronization.h"
#include "lib/imagePresentation.h"
#include "lib/resourcesAndMemory.h"
#include "lib/descriptorSets.h"
#include "lib/graphicsAndComputePipelines.h"
#include "lib/renderPassesAndFramebuffers.h"
#include "lib/commandRecordingAndDrawing.h"
#include "framework/windowFramework.h"
#include "framework/application.h"
#include "framework/helpers.h"

namespace {
constexpr unsigned int WIN_WIDTH = 800, WIN_HEIGHT = 600;
constexpr char *modelPath = "data/models/knot.obj";

struct FrameResources {
	/*
	 *	FIXME: In a real-life application, a single frame will be probably
	 *	composed of multiple command buffers recorded in multiple threads
	 */
	VkCommandBuffer CommandBuffer; //fixme
	oge::VkDestroyer(oge::VkSemaphore) imageAcquiredSemaphore;
	oge::VkDestroyer(oge::VkSemaphore) readyToPresentSemaphore;
	oge::VkDestroyer(oge::VkFence) drawingFinishedFence;
	oge::VkDestroyer(oge::VkImageView) depthAttachment;
	oge::VkDestroyer(oge::VkFramebuffer) framebuffer;

	FrameResources(VkCommandBuffer &command_buffer,
			oge::VkDestroyer(oge::VkSemaphore) &image_acquired_semaphore,
			oge::VkDestroyer(oge::VkSemaphore) &ready_to_present_semaphore,
			oge::VkDestroyer(oge::VkFence) &drawing_finished_fence,
			oge::VkDestroyer(oge::VkImageView) &depth_attachment,
			oge::VkDestroyer(oge::VkFramebuffer) &framebuffer) :
			CommandBuffer(command_buffer), imageAcquiredSemaphore(
					std::move(image_acquired_semaphore)), readyToPresentSemaphore(
					std::move(ready_to_present_semaphore)), drawingFinishedFence(
					std::move(drawing_finished_fence)), depthAttachment(
					std::move(depth_attachment)), framebuffer(
					std::move(framebuffer)) {
	}

	FrameResources(FrameResources &&other) {
		*this = std::move(other);
	}

	FrameResources& operator=(FrameResources &&other) {
		if (this != &other) {
			VkCommandBuffer command_buffer = CommandBuffer;

			CommandBuffer = other.CommandBuffer;
			other.CommandBuffer = command_buffer;
			imageAcquiredSemaphore = std::move(other.imageAcquiredSemaphore);
			readyToPresentSemaphore = std::move(other.readyToPresentSemaphore);
			drawingFinishedFence = std::move(other.drawingFinishedFence);
			depthAttachment = std::move(other.depthAttachment);
			framebuffer = std::move(other.framebuffer);
		}
		return *this;
	}

	FrameResources(FrameResources const&) = delete;
	FrameResources& operator=(FrameResources const&) = delete;
};

struct SwapchainParameters {
	oge::VkDestroyer(oge::VkSwapchainKHR) handle { VK_NULL_HANDLE };
	VkFormat format;
	VkExtent2D size;
	std::vector<VkImage> images;
	std::vector<oge::VkDestroyer(oge::VkImageView)> imageViews;
	std::vector<VkImageView> imageViewsRaw;
};

class AppData {
public:
	GLFWwindow *window { nullptr };
	std::vector<VkExtensionProperties> instance_extension_properties;
	std::vector<const char*> instance_extension_names;
	const char *appName { "vulkan_app" };
	oge::VkDestroyer(oge::VkInstance) instance { VK_NULL_HANDLE };
	oge::VkDestroyer(oge::VkSurfaceKHR) presentationSurface { VK_NULL_HANDLE };
	std::vector<VkPhysicalDevice> physical_devices;
	oge::QueueParameters graphicsQueue;
	oge::QueueParameters computeQueue;
	oge::QueueParameters presentQueue;
	VkPhysicalDevice physicalDevice { VK_NULL_HANDLE };
	VkPhysicalDeviceFeatures *desired_device_features { nullptr };
	oge::VkDestroyer(oge::VkDevice) logicalDevice { VK_NULL_HANDLE };
	VkImageUsageFlags swapchain_image_usage {
			VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT };
	bool use_depth { true };
	VkImageUsageFlags depth_attachment_usage {
			VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT };
	oge::VkDestroyer(oge::VkCommandPool) commandPool { VK_NULL_HANDLE };
	std::vector<oge::VkDestroyer(oge::VkImage)> depthImages;
	std::vector<oge::VkDestroyer(oge::VkDeviceMemory)> depthImagesMemory;
	static constexpr uint32_t FramesCount = 3;
	static constexpr VkFormat DepthFormat = VK_FORMAT_D16_UNORM;
	std::vector<FrameResources> framesResources;
	bool ready { false };
	SwapchainParameters swapchain;
	oge::Mesh model;
	oge::VkDestroyer(oge::VkBuffer) vertexBuffer { VK_NULL_HANDLE };
	oge::VkDestroyer(oge::VkDeviceMemory) vertexBufferMemory { VK_NULL_HANDLE };
};

/// Setting an error callback
void error_callback(int error, const char *description);
void terminate(std::string msg);
bool createSwapchain(VkImageUsageFlags swapchain_image_usage, bool use_depth,
		VkImageUsageFlags depth_attachment_usage, AppData &app);
}

int main(int argc, char **argv) {

	AppData app;

	/// initialization
	{
		// window framework
		{
			/// Setting an error callback
			glfwSetErrorCallback(error_callback);

			/// Initializing GLFW
			if (!glfwInit()) {
				exit(EXIT_FAILURE);
			}

			/// Creating the window
			glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
			app.window = glfwCreateWindow(WIN_WIDTH, WIN_HEIGHT, app.appName,
					nullptr, nullptr);
			if (!app.window) {
				terminate("Creating the window");
			}
		}

		// vulkan
		{
			/// Connecting with vulkan loader library
			if (!oge::connectWithVulkanLoaderLibrary()) {
				terminate("Connecting with vulkan loader library");
			}

			/// Load vkGetInstanceProcAddr
			if (!oge::loadFunctionExportedFromVulkanLoaderLibrary()) {
				terminate("Load vkGetInstanceProcAddr");
			}

			/// Load global level functions (vkCreateInstance, ...)
			if (!oge::loadGlobalLevelFunctions()) {
				terminate(
						"Load global level functions (vkCreateInstance, ...)");
			}

			/// Check available instance extensions
			if (!oge::checkAvailableInstanceExtensions(
					app.instance_extension_properties)) {
				terminate("Check available instance extensions");
			}

			/// Creating an instance
			if (!oge::createVulkanInstanceWithWsiExtensionsEnabled(
					app.instance_extension_names, app.appName, *app.instance)) {
				terminate("Creating an instance");
			}
			InitVkDestroyer(app.instance);

			/// Load instance level functions
			if (!oge::loadInstanceLevelFunctions(*app.instance,
					app.instance_extension_names)) {
				terminate("Load instance level functions");
			}

			/// Creating a Vulkan window surface
			if (!oge::createPresentationSurface(*app.instance, app.window,
					nullptr, &(*app.presentationSurface))) {
				terminate("Creating a Vulkan window surface");
			}
			InitVkDestroyer(app.instance, app.presentationSurface);

			/// Enumerating available physical devices
			std::vector<VkPhysicalDevice> physical_devices;
			if (!oge::enumerateAvailablePhysicalDevices(*app.instance,
					app.physical_devices)) {
				terminate("Enumerating available physical devices");
			}

			/// Creating logical device with graphics, compute and presentation queues
			for (auto &physical_device : physical_devices) {
				if (!oge::selectIndexOfQueueFamilyWithDesiredCapabilities(
						physical_device, VK_QUEUE_GRAPHICS_BIT,
						app.graphicsQueue.FamilyIndex)) {
					continue;
				}

				if (!oge::selectIndexOfQueueFamilyWithDesiredCapabilities(
						physical_device, VK_QUEUE_COMPUTE_BIT,
						app.computeQueue.FamilyIndex)) {
					continue;
				}

				if (!oge::selectQueueFamilyThatSupportsPresentationToGivenSurface(
						physical_device, *app.presentationSurface,
						app.presentQueue.FamilyIndex)) {
					continue;
				}

				std::vector<oge::QueueInfo> requested_queues = { {
						app.graphicsQueue.FamilyIndex, { 1.0f } } };
				if (app.graphicsQueue.FamilyIndex
						!= app.computeQueue.FamilyIndex) {
					requested_queues.push_back( { app.computeQueue.FamilyIndex,
							{ 1.0f } });
				}
				if ((app.graphicsQueue.FamilyIndex
						!= app.presentQueue.FamilyIndex)
						&& (app.computeQueue.FamilyIndex
								!= app.presentQueue.FamilyIndex)) {
					requested_queues.push_back( { app.presentQueue.FamilyIndex,
							{ 1.0f } });
				}
				std::vector<char const*> device_extensions;
				if (!oge::createLogicalDeviceWithWsiExtensionsEnabled(
						physical_device, requested_queues, device_extensions,
						app.desired_device_features, *app.logicalDevice)) {
					continue;
				} else {
					app.physicalDevice = physical_device;
					oge::loadDeviceLevelFunctions(*app.logicalDevice,
							device_extensions);
					oge::getDeviceQueue(*app.logicalDevice,
							app.graphicsQueue.FamilyIndex, 0,
							app.graphicsQueue.Handle);
					oge::getDeviceQueue(*app.logicalDevice,
							app.computeQueue.FamilyIndex, 0,
							app.computeQueue.Handle);
					oge::getDeviceQueue(*app.logicalDevice,
							app.presentQueue.FamilyIndex, 0,
							app.presentQueue.Handle);
					InitVkDestroyer(app.logicalDevice);
					break;
				}
			}
			if (!app.logicalDevice) {
				terminate(
						"Creating logical device with graphics, compute and presentation queues");
			}

			// Prepare frame resources

			/// Creating the command pool
			if (!oge::createCommandPool(*app.logicalDevice,
					VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
					app.graphicsQueue.FamilyIndex, *app.commandPool)) {
				terminate("Creating the command pool");
			}
			InitVkDestroyer(app.logicalDevice, app.commandPool);

			for (uint32_t i = 0; i < AppData::FramesCount; ++i) {
				std::vector<VkCommandBuffer> command_buffer;
				oge::VkDestroyer (oge::VkSemaphore) image_acquired_semaphore;
				InitVkDestroyer(app.logicalDevice, image_acquired_semaphore);
				oge::VkDestroyer (oge::VkSemaphore) ready_to_present_semaphore;
				InitVkDestroyer(app.logicalDevice, ready_to_present_semaphore);
				oge::VkDestroyer (oge::VkFence) drawing_finished_fence;
				InitVkDestroyer(app.logicalDevice, drawing_finished_fence);
				oge::VkDestroyer (oge::VkImageView) depth_attachment;
				InitVkDestroyer(app.logicalDevice, depth_attachment);

				std::string msgSuf(
						std::string(" frame n. ") + std::to_string(i));
				/// Allocating command buffer(s)
				if (!oge::allocateCommandBuffers(*app.logicalDevice,
						*app.commandPool, VK_COMMAND_BUFFER_LEVEL_PRIMARY, 1,
						command_buffer)) {
					terminate("Allocating command buffer(s)" + msgSuf);
				}
				/// Creating acquired semaphore
				if (!oge::createSemaphore(*app.logicalDevice,
						*image_acquired_semaphore)) {
					terminate("Creating acquired semaphore" + msgSuf);
				}
				/// Creating to present semaphore
				if (!oge::createSemaphore(*app.logicalDevice,
						*ready_to_present_semaphore)) {
					terminate("Creating to present semaphore" + msgSuf);
				}
				/// Creating fence
				if (!oge::createFence(*app.logicalDevice, true,
						*drawing_finished_fence)) {
					terminate("Creating fence" + msgSuf);
				}

				oge::VkDestroyer (oge::VkFramebuffer) vkFramebuffer;

				app.framesResources.emplace_back(command_buffer[0],
						image_acquired_semaphore, ready_to_present_semaphore,
						drawing_finished_fence, depth_attachment,
						vkFramebuffer);
			}

			/// Creating the swapchain
			if (!createSwapchain(app.swapchain_image_usage, app.use_depth,
					app.depth_attachment_usage, app)) {
				terminate("Creating the swapchain");
			}
		}

		// Vertex data && Staging buffer && Uniform buffer
		{
			/// Prepare vertex data
			if (!oge::Load3DModelFromObjFile(modelPath, true, false, false,
					true, app.model)) {
				terminate("Prepare vertex data");
			}

			/// Creating a vertex buffer
			if (!oge::createBuffer(*app.logicalDevice,
					sizeof(app.model.Data[0]) * app.model.Data.size(),
					VK_BUFFER_USAGE_TRANSFER_DST_BIT
							| VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
					*app.vertexBuffer)) {
				terminate("Creating a vertex buffer");
			}
			InitVkDestroyer(app.logicalDevice, app.vertexBuffer);

			/// Allocating and binding a memory object for a vertex buffer
			if (!oge::allocateAndBindMemoryObjectToBuffer(app.physicalDevice,
					*app.logicalDevice, *app.vertexBuffer,
					VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
					*app.vertexBufferMemory)) {
				terminate("Allocating and binding a memory object for a buffer");
			}
			InitVkDestroyer(app.logicalDevice, app.vertexBufferMemory);

			/// Using a staging buffer to update a vertex buffer with a device-local memory bound
			if (!oge::useStagingBufferToUpdateBufferWithDeviceLocalMemoryBound(
					app.physicalDevice, *app.logicalDevice,
					sizeof(app.model.Data[0]) * app.model.Data.size(),
					&app.model.Data[0], *app.vertexBuffer, 0, 0,
					VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT,
					VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
					VK_PIPELINE_STAGE_VERTEX_INPUT_BIT,
					app.graphicsQueue.Handle,
					app.framesResources.front().CommandBuffer, { })) {
				terminate("Using a staging buffer to update a buffer with a device-local memory bound");
			}

			// TODO
		}
	}


	return EXIT_SUCCESS;
}

// Function definitions

namespace {
void error_callback(int error, const char *description) {
	std::cerr << "ERROR: " << description << std::endl;
}
void terminate(std::string msg) {
	std::cerr << "ERROR: " << msg << std::endl;
	glfwTerminate();
	exit(EXIT_FAILURE);
}

bool createSwapchain(VkImageUsageFlags swapchain_image_usage, bool use_depth,
		VkImageUsageFlags depth_attachment_usage, AppData &app) {
	oge::waitForAllSubmittedCommandsToBeFinished(*app.logicalDevice);

	app.ready = false;

	app.swapchain.imageViewsRaw.clear();
	app.swapchain.imageViews.clear();
	app.swapchain.images.clear();

	if (!app.swapchain.handle) {
		InitVkDestroyer(app.logicalDevice, app.swapchain.handle);
	}
	oge::VkDestroyer (oge::VkSwapchainKHR) old_swapchain = std::move(
			app.swapchain.handle);
	InitVkDestroyer(app.logicalDevice, app.swapchain.handle);
	if (!oge::createSwapchainWithR8G8B8A8FormatAndMailboxPresentMode(
			app.physicalDevice, *app.presentationSurface, *app.logicalDevice,
			swapchain_image_usage, app.swapchain.size, app.swapchain.format,
			*old_swapchain, *app.swapchain.handle, app.swapchain.images)) {
		return false;
	}
	if (!app.swapchain.handle) {
		return true;
	}

	for (size_t i = 0; i < app.swapchain.images.size(); ++i) {
		app.swapchain.imageViews.emplace_back(
				oge::VkDestroyer(oge::VkImageView)());
		InitVkDestroyer(app.logicalDevice, app.swapchain.imageViews.back());
		if (!oge::createImageView(*app.logicalDevice, app.swapchain.images[i],
				VK_IMAGE_VIEW_TYPE_2D, app.swapchain.format,
				VK_IMAGE_ASPECT_COLOR_BIT, *app.swapchain.imageViews.back())) {
			return false;
		}
		app.swapchain.imageViewsRaw.push_back(*app.swapchain.imageViews.back());
	}

	// When we want to use depth buffering, we need to use a depth attachment
	// It must have the same size as the swapchain, so we need to recreate it along with the swapchain
	app.depthImages.clear();
	app.depthImagesMemory.clear();

	if (use_depth) {
		for (uint32_t i = 0; i < AppData::FramesCount; ++i) {
			app.depthImages.emplace_back(oge::VkDestroyer(oge::VkImage)());
			InitVkDestroyer(app.logicalDevice, app.depthImages.back());
			app.depthImagesMemory.emplace_back(
					oge::VkDestroyer(oge::VkDeviceMemory)());
			InitVkDestroyer(app.logicalDevice, app.depthImagesMemory.back());
			InitVkDestroyer(app.logicalDevice,
					app.framesResources[i].depthAttachment);

			if (!oge::create2DImageAndView(app.physicalDevice,
					*app.logicalDevice, AppData::DepthFormat,
					app.swapchain.size, 1, 1, VK_SAMPLE_COUNT_1_BIT,
					VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
					VK_IMAGE_ASPECT_DEPTH_BIT, *app.depthImages.back(),
					*app.depthImagesMemory.back(),
					*app.framesResources[i].depthAttachment)) {
				return false;
			}
		}
	}

	app.ready = true;
	return true;
}

}
