/**
 * glm.cpp
 *
 *  Created on: Feb 22, 2021
 *      Author: beopenbefree
 */

#include "GL/gl3w.h"
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <iostream>
#include <iomanip>
#include "helpers/Tools.h"
#include "helpers/01 Preparing a translation matrix.h"
#include "helpers/02 Preparing a rotation matrix.h"
#include "helpers/03 Preparing a scaling matrix.h"
#include "helpers/04 Preparing a perspective projection matrix.h"
#include "helpers/05 Preparing an orthographic projection matrix.h"
#include "helpers/07 Loading a 3D model from an OBJ file.h"
#include <cmath>

using namespace std;
using namespace glm;
using namespace VulkanCookbook;

void report(const vec4 &v, const std::string &msg = std::string { });
void report(const mat4 &m, const std::string &msg = std::string { });
void report(const Matrix4x4 &m, const std::string &msg = std::string { });

int main(int argc, char **argv) {

	/// equivalences: rotations, translations between glm and VulkanCookbook REMARK
	array<float, 3> trans { 1.0, 2.0, 3.0 };
	float rotAngle = 130.0;
	array<float, 3> rotAxis { 1.0, 2.0, 3.0 };

	//
	mat4 T = translate(mat4 { 1.0 }, vec3 { trans[0], trans[1], trans[2] });
	Matrix4x4 Tc = PrepareTranslationMatrix(trans[0], trans[1], trans[2]);

	cout << sizeof(T[0][0]) * T.length() * T[0].length() << "==" << sizeof(T) << endl;

	//
//	mat4 R = rotate(mat4{ 1.0 }, radians(-rotAngle), normalize(vec3{rotAxis[0], rotAxis[1], rotAxis[2]}));
	mat4 R = rotate(mat4 { 1.0 }, radians(-rotAngle), vec3 { rotAxis[0],
			rotAxis[1], rotAxis[2] });
	Matrix4x4 Rc = PrepareRotationMatrix(rotAngle, Vector3 { rotAxis[0],
			rotAxis[1], rotAxis[2] }, true);

	report(T);
	report(Tc);
	report(R);
	report(Rc);

	report(R * T);
	report(Rc * Tc);

	report(T * R);
	report(Tc * Rc);

	/// equivalences: perspectives between glm and VulkanCookbook
	float fovy = 75.0; //degrees
	float aspect = 1.33;
	float near = 0.2, far = 25.0;
	mat4 P = perspective(radians(fovy), aspect, near, far);
	mat4 C { vec4 { 1.0, 0.0, 0.0, 0.0 }, vec4 { 0.0, -1.0, 0.0, 0.0 }, vec4 {
			0.0, 0.0, 0.5, 0.0 }, vec4 { 0.0, 0.0, 0.5, 1.0 } };
	Matrix4x4 Pc = PreparePerspectiveProjectionMatrix(aspect, fovy, near, far);

	report(C * P, "C * P");
	report(Pc, "Pc");

	/// glm tests about VIEW matrix: change from World to Camera frames REMARK
//	mat4 i{1.0}, m, v0, v1, v2;
//	vec3 posCam{0.0, 5.0, 5.0};
//	vec3 rotAxisCam{1.0, 0.0, 0.0};
//	float rotAngleCam{radians(-45.f)};
//	// model
//	m = rotate(i, (float) glfwGetTime(), vec3(0.0, 1.0, 0.0));
//	// view 0: using affine geometry theory
//	mat4 tCam{translate(i, posCam)}; // camera position
//	mat4 rCam{rotate(i, rotAngleCam, rotAxisCam)}; // camera rotation
//	v0 = inverse(tCam * rCam);
//	// view 1: matrix's direct construction (column by column)
//	vec4 translation{0.0, 0.0, -sqrt(pow(5.0, 2.0) + pow(5.0, 2.0)), 1.0}; // position of World center wrt Camera frame (ie as seen from Camera frame)
//	vec4 column1{1.0, 0.0, 0.0, 0.0}; // World's x axis wrt Camera frame (ie as computed with Camera frame)
//	vec4 column2{0.0, cos(rotAngleCam), cos(rotAngleCam), 0.0}; // World's y axis wrt Camera frame (ie as computed with Camera frame)
//	vec4 column3{0.0, -cos(rotAngleCam), cos(rotAngleCam), 0.0}; // World's z axis wrt Camera frame (ie as computed with Camera frame)
//	v1 = mat4{column1, column2, column3, translation};
//	// view 3: using lookAt() (center is (0,0,0) due to symmetry)
//	v2 = lookAt(posCam, vec3{}, vec3(0.0, 1.0, 0.0));
//	// check equivalence
//	report (v0, "v0");
//	report (v1, "v1");
//	report (v2, "v2");

	/// Test 0 REMARK
//	vec4 v0(0.0, 0.0, 0.0, 1.0);
//
//	mat4 I(1.0);
//	mat4 T = translate(I, vec3(10.0, 0.0, 0.0));
//	mat4 R = rotate(I, float(radians(45.0)), vec3(0.0, 1.0, 0.0));
//	report(T);
//	report(R);
//	report(R * T);
//	report(R * T * v0);
//
//	mat3x4 M34; // 3 columns, 4 rows
//
//	cout << v0.length() << endl;
//	cout << M34.length() << endl;
//	cout << M34[0].length() << endl;
//	cout << M34.length() * M34[0].length() << endl;

	return EXIT_SUCCESS;
}

void report(const vec4 &v, const std::string &msg) {
	cout << msg << endl;
	for (int c = 0; c < 4; c++) {
		cout << std::fixed << std::setw(9) << std::setprecision(6) << v[c]
				<< (c != 3 ? ", " : "");
	}
	cout << endl;
}
void report(const mat4 &m, const std::string &msg) {
	cout << msg << endl;
	for (int r = 0; r < 4; r++) {
		for (int c = 0; c < 4; c++) {
			cout << std::fixed << std::setw(11) << std::setprecision(6)
					<< m[c][r] << (c != 3 ? ", " : "");
		}
		cout << endl;
	}
	cout << endl;
}
void report(const Matrix4x4 &m, const std::string &msg) {
	cout << msg << endl;
	for (int r = 0; r < 4; r++) {
		for (int c = 0; c < 4; c++) {
			cout << std::fixed << std::setw(11) << std::setprecision(6)
					<< m[c * 4 + r] << (c != 3 ? ", " : "");
		}
		cout << endl;
	}
	cout << endl;
}
