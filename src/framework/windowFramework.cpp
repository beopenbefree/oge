/**
 * windowFramework.cpp
 *
 *  Created on: Mar 5, 2021
 *      Author: beopenbefree
 */

#include "framework/windowFramework.h"
#include "framework/application.h"

namespace oge {

WindowFramework::WindowFramework(const char *window_title, int x, int y,
		int width, int height, ApplicationBase &sample) :
		WindowParams { nullptr }, Sample { sample }, Initialized { false }, Created {
				false } {
	/// Setting an error callback
	glfwSetErrorCallback(error_callback);

	/// Initializing and terminating GLFW
	if (!glfwInit()) {
		return;
	}
	Initialized = true;

	/// Creating the window
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API); // mandatory
//	glfwWindowHint(GLFW_STEREO, GLFW_FALSE); // vulkan ignored
//	glfwWindowHint(GLFW_DOUBLEBUFFER, GLFW_TRUE); // vulkan ignored
//	glfwWindowHint(GLFW_CONTEXT_CREATION_API, GLFW_OSMESA_CONTEXT_API); // vulkan ignored
//	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
//	glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
//	glfwWindowHint(GLFW_DECORATED, GLFW_FALSE);
//	glfwWindowHint(GLFW_FOCUSED, GLFW_FALSE);
//	glfwWindowHint(GLFW_AUTO_ICONIFY, GLFW_FALSE);
//	glfwWindowHint(GLFW_FLOATING, GLFW_TRUE);
//	glfwWindowHint(GLFW_MAXIMIZED, GLFW_TRUE);
//	glfwWindowHint(GLFW_CENTER_CURSOR, GLFW_TRUE);
//	glfwWindowHint(GLFW_TRANSPARENT_FRAMEBUFFER, GLFW_TRUE); // vulkan ignored
//	glfwWindowHint(GLFW_FOCUS_ON_SHOW, GLFW_TRUE);
//	glfwWindowHint(GLFW_SCALE_TO_MONITOR, GLFW_TRUE);
			// Framebuffer related hints: vulkan ignored
//	glfwWindowHint(GLFW_REFRESH_RATE, GLFW_TRUE); // vulkan ?
			// Context related hints: vulkan ignored
//	glfwWindowHint(GLFW_X11_CLASS_NAME, 10);
//	glfwWindowHint(GLFW_X11_INSTANCE_NAME, 11);

	// windowed
	WindowParams = glfwCreateWindow(width, height, window_title, nullptr,
			nullptr);
	// full screen
//	GLFWmonitor *monitor = glfwGetPrimaryMonitor();
//	const GLFWvidmode *mode = glfwGetVideoMode(monitor);
//	glfwWindowHint(GLFW_RED_BITS, mode->redBits);
//	glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
//	glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
//	glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);
//	GLFWwindow *window = glfwCreateWindow(mode->width, mode->height, "My Title",
//			monitor, NULL);

	if (!WindowParams) {
		return;
	}
	Created = true;
	glfwSetWindowUserPointer(WindowParams, this);

	/// Receiving input events
	if (glfwRawMouseMotionSupported()) {
		glfwSetInputMode(WindowParams, GLFW_RAW_MOUSE_MOTION, GLFW_TRUE);
	}
	glfwSetMouseButtonCallback(WindowParams, mouse_button_callback);
	glfwSetCursorPosCallback(WindowParams, cursor_position_callback);
	glfwSetScrollCallback(WindowParams, scroll_callback);
}

WindowFramework::~WindowFramework() {
	/// Destroying the window
	if (Created && WindowParams) {
		glfwDestroyWindow(WindowParams);
		Created = false;
	}

	/// Terminating GLFW
	if (Initialized) {
		glfwTerminate();
		Initialized = false;
	}
}

void WindowFramework::Render() {
	if (Created && Sample.Initialize(WindowParams)) {

		/// Loop: checking the window close flag
		while (!glfwWindowShouldClose(WindowParams)) {
			/// Processing events
			glfwPollEvents();

			if (Sample.IsReady()) {
				Sample.UpdateTime();
				Sample.Draw();
				Sample.MouseReset();
			}
		}
	}
	Sample.Deinitialize();
}

void WindowFramework::error_callback(int error, const char *description) {
	std::cerr << "Error: " << description << std::endl;
}

void WindowFramework::mouse_button_callback(GLFWwindow *window, int button,
		int action, int mods) {
	size_t button_index;
	bool state = (action == GLFW_PRESS ? 1 : 0);
	switch (button) {
	case GLFW_MOUSE_BUTTON_LEFT:
		button_index = 0;
		break;
	case GLFW_MOUSE_BUTTON_RIGHT:
		button_index = 1;
		break;
	case GLFW_MOUSE_BUTTON_MIDDLE:
	default:
		button_index = -1;
		break;
	}
	if (button_index >= 0) {
		static_cast<WindowFramework*>(glfwGetWindowUserPointer(window))->Sample.MouseClick(
				button_index, state > 0);
	}
}

void WindowFramework::cursor_position_callback(GLFWwindow *window, double xpos,
		double ypos) {
	static_cast<WindowFramework*>(glfwGetWindowUserPointer(window))->Sample.MouseMove(
			static_cast<int>(xpos), static_cast<int>(ypos));
}

void WindowFramework::scroll_callback(GLFWwindow *window, double xoffset,
		double yoffset) {
	static_cast<WindowFramework*>(glfwGetWindowUserPointer(window))->Sample.MouseWheel(
			static_cast<float>(yoffset));
}

void WindowFramework::window_size_callback(GLFWwindow *window, int width,
		int height) {
	if (!static_cast<WindowFramework*>(glfwGetWindowUserPointer(window))->Sample.Resize()) {
		glfwSetWindowShouldClose(window, GLFW_TRUE);
	}
}

} /* namespace oge */
