/**
 * application.cpp
 *
 *  Created on: Mar 5, 2021
 *      Author: beopenbefree
 */

#include "framework/application.h"
#include "lib/instanceAndDevices.h"
#include "lib/imagePresentation.h"
#include "lib/commandBuffersAndSynchronization.h"
#include "lib/resourcesAndMemory.h"

namespace oge {

ApplicationBase::ApplicationBase() :
		Ready(false) {
}

ApplicationBase::~ApplicationBase() {
}

void ApplicationBase::MouseClick(size_t button_index, bool state) {
	if (2 > button_index) {
		MouseState.Buttons[button_index].IsPressed = state;
		MouseState.Buttons[button_index].WasClicked = state;
		MouseState.Buttons[button_index].WasRelease = !state;
		OnMouseEvent();
	}
}

void ApplicationBase::MouseMove(int x, int y) {
	MouseState.Position.Delta.X = x - MouseState.Position.X;
	MouseState.Position.Delta.Y = y - MouseState.Position.Y;
	MouseState.Position.X = x;
	MouseState.Position.Y = y;

	OnMouseEvent();
}

void ApplicationBase::MouseWheel(float distance) {
	MouseState.Wheel.WasMoved = true;
	MouseState.Wheel.Distance = distance;
	OnMouseEvent();
}

void ApplicationBase::MouseReset() {
	MouseState.Position.Delta.X = 0;
	MouseState.Position.Delta.Y = 0;
	MouseState.Buttons[0].WasClicked = false;
	MouseState.Buttons[0].WasRelease = false;
	MouseState.Buttons[1].WasClicked = false;
	MouseState.Buttons[1].WasRelease = false;
	MouseState.Wheel.WasMoved = false;
	MouseState.Wheel.Distance = 0.0f;
}

void ApplicationBase::UpdateTime() {
	TimerState.Update();
}

bool ApplicationBase::IsReady() {
	return Ready;
}

void ApplicationBase::OnMouseEvent() {
	// Override this in a derived class to know when a mouse event occured
}

bool Application::InitializeVulkan(GLFWwindow *window_parameters,
		VkPhysicalDeviceFeatures *desired_device_features,
		VkImageUsageFlags swapchain_image_usage, bool use_depth,
		VkImageUsageFlags depth_attachment_usage) {
	if (!connectWithVulkanLoaderLibrary()) {
		return false;
	}

	if (!loadFunctionExportedFromVulkanLoaderLibrary()) {
		return false;
	}

	if (!loadGlobalLevelFunctions()) {
		return false;
	}

	std::vector<char const*> instance_extensions;
	InitVkDestroyer(Instance);
	if (!createVulkanInstanceWithWsiExtensionsEnabled(instance_extensions,
			"Vulkan Cookbook", *Instance)) {
		return false;
	}

	if (!loadInstanceLevelFunctions(*Instance, instance_extensions)) {
		return false;
	}

	InitVkDestroyer(Instance, PresentationSurface);
	if (!createPresentationSurface(*Instance, window_parameters, nullptr,
			&(*PresentationSurface))) {
		return false;
	}

	std::vector<VkPhysicalDevice> physical_devices;
	enumerateAvailablePhysicalDevices(*Instance, physical_devices);

	for (auto &physical_device : physical_devices) {
		if (!selectIndexOfQueueFamilyWithDesiredCapabilities(physical_device,
				VK_QUEUE_GRAPHICS_BIT, GraphicsQueue.FamilyIndex)) {
			continue;
		}

		if (!selectIndexOfQueueFamilyWithDesiredCapabilities(physical_device,
				VK_QUEUE_COMPUTE_BIT, ComputeQueue.FamilyIndex)) {
			continue;
		}

		if (!selectQueueFamilyThatSupportsPresentationToGivenSurface(
				physical_device, *PresentationSurface,
				PresentQueue.FamilyIndex)) {
			continue;
		}

		std::vector<QueueInfo> requested_queues = { { GraphicsQueue.FamilyIndex,
				{ 1.0f } } };
		if (GraphicsQueue.FamilyIndex != ComputeQueue.FamilyIndex) {
			requested_queues.push_back( { ComputeQueue.FamilyIndex, { 1.0f } });
		}
		if ((GraphicsQueue.FamilyIndex != PresentQueue.FamilyIndex)
				&& (ComputeQueue.FamilyIndex != PresentQueue.FamilyIndex)) {
			requested_queues.push_back( { PresentQueue.FamilyIndex, { 1.0f } });
		}
		std::vector<char const*> device_extensions;
		InitVkDestroyer(LogicalDevice);
		if (!createLogicalDeviceWithWsiExtensionsEnabled(physical_device,
				requested_queues, device_extensions, desired_device_features,
				*LogicalDevice)) {
			continue;
		} else {
			PhysicalDevice = physical_device;
			loadDeviceLevelFunctions(*LogicalDevice, device_extensions);
			getDeviceQueue(*LogicalDevice, GraphicsQueue.FamilyIndex, 0,
					GraphicsQueue.Handle);
			getDeviceQueue(*LogicalDevice, ComputeQueue.FamilyIndex, 0,
					ComputeQueue.Handle);
			getDeviceQueue(*LogicalDevice, PresentQueue.FamilyIndex, 0,
					PresentQueue.Handle);
			break;
		}
	}

	if (!LogicalDevice) {
		return false;
	}

	// Prepare frame resources

	InitVkDestroyer(LogicalDevice, CommandPool);
	if (!createCommandPool(*LogicalDevice,
			VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
			GraphicsQueue.FamilyIndex, *CommandPool)) {
		return false;
	}

	for (uint32_t i = 0; i < FramesCount; ++i) {
		std::vector<VkCommandBuffer> command_buffer;
		VkDestroyer (VkSemaphore) image_acquired_semaphore;
		InitVkDestroyer(LogicalDevice, image_acquired_semaphore);
		VkDestroyer (VkSemaphore) ready_to_present_semaphore;
		InitVkDestroyer(LogicalDevice, ready_to_present_semaphore);
		VkDestroyer (VkFence) drawing_finished_fence;
		InitVkDestroyer(LogicalDevice, drawing_finished_fence);
		VkDestroyer (VkImageView) depth_attachment;
		InitVkDestroyer(LogicalDevice, depth_attachment);

		if (!allocateCommandBuffers(*LogicalDevice, *CommandPool,
				VK_COMMAND_BUFFER_LEVEL_PRIMARY, 1, command_buffer)) {
			return false;
		}
		if (!createSemaphore(*LogicalDevice, *image_acquired_semaphore)) {
			return false;
		}
		if (!createSemaphore(*LogicalDevice, *ready_to_present_semaphore)) {
			return false;
		}
		if (!createFence(*LogicalDevice, true, *drawing_finished_fence)) {
			return false;
		}

		VkDestroyer (VkFramebuffer) vkFramebuffer;

		FramesResources.emplace_back(command_buffer[0],
				image_acquired_semaphore, ready_to_present_semaphore,
				drawing_finished_fence, depth_attachment, vkFramebuffer);
	}

	if (!CreateSwapchain(swapchain_image_usage, use_depth,
			depth_attachment_usage)) {
		return false;
	}

	return true;
}

bool Application::CreateSwapchain(VkImageUsageFlags swapchain_image_usage,
		bool use_depth, VkImageUsageFlags depth_attachment_usage) {
	waitForAllSubmittedCommandsToBeFinished(*LogicalDevice);

	Ready = false;

	Swapchain.ImageViewsRaw.clear();
	Swapchain.ImageViews.clear();
	Swapchain.Images.clear();

	if (!Swapchain.Handle) {
		InitVkDestroyer(LogicalDevice, Swapchain.Handle);
	}
	VkDestroyer (VkSwapchainKHR) old_swapchain = std::move(Swapchain.Handle);
	InitVkDestroyer(LogicalDevice, Swapchain.Handle);
	if (!createSwapchainWithR8G8B8A8FormatAndMailboxPresentMode(PhysicalDevice,
			*PresentationSurface, *LogicalDevice, swapchain_image_usage,
			Swapchain.Size, Swapchain.Format, *old_swapchain, *Swapchain.Handle,
			Swapchain.Images)) {
		return false;
	}
	if (!Swapchain.Handle) {
		return true;
	}

	for (size_t i = 0; i < Swapchain.Images.size(); ++i) {
		Swapchain.ImageViews.emplace_back(VkDestroyer(VkImageView)());
		InitVkDestroyer(LogicalDevice, Swapchain.ImageViews.back());
		if (!createImageView(*LogicalDevice, Swapchain.Images[i],
				VK_IMAGE_VIEW_TYPE_2D, Swapchain.Format,
				VK_IMAGE_ASPECT_COLOR_BIT, *Swapchain.ImageViews.back())) {
			return false;
		}
		Swapchain.ImageViewsRaw.push_back(*Swapchain.ImageViews.back());
	}

	// When we want to use depth buffering, we need to use a depth attachment
	// It must have the same size as the swapchain, so we need to recreate it along with the swapchain
	DepthImages.clear();
	DepthImagesMemory.clear();

	if (use_depth) {
		for (uint32_t i = 0; i < FramesCount; ++i) {
			DepthImages.emplace_back(VkDestroyer(VkImage)());
			InitVkDestroyer(LogicalDevice, DepthImages.back());
			DepthImagesMemory.emplace_back(VkDestroyer(VkDeviceMemory)());
			InitVkDestroyer(LogicalDevice, DepthImagesMemory.back());
			InitVkDestroyer(LogicalDevice, FramesResources[i].DepthAttachment);

			if (!create2DImageAndView(PhysicalDevice, *LogicalDevice,
					DepthFormat, Swapchain.Size, 1, 1, VK_SAMPLE_COUNT_1_BIT,
					VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
					VK_IMAGE_ASPECT_DEPTH_BIT, *DepthImages.back(),
					*DepthImagesMemory.back(),
					*FramesResources[i].DepthAttachment)) {
				return false;
			}
		}
	}

	Ready = true;
	return true;
}

void Application::Deinitialize() {
	if (LogicalDevice) {
		waitForAllSubmittedCommandsToBeFinished(*LogicalDevice);
	}
}

} /* namespace oge */
