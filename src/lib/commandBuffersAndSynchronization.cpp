/**
 * commandBuffersAndSynchronization.cpp
 *
 *  Created on: Mar 15, 2021
 *      Author: beopenbefree
 */

#include "lib/commandBuffersAndSynchronization.h"
#include <iostream>

namespace oge {

bool createCommandPool(VkDevice logical_device,
		VkCommandPoolCreateFlags parameters, uint32_t queue_family,
		VkCommandPool &command_pool) {
	VkCommandPoolCreateInfo command_pool_create_info = {
			VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO, // VkStructureType              sType
			nullptr,                       // const void                 * pNext
			parameters,                    // VkCommandPoolCreateFlags     flags
			queue_family       // uint32_t                     queueFamilyIndex
			};

	VkResult result = vkCreateCommandPool(logical_device,
			&command_pool_create_info, nullptr, &command_pool);
	if (VK_SUCCESS != result) {
		std::cout << "Could not create command pool." << std::endl;
		return false;
	}
	return true;
}

bool allocateCommandBuffers(VkDevice logical_device, VkCommandPool command_pool,
		VkCommandBufferLevel level, uint32_t count,
		std::vector<VkCommandBuffer> &command_buffers) {
	VkCommandBufferAllocateInfo command_buffer_allocate_info = {
			VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO, // VkStructureType          sType
			nullptr,                           // const void             * pNext
			command_pool,                // VkCommandPool            commandPool
			level,                             // VkCommandBufferLevel     level
			count                // uint32_t                 commandBufferCount
			};

	command_buffers.resize(count);

	VkResult result = vkAllocateCommandBuffers(logical_device,
			&command_buffer_allocate_info, command_buffers.data());
	if (VK_SUCCESS != result) {
		std::cout << "Could not allocate command buffers." << std::endl;
		return false;
	}
	return true;
}

bool beginCommandBufferRecordingOperation(VkCommandBuffer command_buffer,
		VkCommandBufferUsageFlags usage,
		VkCommandBufferInheritanceInfo *secondary_command_buffer_info) {
	VkCommandBufferBeginInfo command_buffer_begin_info = {
			VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO, // VkStructureType                        sType
			nullptr,             // const void                           * pNext
			usage,               // VkCommandBufferUsageFlags              flags
			secondary_command_buffer_info // const VkCommandBufferInheritanceInfo * pInheritanceInfo
			};

	VkResult result = vkBeginCommandBuffer(command_buffer,
			&command_buffer_begin_info);
	if (VK_SUCCESS != result) {
		std::cout << "Could not begin command buffer recording operation."
				<< std::endl;
		return false;
	}
	return true;
}

bool endCommandBufferRecordingOperation(VkCommandBuffer command_buffer) {
	VkResult result = vkEndCommandBuffer(command_buffer);
	if (VK_SUCCESS != result) {
		std::cout << "Error occurred during command buffer recording."
				<< std::endl;
		return false;
	}
	return true;
}

bool resetCommandBuffer(VkCommandBuffer command_buffer,
		bool release_resources) {
	VkResult result = vkResetCommandBuffer(command_buffer,
			release_resources ?
					VK_COMMAND_BUFFER_RESET_RELEASE_RESOURCES_BIT : 0);
	if (VK_SUCCESS != result) {
		std::cout << "Error occurred during command buffer reset." << std::endl;
		return false;
	}
	return true;
}

bool resetCommandPool(VkDevice logical_device, VkCommandPool command_pool,
		bool release_resources) {
	VkResult result = vkResetCommandPool(logical_device, command_pool,
			release_resources ?
					VK_COMMAND_POOL_RESET_RELEASE_RESOURCES_BIT : 0);
	if (VK_SUCCESS != result) {
		std::cout << "Error occurred during command pool reset." << std::endl;
		return false;
	}
	return true;
}

bool createSemaphore(VkDevice logical_device, VkSemaphore &semaphore) {
	VkSemaphoreCreateInfo semaphore_create_info = {
			VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO, // VkStructureType            sType
			nullptr,                         // const void               * pNext
			0                               // VkSemaphoreCreateFlags     flags
			};

	VkResult result = vkCreateSemaphore(logical_device, &semaphore_create_info,
			nullptr, &semaphore);
	if (VK_SUCCESS != result) {
		std::cout << "Could not create a semaphore." << std::endl;
		return false;
	}
	return true;
}

bool createFence(VkDevice logical_device, bool signaled, VkFence &fence) {
	VkFenceCreateInfo fence_create_info = { VK_STRUCTURE_TYPE_FENCE_CREATE_INFO, // VkStructureType        sType
			nullptr,                             // const void           * pNext
			signaled ? VK_FENCE_CREATE_SIGNALED_BIT : 0u, // VkFenceCreateFlags     flags
			};

	VkResult result = vkCreateFence(logical_device, &fence_create_info, nullptr,
			&fence);
	if (VK_SUCCESS != result) {
		std::cout << "Could not create a fence." << std::endl;
		return false;
	}
	return true;
}

bool waitForFences(VkDevice logical_device, std::vector<VkFence> const &fences,
		VkBool32 wait_for_all, uint64_t timeout) {
	if (fences.size() > 0) {
		VkResult result = vkWaitForFences(logical_device,
				static_cast<uint32_t>(fences.size()), fences.data(),
				wait_for_all, timeout);
		if (VK_SUCCESS != result) {
			std::cout << "Waiting on fence failed." << std::endl;
			return false;
		}
		return true;
	}
	return false;
}

bool resetFences(VkDevice logical_device, std::vector<VkFence> const &fences) {
	if (fences.size() > 0) {
		VkResult result = vkResetFences(logical_device,
				static_cast<uint32_t>(fences.size()), fences.data());
		if (VK_SUCCESS != result) {
			std::cout << "Error occurred when tried to reset fences."
					<< std::endl;
			return false;
		}
		return VK_SUCCESS == result;
	}
	return false;
}

bool submitCommandBuffersToQueue(VkQueue queue,
		std::vector<WaitSemaphoreInfo> wait_semaphore_infos,
		std::vector<VkCommandBuffer> command_buffers,
		std::vector<VkSemaphore> signal_semaphores, VkFence fence) {
	// submit only one batch
	return submitCommandBufferBatchesToQueue(queue, { wait_semaphore_infos }, {
			command_buffers }, { signal_semaphores }, fence);
}

namespace {
inline void doCreatCommandBufferBatchSubmitInfo(
		const std::vector<WaitSemaphoreInfo> &wait_semaphore_infos,
		const std::vector<VkCommandBuffer> &command_buffers,
		const std::vector<VkSemaphore> &signal_semaphores,
		VkSubmitInfo &submit_info,
		std::vector<VkSemaphore> &wait_semaphore_handles,
		std::vector<VkPipelineStageFlags> &wait_semaphore_stages) {

	for (auto &wait_semaphore_info : wait_semaphore_infos) {
		wait_semaphore_handles.emplace_back(wait_semaphore_info.Semaphore);
		wait_semaphore_stages.emplace_back(wait_semaphore_info.WaitingStage);
	}

	submit_info = { VK_STRUCTURE_TYPE_SUBMIT_INFO, // VkStructureType                sType
			nullptr,                     // const void                   * pNext
			static_cast<uint32_t>(wait_semaphore_infos.size()), // uint32_t                       waitSemaphoreCount
			wait_semaphore_handles.data(), // const VkSemaphore            * pWaitSemaphores
			wait_semaphore_stages.data(), // const VkPipelineStageFlags   * pWaitDstStageMask
			static_cast<uint32_t>(command_buffers.size()), // uint32_t                       commandBufferCount
			command_buffers.data(), // const VkCommandBuffer        * pCommandBuffers
			static_cast<uint32_t>(signal_semaphores.size()), // uint32_t                       signalSemaphoreCount
			signal_semaphores.data() // const VkSemaphore            * pSignalSemaphores
			};
}
}

bool submitCommandBufferBatchesToQueue(VkQueue queue,
		std::vector<std::vector<WaitSemaphoreInfo>> batches_wait_semaphore_infos,
		std::vector<std::vector<VkCommandBuffer>> batches_command_buffers,
		std::vector<std::vector<VkSemaphore>> batches_signal_semaphores,
		VkFence fence) {
	// assert: batches_wait_semaphore_infos.size() == batches_command_buffers.size() == batches_signal_semaphores.size()
	std::vector<VkSubmitInfo> batches_submit_info;
	std::vector<std::vector<VkSemaphore>> batches_wait_semaphore_handle;
	std::vector<std::vector<VkPipelineStageFlags>> batches_wait_semaphore_stage;

	for (size_t i = 0; i < batches_command_buffers.size(); i++) {
		batches_submit_info.emplace_back(VkSubmitInfo());
		batches_wait_semaphore_handle.emplace_back(std::vector<VkSemaphore>());
		batches_wait_semaphore_stage.emplace_back(
				std::vector<VkPipelineStageFlags>());

		doCreatCommandBufferBatchSubmitInfo(batches_wait_semaphore_infos[i],
				batches_command_buffers[i], batches_signal_semaphores[i],
				batches_submit_info.back(),
				batches_wait_semaphore_handle.back(),
				batches_wait_semaphore_stage.back());
	}

	VkResult result = vkQueueSubmit(queue, batches_submit_info.size(),
			batches_submit_info.data(), fence);
	if (VK_SUCCESS != result) {
		std::cout << "Error occurred during submission of "
				<< std::to_string(batches_submit_info.size())
				<< " command buffer batch(es)." << std::endl;
		return false;
	}
	return true;
}

bool synchronizeTwoCommandBuffers(VkQueue first_queue,
		std::vector<WaitSemaphoreInfo> first_wait_semaphore_infos,
		std::vector<VkCommandBuffer> first_command_buffers,
		std::vector<WaitSemaphoreInfo> synchronizing_semaphores,
		VkQueue second_queue,
		std::vector<VkCommandBuffer> second_command_buffers,
		std::vector<VkSemaphore> second_signal_semaphores,
		VkFence second_fence) {
	std::vector<VkSemaphore> first_signal_semaphores;
	for (auto &semaphore_info : synchronizing_semaphores) {
		first_signal_semaphores.emplace_back(semaphore_info.Semaphore);
	}
	if (!submitCommandBuffersToQueue(first_queue, first_wait_semaphore_infos,
			first_command_buffers, first_signal_semaphores, VK_NULL_HANDLE)) {
		return false;
	}

	if (!submitCommandBuffersToQueue(second_queue, synchronizing_semaphores,
			second_command_buffers, second_signal_semaphores, second_fence)) {
		return false;
	}
	return true;
}

bool checkIfProcessingOfSubmittedCommandBufferHasFinished(
		VkDevice logical_device, VkQueue queue,
		std::vector<WaitSemaphoreInfo> wait_semaphore_infos,
		std::vector<VkCommandBuffer> command_buffers,
		std::vector<VkSemaphore> signal_semaphores, VkFence fence,
		uint64_t timeout, VkResult &wait_status) {
	if (!submitCommandBuffersToQueue(queue, wait_semaphore_infos,
			command_buffers, signal_semaphores, fence)) {
		return false;
	}

	return waitForFences(logical_device, { fence }, VK_FALSE, timeout);
}

bool waitUntilAllCommandsSubmittedToQueueAreFinished(VkQueue queue) {
	VkResult result = vkQueueWaitIdle(queue);
	if (VK_SUCCESS != result) {
		std::cout << "Waiting for all operations submitted to queue failed."
				<< std::endl;
		return false;
	}
	return true;
}

bool waitForAllSubmittedCommandsToBeFinished(VkDevice logical_device) {
	VkResult result = vkDeviceWaitIdle(logical_device);
	if (VK_SUCCESS != result) {
		std::cout << "Waiting on a device failed." << std::endl;
		return false;
	}
	return true;
}

void destroyFence(VkDevice logical_device, VkFence &fence) {
	if ( VK_NULL_HANDLE != fence) {
		vkDestroyFence(logical_device, fence, nullptr);
		fence = VK_NULL_HANDLE;
	}
}

void destroySemaphore(VkDevice logical_device, VkSemaphore &semaphore) {
	if ( VK_NULL_HANDLE != semaphore) {
		vkDestroySemaphore(logical_device, semaphore, nullptr);
		semaphore = VK_NULL_HANDLE;
	}
}

void freeCommandBuffers(VkDevice logical_device, VkCommandPool command_pool,
		std::vector<VkCommandBuffer> &command_buffers) {
	if (command_buffers.size() > 0) {
		vkFreeCommandBuffers(logical_device, command_pool,
				static_cast<uint32_t>(command_buffers.size()),
				command_buffers.data());
		command_buffers.clear();
	}
}

void destroyCommandPool(VkDevice logical_device, VkCommandPool &command_pool) {
	if ( VK_NULL_HANDLE != command_pool) {
		vkDestroyCommandPool(logical_device, command_pool, nullptr);
		command_pool = VK_NULL_HANDLE;
	}
}

}  // namespace oge

