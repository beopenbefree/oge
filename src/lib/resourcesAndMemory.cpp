/**
 * resourcesAndMemory.cpp
 *
 *  Created on: Mar 17, 2021
 *      Author: beopenbefree
 */

#include <iostream>
#include <cstring>
#include "lib/resourcesAndMemory.h"
#include "lib/commandBuffersAndSynchronization.h"

namespace oge {

bool createBuffer(VkDevice logical_device, VkDeviceSize size,
		VkBufferUsageFlags usage, VkBuffer &buffer, VkSharingMode sharingMode,
		uint32_t queueFamilyIndexCount, const uint32_t *pQueueFamilyIndices) {
	VkBufferCreateInfo buffer_create_info = {
			VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO, // VkStructureType        sType
			nullptr,                             // const void           * pNext
			0,                                   // VkBufferCreateFlags    flags
			size,                                 // VkDeviceSize           size
			usage,                               // VkBufferUsageFlags     usage
			sharingMode,     // VkSharingMode          sharingMode
			queueFamilyIndexCount, // uint32_t               queueFamilyIndexCount
			pQueueFamilyIndices   // const uint32_t       * pQueueFamilyIndices
			};

	VkResult result = vkCreateBuffer(logical_device, &buffer_create_info,
			nullptr, &buffer);
	if (VK_SUCCESS != result) {
		std::cout << "Could not create a buffer." << std::endl;
		return false;
	}
	return true;
}

bool allocateAndBindMemoryObjectToBuffer(VkPhysicalDevice physical_device,
		VkDevice logical_device, VkBuffer buffer,
		VkMemoryPropertyFlags memory_properties,
		VkDeviceMemory &memory_object) {
	VkPhysicalDeviceMemoryProperties physical_device_memory_properties;
	vkGetPhysicalDeviceMemoryProperties(physical_device,
			&physical_device_memory_properties);

	VkMemoryRequirements memory_requirements;
	vkGetBufferMemoryRequirements(logical_device, buffer, &memory_requirements);

	memory_object = VK_NULL_HANDLE;
	for (uint32_t type = 0;
			type < physical_device_memory_properties.memoryTypeCount; ++type) {
		if ((memory_requirements.memoryTypeBits & (1 << type))
				&& ((physical_device_memory_properties.memoryTypes[type].propertyFlags
						& memory_properties) == memory_properties)) {

			VkMemoryAllocateInfo buffer_memory_allocate_info = {
					VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO, // VkStructureType    sType
					nullptr,                         // const void       * pNext
					memory_requirements.size, // VkDeviceSize       allocationSize
					type                  // uint32_t           memoryTypeIndex
					};

			VkResult result = vkAllocateMemory(logical_device,
					&buffer_memory_allocate_info, nullptr, &memory_object);
			if (VK_SUCCESS == result) {
				break;
			}
		}
	}

	if ( VK_NULL_HANDLE == memory_object) {
		std::cout << "Could not allocate memory for a buffer." << std::endl;
		return false;
	}

	VkResult result = vkBindBufferMemory(logical_device, buffer, memory_object,
			0);
	if (VK_SUCCESS != result) {
		std::cout << "Could not bind memory object to a buffer." << std::endl;
		return false;
	}
	return true;
}

void setBufferMemoryBarrier(VkCommandBuffer command_buffer,
		VkPipelineStageFlags generating_stages,
		VkPipelineStageFlags consuming_stages,
		std::vector<BufferTransition> buffer_transitions, VkDeviceSize offset,
		VkDeviceSize size) {

	std::vector<VkBufferMemoryBarrier> buffer_memory_barriers;

	for (auto &buffer_transition : buffer_transitions) {
		buffer_memory_barriers.push_back( {
				VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER, // VkStructureType    sType
				nullptr,                             // const void       * pNext
				buffer_transition.CurrentAccess, // VkAccessFlags      srcAccessMask
				buffer_transition.NewAccess, // VkAccessFlags      dstAccessMask
				buffer_transition.CurrentQueueFamily, // uint32_t           srcQueueFamilyIndex
				buffer_transition.NewQueueFamily, // uint32_t           dstQueueFamilyIndex
				buffer_transition.Buffer,           // VkBuffer           buffer
				offset,                             // VkDeviceSize       offset
				size                        // VkDeviceSize       size
				});
	}

	if (buffer_memory_barriers.size() > 0) {
		vkCmdPipelineBarrier(command_buffer, generating_stages,
				consuming_stages, 0, 0, nullptr,
				static_cast<uint32_t>(buffer_memory_barriers.size()),
				buffer_memory_barriers.data(), 0, nullptr);
	}
}

bool createBufferView(VkDevice logical_device, VkBuffer buffer, VkFormat format,
		VkDeviceSize memory_offset, VkDeviceSize memory_range,
		VkBufferView &buffer_view) {
	VkBufferViewCreateInfo buffer_view_create_info = {
			VK_STRUCTURE_TYPE_BUFFER_VIEW_CREATE_INFO, // VkStructureType            sType
			nullptr,                         // const void               * pNext
			0,                               // VkBufferViewCreateFlags    flags
			buffer,                         // VkBuffer                   buffer
			format,                         // VkFormat                   format
			memory_offset,                  // VkDeviceSize               offset
			memory_range                    // VkDeviceSize               range
			};

	VkResult result = vkCreateBufferView(logical_device,
			&buffer_view_create_info, nullptr, &buffer_view);
	if (VK_SUCCESS != result) {
		std::cout << "Could not creat buffer view." << std::endl;
		return false;
	}
	return true;
}

bool createImage(VkDevice logical_device, VkImageType type, VkFormat format,
		VkExtent3D size, uint32_t num_mipmaps, uint32_t num_layers,
		VkSampleCountFlagBits samples, VkImageUsageFlags usage_scenarios,
		bool cubemap, VkImage &image, VkImageTiling tiling,
		VkSharingMode sharingMode, VkImageLayout initialLayout) {
	VkImageCreateInfo image_create_info = { VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO, // VkStructureType          sType
			nullptr,                           // const void             * pNext
			cubemap ? VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT : 0u, // VkImageCreateFlags       flags
			type,                          // VkImageType              imageType
			format,                           // VkFormat                 format
			size,                             // VkExtent3D               extent
			num_mipmaps,                   // uint32_t                 mipLevels
			cubemap ? 6 * num_layers : num_layers, // uint32_t                 arrayLayers
			samples,                         // VkSampleCountFlagBits    samples
			tiling,          // VkImageTiling            tiling
			usage_scenarios,                   // VkImageUsageFlags        usage
			sharingMode,   // VkSharingMode            sharingMode
			0,                 // uint32_t                 queueFamilyIndexCount
			nullptr,             // const uint32_t         * pQueueFamilyIndices
			initialLayout // VkImageLayout            initialLayout
			};

	VkResult result = vkCreateImage(logical_device, &image_create_info, nullptr,
			&image);
	if (VK_SUCCESS != result) {
		std::cout << "Could not create an image." << std::endl;
		return false;
	}
	return true;
}

bool allocateAndBindMemoryObjectToImage(VkPhysicalDevice physical_device,
		VkDevice logical_device, VkImage image,
		VkMemoryPropertyFlagBits memory_properties,
		VkDeviceMemory &memory_object) {
	VkPhysicalDeviceMemoryProperties physical_device_memory_properties;
	vkGetPhysicalDeviceMemoryProperties(physical_device,
			&physical_device_memory_properties);

	VkMemoryRequirements memory_requirements;
	vkGetImageMemoryRequirements(logical_device, image, &memory_requirements);

	memory_object = VK_NULL_HANDLE;
	for (uint32_t type = 0;
			type < physical_device_memory_properties.memoryTypeCount; ++type) {
		if ((memory_requirements.memoryTypeBits & (1 << type))
				&& ((physical_device_memory_properties.memoryTypes[type].propertyFlags
						& memory_properties) == memory_properties)) {

			VkMemoryAllocateInfo image_memory_allocate_info = {
					VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO, // VkStructureType    sType
					nullptr,                         // const void       * pNext
					memory_requirements.size, // VkDeviceSize       allocationSize
					type                  // uint32_t           memoryTypeIndex
					};

			VkResult result = vkAllocateMemory(logical_device,
					&image_memory_allocate_info, nullptr, &memory_object);
			if (VK_SUCCESS == result) {
				break;
			}
		}
	}

	if ( VK_NULL_HANDLE == memory_object) {
		std::cout << "Could not allocate memory for an image." << std::endl;
		return false;
	}

	VkResult result = vkBindImageMemory(logical_device, image, memory_object,
			0);
	if (VK_SUCCESS != result) {
		std::cout << "Could not bind memory object to an image." << std::endl;
		return false;
	}
	return true;
}

void setImageMemoryBarrier(VkCommandBuffer command_buffer,
		VkPipelineStageFlags generating_stages,
		VkPipelineStageFlags consuming_stages,
		std::vector<ImageTransition> image_transitions) {
	std::vector<VkImageMemoryBarrier> image_memory_barriers;

	for (auto &image_transition : image_transitions) {
		image_memory_barriers.push_back( {
				VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER, // VkStructureType            sType
				nullptr,                     // const void               * pNext
				image_transition.CurrentAccess, // VkAccessFlags              srcAccessMask
				image_transition.NewAccess, // VkAccessFlags              dstAccessMask
				image_transition.CurrentLayout, // VkImageLayout              oldLayout
				image_transition.NewLayout, // VkImageLayout              newLayout
				image_transition.CurrentQueueFamily, // uint32_t                   srcQueueFamilyIndex
				image_transition.NewQueueFamily, // uint32_t                   dstQueueFamilyIndex
				image_transition.Image,      // VkImage                    image
				{                 // VkImageSubresourceRange    subresourceRange
				image_transition.Aspect, // VkImageAspectFlags         aspectMask
						0,            // uint32_t                   baseMipLevel
						VK_REMAINING_MIP_LEVELS, // uint32_t                   levelCount
						0,          // uint32_t                   baseArrayLayer
						VK_REMAINING_ARRAY_LAYERS // uint32_t                   layerCount
				} });
	}

	if (image_memory_barriers.size() > 0) {
		vkCmdPipelineBarrier(command_buffer, generating_stages,
				consuming_stages, 0, 0, nullptr, 0, nullptr,
				static_cast<uint32_t>(image_memory_barriers.size()),
				image_memory_barriers.data());
	}
}

bool createImageView(VkDevice logical_device, VkImage image,
		VkImageViewType view_type, VkFormat format, VkImageAspectFlags aspect,
		VkImageView &image_view, VkComponentMapping components) {
	VkImageViewCreateInfo image_view_create_info = {
			VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO, // VkStructureType            sType
			nullptr,                         // const void               * pNext
			0,                               // VkImageViewCreateFlags     flags
			image,                           // VkImage                    image
			view_type,                    // VkImageViewType            viewType
			format,                         // VkFormat                   format
			components, {         // VkImageSubresourceRange    subresourceRange
			aspect,                     // VkImageAspectFlags         aspectMask
					0,                // uint32_t                   baseMipLevel
					VK_REMAINING_MIP_LEVELS, // uint32_t                   levelCount
					0,              // uint32_t                   baseArrayLayer
					VK_REMAINING_ARRAY_LAYERS // uint32_t                   layerCount
			} };

	VkResult result = vkCreateImageView(logical_device, &image_view_create_info,
			nullptr, &image_view);
	if (VK_SUCCESS != result) {
		std::cout << "Could not create an image view." << std::endl;
		return false;
	}
	return true;
}

bool create2DImageAndView(VkPhysicalDevice physical_device,
		VkDevice logical_device, VkFormat format, VkExtent2D size,
		uint32_t num_mipmaps, uint32_t num_layers,
		VkSampleCountFlagBits samples, VkImageUsageFlags usage,
		VkImageAspectFlags aspect, VkImage &image,
		VkDeviceMemory &memory_object, VkImageView &image_view) {
	if (!createImage(logical_device, VK_IMAGE_TYPE_2D, format, { size.width,
			size.height, 1 }, num_mipmaps, num_layers, samples, usage, false,
			image)) {
		return false;
	}

	if (!allocateAndBindMemoryObjectToImage(physical_device, logical_device,
			image, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, memory_object)) {
		return false;
	}

	if (!createImageView(logical_device, image, VK_IMAGE_VIEW_TYPE_2D, format,
			aspect, image_view)) {
		return false;
	}

	return true;
}

bool createLayered2DImageWithCubemapView(VkPhysicalDevice physical_device,
		VkDevice logical_device, uint32_t size, uint32_t num_mipmaps,
		VkImageUsageFlags usage, VkImageAspectFlags aspect, VkImage &image,
		VkDeviceMemory &memory_object, VkImageView &image_view) {
	if (!createImage(logical_device, VK_IMAGE_TYPE_2D, VK_FORMAT_R8G8B8A8_UNORM,
			{ size, size, 1 }, num_mipmaps, 6, VK_SAMPLE_COUNT_1_BIT, usage,
			true, image)) {
		return false;
	}

	if (!allocateAndBindMemoryObjectToImage(physical_device, logical_device,
			image, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, memory_object)) {
		return false;
	}

	if (!createImageView(logical_device, image, VK_IMAGE_VIEW_TYPE_CUBE,
			VK_FORMAT_R8G8B8A8_UNORM, aspect, image_view)) {
		return false;
	}

	return true;
}

bool mapUpdateAndUnmapHostVisibleMemory(VkDevice logical_device,
		VkDeviceMemory memory_object, VkDeviceSize offset,
		VkDeviceSize data_size, void *data, bool unmap, void **pointer) {
	VkResult result;
	void *local_pointer;
	result = vkMapMemory(logical_device, memory_object, offset, data_size, 0,
			&local_pointer);
	if (VK_SUCCESS != result) {
		std::cout << "Could not map memory object." << std::endl;
		return false;
	}

	std::memcpy(local_pointer, data, static_cast<size_t>(data_size));

	std::vector<VkMappedMemoryRange> memory_ranges = { {
			VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE,  // VkStructureType    sType
			nullptr,                                // const void       * pNext
			memory_object,                          // VkDeviceMemory     memory
			offset,                                 // VkDeviceSize       offset
			VK_WHOLE_SIZE                           // VkDeviceSize       size
			} };

	result = vkFlushMappedMemoryRanges(logical_device,
			static_cast<uint32_t>(memory_ranges.size()), memory_ranges.data());
	if (VK_SUCCESS != result) {
		std::cout << "Could not flush mapped memory." << std::endl;
		return false;
	}

	if (unmap) {
		vkUnmapMemory(logical_device, memory_object);
	} else if (nullptr != pointer) {
		*pointer = local_pointer;
	}

	return true;
}

bool mapHostVisibleMemory(VkDevice logical_device, VkDeviceMemory memory_object,
		VkDeviceSize offset, VkDeviceSize data_size, void **pointer) {
	void *local_pointer;
	VkResult result = vkMapMemory(logical_device, memory_object, offset,
			data_size, 0, &local_pointer);
	if (VK_SUCCESS != result) {
		std::cout << "Could not map memory object." << std::endl;
		return false;
	}

	*pointer = local_pointer;

	return true;
}

bool updateHostVisibleMemory(VkDevice logical_device,
		VkDeviceMemory memory_object, VkDeviceSize offset,
		VkDeviceSize data_size, void *data, void *local_pointer) {
	std::memcpy(local_pointer, data, static_cast<size_t>(data_size));

	std::vector<VkMappedMemoryRange> memory_ranges = { {
			VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE,  // VkStructureType    sType
			nullptr,                                // const void       * pNext
			memory_object,                          // VkDeviceMemory     memory
			offset,                                 // VkDeviceSize       offset
			VK_WHOLE_SIZE                           // VkDeviceSize       size
			} };

	VkResult result = vkFlushMappedMemoryRanges(logical_device,
			static_cast<uint32_t>(memory_ranges.size()), memory_ranges.data());
	if (VK_SUCCESS != result) {
		std::cout << "Could not flush mapped memory." << std::endl;
		return false;
	}

	return true;
}

bool unmapHostVisibleMemory(VkDevice logical_device,
		VkDeviceMemory memory_object) {
	vkUnmapMemory(logical_device, memory_object);

	return true;
}

void copyDataBetweenBuffers(VkCommandBuffer command_buffer,
		VkBuffer source_buffer, VkBuffer destination_buffer,
		std::vector<VkBufferCopy> regions) {
	if (regions.size() > 0) {
		vkCmdCopyBuffer(command_buffer, source_buffer, destination_buffer,
				static_cast<uint32_t>(regions.size()), regions.data());
	}
}

void copyDataFromBufferToImage(VkCommandBuffer command_buffer,
		VkBuffer source_buffer, VkImage destination_image,
		VkImageLayout image_layout, std::vector<VkBufferImageCopy> regions) {
	if (regions.size() > 0) {
		vkCmdCopyBufferToImage(command_buffer, source_buffer, destination_image,
				image_layout, static_cast<uint32_t>(regions.size()),
				regions.data());
	}
}

void copyDataFromImageToBuffer(VkCommandBuffer command_buffer,
		VkImage source_image, VkImageLayout image_layout,
		VkBuffer destination_buffer, std::vector<VkBufferImageCopy> regions) {
	if (regions.size() > 0) {
		vkCmdCopyImageToBuffer(command_buffer, source_image, image_layout,
				destination_buffer, static_cast<uint32_t>(regions.size()),
				regions.data());
	}
}

bool useStagingBufferToUpdateBufferWithDeviceLocalMemoryBound(
		VkPhysicalDevice physical_device, VkDevice logical_device,
		VkDeviceSize data_size, void *data, VkBuffer destination_buffer,
		VkDeviceSize destination_offset,
		VkAccessFlags destination_buffer_current_access,
		VkAccessFlags destination_buffer_new_access,
		VkPipelineStageFlags destination_buffer_generating_stages,
		VkPipelineStageFlags destination_buffer_consuming_stages, VkQueue queue,
		VkCommandBuffer command_buffer,
		std::vector<VkSemaphore> signal_semaphores) {

	VkDestroyer (VkBuffer) staging_buffer;
	InitVkDestroyer(logical_device, staging_buffer);
	if (!createBuffer(logical_device, data_size,
			VK_BUFFER_USAGE_TRANSFER_SRC_BIT, *staging_buffer)) {
		return false;
	}

	VkDestroyer (VkDeviceMemory) memory_object;
	InitVkDestroyer(logical_device, memory_object);
	if (!allocateAndBindMemoryObjectToBuffer(physical_device, logical_device,
			*staging_buffer, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
			*memory_object)) {
		return false;
	}

	if (!mapUpdateAndUnmapHostVisibleMemory(logical_device, *memory_object, 0,
			data_size, data, true, nullptr)) {
		return false;
	}

	if (!beginCommandBufferRecordingOperation(command_buffer,
			VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT, nullptr)) {
		return false;
	}

	setBufferMemoryBarrier(command_buffer, destination_buffer_generating_stages,
			VK_PIPELINE_STAGE_TRANSFER_BIT, { { destination_buffer,
					destination_buffer_current_access,
					VK_ACCESS_TRANSFER_WRITE_BIT, VK_QUEUE_FAMILY_IGNORED,
					VK_QUEUE_FAMILY_IGNORED } });

	copyDataBetweenBuffers(command_buffer, *staging_buffer, destination_buffer,
			{ { 0, destination_offset, data_size } });

	setBufferMemoryBarrier(command_buffer, VK_PIPELINE_STAGE_TRANSFER_BIT,
			destination_buffer_consuming_stages, { { destination_buffer,
					VK_ACCESS_TRANSFER_WRITE_BIT, destination_buffer_new_access,
					VK_QUEUE_FAMILY_IGNORED,
					VK_QUEUE_FAMILY_IGNORED } });

	if (!endCommandBufferRecordingOperation(command_buffer)) {
		return false;
	}

	VkDestroyer (VkFence) fence;
	InitVkDestroyer(logical_device, fence);
	if (!createFence(logical_device, false, *fence)) {
		return false;
	}

	if (!submitCommandBuffersToQueue(queue, { }, { command_buffer },
			signal_semaphores, *fence)) {
		return false;
	}

	if (!waitForFences(logical_device, { *fence }, VK_FALSE, 500000000)) {
		return false;
	}

	return true;
}

bool useStagingBufferToUpdateImageWithDeviceLocalMemoryBound(
		VkPhysicalDevice physical_device, VkDevice logical_device,
		VkDeviceSize data_size, void *data, VkImage destination_image,
		VkImageSubresourceLayers destination_image_subresource,
		VkOffset3D destination_image_offset, VkExtent3D destination_image_size,
		VkImageLayout destination_image_current_layout,
		VkImageLayout destination_image_new_layout,
		VkAccessFlags destination_image_current_access,
		VkAccessFlags destination_image_new_access,
		VkImageAspectFlags destination_image_aspect,
		VkPipelineStageFlags destination_image_generating_stages,
		VkPipelineStageFlags destination_image_consuming_stages, VkQueue queue,
		VkCommandBuffer command_buffer,
		std::vector<VkSemaphore> signal_semaphores) {

	VkDestroyer(VkBuffer) staging_buffer;
	InitVkDestroyer(logical_device, staging_buffer);
	if (!createBuffer(logical_device, data_size,
			VK_BUFFER_USAGE_TRANSFER_SRC_BIT, *staging_buffer)) {
		return false;
	}

	VkDestroyer(VkDeviceMemory) memory_object;
	InitVkDestroyer(logical_device, memory_object);
	if (!allocateAndBindMemoryObjectToBuffer(physical_device, logical_device,
			*staging_buffer, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
			*memory_object)) {
		return false;
	}

	if (!mapUpdateAndUnmapHostVisibleMemory(logical_device, *memory_object, 0,
			data_size, data, true, nullptr)) {
		return false;
	}

	if (!beginCommandBufferRecordingOperation(command_buffer,
			VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT, nullptr)) {
		return false;
	}

	setImageMemoryBarrier(command_buffer, destination_image_generating_stages,
			VK_PIPELINE_STAGE_TRANSFER_BIT, { { destination_image, // VkImage            Image
					destination_image_current_access, // VkAccessFlags      CurrentAccess
					VK_ACCESS_TRANSFER_WRITE_BIT, // VkAccessFlags      NewAccess
					destination_image_current_layout, // VkImageLayout      CurrentLayout
					VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, // VkImageLayout      NewLayout
					VK_QUEUE_FAMILY_IGNORED, // uint32_t           CurrentQueueFamily
					VK_QUEUE_FAMILY_IGNORED, // uint32_t           NewQueueFamily
					destination_image_aspect       // VkImageAspectFlags Aspect
					} });

	copyDataFromBufferToImage(command_buffer, *staging_buffer,
			destination_image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, { { 0, // VkDeviceSize               bufferOffset
					0,             // uint32_t                   bufferRowLength
					0,           // uint32_t                   bufferImageHeight
					destination_image_subresource, // VkImageSubresourceLayers   imageSubresource
					destination_image_offset, // VkOffset3D                 imageOffset
					destination_image_size, // VkExtent3D                 imageExtent
					} });

	setImageMemoryBarrier(command_buffer, VK_PIPELINE_STAGE_TRANSFER_BIT,
			destination_image_consuming_stages, { { destination_image, // VkImage            Image
					VK_ACCESS_TRANSFER_WRITE_BIT, // VkAccessFlags      CurrentAccess
					destination_image_new_access, // VkAccessFlags      NewAccess
					VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, // VkImageLayout      CurrentLayout
					destination_image_new_layout, // VkImageLayout      NewLayout
					VK_QUEUE_FAMILY_IGNORED, // uint32_t           CurrentQueueFamily
					VK_QUEUE_FAMILY_IGNORED, // uint32_t           NewQueueFamily
					destination_image_aspect       // VkImageAspectFlags Aspect
					} });

	if (!endCommandBufferRecordingOperation(command_buffer)) {
		return false;
	}

	VkDestroyer(VkFence) fence;
	InitVkDestroyer(logical_device, fence);
	if (!createFence(logical_device, false, *fence)) {
		return false;
	}

	if (!submitCommandBuffersToQueue(queue, { }, { command_buffer },
			signal_semaphores, *fence)) {
		return false;
	}

	if (!waitForFences(logical_device, { *fence }, VK_FALSE, 500000000)) {
		return false;
	}

	return true;
}

void destroyImageView(VkDevice logical_device, VkImageView &image_view) {
	if ( VK_NULL_HANDLE != image_view) {
		vkDestroyImageView(logical_device, image_view, nullptr);
		image_view = VK_NULL_HANDLE;
	}
}

void destroyImage(VkDevice logical_device, VkImage &image) {
	if ( VK_NULL_HANDLE != image) {
		vkDestroyImage(logical_device, image, nullptr);
		image = VK_NULL_HANDLE;
	}
}

void destroyBufferView(VkDevice logical_device, VkBufferView &buffer_view) {
	if ( VK_NULL_HANDLE != buffer_view) {
		vkDestroyBufferView(logical_device, buffer_view, nullptr);
		buffer_view = VK_NULL_HANDLE;
	}
}

void freeMemoryObject(VkDevice logical_device, VkDeviceMemory &memory_object) {
	if ( VK_NULL_HANDLE != memory_object) {
		vkFreeMemory(logical_device, memory_object, nullptr);
		memory_object = VK_NULL_HANDLE;
	}
}

void destroyBuffer(VkDevice logical_device, VkBuffer &buffer) {
	if ( VK_NULL_HANDLE != buffer) {
		vkDestroyBuffer(logical_device, buffer, nullptr);
		buffer = VK_NULL_HANDLE;
	}
}

} // namespace oge

