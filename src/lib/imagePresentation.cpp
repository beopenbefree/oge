/**
 * imagePresentation.cpp
 *
 *  Created on: Mar 5, 2021
 *      Author: beopenbefree
 */

#include <iostream>
#include <algorithm>
#include <cstring>
#include "lib/imagePresentation.h"
#include "lib/instanceAndDevices.h"

namespace oge {

bool createVulkanInstanceWithWsiExtensionsEnabled(
		std::vector<char const*> &desired_extensions,
		char const *const application_name, VkInstance &instance) {
	std::vector<const char*> required_instance_extensions;
	if (!getRequiredInstanceExtensions(required_instance_extensions)) {
		return false;
	}
	for (const auto &ext : required_instance_extensions) {
		if (std::find_if(desired_extensions.begin(), desired_extensions.end(),
				[&ext](const char *dExt) {
					return std::strcmp(ext, dExt) == 0;
				}) == desired_extensions.end()) {
			desired_extensions.emplace_back(ext);
		}
	}
	return createVulkanInstance(desired_extensions, application_name, instance);
}

bool createPresentationSurface(VkInstance instance, GLFWwindow *window,
		const VkAllocationCallbacks *allocator,
		VkSurfaceKHR *presentation_surface) {
	VkResult result = glfwCreateWindowSurface(instance, window, allocator,
			presentation_surface);
	if (result != VK_SUCCESS) {
		std::cout << "Could not create presentation surface." << std::endl;
	}
	return true;
}

bool selectQueueFamilyThatSupportsPresentationToGivenSurface(
		VkPhysicalDevice physical_device, VkSurfaceKHR presentation_surface,
		uint32_t &queue_family_index) {
	std::vector<VkQueueFamilyProperties> queue_families;
	if (!checkAvailableQueueFamiliesAndTheirProperties(physical_device,
			queue_families)) {
		return false;
	}

	for (uint32_t index = 0;
			index < static_cast<uint32_t>(queue_families.size()); ++index) {
		VkBool32 presentation_supported = VK_FALSE;
		VkResult result = vkGetPhysicalDeviceSurfaceSupportKHR(physical_device,
				index, presentation_surface, &presentation_supported);
		if ((VK_SUCCESS == result) && (VK_TRUE == presentation_supported)) {
			queue_family_index = index;
			return true;
		}
	}
	return false;
}

bool createLogicalDeviceWithWsiExtensionsEnabled(
		VkPhysicalDevice physical_device, std::vector<QueueInfo> queue_infos,
		std::vector<char const*> &desired_extensions,
		VkPhysicalDeviceFeatures *desired_features, VkDevice &logical_device) {
	if (std::find_if(desired_extensions.begin(), desired_extensions.end(),
			[](const char *ext) {
				return std::strcmp(VK_KHR_SWAPCHAIN_EXTENSION_NAME, ext) == 0;
			}) == desired_extensions.end()) {
		desired_extensions.emplace_back( VK_KHR_SWAPCHAIN_EXTENSION_NAME);
	}

	return createLogicalDevice(physical_device, queue_infos, desired_extensions,
			desired_features, logical_device);
}

bool selectDesiredPresentationMode(VkPhysicalDevice physical_device,
		VkSurfaceKHR presentation_surface,
		VkPresentModeKHR desired_present_mode, VkPresentModeKHR &present_mode) {
	// Enumerate supported present modes
	uint32_t present_modes_count = 0;
	VkResult result = VK_SUCCESS;

	result = vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device,
			presentation_surface, &present_modes_count, nullptr);
	if ((VK_SUCCESS != result) || (0 == present_modes_count)) {
		std::cout << "Could not get the number of supported present modes."
				<< std::endl;
		return false;
	}

	std::vector<VkPresentModeKHR> present_modes(present_modes_count);
	result = vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device,
			presentation_surface, &present_modes_count, present_modes.data());
	if ((VK_SUCCESS != result) || (0 == present_modes_count)) {
		std::cout << "Could not enumerate present modes." << std::endl;
		return false;
	}

	// Select present mode
	for (auto &current_present_mode : present_modes) {
		if (current_present_mode == desired_present_mode) {
			present_mode = desired_present_mode;
			return true;
		}
	}

	std::cout
			<< "Desired present mode is not supported. Selecting default FIFO mode."
			<< std::endl;
	for (auto &current_present_mode : present_modes) {
		if (current_present_mode == VK_PRESENT_MODE_FIFO_KHR) {
			present_mode = VK_PRESENT_MODE_FIFO_KHR;
			return true;
		}
	}

	std::cout
			<< "VK_PRESENT_MODE_FIFO_KHR is not supported though it's mandatory for all drivers!"
			<< std::endl;
	return false;
}

bool getCapabilitiesOfPresentationSurface(VkPhysicalDevice physical_device,
		VkSurfaceKHR presentation_surface,
		VkSurfaceCapabilitiesKHR &surface_capabilities) {
	VkResult result = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physical_device,
			presentation_surface, &surface_capabilities);

	if (VK_SUCCESS != result) {
		std::cout << "Could not get the capabilities of a presentation surface."
				<< std::endl;
		return false;
	}
	return true;
}

bool selectNumberOfSwapchainImages(
		VkSurfaceCapabilitiesKHR const &surface_capabilities,
		uint32_t &number_of_images) {
	number_of_images = surface_capabilities.minImageCount + 1;
	if ((surface_capabilities.maxImageCount > 0)
			&& (number_of_images > surface_capabilities.maxImageCount)) {
		number_of_images = surface_capabilities.maxImageCount;
	}
	return true;
}

bool chooseSizeOfSwapchainImages(
		VkSurfaceCapabilitiesKHR const &surface_capabilities,
		VkExtent2D &size_of_images, const VkExtent2D &default_size_of_images) {
	size_of_images = default_size_of_images;
	if (0xFFFFFFFF == surface_capabilities.currentExtent.width) {
		if (size_of_images.width < surface_capabilities.minImageExtent.width) {
			size_of_images.width = surface_capabilities.minImageExtent.width;
		} else if (size_of_images.width
				> surface_capabilities.maxImageExtent.width) {
			size_of_images.width = surface_capabilities.maxImageExtent.width;
		}

		if (size_of_images.height
				< surface_capabilities.minImageExtent.height) {
			size_of_images.height = surface_capabilities.minImageExtent.height;
		} else if (size_of_images.height
				> surface_capabilities.maxImageExtent.height) {
			size_of_images.height = surface_capabilities.maxImageExtent.height;
		}
	} else {
		size_of_images = surface_capabilities.currentExtent;
	}
	return true;
}

bool selectDesiredUsageScenariosOfSwapchainImages(
		VkSurfaceCapabilitiesKHR const &surface_capabilities,
		VkImageUsageFlags desired_usages, VkImageUsageFlags &image_usage) {
	image_usage = desired_usages & surface_capabilities.supportedUsageFlags;

	return desired_usages == image_usage;
}

bool selectTransformationOfSwapchainImages(
		VkSurfaceCapabilitiesKHR const &surface_capabilities,
		VkSurfaceTransformFlagBitsKHR desired_transform,
		VkSurfaceTransformFlagBitsKHR &surface_transform) {
	if (surface_capabilities.supportedTransforms & desired_transform) {
		surface_transform = desired_transform;
	} else {
		surface_transform = surface_capabilities.currentTransform;
	}
	return true;
}

bool selectFormatOfSwapchainImages(VkPhysicalDevice physical_device,
		VkSurfaceKHR presentation_surface,
		VkSurfaceFormatKHR desired_surface_format, VkFormat &image_format,
		VkColorSpaceKHR &image_color_space) {
	// Enumerate supported formats
	uint32_t formats_count = 0;
	VkResult result = VK_SUCCESS;

	result = vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device,
			presentation_surface, &formats_count, nullptr);
	if ((VK_SUCCESS != result) || (0 == formats_count)) {
		std::cout << "Could not get the number of supported surface formats."
				<< std::endl;
		return false;
	}

	std::vector<VkSurfaceFormatKHR> surface_formats(formats_count);
	result = vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device,
			presentation_surface, &formats_count, surface_formats.data());
	if ((VK_SUCCESS != result) || (0 == formats_count)) {
		std::cout << "Could not enumerate supported surface formats."
				<< std::endl;
		return false;
	}

	// Select surface format
	// fixme (The number of format pairs supported must be greater than or equal to 1.
	//       pSurfaceFormats must not contain an entry whose value for format is VK_FORMAT_UNDEFINED.)
	if ((1 == surface_formats.size())
			&& (VK_FORMAT_UNDEFINED == surface_formats[0].format)) {
		image_format = desired_surface_format.format;
		image_color_space = desired_surface_format.colorSpace;
		return true;
	}

	for (auto &surface_format : surface_formats) {
		if ((desired_surface_format.format == surface_format.format)
				&& (desired_surface_format.colorSpace
						== surface_format.colorSpace)) {
			image_format = desired_surface_format.format;
			image_color_space = desired_surface_format.colorSpace;
			return true;
		}
	}

	for (auto &surface_format : surface_formats) {
		if ((desired_surface_format.format == surface_format.format)) {
			image_format = desired_surface_format.format;
			image_color_space = surface_format.colorSpace;
			std::cout
					<< "Desired combination of format and colorspace is not supported. Selecting other colorspace."
					<< std::endl;
			return true;
		}
	}

	image_format = surface_formats[0].format;
	image_color_space = surface_formats[0].colorSpace;
	std::cout
			<< "Desired format is not supported. Selecting available format - colorspace combination."
			<< std::endl;
	return true;
}

bool createSwapchain(VkDevice logical_device, VkSurfaceKHR presentation_surface,
		uint32_t image_count, VkSurfaceFormatKHR surface_format,
		VkExtent2D image_size, VkImageUsageFlags image_usage,
		VkSurfaceTransformFlagBitsKHR surface_transform,
		VkPresentModeKHR present_mode, VkSwapchainKHR &old_swapchain,
		VkSwapchainKHR &swapchain, VkSharingMode imageSharingMode,
		std::vector<uint32_t> queueFamilyIndices,
		VkCompositeAlphaFlagBitsKHR compositeAlpha, VkBool32 clipped) {
	VkSwapchainCreateInfoKHR swapchain_create_info = {
			VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR, // VkStructureType                  sType
			nullptr,                   // const void                     * pNext
			0,                         // VkSwapchainCreateFlagsKHR        flags
			presentation_surface,    // VkSurfaceKHR                     surface
			image_count,       // uint32_t                         minImageCount
			surface_format.format, // VkFormat                         imageFormat
			surface_format.colorSpace, // VkColorSpaceKHR                  imageColorSpace
			image_size,          // VkExtent2D                       imageExtent
			1,              // uint32_t                         imageArrayLayers
			image_usage,          // VkImageUsageFlags                imageUsage
			imageSharingMode, // VkSharingMode                    imageSharingMode
			static_cast<uint32_t>(queueFamilyIndices.size()), // uint32_t                         queueFamilyIndexCount
			queueFamilyIndices.data(), // const uint32_t                 * pQueueFamilyIndices
			surface_transform,  // VkSurfaceTransformFlagBitsKHR    preTransform
			compositeAlpha, // VkCompositeAlphaFlagBitsKHR      compositeAlpha
			present_mode,        // VkPresentModeKHR                 presentMode
			clipped,                 // VkBool32                         clipped
			old_swapchain      // VkSwapchainKHR                   oldSwapchain
			};

	VkResult result = vkCreateSwapchainKHR(logical_device,
			&swapchain_create_info, nullptr, &swapchain);
	if ((VK_SUCCESS != result) || (VK_NULL_HANDLE == swapchain)) {
		std::cout << "Could not create a swapchain." << std::endl;
		return false;
	}

	if ( VK_NULL_HANDLE != old_swapchain) {
		vkDestroySwapchainKHR(logical_device, old_swapchain, nullptr);
		old_swapchain = VK_NULL_HANDLE;
	}

	return true;
}

bool getHandlesOfSwapchainImages(VkDevice logical_device,
		VkSwapchainKHR swapchain, std::vector<VkImage> &swapchain_images) {
	uint32_t images_count = 0;
	VkResult result = VK_SUCCESS;

	result = vkGetSwapchainImagesKHR(logical_device, swapchain, &images_count,
			nullptr);
	if ((VK_SUCCESS != result) || (0 == images_count)) {
		std::cout << "Could not get the number of swapchain images."
				<< std::endl;
		return false;
	}

	swapchain_images.resize(images_count);
	result = vkGetSwapchainImagesKHR(logical_device, swapchain, &images_count,
			swapchain_images.data());
	if ((VK_SUCCESS != result) || (0 == images_count)) {
		std::cout << "Could not enumerate swapchain images." << std::endl;
		return false;
	}

	return true;
}

bool createSwapchainWithR8G8B8A8FormatAndMailboxPresentMode(
		VkPhysicalDevice physical_device, VkSurfaceKHR presentation_surface,
		VkDevice logical_device, VkImageUsageFlags swapchain_image_usage,
		VkExtent2D &image_size, VkFormat &image_format,
		VkSwapchainKHR &old_swapchain, VkSwapchainKHR &swapchain,
		std::vector<VkImage> &swapchain_images) {
	VkPresentModeKHR desired_present_mode;
	if (!selectDesiredPresentationMode(physical_device, presentation_surface,
			VK_PRESENT_MODE_MAILBOX_KHR, desired_present_mode)) {
		return false;
	}

	VkSurfaceCapabilitiesKHR surface_capabilities;
	if (!getCapabilitiesOfPresentationSurface(physical_device,
			presentation_surface, surface_capabilities)) {
		return false;
	}

	uint32_t number_of_images;
	if (!selectNumberOfSwapchainImages(surface_capabilities,
			number_of_images)) {
		return false;
	}

	if (!chooseSizeOfSwapchainImages(surface_capabilities, image_size)) {
		return false;
	}
	if ((0 == image_size.width) || (0 == image_size.height)) {
		return true;
	}

	VkImageUsageFlags image_usage;
	if (!selectDesiredUsageScenariosOfSwapchainImages(surface_capabilities,
			swapchain_image_usage, image_usage)) {
		return false;
	}

	VkSurfaceTransformFlagBitsKHR surface_transform;
	selectTransformationOfSwapchainImages(surface_capabilities,
			VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR, surface_transform);

	VkColorSpaceKHR image_color_space;
	if (!selectFormatOfSwapchainImages(physical_device, presentation_surface, {
			VK_FORMAT_R8G8B8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR },
			image_format, image_color_space)) {
		return false;
	}

	if (!createSwapchain(logical_device, presentation_surface, number_of_images,
			{ image_format, image_color_space }, image_size, image_usage,
			surface_transform, desired_present_mode, old_swapchain,
			swapchain)) {
		return false;
	}

	if (!getHandlesOfSwapchainImages(logical_device, swapchain,
			swapchain_images)) {
		return false;
	}
	return true;
}

bool acquireSwapchainImage(VkDevice logical_device, VkSwapchainKHR swapchain,
		VkSemaphore semaphore, VkFence fence, uint32_t &image_index) {
	VkResult result;

	result = vkAcquireNextImageKHR(logical_device, swapchain, UINT64_MAX,
			semaphore, fence, &image_index);
	switch (result) {
	case VK_SUCCESS:
	case VK_SUBOPTIMAL_KHR:
		return true;
	default:
		return false;
	}
}

bool presentImage(VkQueue queue, std::vector<VkSemaphore> rendering_semaphores,
		std::vector<PresentInfo> images_to_present) {
	VkResult result;
	std::vector<VkSwapchainKHR> swapchains;
	std::vector<uint32_t> image_indices;

	for (auto &image_to_present : images_to_present) {
		swapchains.emplace_back(image_to_present.Swapchain);
		image_indices.emplace_back(image_to_present.ImageIndex);
	}

	VkPresentInfoKHR present_info = { VK_STRUCTURE_TYPE_PRESENT_INFO_KHR, // VkStructureType          sType
			nullptr,                           // const void*              pNext
			static_cast<uint32_t>(rendering_semaphores.size()), // uint32_t                 waitSemaphoreCount
			rendering_semaphores.data(), // const VkSemaphore      * pWaitSemaphores
			static_cast<uint32_t>(swapchains.size()), // uint32_t                 swapchainCount
			swapchains.data(),           // const VkSwapchainKHR   * pSwapchains
			image_indices.data(),      // const uint32_t         * pImageIndices
			nullptr                        // VkResult*                pResults
			};

	result = vkQueuePresentKHR(queue, &present_info);
	switch (result) {
	case VK_SUCCESS:
		return true;
	default:
		return false;
	}
}

void destroySwapchain(VkDevice logical_device, VkSwapchainKHR &swapchain) {
	if (swapchain) {
		vkDestroySwapchainKHR(logical_device, swapchain, nullptr);
		swapchain = VK_NULL_HANDLE;
	}
}

void destroyPresentationSurface(VkInstance instance,
		VkSurfaceKHR &presentation_surface) {
	if (presentation_surface) {
		vkDestroySurfaceKHR(instance, presentation_surface, nullptr);
		presentation_surface = VK_NULL_HANDLE;
	}
}
} // namespace oge

