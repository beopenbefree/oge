/**
 * SceneCreator.cpp
 *
 *  Created on: Feb 22, 2021
 *      Author: beopenbefree
 */

#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/DefaultLogger.hpp>
#include <iostream>
#include "SceneCreator.h"

namespace oge
{

SceneCreator::SceneCreator(const std::string &pFile) :
		mFile
		{ pFile }
{
	doTheImportThing();
}

SceneCreator::~SceneCreator()
{
}

const std::string& SceneCreator::getFile() const
{
	return mFile;
}

SceneObject* SceneCreator::getObject() const
{
	return mObject;
}

bool SceneCreator::doTheImportThing()
{
	// Create an instance of the Importer class
	Assimp::Importer importer;
	Assimp::DefaultLogger::create("import.log", Assimp::Logger::VERBOSE,
			aiDefaultLogStream_STDOUT | aiDefaultLogStream_FILE);

	// And have it read the given file with some example postprocessing
	// Usually - if speed is not the most important aspect for you - you'll
	// probably to request more postprocessing than we do in this example.

//	const aiScene* scene = importer.ReadFile(mFile,
//			aiProcess_CalcTangentSpace | aiProcess_Triangulate
//					| aiProcess_JoinIdenticalVertices | aiProcess_SortByPType);
	const aiScene *scene = importer.ReadFile(mFile, 0);

	// If the import failed, report it
	if (!scene)
	{
		doTheErrorLogging(importer.GetErrorString());
		return false;
	}

	// query scene's hierarchy
	doWalkSceneInfos(scene->mRootNode, "", scene);

	// Now we can access the file's contents.
	doCreateSceneHierarchy(*scene->mRootNode, nullptr, aiMatrix4x4(), scene);

	Assimp::DefaultLogger::kill();

	// We're done. Everything will be cleaned up by the importer destructor
	return true;
}

void SceneCreator::doWalkSceneInfos(aiNode *node, const std::string &indentStr,
		const aiScene *&scene)
{
	Assimp::Logger *logger = Assimp::DefaultLogger::get();
	logger->info(indentStr + std::string(node->mName.C_Str()));
	const std::string &currIndent = indentStr + "\t";
	// transform
	const aiMatrix4x4 &transform = node->mTransformation;
	logger->info(currIndent + "transform:");
	for (uint row = 0; row < 4; row++)
	{
		std::string rowStr;
		for (uint col = 0; col < 4; col++)
		{
			col != 3 ?
					rowStr += std::to_string(transform[row][col]) + ", " :
					rowStr += std::to_string(transform[row][col]);
		}
		logger->info(currIndent + "\t" + rowStr);
	}
	// meshes
	logger->info(currIndent + "meshes: " + std::to_string(node->mNumMeshes));
	for (uint i = 0; i < node->mNumMeshes; i++)
	{
		const aiMesh *mesh = scene->mMeshes[node->mMeshes[i]];

		logger->info(
				currIndent + "\t" + std::string(mesh->mName.C_Str()) + ":");
		logger->info(
				currIndent + "\t\tvertices: "
						+ std::to_string(mesh->mNumVertices));
		logger->info(
				currIndent + "\t\tfaces: " + std::to_string(mesh->mNumFaces));
	}

	// depth first pre-order tree walk
	logger->info(
			currIndent + "children: " + std::to_string(node->mNumChildren));
	for (uint i = 0; i < node->mNumChildren; i++)
	{
		doWalkSceneInfos(node->mChildren[i], currIndent + "\t", scene);
	}
	logger->info(indentStr + "____");
}

void SceneCreator::doTheErrorLogging(const std::string &msg)
{
	std::cout << msg << std::endl;
}

void SceneCreator::doCreateSceneHierarchy(const aiNode &node,
		SceneObject *parent, aiMatrix4x4 accTransform, const aiScene *&scene)
{
	SceneObject *newParent = parent;
	aiMatrix4x4 newAccTransform = accTransform * node.mTransformation;

	// if node has meshes, create a new scene object for it
	if (node.mNumMeshes > 0)
	{
		SceneObject *newObject = new SceneObject();
		if (!parent)
		{
			// root SceneObject
			mObject = newObject;
		}
		else
		{
			parent->addChild(newObject);
		}
		// copy the meshes
		doCopyMeshes(node, newObject, scene);

		// copy transform
		newObject->setTransform(
				glm::mat4(
						newAccTransform.a1, newAccTransform.b1, newAccTransform.c1, newAccTransform.d1,
						newAccTransform.a2, newAccTransform.b2, newAccTransform.c2, newAccTransform.d2,
						newAccTransform.a3, newAccTransform.b3, newAccTransform.c3, newAccTransform.d3,
						newAccTransform.a4, newAccTransform.b4, newAccTransform.c4, newAccTransform.d4)
						);

		// the new object is the parent for all child nodes
		newParent = newObject;
		// reset the transform
		newAccTransform = aiMatrix4x4();
	}

	// continue for all child nodes
	for (uint n = 0; n < node.mNumChildren; n++)
	{
		doCreateSceneHierarchy(*node.mChildren[n], newParent, newAccTransform,
				scene);
	}
}

void SceneCreator::doCopyMeshes(const aiNode &node, SceneObject *object,
		const aiScene *&scene)
{
	// iterate over meshes
	for (uint m = 0; m < scene->mNumMeshes; m++)
	{
		// TODO
	}
}

} // namespace oge
