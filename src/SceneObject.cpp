/**
 * SceneObject.cpp
 *
 *  Created on: Feb 22, 2021
 *      Author: beopenbefree
 */

#include "SceneObject.h"

namespace oge
{

SceneObject::SceneObject()
{
	// TODO Auto-generated constructor stub

}

SceneObject::~SceneObject()
{
	{
		for (auto o : mChildren)
		{
			delete o;
		}
	}
}

void SceneObject::addChild(SceneObject *child)
{
	mChildren.push_back(child);
}

const glm::mat4& SceneObject::getTransform() const
{
	return mTransform;
}

void SceneObject::setTransform(const glm::mat4 &mTransform)
{
	this->mTransform = mTransform;
}

} // namespace oge
